<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Backend','prefix' => 'superadmin'], function () {
    require(__DIR__ . '/Routes/Web/web_backend_controls.php');
});

Route::domain('feedback.abharan.com')->group(function () {
    require(__DIR__ . '/Routes/Web/web_feedback_controls.php');
});

Route::group(['namespace' => 'Frontend'], function () {
    require(__DIR__ . '/Routes/Web/web_frontend_controls.php');
});
