<?php

Route::get('/signout', 'AuthController@signout');

Route::get('/', 'AuthController@index');
Route::get('/login', 'AuthController@login');
Route::get('/filters', 'AuthController@filters');

Route::get('/about-us', 'MetaController@about_us');
Route::get('/jewellery-care', 'MetaController@jewllery_care');
Route::get('/about-jewellry', 'MetaController@about_jewellry');
Route::get('/contact-us', 'MetaController@contact_us');
Route::post('/submit-feedback', 'MetaController@save_feedback');
Route::get('/faqs', 'MetaController@faq');
Route::get('/return-policy', 'MetaController@return_policy');

Route::get('/collections', 'MetaController@scheme_collections');
Route::get('/our-schemes', 'MetaController@our_schemes');
Route::get('/aasthana-collection', 'MetaController@aasthana_collection');
Route::get('/azva-collection', 'MetaController@azva_collection');
Route::get('/blue-pottery-collection', 'MetaController@blue_pottery_collection');
Route::get('/ganesh-chaturthi-collection', 'MetaController@ganesh_chaturthi_collection');
Route::get('/celesta-collection', 'MetaController@celesta_collection');
Route::get('/mantra-collection', 'MetaController@mantra_collection');
Route::get('/monsoon-mystica', 'MetaController@monsoon_mystica');
Route::get('/noorie-collection', 'MetaController@noorie_collection');
Route::get('/pritty-collection', 'MetaController@pritty_collection');
Route::get('/pritty-silver-collection', 'MetaController@pritty_silver_collection');
Route::get('/shezmin-collection', 'MetaController@shezmin_collection');
Route::get('/silver-collection', 'MetaController@silver_collection');
Route::get('/taarika-collection', 'MetaController@taarika_collection');
Route::get('/tanvi-collection', 'MetaController@tanvi_collection');
Route::get('/utsav-temple-collection', 'MetaController@utsav_temple_collection');

Route::get('/oops', 'ProductController@oops');
// Route::post('payment/callback', 'ProductController@responsePayu');
Route::get('/product/storage/{dimension}/{filename}', function ($dimension, $filename) {
    $remoteImage = storage_path('app/public/uploads/product-images/' .$dimension. '/'. $filename) ;
    return Image::make($remoteImage)->response();
});
Route::group(['prefix' => 'product'], function () {
    Route::get('/', 'ProductController@index');
    Route::get('/rates', 'ProductController@rates');
    Route::get('/collection', 'ProductController@view_collection');
    Route::get('/new-collection', 'ProductController@new_collection');
    Route::get('/get-collection', 'ProductController@get_collections');
    Route::get('/get-main-cat', 'ProductController@get_main_cat');
    Route::get('/get-sub-cat', 'ProductController@get_sub_cat');

    Route::group(['prefix' => 'cart'], function () {
        Route::get('/', 'ProductController@view_cart');
        Route::get('/{get}', 'ProductController@view_cart')->where(['get' => '\bview\b|\bdata\b']);
        Route::post('/add', 'ProductController@add_cart');
        Route::get('/delete', 'ProductController@delete_cart');
    });
});

Route::get('/product-details', 'ProductController@details');

Route::group(['middleware' => ['guest']], function () {
    Route::get('/register', 'AuthController@register');
    Route::post('/create-account', 'AuthController@create_account');
    Route::get('/resend-otp', 'AuthController@resend_otp');
    Route::post('/verify-otp', 'AuthController@verify_otp');

    Route::post('/auth-login', 'AuthController@auth_login');
    
    Route::get('/forgot-password', 'AuthController@forgot_pass');
    Route::post('/recovery-mobile', 'AuthController@recovery_mobile');
    Route::get('/reset-password', 'AuthController@reset_form');
    Route::post('/reset-request', 'AuthController@otp_ver_reset_pass');
});


Route::group(['middleware' => ['auth', 'revalidate']], function () {
    Route::get('/settings', 'AuthController@settings');
    Route::post('/change-pass', 'AuthController@change_pass');

    Route::get('/product/checkout', 'ProductController@checkout');
    Route::get('/product/order-history', 'ProductController@order_history');
    Route::get('/product/order-history/{data}', 'ProductController@order_history');
    Route::post('/product/checkout-order', 'ProductController@checkout_order');
    Route::get('/product/checkout/{response}', 'ProductController@checkout_resp');

    Route::get('payment/callback', 'ProductController@responseMastercard');
});
