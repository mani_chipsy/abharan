<?php

use Image as Image;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/signout', 'AuthController@signout');

Route::group(['middleware' => ['guest']], function () {
    Route::get('/', ['as'=> 'login', 'uses' => 'AuthController@index']);
    Route::get('/login', function () {
        return redirect('superadmin/');
    });

    Route::post('/auth-login', 'AuthController@auth_login');
});


Route::group(['middleware' => ['auth.admin']], function () {
    Route::get('/feedbacks', 'MetaController@index');
    Route::get('/feedbacks/get/{data}', 'MetaController@index');
    Route::group(['prefix' => 'app'], function () {
        Route::get('/feedbacks', 'MetaController@app_feedbacks'); 
        Route::get('/feedbacks/get/{data}', 'MetaController@app_feedbacks');
        Route::get('feedbacks/export', 'MetaController@app_feedback_exp');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('/', 'ProductController@index');
        Route::get('/download/{filename}', 'ProductController@getDownload');
        Route::get('/get/{data}', 'ProductController@index');
        Route::post('/up-products', 'ProductController@up_products');

        Route::get('/images', 'ProductController@images');
        Route::get('/images/get/{data}', 'ProductController@images');
        Route::post('/up-image', 'ProductController@up_image');
        
        Route::get('/rates', 'ProductController@rates');
        Route::post('/up-rates', 'ProductController@up_rates');

        Route::get('/offers', 'ProductController@offers');
        Route::get('/offers/get/{data}', 'ProductController@offers');
        Route::post('/up-offers', 'ProductController@up_offers');

        Route::get('/storage/{dimension}/{filename}', function ($dimension, $filename) {
            return Image::make(storage_path('app/public/uploads/product-images/' .$dimension. '/'. $filename))->response();
        });
    });
});
