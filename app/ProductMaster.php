<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMaster extends Model
{
    protected $table = 'product_master';
    protected $primaryKey = 'product_id';
    protected $fillable = ['SKU', 'name', 'm_category_id', 's_category_id'];

    public function main_category()
    {
        return $this->belongsTo('App\ProductCategory', 'm_category_id', 'm_category_id');
    }

    public function sub_category()
    {
        return $this->belongsTo('App\ProductSubCategory', 's_category_id', 's_category_id');
    }

    public function details()
    {
        return $this->hasOne('App\ProductDetails', 'product_id', 'product_id');
    }

    public function image()
    {
        return $this->hasOne('App\ImageMaster', 'global_name', 'SKU');
    }
    
    public function price()
    {
        return $this->hasOne('App\CollectionPrice', 'product_id');
    }
}
