<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cust_Feedback extends Model
{
    protected $table = 'app_feedbacks';
    
    public function suggestion()
    {
        return $this->hasOne('App\FeedbackSuggestion', 'app_feedback_id');
    }
    
    public function place()
    {
        return $this->hasOne('App\Place', 'place_id', 'place_id');
    }
}
