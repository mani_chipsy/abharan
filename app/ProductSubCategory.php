<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSubCategory extends Model
{
    protected $table = '_sub_category';
    public $timestamps = false;
    public $primarykey = 's_category_id';
    

    public function product()
    {
        return $this->hasMany('App\ProductMaster');
    }
}
