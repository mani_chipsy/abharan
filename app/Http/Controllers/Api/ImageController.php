<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Image;

class ImageController extends Controller
{
    public function index(Request $request, $dimension, $filename)
    {
        $resize = $request->query();
        
        $remoteImage = storage_path('app/public/uploads/product-images/' .$dimension. '/'. $filename) ;

        list($width, $height) = getimagesize($remoteImage);

        if (!isset($resize['width']) || !isset($resize['height'])) {
            $resize['width'] = $width;
            $resize['height'] = $height;
        }

        $img = Image::make($remoteImage, true)->resize($resize['width'], $resize['height'], function ($c) {
            $c->aspectRatio();
            $c->upsize();
        });

        // $img->encode('jpg');

        return $img->response();
    }

    public function blog($dirYear, $dirMonth, $image_size, $image_name, $image_ext)
    {
        $remoteImage = storage_path() . "/app/public/uploads/blog-images/" . $dirYear . "/" . $dirMonth . "/" .$image_size . "/". $image_name ."." . $image_ext;
        
        if (file_exists($remoteImage)) {
            $imginfo = getimagesize($remoteImage);
            header("Content-type: {$imginfo['mime']}");
            readfile($remoteImage);
        } else {
            die(json_encode("Image Not found"));
        }
    }
}
