<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\User;
use App\ImageMaster;
use App\ProductMaster;
use App\ProductDetails;
use App\ProductOrders;
use App\ProductOffer;
use App\ProductImage;
use App\ProductCategory;
use App\ProductSubCategory;
use App\ProductRates;
use App\VpcError;
use Storage;
use Image;
use Excel;
use Redirect;
use PDF;
use DB;

class ProductController extends Controller
{
    public function index(Request $request, $render='view')
    {
        if ($render == 'view') {
            return view('Backend.pages.product');
        } elseif ($render == 'get_data') {
            $builder = ProductMaster::query()->with(['main_category', 'sub_category', 'details']);

            $query = $request->query();

            if ($cond = $query['sku']) {
                $builder->where('SKU', 'like', '%'.$cond.'%');
            }

            return $builder->orderBy('created_at', 'DESC')->paginate(10);
        } else {
            return view('Backend.pages.view_product', ['product' => ProductMaster::find($render)]);
        }
    }

    public function offers()
    {
        $offer = ProductOffer::first();
        
        if (!empty($offer)) {
            $offer->status_str = ($offer->status) ? 'Active' : 'Inactive';
        }

        return view('Backend.pages.offers', [
                'offer' => $offer,
            ]
        );
    }

    public function up_offers(Request $request)
    {
        $rules = array(
            'offer_price' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        // process the form
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        } else {
            $data = $request->all();

            $prod_offer = ProductOffer::firstOrNew(['p_offer_id' => $data['offer_id']]);
            $prod_offer->discount = $data['offer_price'];
            $prod_offer->status = (int)$data['status'];
            $prod_offer->save();

            return response()->json(array(
                'success' => true,
                'message' => 'Saved Successfully',
                'offer_id' => $prod_offer->p_offer_id
            ));
        }
    }


    public function up_order_print($order_id)
    {
        $modes = ['1' => 'COD', '2' => 'Online Payment'];
        $order = ProductOrders::with('details')->find($order_id);

        $user = User::with('details')->find($order->user_id);

        $order_no = $order->order_no;
        $order_mode = $modes[$order->mode];
        $order_date = date('F d, Y', strtotime($order->created_at));

        $table_items = '';
        $i = 1;
        $cart_gst = 0;
        $sub_total = 0;
        $calc_tax = 0;

        foreach ($order->details as $item) {
            $itm_tot = round(($item->price * $item->qty), 2);

            $product = ProductMaster::find($item->product_id);
            $table_items .= '<tr>
                    <td>'.$i.'</td>
                    <td>'.$product->SKU.'</td>
                    <td>'.$product->name.'</td>
                    <td align="right">'.$this->inrCurrency($item->price).'</td>
                    <td align="right">'.$item->qty.'</td>
                    <td align="right">'.$this->inrCurrency($itm_tot).'</td>
                </tr>';
            $i++;

            if ($item->gst_rate) {
                $cart_gst = $item->gst_rate * 100;
            }
            $sub_total += $itm_tot;
            $calc_tax += $item->gst_rate ? ($itm_tot * $item->gst_rate) : 0;
        }

        $sub_total = round($sub_total, 2);
        $calc_tax = round($calc_tax, 2);

        $cart_subtotal = $this->inrCurrency($sub_total);
        $cart_tax = $this->inrCurrency($calc_tax);
        
        $cart_total = $this->inrCurrency(($sub_total + $calc_tax));

        $ship_address = $order->recpnt_name.'<br>';
        $ship_address .= $order->address.'<br>';
        $ship_address .= $order->city.' - '.$order->pincode.'<br>';
        $ship_address .= $order->state.'<br>';
        $ship_address .= 'Phone: '.$user->mobile.'<br>';
        $ship_address .= 'Email: '.$user->email.'<br>';
        
        if ($order->landmark) {
            $ship_address .= 'Landmark: '.$order->landmark.'<br>';
        }

        if ($order->alternate_mobile) {
            $ship_address .= 'Alternate Phone: '.$order->alternate_mobile.'<br>';
        }

        $message_body = '


        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                    <td valign="top" align="left">
             <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">

  <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td width="100%" align="center" valign="middle" bgcolor="">
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tbody>
                                                                <tr>
     <td width="100%" align="center" valign="top" style=""><img src="https://www.abharan.com/logo1.png" width="150px"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>




                <tr>
                    <td valign="top" align="left">
             <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tbody>
                               
                               
                                <tr>
                                    <td colspan="2" height="25"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#fff">
                                            <tbody>
                                                <tr>
                                                    <td height="2" colspan="5" bgcolor="#f2e4cc"></td>
                                                </tr>
                                                <tr>
                                                    <td height="12" width="24"></td>
                                                    <td width="145"></td>
                                                    <td width="26"></td>
                                                    <td>&nbsp;</td>
                                                    <td width="24"></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="3"><font face="Verdana, Arial" color="#1a1a1a" style="font-size:12px"><b>Order# '.$order_no.'</b></font></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                      <td style="background:none;border: solid 1px #f2e4cc;border-width:1px 0 0 0;height:1px;width: 100%;margin:0px 0px 0px 0px;padding-top:10px;padding-bottom:10px;" colspan="5">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="3">
                                                        <address>
                                                            <b>Shipped To:</b><br>'.$ship_address.'
                                                        </address>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="12"></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="3">
                                                        <address>
                                                            <b>Payment Status:</b><br>'.$order_mode.'
                                                        </address>
                                                    </td>
                                                    <td align="left">
                                                        <address>
                                                            <b>Order Date:</b><br>
                                                            '.$order_date.'<br><br>
                                                        </address>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="12"></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="12"></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="12"></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="3"><b>Order Summary</b></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="5">
            <table width="68%" border="1" align="left" cellpadding="3" cellspacing="0" bgcolor="#fff" style="
                                                            border-collapse: collapse;
                                                            ">
                                                            <thead>
                                                                <tr>
                                                                    <td><b>Sl no.</b></td>
                                                                    <td><b>SKU</b></td>
                                                                    <td><b>Name</b></td>
                                                                    <td align="right"><b>Price</b></td>
                                                                    <td align="right"><b>Quantity</b></td>
                                                                    <td align="right"><b>Total</b></td>
                                                                </tr>
                                                            </thead>
                            <tbody>'.$table_items ;
        if ($cart_gst) {
            $message_body .= '<tr>
                                                                    <td></td>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td align="right"><b>Subtotal</b></td>
                                                                    <td align="right"><b>'.$cart_subtotal.'</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td align="right"><b>GST('.$cart_gst.'%)</b></td>
                                                                    <td align="right"><b>'.$cart_tax.'</b></td>
                                                                </tr>';
        }

        $message_body .='                   <tr>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td align="right"><b>Total</b></td>
                         <td align="right"><b>Rs. '.$cart_total.'/-</b></td>
                                                                </tr>
                                                          
                                               
                                                
                                               
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                               
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                               


                            </tbody>
                        </table>
</td></tr>

                           </tbody>
                        </table>


                    </td>
                </tr>

            </tbody>
        </table>
        </tbody>
        </table>


        ';
                                          
        $pdf = PDF::loadHtml($message_body);
       // $pdf->setPaper('A4', 'landscape');

          return $pdf->stream();
    }
    public function inrCurrency($num)
    {
        $num = preg_replace('/[^\d\.]+/', '', $num);
        $exp = '';
        if (strpos($num, '.') !== false) {
            $val_array = explode('.', $num);
            $num = $val_array[0];
            $exp = '.'.$val_array[1];
        }

        $explrestunits = "" ;

        if (strlen($num)>3) {
            $lastthree = substr($num, strlen($num)-3, strlen($num));
            $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
            $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
            $expunit = str_split($restunits, 2);
            for ($i=0; $i<sizeof($expunit); $i++) {
                // creates each of the 2's group and adds a comma to the end
                if ($i==0) {
                    $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
                } else {
                    $explrestunits .= $expunit[$i].",";
                }
            }
            return $explrestunits.$lastthree.$exp;
        } else {
            return $num.$exp;
        }
    }

    public function up_order_list(Request $request, $render='view')
    {
        if ($render == 'view') {
            return view('Backend.pages.order_list');
        } elseif ($render == 'get_data') {
            $data = $request->query();

            if ($data['page_tab'] == 'online') {
                $online = ProductOrders::query()->whereHas('transaction', function ($q) {
                    $q->where('status', 1);
                })->with('details');

                $online = $online->orderBy('created_at', 'DESC')->paginate(20);
                
                foreach ($online as $ro1) {
                    $ro1->date = ($time = strtotime($ro1->created_at)) ? date('d-m-Y', $time) : '-';
                }

                return $online;
            }

            if ($data['page_tab'] == 'cod') {
                $cod = ProductOrders::query()->with('details')->where('mode', 1);

                $cod = $cod->orderBy('created_at', 'DESC')->paginate(20);
                
                foreach ($cod as $ro2) {
                    $ro2->date = ($time = strtotime($ro2->created_at)) ? date('d-m-Y', $time) : '-';
                }

                return $cod;
            }


            if ($data['page_tab'] == 'missed') {
                $missed = ProductOrders::query()->whereHas('transaction', function ($q) {
                    $q->where('status', 2);
                })->with('details', 'transaction.gateway_resp');

                $missed = $missed->orderBy('created_at', 'DESC')->paginate(20);
                // print_r($missed->toArray());
                foreach ($missed as $ro3) {
                    $ro3->date = ($time = strtotime($ro3->created_at)) ? date('d-m-Y', $time) : '-';
                    if ($ro3->transaction->gateway_resp->count()) {
                        $response = json_decode($ro3->transaction->gateway_resp->response, true);
                        if (isset($response['vpc_TxnResponseCode'])) {
                            $ro3->reason = VpcError::where('vpc_TxnResponseCode', $response['vpc_TxnResponseCode'])->first()['description'];
                        } else {
                            $ro3->reason = '';
                        }
                    } else {
                        $ro3->reason = '';
                    }
                }

                return $missed;
            }
        }
    }
    
    public function getSubCat(Request $request)
    {
        $params = $request->query();

        $not_siver = [
            config('abharan.sub_category.waist_chain'),
            config('abharan.sub_category.haram'),
            config('abharan.sub_category.kanti')
        ];

        $not_diamond = [
            config('abharan.sub_category.waist_chain'),
            config('abharan.sub_category.haram'),
            config('abharan.sub_category.kanti'),
            config('abharan.sub_category.chain'),
            config('abharan.sub_category.bracelet'),
            config('abharan.sub_category.article')
        ];

        $sub_cat = ProductSubCategory::query();

        switch ($params['main_category']) {
            case config('abharan.main_category.diamond'):
                $sub_cat->whereNotIn('s_category_id', $not_diamond);
                break;
            case config('abharan.main_category.silver'):
                $sub_cat->whereNotIn('s_category_id', $not_siver);
                break;
            case config('abharan.main_category.gold'):
                $sub_cat->where('s_category_id', '!=', config('abharan.sub_category.article'));
                break;

        }


        return $sub_cat->orderBy('s_category_name', 'ASC')->get();
    }

    public function update(Request $request, $productId)
    {
        $rules = array(
            'sku' => 'required',
            'main_category' => 'required',
            'sub_category' => 'required',
            'prod_name' => 'required',
            'cat_grade' => 'required',
            'weight' => 'required',
            'quote' => 'required',
            'purity' => 'required',
            'metal' => 'required',
            'beeds' => 'required',
            'beeds_value' => 'required',
            'wastage' => 'required',
            'mak_charges' => 'required',
            'cents' => 'required',
            'diamond_value' => 'required',
            'stones' => 'required',
            'cost' => 'required',
            'production' => 'required',
            'max' => 'required',
            'certification' => 'required',
            'cut' => 'required',
            'color' => 'required',
            'clarity' => 'required',
            'women' => 'required',
            'men' => 'required',
            'kids' => 'required',
            'collection' => 'required',
            'occasion' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        // process the form
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        } else {
            $previous_precision = ini_get('precision');
            ini_set('precision', 5);

            $data = $request->all();
            
            $prod_master = ProductMaster::firstOrNew(['SKU' => trim($data['sku'])]);
            $prod_master->m_category_id = $data['main_category'];
            $prod_master->s_category_id = $data['sub_category'];
            $prod_master->name = $data['prod_name'];

            $res = $prod_master->save();
            $last_id = $prod_master->product_id;


            $prod_det = ProductDetails::firstOrNew(['product_id' => $last_id]);
            $prod_det->category = $data['cat_grade'];
            $prod_det->weight = round($data['weight'], 3);
            $prod_det->quote = $data['quote'];
            $prod_det->purity = $data['purity'];
            $prod_det->metal = $data['metal'];
            $prod_det->beeds = round($data['beeds'], 3);
            $prod_det->beeds_value = $data['beeds_value'];
            $prod_det->beeds_description=$data['beeds_description'];
            $prod_det->wastage = $data['wastage'];

            $prod_det->making_amount = $data['mak_charges'];
            $prod_det->mak_amt_percentage = $data['mc_percentage'];
            $prod_det->mak_amt_per_gram = $data['mc_per_gram'];

            $prod_det->cents = $data['cents'];
            $prod_det->daimond = $data['diamond_value'];
            $prod_det->stone = $data['stones'];
            $prod_det->cost = $data['cost'];
            $prod_det->description = $data['description'];
            $prod_det->production = $data['production'];
            $prod_det->max = $data['max'];
            $prod_det->certification = $data['certification'];
            $prod_det->cut = $data['cut'];
            $prod_det->colour = $data['color'];
            $prod_det->clarity = $data['clarity'];
            
            $prod_det->women = $data['women'];
            $prod_det->men = $data['men'];
            $prod_det->kids = $data['kids'];

            $prod_det->collection = $data['collection'];
            $prod_det->occasion = $data['occasion'];

            $res = $prod_det->save();
            ini_set('precision', $previous_precision);

            if ($res) {
                return response()->json(array(
                    'success' => true,
                    'message' => 'Updated Successfully'
                ));
            } else {
                return response()->json(array(
                    'success' => false,
                    'message' => 'Some error occured'
                ));
            }
        }
    }

    public function up_delete_products()
    {
        return view('Backend.pages.delete_product');
    }

    public function delete_products(Request $request)
    {
        $rules = array(
            'prod_file' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        // process the form
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        } else {
            $data = $request->all();
            $file_path = $data['prod_file']->getRealPath();

            $file_contents = Excel::load($file_path, function ($reader) {
            })->get();

            $i="";
            
            if (!empty($file_contents) && $file_contents->count()) {
                $file_contents = array_filter($file_contents->toArray());
                $empty = true;
                  
                foreach ($file_contents as $value) {
                    if (!empty($value)) {
                        foreach ($value as $v) {
                            if (array_filter($v)) {
                                $i=$v['sku'];
                                $empty = false;
				
				$prod_master = ProductMaster::firstOrNew(['SKU' => trim($v['sku'])]);
                           	$id= $prod_master->product_id;

                                $prod_det = ProductDetails::firstOrNew(['product_id' => $id]);
                                $prod_master->delete();
                                $prod_det->delete();

                                $prod_img = ProductImage::where(['global_name' => trim($v['sku'])])->delete();
                            
                                $image_master = ImageMaster::firstOrNew(['global_name' => trim($v['sku'])]);
                         
                          
                                if ($image_master) {
                                    $img_path_PH = 'uploads/product-images/PH/';
                                    $img_path_IN = 'uploads/product-images/IN/';
                                    $img_path_TH = 'uploads/product-images/TH/';
               

                                    $image_path1 = Storage::disk('public')->delete($img_path_PH.'/'.$image_master->global_name.'_PH.'.$image_master->image_ext);
                                    $image_path2 = Storage::disk('public')->delete($img_path_IN.'/'.$image_master->global_name.'_IN.'.$image_master->image_ext);
                                    $image_path3 = Storage::disk('public')->delete($img_path_TH.'/'.$image_master->global_name.'_TH.'.$image_master->image_ext);
                                }
                            
                                $image_master->delete();
                            }
                        }
                    }
                }
            }
   
            if (!$empty) {
                return response()->json(array(
                        'success' => true,
                        'message' => 'Deleted Successfully',
                        'i'=>$i
                    ));
            } else {
                return response()->json(array(
                        'success' => false,
                        'message' => 'No products found in file'
                    ));
            }
        }
    }
    public function up_products(Request $request)
    {
        $rules = array(
            'prod_file' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        // process the form
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        } else {
            $data = $request->all();
            $file_path = $data['prod_file']->getRealPath();

            $file_contents = Excel::load($file_path, function ($reader) {
            })->get();
            
            if (!empty($file_contents) && $file_contents->count()) {
                $main_categories = ProductCategory::pluck('m_category_id', 'm_category_name')->toArray();
                $sub_categories = ProductSubCategory::pluck('s_category_id', 's_category_name')->toArray();

                $main_categories = array_change_key_case($main_categories, CASE_LOWER);
                $sub_categories = array_change_key_case($sub_categories, CASE_LOWER);

                $file_contents = array_filter($file_contents->toArray());
                $empty = true;
                
                $previous_precision = ini_get('precision');
                ini_set('precision', 5);
                foreach ($file_contents as $value) {
                    if (!empty($value)) {
                        foreach ($value as $v) {
                            if (array_filter($v)) {
                                $empty = false;
                                $prod_master = ProductMaster::firstOrNew(['SKU' => trim($v['sku'])]);
                                // Give trim function to remove spaces
                                if (array_key_exists(strtolower($v['main_category']), $main_categories)) {
                                    $prod_master->m_category_id = $main_categories[strtolower($v['main_category'])];
                                } else {
                                    return response()->json(array(
                                        'success' => false,
                                        'message' => 'Products upload interrupted at product ( SKU : '.$v['sku'].')! Reason: Unknown Main category "'.$v['main_category'].'", please try again '
                                    ));
                                }
                                
                                $prod_master->name = $v['product_name'];

                                // Give trim function to remove spaces
                                if (array_key_exists(strtolower($v['category']), $sub_categories)) {
                                    $prod_master->s_category_id = $sub_categories[strtolower($v['category'])];
                                } else {
                                    return response()->json(array(
                                        'success' => false,
                                        'message' => 'Products upload interrupted at product ( SKU : '.$v['sku'].')! Reason: Unknown Category "'.$v['category'].'", please try again '
                                    ));
                                }

                                $rules     = array(
                                    'quote_weight' => 'numeric',
                                    'purity' => 'numeric',
                                    'beeds_weight' => 'numeric',
                                    'beeds_value' => 'numeric',
                                    'cents' => 'numeric',
                                    'making_charges' => 'numeric',
                                    'diamond_value' => 'numeric',
                                    'stones' => 'numeric',
                                    'max_shipping_days' => 'numeric',
                                    'certifications' => 'numeric',
                                    'women' => 'numeric',
                                    'men' => 'numeric',
                                    'kids' => 'numeric',
                                );

                                if (isset($v['wastage'])) {
                                    $rules['wastage'] = 'numeric';
                                }

                                if ($prod_master->m_category_id == config('abharan.main_category.silver')) {
                                    $rules['mc_percentage'] = 'numeric';
                                    $rules['mc_per_gram'] = 'numeric';
                                }

                                $validator = Validator::make($v, $rules);

                                if ($validator->fails()) {
                                    $message = 'Products upload interrupted at product ( SKU : '.$v['sku'].')! Reason: ';
                                    foreach ($validator->getMessageBag()->toArray() as $k => $tmp_msg) {
                                        $message .= 'Unknown value "'.$v[$k].'": ';
                                        $message .= rtrim($tmp_msg[0], '.').', ';
                                    }

                                    return response()->json(array(
                                        'success' => false,
                                        'message' => rtrim($message, ', ')
                                    ));
                                }

                                $prod_master->save();
                                $last_id = $prod_master->product_id;


                                $prod_det = ProductDetails::firstOrNew(['product_id' => $last_id]);
                                $prod_det->category = $v['category_grades'];
                                $prod_det->weight = round($v['weight'], 3);
                                $prod_det->quote = $v['quote_weight'];
                                $prod_det->purity = $v['purity'];
                                $prod_det->metal = $v['metal'];
                                // $prod_det->beeds = round($v['beeds_weight'], 3);
                                // $prod_det->beeds_value = $v['beeds_value'];
                                $prod_det->beeds = round($v['stone_weight'], 3);
                                $prod_det->beeds_value = $v['stone_value'];
                                $prod_det->beeds_description = $v['stone_details'];

                                if (isset($v['wastage'])) {
                                    $prod_det->wastage = $v['wastage'];
                                }

                                $prod_det->making_amount = $v['making_charges'];
                                
                                if ($prod_master->m_category_id == config('abharan.main_category.silver')) {
                                    $prod_det->mak_amt_percentage = $v['mc_percentage'];
                                    $prod_det->mak_amt_per_gram = $v['mc_per_gram'];
                                }

                                $prod_det->cents = $v['cents'];
                                $prod_det->daimond = $v['diamond_value'];
                                $prod_det->stone = $v['stones'];
                                $prod_det->cost = $v['cost'];
                                $prod_det->description = $v['detailed_description'];
                                $prod_det->production = $v['product_taxableyesno'];
                                $prod_det->max = $v['max_shipping_days'];
                                $prod_det->certification = $v['certifications'];
                                $prod_det->cut = $v['cut'];
                                $prod_det->colour = $v['color'];
                                $prod_det->clarity = $v['clarity'];
                                
                                $prod_det->women = $v['women'];
                                $prod_det->men = $v['men'];
                                $prod_det->kids = $v['kids'];

                                $prod_det->collection = $v['collection'];
                                $prod_det->occasion = $v['occasion'];

                                $prod_det->save();

                                $prod_img    = ProductImage::firstOrNew(['product_id' => $last_id]);

                                $prod_img->global_name = $v['sku'];

                                $prod_img->save();
                            }
                        }
                    }
                }
                ini_set('precision', $previous_precision);
                
                if (!$empty) {
                    return response()->json(array(
                        'success' => true,
                        'message' => 'Uploaded Successfully'
                    ));
                } else {
                    return response()->json(array(
                        'success' => false,
                        'message' => 'No products found in file'
                    ));
                }
            }
        }
    }

    public function images(Request $request, $render='view')
    {
        if ($render == 'view') {
            return view('Backend.pages.image');
        } elseif ($render == 'get_data') {
            $builder = ImageMaster::query();

            $query = $request->query();

            if ($cond = @$query['global_name']) {
                $builder->where('global_name', 'like', '%'.$cond.'%');
            }

            return $builder->paginate(10);
        }
    }

    public function up_image(Request $request)
    {
        $data = $request->all();

        // $data['image_file'] = array_filter($data['image_file']);
        
        if (!empty($data['image_file'])) {
            // for ($i=0;$i<count($data['image_file']);$i++) {
                // $tmp_img = $data['image_file'][$i];
            $tmp_img = $data['image_file'];

            list($width, $height) = getimagesize($tmp_img);
            $ratio = $width/$height; // width/height

            $parts = explode('.', $tmp_img->getClientOriginalName());
            $last = array_pop($parts);
            $parts = array(implode('.', $parts), $last);

            $img_name = $parts[0];
            $img_ext = $parts[1];

            $file_name = $tmp_img->getClientOriginalName();
            preg_match("/['^£$%&*()}{@#~<>,|=_+¬-]/im", $file_name, $delimiter);

            if (isset($delimiter[0])) {
                $globl_name = strtoupper(explode($delimiter[0], $file_name)[0]);
            } else {
                $globl_name = strtoupper(explode('.', $file_name)[0]);
            }

            $img_path_PH = 'uploads/product-images/PH/';
            $img_path_IN = 'uploads/product-images/IN/';
            $img_path_TH = 'uploads/product-images/TH/';

            if ($ratio > 1) {
                $ph_width = 700;
                $ph_height = 700/$ratio;

                $in_width = 400;
                $in_height = 400/$ratio;

                $th_width = 200;
                $th_height = 200/$ratio;
            } else {
                $ph_width = 700*$ratio;
                $ph_height = 700;

                $in_width = 400*$ratio;
                $in_height = 400;

                $th_width = 200*$ratio;
                $th_height = 200;
            }
            $globl_name = trim($globl_name);

            $ph_image = $in_image = $th_image = Image::make($tmp_img);

            $ph_image->fit($ph_width, $ph_height, function ($constraint) {
                $constraint->aspectRatio();
            });

            $res1 =Storage::disk('public')->put($img_path_PH.$globl_name.'_PH.'.$img_ext, (string) $ph_image->encode());
                
            $in_image->fit($in_width, $in_height, function ($constraint) {
                $constraint->aspectRatio();
            });

            $res2 = Storage::disk('public')->put($img_path_IN.$globl_name.'_IN.'.$img_ext, (string) $in_image->encode());
                
            $th_image->fit($th_width, $th_height, function ($constraint) {
                $constraint->aspectRatio();
            });

            $res3 = Storage::disk('public')->put($img_path_TH.$globl_name.'_TH.'.$img_ext, (string) $th_image->encode());

            if ($res1 && $res2 && $res3) {
                $image_master = ImageMaster::firstOrNew(['global_name' => $globl_name]);

                $image_master->image_name = $img_name;
                $image_master->image_ext = $img_ext;
                $image_master->save();
            }
            // }

            return response()->json(array(
                'success' => true,
                'message' => 'Images Uploaded'
            ));
        }

        return response()->json(array(
            'success' => false,
            'message' => 'Please upload images.'
        ));
    }

    public function rates()
    {
        return view('Backend.pages.rates', [
                'metals' => ProductRates::get(),
            ]
        );
    }

    public function up_rates(Request $request)
    {
        $data = $request->all();
        $res = true;

        foreach ($data['data'] as $metal) {
            $prod_rates = ProductRates::firstOrNew(['purity' => $metal['purity']]);
            $prod_rates->amount = $metal['amount'];
            $res = $prod_rates->save();
        }


        if ($res) {
            return response()->json(array(
                'success' => true,
                'message' => 'Updated Successfully'
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => 'Some error occured'
            ));
        }
    }

    public function getDownload($filename)
    {
        $file_arr = explode('.', $filename);
        $count_explode = count($file_arr);
        $ext = strtolower($file_arr[$count_explode-1]);

        $mimet = [
            'txt' => 'text/plain',
            'json' => 'application/json',
            'xml' => 'application/xml',
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // adobe
            'pdf' => 'application/pdf',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',


            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        ];

        if (isset($mimet[$ext])) {
            $cont_type = $mimet[$ext];
        } else {
            $cont_type = 'application/octet-stream';
        }

        //PDF file is stored under project/public/download/info.pdf
        $file= storage_path(). "/download/".$filename;

        $headers = [
            'Content-Type: '.$cont_type,
        ];

        return response()->download($file, $filename, $headers);
    }
}
