<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Storage;
use Image;
use App\User;
use App\Blog;
use App\BlogImage;

class BlogController extends Controller
{
    public function index(Request $request, $render='view')
    {
        $builder = Blog::query()->with('image');
        $rows = $builder->orderBy('created_at', 'DESC')->paginate(10);

        if ($render == 'view') {
            return view('Backend.pages.blog_list', ['data' => $rows]);
        } else {
            return $rows;
        }
    }

    public function add(Request $request)
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'product_image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ];

        $validator = Validator::make($request->all(), $rules);
        $data = $request->all();
    
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }
          

        $title= $request->get('title');
        $description =$request->get('description');
               
        $insert= [
            'title' => $title,
            'description'=>$description,
        ];


        $allowed =  array('png', 'jpg', 'jpeg');
        $filename = $_FILES['product_image']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);

        if (!in_array($ext, $allowed)) {
            return response()->json(array(
                'success' => false,
                'message' => "Only png or jpg image are allowed"
            ));
        }
       
        if ($request->hasFile('product_image')) {
            $blog = new Blog;
            $blog->title = $insert['title'];
            $blog->description = $insert['description'];
            $blog->save();

            $img = $request->file('product_image');


            $i_year=date('Y');
            $i_month=date('m');

            $img_path_lg = 'uploads/blog-images/'.$i_year.'/'.$i_month.'/lg/';
            $img_path_th = 'uploads/blog-images/'.$i_year.'/'.$i_month.'/th/';

            $inputwidth_lg = 482;
            $inputheight_lg = 350;

            $inputwidth_th= 150;
            $inputheight_th = 90;

            // Get the width and height of the Image
            list($width, $height) = getimagesize($img);

            // So then if the image is wider rather than taller, set the width and figure out the height
            if (($width/$height) > ($inputwidth_lg/$inputheight_lg)) {
                $outputwidth_lg = $inputwidth_lg;
                $outputheight_lg = round(($inputwidth_lg * $height)/ $width);
            }
            // And if the image is taller rather than wider, then set the height and figure out the width
            elseif (($width/$height) < ($inputwidth_lg/$inputheight_lg)) {
                $outputwidth_lg = round(($inputheight_lg * $width)/ $height);
                $outputheight_lg = $inputheight_lg;
            }
            // And because it is entirely possible that the image could be the exact same size/aspect ratio of the desired area, so we have that covered as well
            elseif (($width/$height) == ($inputwidth_lg/$inputheight_lg)) {
                $outputwidth_lg = $inputwidth_lg;
                $outputheight_lg = $inputheight_lg;
            }


            if (($width/$height) > ($inputwidth_th/$inputheight_th)) {
                $outputwidth_th = $inputwidth_th;
                $outputheight_th = round(($inputwidth_th * $height)/ $width);
            }
            // And if the image is taller rather than wider, then set the height and figure out the width
            elseif (($width/$height) < ($inputwidth_th/$inputheight_th)) {
                $outputwidth_th = round(($inputheight_th * $width)/ $height);
                $outputheight_th = $inputheight_th;
            }
            // And because it is entirely possible that the image could be the exact same size/aspect ratio of the desired area, so we have that covered as well
            elseif (($width/$height) == ($inputwidth_th/$inputheight_th)) {
                $outputwidth_th = $inputwidth_th;
                $outputheight_th = $inputheight_th;
            }

            $image_name=uniqid();
            $ext=$img->getClientOriginalExtension();

            $lg_image = $th_image = Image::make($img);

            $lg_image->fit($outputwidth_lg, $outputheight_lg, function ($constraint) {
                $constraint->aspectRatio();
            });

            $res1 = Storage::disk('public')->put($img_path_lg.$image_name.'.'.$ext, (string) $lg_image->encode());

            $th_image->fit($outputwidth_th, $outputheight_th, function ($constraint) {
                $constraint->aspectRatio();
            });

            $res2 = Storage::disk('public')->put($img_path_th.$image_name.'.'.$ext, (string) $th_image->encode());
            
            if ($res1 && $res2) {
                $b_img = new BlogImage;
                $b_img->blog_id = $blog->id;
                $b_img->year = $i_year;
                $b_img->month = $i_month;
                $b_img->img_name = $image_name;
                $b_img->ext = $ext;
                $b_img->save();
            }

                        
            return response()->json(array(
                'success' => true,
                'message' => "News inserted successfully."
            ));
        }
             
        return response()->json(array(
            'success' => false,
            'message' => "Some error occured!"
        ));
    }

    public function edit(Request $request, $id)
    {
        $blog =  Blog::with('image')->find($id);

        return response()->json(array(
            'success' => true,
            'data'=>$blog
        ));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        $data = $request->all();
    
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }
        

        if ($request->hasFile('product_image') && exif_imagetype($request->file('product_image'))) {
            $allowed =  array('png','jpg', 'jpeg');
            $filename = $_FILES['product_image']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);

            if (!in_array($ext, $allowed)) {
                return response()->json(array(
                    'success' => false,
                    'message' => "Only png or jpg image are allowed"
                ));
            }

            $img = $request->file('product_image');

            $i_year=date('Y');
            $i_month=date('m');

            $img_path_lg = 'uploads/blog-images/'.$i_year.'/'.$i_month.'/lg/';
            $img_path_th = 'uploads/blog-images/'.$i_year.'/'.$i_month.'/th/';

            $image_name=uniqid();
            $ext=$img->getClientOriginalExtension();
            $inputwidth_lg = 482;
            $inputheight_lg = 350;

            $inputwidth_th= 150;
            $inputheight_th = 90;

            // Get the width and height of the Image
            list($width, $height) = getimagesize($img);

            // So then if the image is wider rather than taller, set the width and figure out the height
            if (($width/$height) > ($inputwidth_lg/$inputheight_lg)) {
                $outputwidth_lg = $inputwidth_lg;
                $outputheight_lg = round(($inputwidth_lg * $height)/ $width);
            }
            // And if the image is taller rather than wider, then set the height and figure out the width
            elseif (($width/$height) < ($inputwidth_lg/$inputheight_lg)) {
                $outputwidth_lg = round(($inputheight_lg * $width)/ $height);
                $outputheight_lg = $inputheight_lg;
            }
            // And because it is entirely possible that the image could be the exact same size/aspect ratio of the desired area, so we have that covered as well
            elseif (($width/$height) == ($inputwidth_lg/$inputheight_lg)) {
                $outputwidth_lg = $inputwidth_lg;
                $outputheight_lg = $inputheight_lg;
            }


            if (($width/$height) > ($inputwidth_th/$inputheight_th)) {
                $outputwidth_th = $inputwidth_th;
                $outputheight_th = round(($inputwidth_th * $height)/ $width);
            }
            // And if the image is taller rather than wider, then set the height and figure out the width
            elseif (($width/$height) < ($inputwidth_th/$inputheight_th)) {
                $outputwidth_th = round(($inputheight_th * $width)/ $height);
                $outputheight_th = $inputheight_th;
            }
            // And because it is entirely possible that the image could be the exact same size/aspect ratio of the desired area, so we have that covered as well
            elseif (($width/$height) == ($inputwidth_th/$inputheight_th)) {
                $outputwidth_th = $inputwidth_th;
                $outputheight_th = $inputheight_th;
            }

            $image_name=uniqid();
            $ext=$img->getClientOriginalExtension();

            $lg_image = $th_image = Image::make($img);

            $lg_image->fit($outputwidth_lg, $outputheight_lg, function ($constraint) {
                $constraint->aspectRatio();
            });

            $res1 = Storage::disk('public')->put($img_path_lg.$image_name.'.'.$ext, (string) $lg_image->encode());

            $th_image->fit($outputwidth_th, $outputheight_th, function ($constraint) {
                $constraint->aspectRatio();
            });

            $res2 = Storage::disk('public')->put($img_path_th.$image_name.'.'.$ext, (string) $th_image->encode());
            
            if ($res1 && $res2) {
                $b_img = BlogImage::where('blog_id', $id)->first();

                Storage::disk('public')->delete('uploads/blog-images/'.$b_img->year.'/'.$b_img->month.'/lg/'.$b_img->img_name.'.'.$b_img->ext);
                Storage::disk('public')->delete('uploads/blog-images/'.$b_img->year.'/'.$b_img->month.'/th/'.$b_img->img_name.'.'.$b_img->ext);
                $b_img->year = $i_year;
                $b_img->month = $i_month;
                $b_img->img_name = $image_name;
                $b_img->ext = $ext;

                $b_img->save();
            }
        }
       

        $title= $request->get('title');
        $description =$request->get('description');
        
        $blog = Blog::find($id);
        $blog->title = $title;
        $blog->description = $description;
        $res  = $blog->save();

        if ($res) {
            return response()->json(array(
                'data' =>$data,
                'success' => true,
                'message' => "News updated successfully."
            ));
        } else {
            return response()->json(array(
                'data' =>$data,
                'success' => false,
                'message' => "Some error occured!"
            ));
        }
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $b_img = BlogImage::where('blog_id', $id)->first();

        $res1 = Storage::disk('public')->delete('uploads/blog-images/'.$b_img->year.'/'.$b_img->month.'/lg/'.$b_img->img_name.'.'.$b_img->ext);
        $res2 = Storage::disk('public')->delete('uploads/blog-images/'.$b_img->year.'/'.$b_img->month.'/th/'.$b_img->img_name.'.'.$b_img->ext);

        if ($res1 && $res2) {
            BlogImage::where('blog_id', $id)->delete();
        }
        
        $res = Blog::where('id', $id)->delete();

        if ($res) {
            return response()->json(array(
                'success' => true,
                'message' => "News deleted successfully."
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => "Some error occured!"
            ));
        }
    }
}
