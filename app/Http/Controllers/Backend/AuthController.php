<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Hash;
use Validator;
use App\User;

class AuthController extends Controller
{
    public function index()
    {
        return view('Backend.login');
    }

    public function auth_login(Request $request)
    {
        $rules     = array(
            'user_email' => 'required|max:255',
            'user_password' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $user = User::where('email', $data['user_email'])->select('id', 'password')->first();

        if (!empty($user)) {
            if (Hash::check($data['user_password'], $user['password'])) {
                Auth::attempt([
                        'email' => $data['user_email'],
                        'password' => $data['user_password'],
                        'remember_token' =>(isset($data['rememberme']))? true : false
                    ]
                );
                
                Auth::loginUsingId($user['id']);
                
                return response()->json(array(
                    'success' => true,
                ));
            }
        }
        return response()->json(array(
            'success' => false,
            'message' => "Invalid user name or password"
        ));
    }

    public function settings_view()
    {
        return view('Backend.pages.settings');
    }

    public function change_settings(Request $request)
    {
        $rules     = array(
            'username' => 'required',
            'current_password' => 'required'
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }


        if (Hash::check($data['current_password'], Auth::user()->password)) {
            $user = User::find(Auth::id());

            if ($change_pass) {
                $n_password = Hash::make($data['new_password']);
                $user->password = $n_password;
            }

            $user->email = $data['username'];
            $user->save();
            // Auth::logout();
            Auth::loginUsingId(Auth::id());

            return response()->json(array(
                'success' => true,
                'message' => 'Success',
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => 'Current password entered is incorrect',
            ));
        }
    }

    public function signout()
    {
        Auth::logout();

        return redirect('/superadmin/');
    }
}
