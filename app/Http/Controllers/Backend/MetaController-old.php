<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Feedback;
use Excel;
use App\Cust_Feedback;

class MetaController extends Controller
{
    public function index(Request $request, $render='view')
    {
        if ($render == 'view') {
            return view('Backend.pages.feedbacks');
        } elseif ($render == 'get_data') {
            $builder = Feedback::query();

            $query = $request->query();

            if (isset($query['email']) && ($cond = $query['email'])) {
                $builder->where('email', 'like', '%'.$cond.'%');
            }

            $rows = $builder->orderBy('created_at', 'DESC')->paginate(10);

            foreach ($rows as $row) {
                $row->date = ($time = strtotime($row->created_at)) ? date('d-m-Y', $time) : '-';
            }

            return $rows;
        }
    }
    
    public function app_feedbacks(Request $request, $render='view')
    {
        if ($render == 'view') {
            return view('Backend.pages.app_feedbacks');
        } elseif ($render == 'get_data') {
            $builder = Cust_Feedback::query()->with('suggestion', 'place');

            $query = $request->query();

            if (isset($query['name']) && ($cond = $query['name'])) {
                $builder->where('name', 'like', '%'.$cond.'%');
            }

            if (isset($query['mobile']) && ($cond = $query['mobile'])) {
                $builder->where('mobile', 'like', '%'.$cond.'%');
            }

            if (isset($query['feedback_date']) && ($cond = $query['feedback_date'])) {
                $range = explode(' - ', $query['feedback_date']);
                if (isset($range[0])) {
                    $range[0] = date('Y-m-d H:i:s', strtotime($range[0].'00:00:00'));
                } else {
                    $range[0] = date('Y-m-d H:i:s', strtotime(date('d-m-Y').'00:00:00'));                  
                }

                if (isset($range[1])) {
                    $range[1] = date('Y-m-d H:i:s', strtotime($range[1].'23:59:59'));
                } else {
                    $range[1] = date('Y-m-d H:i:s', strtotime(date('d-m-Y').'23:59:59'));
                }

                $builder->whereBetween('created_at', $range);
            }

            $rows = $builder->orderBy('created_at', 'DESC')->paginate(50);

            foreach ($rows as $row) {
                $row->staff_code = ($row->staff_code) ? $row->staff_code : '-';
                $row->doc_no = ($row->doc_no) ? $row->doc_no : '-';
                $row->rev_no = ($row->rev_no) ? $row->rev_no : '-';
                $row->date = ($time = strtotime($row->created_at)) ? date('d-m-Y', $time) : '-';
                $row->name = ($row->name) ? $row->name : '-';
                $row->landline = ($row->landline) ? $row->landline : '-';
                $row->mobile = ($row->mobile) ? $row->mobile : '-';
            }

            return $rows;
        }
    }

    public function app_feedback_exp(Request $request)
    {
        $query = $request->query();
        
        $builder = Cust_Feedback::query()->with('suggestion', 'place');
        $query = $request->query();

        if (isset($query['name']) && ($cond = $query['name'])) {
            $builder->where('name', 'like', '%'.$cond.'%');
        }

        if (isset($query['mobile']) && ($cond = $query['mobile'])) {
            $builder->where('mobile', 'like', '%'.$cond.'%');
        }

        if (isset($query['feedback_date']) && ($cond = $query['feedback_date'])) {
            $range = explode(' - ', $query['feedback_date']);
            if (isset($range[0])) {
                $range[0] = date('Y-m-d H:i:s', strtotime($range[0].'00:00:00'));
            } else {
                $range[0] = date('Y-m-d H:i:s', strtotime(date('d-m-Y').'00:00:00'));                  
            }

            if (isset($range[1])) {
                $range[1] = date('Y-m-d H:i:s', strtotime($range[1].'23:59:59'));
            } else {
                $range[1] = date('Y-m-d H:i:s', strtotime(date('d-m-Y').'23:59:59'));
            }
            
            $builder->whereBetween('created_at', $range);
        }
        
        $feedbacks = $builder->orderBy('created_at', 'DESC')->get();

        $ratings = [
            '1' => 'Excellent',
            '2' => 'Good',
            '3' => 'Average',
            '4' => 'Poor',
        ];
        
        $mood = [
            '1' => 'Hate It',
            '2' => 'Dont Like',
            '3' => 'Its OK',
            '4' => 'Looks Good',
            '5' => 'Love It'
        ];

        $answers = [
            '1' => 'Yes',
            '2' => 'Can\'t Say',
            '3' => 'No'
        ];

        $i = 1;
        foreach ($feedbacks as $feedback) {
            $feedback->sl_no = $i;
            $feedback->quality_of_service_str = $ratings[$feedback->quality_of_service];
            $feedback->exclusive_collection_str = $ratings[$feedback->exclusive_collection];
            $feedback->service_sales_statff_str = $ratings[$feedback->service_sales_statff];
	    $feedback->date = ($time = strtotime($feedback->created_at)) ? date('d-m-Y', $time) : '-';
            $feedback->convenience_of_shopping_str = $ratings[$feedback->convenience_of_shopping];
            $feedback->response_to_enquiry_str = $ratings[$feedback->response_to_enquiry];

            $feedback->quality_of_ornaments_str = $ratings[$feedback->quality_of_ornaments];
            $feedback->delivery_time_str = $ratings[$feedback->delivery_time];
            
            $feedback->delivery_time_str = $ratings[$feedback->delivery_time];
            $feedback->showroom_ambience_str = $ratings[$feedback->showroom_ambience];

            $feedback->info_on_prod_features_str = $ratings[$feedback->info_on_prod_features];
            $feedback->attending_to_queries_str = $ratings[$feedback->attending_to_queries];

            $feedback->is_abharan_first_choice_str = $answers[$feedback->suggestion->is_abharan_first_choice];
            $feedback->recommend_us_to_othrs_str = $answers[$feedback->suggestion->recommend_us_to_othrs];

            $feedback->your_contact_str = $answers[$feedback->suggestion->your_contact];
            $feedback->more_suggestions_str = $feedback->suggestion->more_suggestions;
            $feedback->place_str = $feedback->place->name;
            $feedback->rating = $mood[$feedback->Ratings];
            $i++;
        }

        $titles = [
            "sl_no" => 'Sl No.',
            "staff_code" => 'Staff Code',
            "doc_no" => 'DOC No.',
            "rev_no" => 'Rev No.',
            "date" => 'Date',
            "name" => 'Name',
            "landline" => 'Landline',
            "mobile" => 'Mobile',
            "quality_of_service_str" => 'Quality of Service',
            "exclusive_collection_str" => 'Exclusive Collection',
            "service_sales_statff_str" => 'Service of sales staff',
            "convenience_of_shopping_str" => 'Convenience of shopping the Showroom',
            "response_to_enquiry_str" => 'Response to enquiry',
            'quality_of_ornaments_str' => "Quality of ornaments",
            "delivery_time_str" => 'Delivery Time',
            'showroom_ambience_str' => "Showroom ambience",
            'info_on_prod_features_str' => "Information on product features",
            'attending_to_queries_str' => "Attending to queries",
            'is_abharan_first_choice_str' => "Is the selection of our showroom your first choice?",
            'recommend_us_to_othrs_str' => "Will you recommend our services/ products to others?",
            'your_contact_str' => "Whether the phone number given by you while billing belongs to you?",
            'more_suggestions_str' => "Your valuable suggestions for improvement our customer service",
            'place_str' => "Place",
            'rating' => "Rating",
        ];

        $type = 'xls';
        $data = $feedbacks->toArray();
        return Excel::create('Customer-Ratings-List', function ($excel) use ($data, $titles) {
            $excel->sheet('ratings', function ($sheet) use ($data, $titles) {
                $sheet->row(1, $titles);
                $i = 2;
                foreach ($data as $value) {
                    $tmp = [];

                    foreach ($titles as $key => $v) {
                        $tmp[$key] = $value[$key];
                    }

                    $sheet->row($i, $tmp);
                    $i++;
                }
            });
        })->download($type);
    }
}
