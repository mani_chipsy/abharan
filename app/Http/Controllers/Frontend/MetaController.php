<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Validator;
use App\Feedback;

class MetaController extends Controller
{
    public function videos($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.video_gallery');
        } elseif ($get == 'data') {
        }
    }
    
    public function about_jewellry($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.about_jewellry');
        } elseif ($get == 'data') {
        }
    }
    
    public function jewllery_care($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.jewell_care');
        } elseif ($get == 'data') {
        }
    }

    public function about_us($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.about_us');
        } elseif ($get == 'data') {
        }
    }

    public function contact_us($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.contact_us');
        } elseif ($get == 'data') {
        }
    }

    public function save_feedback(Request $request)
    {
        $rules = array(
            'email' => 'required',
            'body' => 'required',
        );

        $contact_det = $request->only('contact')['contact'];

        $validator = Validator::make($contact_det, $rules);
        // process the form
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $feedback = new Feedback;
        $feedback->name = $contact_det['name'];
        $feedback->email = $contact_det['email'];
        $feedback->message = $contact_det['body'];

        $res = $feedback->save();

        if ($res) {
            return response()->json(array(
                'success' => true,
                'message' => 'Succesfully Submitted',
                'redirect' => '/'
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => 'Some error occured'
            ));
        }
    }

    public function faq($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.faqs');
        } elseif ($get == 'data') {
        }
    }

    public function return_policy($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.return_policy');
        } elseif ($get == 'data') {
        }
    }

    public function our_schemes($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.our_schemes');
        } elseif ($get == 'data') {
        }
    }

    public function scheme_collections($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.scheme_collections');
        } elseif ($get == 'data') {
        }
    }

    public function aasthana_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.aasthana_collection');
        } elseif ($get == 'data') {
        }
    }

    public function azva_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.azva_collection');
        } elseif ($get == 'data') {
        }
    }

    public function blue_pottery_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.blue_pottery_collection');
        } elseif ($get == 'data') {
        }
    }

    public function celesta_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.celesta_collection');
        } elseif ($get == 'data') {
        }
    }

    public function ganesh_chaturthi_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.ganesh_chaturthi_collection');
        } elseif ($get == 'data') {
        }
    }
    
    public function mantra_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.mantra_collection');
        } elseif ($get == 'data') {
        }
    }

    public function monsoon_mystica($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.monsoon_mystica');
        } elseif ($get == 'data') {
        }
    }

    public function noorie_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.noorie_collection');
        } elseif ($get == 'data') {
        }
    }

    public function pritty_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.pritty_collection');
        } elseif ($get == 'data') {
        }
    }

    public function pritty_silver_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.pritty_silver_collection');
        } elseif ($get == 'data') {
        }
    }

    public function shezmin_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.shezmin_collection');
        } elseif ($get == 'data') {
        }
    }


    public function silver_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.silver_collection');
        } elseif ($get == 'data') {
        }
    }

    public function taarika_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.taarika_collection');
        } elseif ($get == 'data') {
        }
    }

    public function tanvi_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.tanvi_collection');
        } elseif ($get == 'data') {
        }
    }

    public function utsav_temple_collection($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.utsav_temple_collection');
        } elseif ($get == 'data') {
        }
    }
}
