<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Hash;
use Validator;
use App\User;
use App\UserDetails;
use Cart;
use Session;
use App\ProductSubCategory;
use App\Repository\Frontend\Sms;

class AuthController extends Controller
{
    public function index()
    {
        return view('Frontend.index');
    }
    
    public function login()
    {
        return view('Frontend.login');
    }

    public function filters()
    {
        $g_temp = ProductSubCategory::where('s_category_id', '!=', config('abharan.sub_category.article'))->orderBy('s_category_name', 'ASC')->get()->toArray();

        $not_siver = [
            config('abharan.sub_category.waist_chain'),
            config('abharan.sub_category.haram'),
            config('abharan.sub_category.kanti')
        ];

        $not_diamond = [
            config('abharan.sub_category.waist_chain'),
            config('abharan.sub_category.haram'),
            config('abharan.sub_category.kanti'),
            config('abharan.sub_category.chain'),
            config('abharan.sub_category.bracelet'),
            config('abharan.sub_category.article')
        ];

        $s_temp = ProductSubCategory::whereNotIn('s_category_id', $not_siver)->orderBy('s_category_name', 'ASC')->get()->toArray();
        $d_temp = ProductSubCategory::whereNotIn('s_category_id', $not_diamond)->orderBy('s_category_name', 'ASC')->get()->toArray();


        $g_sub_cat = array_chunk($g_temp, 4);
        $s_sub_cat = array_chunk($s_temp, 3);
        $d_sub_cat = array_chunk($d_temp, 2);

        $g_filter_cat = [['men' => 'Men', 'women'=> 'Women', 'kids' => 'Kids']];
        $s_filter_cat = [['men' => 'Men', 'women'=> 'Women', 'kids' => 'Kids']];
        $d_filter_cat = [['men' => 'Men', 'women'=> 'Women']];

        return [
            'g_sub_cat' => $g_sub_cat,
            'g_filter_cat' => $g_filter_cat,
            's_sub_cat' => $s_sub_cat,
            's_filter_cat' => $s_filter_cat,
            'd_sub_cat' => $d_sub_cat,
            'd_filter_cat' => $d_filter_cat,
        ];
    }
    public function auth_login(Request $request)
    {
        $rules     = array(
            'mobile' => 'required|max:255',
            'password' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $user = User::where('mobile', $data['mobile'])->select('id', 'password')->first();

        if (!empty($user)) {
            if (Hash::check($data['password'], $user['password'])) {
                Auth::attempt([
                        'email' => $data['mobile'],
                        'password' => $data['password'],
                        'remember_token' =>(isset($data['rememberme']))? true : false
                    ]
                );
                
                Auth::loginUsingId($user['id']);

                $intended_url = Session::get('url.intended', url('/product/collection'));
                Session::forget('url.intended');

                return response()->json(array(
                    'success' => true,
                    'redirect' => $intended_url
                ));
            }
        }
        return response()->json(array(
            'success' => false,
            'message' => "Invalid user name or password"
        ));
    }

    public function signout()
    {
        Auth::logout();

        return redirect('/');
    }
    
    public function register()
    {
        return view('Frontend.register');
    }

    public function create_account(Request $request)
    {
        $inputs    = $request->all();
        $rules     = array(
            'full_name' => 'required|max:255',
            'email' => 'required',
            'mobile' => 'required|unique:users',
            'password'=>'required',
            'cp_password'  =>  'required|same:password'
        );

        $messages = [
            'cp_password.same' => 'Password Confirmation should match the Password',
            'cp_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $user = new User;

        $user->name = $inputs['full_name'];
        $user->email = $inputs['email'];
        $user->mobile = $inputs['mobile'];
        $user->password = Hash::make($inputs['password']);
        $user->status = 0;
        $user->power = 2;

        $user->save();

        $userid = $user->id;
        $user_detail = new UserDetails();
        $user_detail->user_id = (int)$userid;
        $user_detail->save();

        if ($userid) {
            $otp = rand(1000, 9999);
            // $otp = '1111';

            $message = 'Hi, '.$otp
            .' is your one time password(OTP), please enter the same to'
            .' complete your registration process. OTP will expire in 1 hour. Thank you,'
            .' Team Abharan';
            // Send OTP to mobile number
            $res = $this->send_otp($message, $inputs['mobile']);

            if ($res) {
                Session::put('otp', $otp);
                Session::put('userid', $userid);
                    
                return response()->json([
                        'success' => true,
                        'message' => 'Success',
                        'mobile' => str_pad(substr($inputs['mobile'], -2), strlen($inputs['mobile']), '*', STR_PAD_LEFT)
                    ]
                );
            }
        } else {
            return response()->json([
                    'success' => false,
                    'message' => 'Some error occured, please try again',
                ]
            );
        }
    }

    public function settings()
    {
        return view('Frontend.pages.settings');
    }

    public function change_pass(Request $request)
    {
        $rules     = array(
            'old_password' => 'required',
            'password' => 'required',
            'password2' => 'required|same:password',
        );

        $messages = [
            'password2.same' => 'Password Confirmation should match the Password',
            'password2.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        if (Hash::check($data['old_password'], Auth::user()->password)) {
            $n_password = Hash::make($data['password']);
            
            $user = User::find(Auth::id());

            $user->password = $n_password;

            $user->save();
            // Auth::logout();

            return response()->json(array(
                'success' => true,
                'message' => 'Success',
                'redirect' => '/'
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => 'Old password entered is incorrect',
            ));
        }
    }

    public function forgot_pass()
    {
        return view('Frontend.forgot_password');
    }

    public function reset_form(Request $request)
    {
        $userid = $request->session()->get('userid');

        return view('Frontend.reset_pass_form', ['userid' => $userid]);
    }

    public function recovery_mobile(Request $request)
    {
        $inputs    = $request->all();
        $rules     = array(
            'mobile' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $user = User::where('mobile', $data['mobile'])->first();

        if (!empty($user)) {
            // $otp = '1111';
            $otp = rand(1000, 9999);

            $message = 'Hi, '.$otp
            .' is your one time password(OTP), please enter the same to'
            .' reset your password. OTP will expire in 1 hour. Thank you,'
            .' Team Abharan';
            // Send OTP to mobile number
            $res = $this->send_otp($message, $user['mobile']);
            // $res =true;

            if ($res) {
                Session::put('otp', $otp);
                Session::put('userid', $user->id);

                return response()->json([
                        'success' => true,
                        'message' => 'OTP sent successfully',
                        'redirect' => '/reset-password',
                        'mobile' => str_pad(substr($data['mobile'], -2), strlen($inputs['mobile']), '*', STR_PAD_LEFT)
                    ]
                );
            }
        } else {
            return response()->json([
                    'success' => false,
                    'message' => 'Entered mobile number does not exist in our records',
                ]
            );
        }
    }

    public function otp_ver_reset_pass(Request $request)
    {
        $inputs    = $request->all();
        $rules     = array(
            'otp' => 'required',
            'password'=>'required',
            'confirm_password'  =>  'required|same:password'
        );

        $messages = [
            'confirm_password.same' => 'Password Confirmation should match the Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $otp = $request->session()->get('otp');
        $userid = $request->session()->get('userid');

        if ($otp == $data['otp']) {
            $user = User::where('id', $data['userid'])->first();

            if (!empty($user)) {
                $user->password = Hash::make($data['password']);

                $res = $user->save();

                if ($res) {
                    Session::forget('otp');
                    Session::forget('userid');

                    return response()->json([
                            'success' => true,
                            'message' => 'Password changed successfully',
                        ]
                    );
                }
            } else {
                return response()->json([
                        'success' => false,
                        'message' => 'Some error occured',
                    ]
                );
            }
        } else {
            return response()->json([
                    'success' => false,
                    'message' => 'OTP does not match.',
                ]
            );
        }
    }

    protected function send_otp($message, $mobile_no)
    {
        $sms = new Sms();
        return $sms->send($message, $mobile_no);
    }

    public function resend_otp(Request $request)
    {
        $otp = $request->session()->get('otp');

        $userid = $request->session()->get('userid');
        $user = User::find($userid);

        $res = $this->send_otp($otp, $user->mobile);

        if ($res) {
            return response()->json([
                    'success' => true,
                    'message' => 'OTP resent successfully',
                ]
            );
        }
    }
    public function verify_otp(Request $request)
    {
        $enteredOtp = $request->input('otp');

        $otp = $request->session()->get('otp');
        if ($otp == $enteredOtp) {
            $userid = $request->session()->get('userid');
            // Updating user's status as 1.
            User::where('id', $userid)->update(['status' => 1]);

            //Removing Session variable
            Session::forget('otp');
            Session::forget('userid');
            
            Auth::loginUsingId($userid);

            $intended_url = Session::get('url.intended', url('/product/collection'));

            Session::forget('url.intended');

            return response()->json([
                    'success' => true,
                    'message' => 'Your Mobile Number is Verified.',
                    'redirect' => $intended_url
                ]
            );
        } else {
            return response()->json([
                    'success' => false,
                    'message' => 'OTP does not match.',
                ]
            );
        }
    }
}
