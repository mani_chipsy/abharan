<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\UserDetails;
use App\ProductMaster;
use App\ProductOrders;
use App\ProductOrderDetails;
use App\ProductRates;
use App\ProductOffer;
use App\ProductCategory;
use App\ProductSubCategory;
use Illuminate\Http\Request;
use Cart;
use DB;
use Validator;
use Redirect;
use App\Repository\Frontend\Sms;
use App\Repository\Frontend\Mastercard;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductController extends Controller
{
    public function index()
    {
        setlocale(LC_MONETARY, 'en_IN');
        $collections = ProductMaster::with(['main_category', 'sub_category', 'details', 'image'])->has('details')->orderBy('created_at', 'DESC')->paginate(4);

        foreach ($collections as $collection) {
            $collection->gross_value = 0;
            $gross_value = 0;
            $gst_tax = config('cart.tax') ? config('cart.tax')/100 : 0;

            if (($collection->main_category->m_category_id == config('abharan.main_category.diamond') || $collection->main_category->m_category_id == config('abharan.main_category.silver')) && !(float)$collection->details->weight && $collection->details->cost) {
                $gross_value = $collection->details->cost;
            } else {
                $gross_weight = $collection->details->weight;
                $beeds_weight = $collection->details->beeds;
                $beeds_value = $collection->details->beeds_value;

                if ($collection->main_category->m_category_id != config('abharan.main_category.silver')) {
                    $purity = $collection->details->purity;
                } else {
                    $purity = 0;
                }

                $metal_value  = ProductRates::where('purity', $purity)->first()['amount'];

                $diamond_value   = $collection->details->daimond;

                // Calculate Gross Cost of Collection
                $net_weight = $gross_weight - $beeds_weight;
                $offer_per_gram = ProductOffer::where('status', 1)->first();
                $net_value1 = $net_weight * $metal_value;
                
                // Calculate Making amount
                if ($collection->main_category->m_category_id == config('abharan.main_category.diamond')) {
                    if ((float)$collection->details->making_amount) {
                        $making_amount = (float)$collection->details->making_amount;
                    } else {
                        $making_per = (float)$collection->details->wastage ? (float)$collection->details->wastage/100 : 0;
                        $making_amount = $net_value1 * $making_per;
                    }
                } elseif ($collection->main_category->m_category_id == config('abharan.main_category.silver')) {
                    if ((float)$collection->details->making_amount) {
                        $making_amount = (float)$collection->details->making_amount;
                    } elseif ((float)$collection->details->mak_amt_percentage) {
                        $making_per = (float)$collection->details->mak_amt_percentage/100;
                        $making_amount = $net_value1 * $making_per;
                    } else {
                        $making_amount = (float)$collection->details->mak_amt_per_gram ? ($net_weight * (float)$collection->details->mak_amt_per_gram) : 0;
                    }
                } else {
                    $making_per   = (float)$collection->details->making_amount ? (float)$collection->details->making_amount/100 : 0;
                    $making_amount = $net_value1 * $making_per;
                }

                if (!empty($offer_per_gram)) {
                    $net_value2 = $net_weight * ($metal_value - $offer_per_gram->discount); // Multiply weight of collection with daily rate of metal
                    $offer_value = ($net_value2 + $beeds_value + $making_amount + $diamond_value);
                    $collection->gst_amount = $offer_value * $gst_tax;
                    $offer_value += $collection->gst_amount;
                    $collection->offer_value = money_format('%!i', $offer_value);
                }

                $gross_value = ($net_value1 + $beeds_value + $making_amount + $diamond_value);
            }
            
            $gross_gst_amount = $gross_value * $gst_tax;
            $gross_value += $gross_gst_amount;
                
            if (!isset($collection->gst_amount)) {
                $collection->gst_amount = $gross_gst_amount;
            }

            $collection->gross_value = money_format('%!i', $gross_value);
            $collection->gst_amount = money_format('%!i', $collection->gst_amount);
            $collection->gst = config('cart.tax');

            $collection->details->weight = (float)$collection->details->weight ? $collection->details->weight : 'NA';
            $collection->details->beeds = (float)$collection->details->beeds ? $collection->details->beeds : 'NA';
            $collection->details->cents = (float)$collection->details->cents ? $collection->details->cents : 'NA';
            $collection->details->purity = (int)$collection->details->purity ? $collection->details->purity : 'NA';
        }

        return $collections;
    }

    public function order_history($render='view')
    {
        if ($render == 'view') {
            return view('Frontend.pages.ord_history');
        } elseif ($render == 'get_data') {
            $orders = ProductOrders::with(['details'])->where('user_id', Auth::id())->orderBy('created_at', 'DESC')->paginate(8);
            $modes = ['1' => 'COD', '2' => 'Online Payment'];

            foreach ($orders as $order) {
                $order->mode_str = $modes[$order->mode];

                $cart_gst = 0;
                $sub_total = 0;
                $calc_tax = 0;

                foreach ($order->details as $item) {
                    $itm_tot = $item->price * $item->qty;

                    if ($item->gst_rate) {
                        $cart_gst = $item->gst_rate * 100;
                    }

                    $sub_total += $itm_tot;
                    $calc_tax += $item->gst_rate ? ($itm_tot * $item->gst_rate) : 0;
                }

                // $cart_subtotal = $this->inrCurrency($sub_total);
                // $cart_tax = $this->inrCurrency($calc_tax);
                $order->total = round(($sub_total + $calc_tax), 2);
                $order->total = $this->inrCurrency($order->total);
                $order->created_str = ($time = strtotime($order->created_at)) ? date('d F, Y', $time) : '-';
            }

            return $orders;
        } else {
            $order = ProductOrders::with(['details'])->where('user_id', Auth::id())->find($render);
            $user = User::with('details')->find($order->user_id);

            $modes = ['1' => 'COD', '2' => 'Online Payment'];

            $order->mode_str = $modes[$order->mode];
            $cart_gst = 0;
            $sub_total = 0;
            $calc_tax = 0;

            foreach ($order->details as $item) {
                $item->product = ProductMaster::with('image')->find($item->product_id);
                $itm_tot = $item->price * $item->qty;

                $itm_tot = round($itm_tot, 2);
                $item->subtotal = $this->inrCurrency($itm_tot);

                $item->price = round($item->price, 2);
                $item->price = $this->inrCurrency($item->price);

                if ($item->gst_rate) {
                    $cart_gst = $item->gst_rate * 100;
                }

                $sub_total += $itm_tot;
                $calc_tax += $item->gst_rate ? ($itm_tot * $item->gst_rate) : 0;
                
                if ($item->gst_rate > 0 && !isset($order->gst)) {
                    $order->gst = ($item->gst_rate * 100);
                }
            }
            $sub_total = round($sub_total, 2);
            $order->subtotal = $this->inrCurrency($sub_total);

            $calc_tax = round($calc_tax, 2);
            $order->tax = $this->inrCurrency($calc_tax);
            
            $order->total = round(($sub_total + $calc_tax), 2);
            $order->total = $this->inrCurrency($order->total);

            $ship_address = $order->recpnt_name.', ';
            $ship_address .= $order->address.', ';
            $ship_address .= $order->city.' - '.$order->pincode.', ';
            $ship_address .= $order->state.', ';
            $ship_address .= 'Phone: '.$order->mobile.', ';
            $ship_address .= 'Email: '.$user->email.', ';
            
            if ($order->landmark) {
                $ship_address .= 'Landmark: '.$order->landmark.', ';
            }

            if ($order->alternate_mobile) {
                $ship_address .= 'Alternate Phone: '.$order->alternate_mobile.'.';
            }
            $order->ship_address = $ship_address;
            $order->created_str = ($time = strtotime($order->created_at)) ? date('d F, Y', $time) : '-';

            // echo '<pre>';
            // print_r($order);
            // die;
            return view('Frontend.pages.view_order', ['order' => $order]);
        }
    }

    public function order_details($order_no='')
    {
        if ($order_no) {
            $orders = ProductOrders::with(['details'])->paginate(8);
            $modes = ['1' => 'COD', '2' => 'Online Payment'];

            foreach ($orders as $order) {
                $order->mode_str = $modes[$order->mode];
                $cart_gst = 0;
                $sub_total = 0;
                $calc_tax = 0;

                foreach ($order->details as $item) {
                    $itm_tot = $item->price * $item->qty;

                    if ($item->gst_rate) {
                        $cart_gst = $item->gst_rate * 100;
                    }

                    $sub_total += $itm_tot;
                    $calc_tax += $item->gst_rate ? ($itm_tot * $item->gst_rate) : 0;
                }

                // $cart_subtotal = $this->inrCurrency($sub_total);
                // $cart_tax = $this->inrCurrency($calc_tax);
                
                $order->total = $this->inrCurrency(($sub_total + $calc_tax));
            }

            return $orders;
        } else {
            return view('Frontend.pages.ord_history');
        }
    }

    public function new_collection()
    {
        $collections = ProductMaster::with(['main_category', 'sub_category', 'details', 'image'])->has('details')->orderBy('created_at', 'DESC')->limit(15)->get();

        foreach ($collections as $collection) {
            $collection->gross_value = 0;
            $gross_value = 0;
            $gst_tax = config('cart.tax') ? config('cart.tax')/100 : 0;
            setlocale(LC_MONETARY, 'en_IN');

            if (($collection->main_category->m_category_id == config('abharan.main_category.diamond') || $collection->main_category->m_category_id == config('abharan.main_category.silver')) && !(float)$collection->details->weight && $collection->details->cost) {
                $gross_value = $collection->details->cost;
            } else {
                $gross_weight = $collection->details->weight;
                $beeds_weight = $collection->details->beeds;
                $beeds_value = $collection->details->beeds_value;

                if ($collection->main_category->m_category_id != config('abharan.main_category.silver')) {
                    $purity = $collection->details->purity;
                } else {
                    $purity = 0;
                }

                $metal_value  = ProductRates::where('purity', $purity)->first()['amount'];

                $diamond_value   = $collection->details->daimond;

                // Calculate Gross Cost of Collection
                $net_weight = $gross_weight - $beeds_weight;
                $offer_per_gram = ProductOffer::where('status', 1)->first();
                $net_value1 = $net_weight * $metal_value;
                
                // Calculate Making amount
                if ($collection->main_category->m_category_id == config('abharan.main_category.diamond')) {
                    if ((float)$collection->details->making_amount) {
                        $making_amount = (float)$collection->details->making_amount;
                    } else {
                        $making_per = (float)$collection->details->wastage ? (float)$collection->details->wastage/100 : 0;
                        $making_amount = $net_value1 * $making_per;
                    }
                } elseif ($collection->main_category->m_category_id == config('abharan.main_category.silver')) {
                    if ((float)$collection->details->making_amount) {
                        $making_amount = (float)$collection->details->making_amount;
                    } elseif ((float)$collection->details->mak_amt_percentage) {
                        $making_per = (float)$collection->details->mak_amt_percentage/100;
                        $making_amount = $net_value1 * $making_per;
                    } else {
                        $making_amount = (float)$collection->details->mak_amt_per_gram ? ($net_weight * (float)$collection->details->mak_amt_per_gram) : 0;
                    }
                } else {
                    $making_per   = (float)$collection->details->making_amount ? (float)$collection->details->making_amount/100 : 0;
                    $making_amount = $net_value1 * $making_per;
                }

                if (!empty($offer_per_gram)) {
                    $net_value2 = $net_weight * ($metal_value - $offer_per_gram->discount); // Multiply weight of collection with daily rate of metal
                    $offer_value = ($net_value2 + $beeds_value + $making_amount + $diamond_value);
                    $collection->gst_amount = $offer_value * $gst_tax;
                    $offer_value += $collection->gst_amount;
                    $collection->offer_value = money_format('%!i', $offer_value);
                }

                $gross_value = ($net_value1 + $beeds_value + $making_amount + $diamond_value);
            }

            $gross_gst_amount = $gross_value * $gst_tax;
            $gross_value += $gross_gst_amount;
                
            if (!isset($collection->gst_amount)) {
                $collection->gst_amount = $gross_gst_amount;
            }

            $collection->gross_value = money_format('%!i', $gross_value);
            $collection->gst_amount = money_format('%!i', $collection->gst_amount);
            $collection->gst = config('cart.tax');
            
            $collection->details->weight = (float)$collection->details->weight ? $collection->details->weight : 'NA';
            $collection->details->beeds = (float)$collection->details->beeds ? $collection->details->beeds : 'NA';
            $collection->details->cents = (float)$collection->details->cents ? $collection->details->cents : 'NA';
            $collection->details->purity = (int)$collection->details->purity ? $collection->details->purity : 'NA';
        }

        return $collections;
    }
    public function get_main_cat()
    {
        return ProductCategory::orderBy('ordering', 'ASC')->get();
    }

    public function get_sub_cat()
    {
        return ProductSubCategory::orderBy('s_category_name', 'ASC')->get();
    }

    public function view_collection(Request $request)
    {
        $query = $request->query();

        return view('Frontend.pages.collections', [
                'main_cat' => $this->get_main_cat()->pluck('m_category_name', 'm_category_id'),
                'sub_cat' => $this->get_sub_cat()->pluck('s_category_name', 's_category_id'),
                'm_category_id' => @$query['m_category_id'],
                's_category_id' => @$query['s_category_id'],
                'filter_cat'    => @$query['filter_cat'],
            ]
        );
    }

    protected function paginate($items, $perPage = 12)
    {
        //Get current page form url e.g. &page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        //Slice the collection to get the items to display in current page
        $currentPageItems = $items->slice(($currentPage - 1) * $perPage, $perPage);

        //Create our paginator and pass it to the view
        return new LengthAwarePaginator($currentPageItems, count($items), $perPage);
    }

    public function get_collections(Request $request)
    {
        $query = $request->query();
        
        $builder = ProductMaster::query()->with(['main_category', 'sub_category', 'details', 'image'])->has('details');

        if (!empty($query['m_category_id'])) {
            $builder->whereIn('m_category_id', $query['m_category_id']);
        }

        if (!empty($query['s_category_id'])) {
            $builder->whereIn('s_category_id', $query['s_category_id']);
        }

        if (!empty($query['filter_cat'])) {
            $filter_cat = $query['filter_cat'];
            $builder->whereHas('details', function ($q) use ($filter_cat) {
                $i = 1;
                foreach ($filter_cat as $cat) {
                    if ($i == 1) {
                        $q->where($cat, '=', 1);
                    } else {
                        $q->orWhere($cat, '=', 1);
                    }
                    $i++;
                }
            });
        }

        if ($cond = $query['smart_query']) {
            $builder->where(function ($q) use ($cond) {
                $q->orWhere('name', 'like', '%'.$cond.'%')->orWhere('SKU', 'like', '%'.$cond.'%');
            });
        }

        $price_range = array_filter(explode('-', $query['price_filter']));
        // DB::enableQueryLog();
        $collections = $builder->orderBy('updated_at', 'DESC')->get();
        // echo '<pre>';
        // print_r($collections->toArray());
        // die;
        
        foreach ($collections as $key => $collection) {
            $collection->gross_value = 0;
            $gross_value = 0;
            $offer_value = 0;
            $gst_tax = config('cart.tax') ? config('cart.tax')/100 : 0;
            setlocale(LC_MONETARY, 'en_IN');

            if (($collection->main_category->m_category_id == config('abharan.main_category.diamond') || $collection->main_category->m_category_id == config('abharan.main_category.silver')) && !(float)$collection->details->weight && $collection->details->cost) {
                $gross_value = $collection->details->cost;
            } else {
                $gross_weight = $collection->details->weight;
                $beeds_weight = $collection->details->beeds;
                $beeds_value = $collection->details->beeds_value;

                if ($collection->main_category->m_category_id != config('abharan.main_category.silver')) {
                    $purity = $collection->details->purity;
                } else {
                    $purity = 0;
                }

                $metal_value  = ProductRates::where('purity', $purity)->first()['amount'];

                $diamond_value   = $collection->details->daimond;

                // Calculate Gross Cost of Collection
                $net_weight = $gross_weight - $beeds_weight;
                $offer_per_gram = ProductOffer::where('status', 1)->first();
                $net_value1 = $net_weight * $metal_value;
                
                // Calculate Making amount
                if ($collection->main_category->m_category_id == config('abharan.main_category.diamond')) {
                    if ((float)$collection->details->making_amount) {
                        $making_amount = (float)$collection->details->making_amount;
                    } else {
                        $making_per = (float)$collection->details->wastage ? (float)$collection->details->wastage/100 : 0;
                        $making_amount = $net_value1 * $making_per;
                    }
                } elseif ($collection->main_category->m_category_id == config('abharan.main_category.silver')) {
                    if ((float)$collection->details->making_amount) {
                        $making_amount = (float)$collection->details->making_amount;
                    } elseif ((float)$collection->details->mak_amt_percentage) {
                        $making_per = (float)$collection->details->mak_amt_percentage/100;
                        $making_amount = $net_value1 * $making_per;
                    } else {
                        $making_amount = (float)$collection->details->mak_amt_per_gram ? ($net_weight * (float)$collection->details->mak_amt_per_gram) : 0;
                    }
                } else {
                    $making_per   = (float)$collection->details->making_amount ? (float)$collection->details->making_amount/100 : 0;
                    $making_amount = $net_value1 * $making_per;
                }

                $offer_value = 0;

                if (!empty($offer_per_gram)) {
                    $net_value2 = $net_weight * ($metal_value - $offer_per_gram->discount); // Multiply weight of collection with daily rate of metal
                    $offer_value = ($net_value2 + $beeds_value + $making_amount + $diamond_value);
                    $collection->gst_amount = $offer_value * $gst_tax;
                    $offer_value += $collection->gst_amount;
                    $collection->offer_value = money_format('%!i', $offer_value);
                }

                $gross_value = ($net_value1 + $beeds_value + $making_amount + $diamond_value);
            }

            $gross_gst_amount = $gross_value * $gst_tax;
            $gross_value += $gross_gst_amount;
                
            if (!isset($collection->gst_amount)) {
                $collection->gst_amount = $gross_gst_amount;
            }

            if (!empty($price_range)) {
                if (!is_numeric($price_range[0]) || !is_numeric($price_range[1])) {
                    if (is_numeric($price_range[0])) {
                        if (($offer_value && $offer_value < $price_range[0]) || ($gross_value < $price_range[0])) {
                            $collections->forget($key);
                            continue;
                        }
                    } else {
                        if (($offer_value && $offer_value > $price_range[1]) || ($gross_value > $price_range[1])) {
                            $collections->forget($key);
                            continue;
                        }
                    }
                } else {
                    if (($offer_value && !($price_range[0] < $offer_value && $offer_value < $price_range[1])) || !($price_range[0] < $gross_value && $gross_value < $price_range[1])) {
                        $collections->forget($key);
                        continue;
                    }
                }
            }
            

            $collection->gross_value = money_format('%!i', $gross_value);
            $collection->gst_amount = money_format('%!i', $collection->gst_amount);
            $collection->gst = config('cart.tax');
            
            $collection->details->weight = (float)$collection->details->weight ? $collection->details->weight : 'NA';
            $collection->details->beeds = (float)$collection->details->beeds ? $collection->details->beeds : 'NA';
            $collection->details->cents = (float)$collection->details->cents ? $collection->details->cents : 'NA';
            $collection->details->purity = (int)$collection->details->purity ? $collection->details->purity : 'NA';
        }

        // return $collections;
        return $this->paginate($collections)->setPath($request->path());
    }

    public function details(Request $request)
    {
        // return view('Frontend.pages.prod_details');
        $data = $request->query();

        if ($data['product_id']) {
            $prod_det = ProductMaster::with(['main_category', 'sub_category', 'details', 'image'])->find($data['product_id']);

            $prod_det->gross_value = 0;
            $gross_value = 0;
            $gst_tax = config('cart.tax') ? config('cart.tax')/100 : 0;
            setlocale(LC_MONETARY, 'en_IN');

            if (($prod_det->main_category->m_category_id == config('abharan.main_category.diamond') || $prod_det->main_category->m_category_id == config('abharan.main_category.silver')) && !(float)$prod_det->details->weight && $prod_det->details->cost) {
                $gross_value = $prod_det->details->cost;
            } else {
                $gross_weight = $prod_det->details->weight;
                $beeds_weight = $prod_det->details->beeds;
                $beeds_value = $prod_det->details->beeds_value;

                if ($prod_det->main_category->m_category_id != config('abharan.main_category.silver')) {
                    $purity = $prod_det->details->purity;
                } else {
                    $purity = 0;
                }

                $metal_value  = ProductRates::where('purity', $purity)->first()['amount'];

                $diamond_value   = $prod_det->details->daimond;

                // Calculate Gross Cost of Collection
                $net_weight = $gross_weight - $beeds_weight;
                $offer_per_gram = ProductOffer::where('status', 1)->first();
                $net_value1 = $net_weight * $metal_value;

                // Calculate Making amount
                if ($prod_det->main_category->m_category_id == config('abharan.main_category.diamond')) {
                    if ((float)$prod_det->details->making_amount) {
                        $making_amount = (float)$prod_det->details->making_amount;
                    } else {
                        $making_per = (float)$prod_det->details->wastage ? (float)$prod_det->details->wastage/100 : 0;
                        $making_amount = $net_value1 * $making_per;
                    }
                } elseif ($prod_det->main_category->m_category_id == config('abharan.main_category.silver')) {
                    if ((float)$prod_det->details->making_amount) {
                        $making_amount = (float)$prod_det->details->making_amount;
                    } elseif ((float)$prod_det->details->mak_amt_percentage) {
                        $making_per = (float)$prod_det->details->mak_amt_percentage/100;
                        $making_amount = $net_value1 * $making_per;
                    } else {
                        $making_amount = (float)$prod_det->details->mak_amt_per_gram ? ($net_weight * (float)$prod_det->details->mak_amt_per_gram) : 0;
                    }
                } else {
                    $making_per   = (float)$prod_det->details->making_amount ? (float)$prod_det->details->making_amount/100 : 0;
                    $making_amount = $net_value1 * $making_per;
                }

                if (!empty($offer_per_gram)) {
                    $net_value2 = $net_weight * ($metal_value - $offer_per_gram->discount); // Multiply weight of collection with daily rate of metal
                    $offer_value = ($net_value2 + $beeds_value + $making_amount + $diamond_value);
                    $prod_det->gst_amount = $offer_value * $gst_tax;
                    $offer_value += $prod_det->gst_amount;
                    $prod_det->offer_value = money_format('%!i', $offer_value);
                }

                $gross_value = ($net_value1 + $beeds_value + $making_amount + $diamond_value);
            }

            $gross_gst_amount = $gross_value * $gst_tax;
            $gross_value += $gross_gst_amount;
                
            if (!isset($prod_det->gst_amount)) {
                $prod_det->gst_amount = $gross_gst_amount;
            }
            $gross_value = round($gross_value, 2);
            $prod_det->gst_amount = round($prod_det->gst_amount, 2);

            $prod_det->gross_value = money_format('%!i', $gross_value);
            $prod_det->gst_amount = money_format('%!i', $prod_det->gst_amount);
            
            $prod_det->details->weight = (float)$prod_det->details->weight ? $prod_det->details->weight : 'NA';
            $prod_det->details->beeds = (float)$prod_det->details->beeds ? $prod_det->details->beeds : 'NA';
            $prod_det->details->cents = (float)$prod_det->details->cents ? $prod_det->details->cents : 'NA';
            $prod_det->details->purity = (int)$prod_det->details->purity ? $prod_det->details->purity : 'NA';
            
            return view('Frontend.pages.prod_details', ['prod_det' => $prod_det]);
        }
    }

    public function rates()
    {
        $rates = ProductRates::get();

        foreach ($rates as $rate) {
            $rate->amount = round($rate->amount, 2);
        }

        return $rates;
    }

    public function view_cart($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.pages.cart');
        } elseif ($get == 'data') {
            // echo '<pre>';
            // print_r([
            //     'cartItems' => Cart::content(),
            //     'cartCount' => Cart::content()->count(),
            //     'cartTax' => Cart::tax(),
            //     'cartSubtotal' => Cart::subtotal(),
            //     'cartTotal' => Cart::total()
            // ]);
            // die;
            return [
                'cartItems' => Cart::content(),
                'cartCount' => Cart::content()->count(),
                'cartTax' => Cart::tax(),
                'cartSubtotal' => Cart::subtotal(),
                'cartTotal' => Cart::total()
            ];
        }
    }

    public function add_cart(Request $request)
    {
        if ($request->method() == 'POST') {
            $data = $request->all();

            if ($data['task'] == 'add') {
                $product_id = $data['product_id'];
                $prod_det = ProductMaster::with(['main_category', 'details'])->find($product_id);

                $prod_det->gross_value = 0;
                $gross_value = 0;

                if (($prod_det->main_category->m_category_id == config('abharan.main_category.diamond') || $prod_det->main_category->m_category_id == config('abharan.main_category.silver')) && !(float)$prod_det->details->weight && $prod_det->details->cost) {
                    $gross_value = $prod_det->details->cost;
                } else {
                    $gross_weight = $prod_det->details->weight;
                    $beeds_weight = $prod_det->details->beeds;
                    $beeds_value = $prod_det->details->beeds_value;

                    if ($prod_det->main_category->m_category_id != config('abharan.main_category.silver')) {
                        $purity = $prod_det->details->purity;
                    } else {
                        $purity = 0;
                    }

                    $metal_value  = ProductRates::where('purity', $purity)->first()['amount'];

                    $diamond_value   = $prod_det->details->daimond;

                    // Calculate Gross Cost of Collection
                    $net_weight = $gross_weight - $beeds_weight;
                    $offer_per_gram = ProductOffer::where('status', 1)->first();
                    $net_value1 = $net_weight * $metal_value;
                    
                    // Calculate Making amount
                    if ($prod_det->main_category->m_category_id == config('abharan.main_category.diamond')) {
                        if ((float)$prod_det->details->making_amount) {
                            $making_amount = (float)$prod_det->details->making_amount;
                        } else {
                            $making_per = (float)$prod_det->details->wastage ? (float)$prod_det->details->wastage/100 : 0;
                            $making_amount = $net_value1 * $making_per;
                        }
                    } elseif ($prod_det->main_category->m_category_id == config('abharan.main_category.silver')) {
                        if ((float)$prod_det->details->making_amount) {
                            $making_amount = (float)$prod_det->details->making_amount;
                        } elseif ((float)$prod_det->details->mak_amt_percentage) {
                            $making_per = (float)$prod_det->details->mak_amt_percentage/100;
                            $making_amount = $net_value1 * $making_per;
                        } else {
                            $making_amount = (float)$prod_det->details->mak_amt_per_gram ? ($net_weight * (float)$prod_det->details->mak_amt_per_gram) : 0;
                        }
                    } else {
                        $making_per   = (float)$prod_det->details->making_amount ? (float)$prod_det->details->making_amount/100 : 0;
                        $making_amount = $net_value1 * $making_per;
                    }

                    if (!empty($offer_per_gram)) {
                        $net_value2 = $net_weight * ($metal_value - $offer_per_gram->discount); // Multiply weight of collection with daily rate of metal
                        $offer_value = ($net_value2 + $beeds_value + $making_amount + $diamond_value);
                        $prod_det->offer_value = $offer_value;
                    }

                    $gross_value = ($net_value1 + $beeds_value + $making_amount + $diamond_value);
                }

                $prod_det->gross_value = $gross_value;

                $prod_det->offer_value = round($prod_det->offer_value, 2);
                $prod_det->gross_value = round($prod_det->gross_value, 2);

                    
                $cart_det = [
                    'id' => $prod_det->product_id,
                    'name' => $prod_det->name,
                    'qty' => 1,
                    'price' => (isset($prod_det->offer_value) && $prod_det->offer_value) ? $prod_det->offer_value : $prod_det->gross_value,
                    'options' => [
                        'th_image' => ($prod_det->image) ? ($prod_det->image->global_name.'_TH.'.$prod_det->image->image_ext) : 'NO_IMAGE_TH.png',
                        'gross_value' => $prod_det->gross_value,
                        'cart_gst' => config('cart.tax')
                    ]
                ];

                //die(json_encode($cart_det));
                Cart::add($cart_det);
            }

            if ($data['task'] == 'up_qty') {
                $qty_updates = $data['qty_updates'];

                foreach ($qty_updates as $product_id => $qty) {
                    $item = Cart::search(function ($cartItem, $rowId) use ($product_id) {
                        return $cartItem->id == $product_id;
                    })->first();

                    Cart::update($item->rowId, $qty);
                }

                return response()->json(array(
                    'success' => true,
                    'cartTotal' => Cart::total(),
                    'message' => 'Success'
                ));
            }
        }

        return redirect('/product/cart');
    }

    public function delete_cart(Request $request)
    {
        $data = $request->query();
        $product_id = $data['product_id'];

        if ($product_id) {
            $item = Cart::search(function ($cartItem, $rowId) use ($product_id) {
                return $cartItem->id == $product_id;
            })->first();

            Cart::remove($item->rowId);
        } elseif ($product_id == 'all') {
            Cart::destroy();
        }

        return response()->json(array(
            'success' => true,
            'cartTotal' => Cart::total(),
            'message' => 'Success'
        ));
    }

    public function checkout(Request $request)
    {
        setlocale(LC_MONETARY, 'en_IN');

        $cart_tot = preg_replace('/[^\d\.]+/', '', Cart::total());
        // echo '<pre>';
        // print_r(User::with('details')->find(Auth::id()));
        // die;
        return view('Frontend.pages.checkout', [
                'user_details' =>  User::with('details')->find(Auth::id()),
                'cart_total' =>  money_format('%!i', $cart_tot)
            ]
        );
    }

    public function oops()
    {
        return view('Frontend.oops');
    }
    
    public function checkout_order(Request $request)
    {
        $inputs    = $request->all();
        $rules     = array(
            'name'    => 'required',
            'mobile'   => 'required',
            'pincode' => 'required',
            'address' =>'required',
            'city'    =>'required',
            'state'   =>'required',
        );

        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        // Temporary
        // if ($data['order_mode'] == '2') {
        //     return redirect('/oops');
        // }

        $user_det = UserDetails::firstOrNew(['user_id' => (int)Auth::id()]);

        if ($data['alternate_mobile']) {
            $user_det->alternate_mobile = $data['alternate_mobile'];
        }

        if ($data['locality']) {
            $user_det->locality = $data['locality'];
        }
        if ($data['landmark']) {
            $user_det->landmark = $data['landmark'];
        }

        if ($data['address']) {
            $user_det->address = $data['address'];
        }

        if ($data['alternate_mobile']) {
            $user_det->alternate_mobile = $data['alternate_mobile'];
        }

        if ($data['city']) {
            $user_det->city = $data['city'];
        }

        if ($data['pincode']) {
            $user_det->pincode = $data['pincode'];
        }

        if ($data['state']) {
            $user_det->state = $data['state'];
        }

        $user_det->save();

        $order = ProductOrders::firstOrNew(['p_order_id' => $data['order_id']]);

        if ($order->exists) {
            $order->mode = $data['order_mode'];
            $order->recpnt_name = $data['name'];
            $order->mobile = $data['mobile'];
            $order->alternate_mobile = $data['alternate_mobile'];
            $order->locality = $data['locality'];
            $order->landmark = $data['landmark'];
            $order->address = $data['address'];
            $order->city = $data['city'];
            $order->state = $data['state'];
            $order->pincode = $data['pincode'];

            $order->save();
        } else {
            $order->order_no = 'ORD'.date("YmdHis");
            $order->user_id = (int)Auth::id();
            $order->mode = $data['order_mode'];
            $order->recpnt_name = $data['name'];
            $order->mobile = $data['mobile'];
            $order->alternate_mobile = $data['alternate_mobile'];
            $order->locality = $data['locality'];
            $order->landmark = $data['landmark'];
            $order->address = $data['address'];
            $order->city = $data['city'];
            $order->state = $data['state'];
            $order->pincode = $data['pincode'];
            
            $order->save();
            
            foreach (Cart::content() as $item) {
                $order_det = new ProductOrderDetails();

                $order_det->order_id = $order->p_order_id;
                $order_det->product_id = $item->id;
                $order_det->qty = $item->qty;
                $order_det->price = $item->price;
                $order_det->gst_rate = $item->options->cart_gst ? $item->options->cart_gst/100 : 0;

                $order_det->save();
            }
        }

        if ($order->mode == '2') {
            $params = [];
            $params['order_id'] = $order->p_order_id;
            $params['amount'] = preg_replace('/[^\d\.]+/', '', Cart::total());
            $params['firstname'] = $order->recpnt_name;
            $params['email'] = Auth::user()->email;
            $params['phone'] = $order->mobile;
            $params['productinfo'] = 'Online Purchase';
            
            return Mastercard::redirectToVpc($params);
            // return PayU::redirectToPay($params);
        } else {
            // $send=true;
            $send = $this->placeOrder($order->p_order_id);
            
            if ($send) {
                Cart::destroy();
                return [
                    'success' => true,
                    'redirect_url' => '/product/checkout/success',
                    'order_no'       => $order->order_no,
                ];
                // return Redirect::to('/product/checkout/success?order_no='.$order->order_no);
            } else {
                return [
                    'success' => true,
                    'redirect_url' => '/product/checkout/failed',
                ];
                // return Redirect::to('/product/checkout/failed');
            }
        }
    }	

    public function checkout_resp(Request $request, $response)
    {
        $data = $request->query();

        if ($response == 'success') {
            return view('Frontend.pages.checkout_res', [
                    'order_no' => $data['order_no'],
                    'checkout_resp' => 'Success'
                ]
            );
        } elseif ($response == 'failed') {
            return view('Frontend.pages.checkout_res', [
                    'checkout_resp' => 'Failed'
                ]
            );
        }
    }

    public function responseMastercard(Request $request)
    {
        $data = $request->query();

        $response = Mastercard::handleResponse($data);

        if ($response) {
            $order = ProductOrders::with('details')->find($response['order_id']);
            
            if ($response['status'] == '1') {
                $send = $this->placeOrder($order->p_order_id);
                Cart::destroy();
                return view('Frontend.pages.payment_resp', [
                        'payment_resp' => '1',
                        'order_no' => $order->order_no,
                        'txn_ref_id' => $response['txn_ref_id'],
                    ]
                );
            } else {
                return view('Frontend.pages.payment_resp', [
                        'payment_resp' => '0',
                        'order_no' => $order->order_no,
                        'txn_ref_id' => $response['txn_ref_id'],
                    ]
                );
            }
        } else {
            return view('Frontend.pages.payment_resp', [
                    'payment_resp' => '0'
                ]
            );
        }
    }

    public function placeOrder($order_id)
    {
        $modes = ['1' => 'COD', '2' => 'Online Payment'];
        $order = ProductOrders::with('details')->find($order_id);
        
	$order->status = 1;
        $order->save();
        
        $user = User::with('details')->find($order->user_id);

        $mobile_no = $user->mobile ? : $user->details->alternate_mobile;

        $message = 'Hi '.$user->name
            .', Your order has been successfully placed with order no. '.$order->order_no
            .'. Thank you,'
            .' Team Abharan';

        $sms = new Sms();
        $sms->send($message, $mobile_no);

        $to = "info@abharan.com";

        $bcc = [];
        $bcc[] = 'abharanworks@gmail.com';
        //$bcc[] = 'chipsyinfo@gmail.com';

        // $to = (env('APP_ENV') == 'local') ?
        //     "chipsyinfo@gmail.com" : "info@abharan.com";
        $from_email  = $user->email;
        $sender_name = $user->name ;

        $message_subject = 'New Order ('.$order->order_no.') Requested';

        $order_no = $order->order_no;
        $order_mode = $modes[$order->mode];
        $order_date = date('F d, Y', strtotime($order->created_at));

        $table_items = '';
        $i = 1;
        $cart_gst = 0;
        $sub_total = 0;
        $calc_tax = 0;

        foreach ($order->details as $item) {
            $itm_tot = round(($item->price * $item->qty), 2);

            $product = ProductMaster::find($item->product_id);
            $table_items .= '<tr>
                    <td>'.$i.'</td>
                    <td>'.$product->SKU.'</td>
                    <td>'.$product->name.'</td>
                    <td align="right">'.$this->inrCurrency($item->price).'</td>
                    <td align="right">'.$item->qty.'</td>
                    <td align="right">'.$this->inrCurrency($itm_tot).'</td>
                </tr>';
            $i++;

            if ($item->gst_rate) {
                $cart_gst = $item->gst_rate * 100;
            }
            $sub_total += $itm_tot;
            $calc_tax += $item->gst_rate ? ($itm_tot * $item->gst_rate) : 0;
        }

        $sub_total = round($sub_total, 2);
        $calc_tax = round($calc_tax, 2);

        $cart_subtotal = $this->inrCurrency($sub_total);
        $cart_tax = $this->inrCurrency($calc_tax);
        
        $cart_total = $this->inrCurrency(($sub_total + $calc_tax));

        $ship_address = $order->recpnt_name.'<br>';
        $ship_address .= $order->address.'<br>';
        $ship_address .= $order->city.' - '.$order->pincode.'<br>';
        $ship_address .= $order->state.'<br>';
        $ship_address .= 'Phone: '.$user->mobile.'<br>';
        $ship_address .= 'Email: '.$user->email.'<br>';
        
        if ($order->landmark) {
            $ship_address .= 'Landmark: '.$order->landmark.'<br>';
        }

        if ($order->alternate_mobile) {
            $ship_address .= 'Alternate Phone: '.$order->alternate_mobile.'<br>';
        }

        $message_body = '<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td valign="top" align="left">
                        <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td height="83" colspan="2" valign="bottom" bgcolor="#f2e4cc">
                                        <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td width="100%" align="center" valign="middle" bgcolor="">
                                                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" align="center" valign="top" style="font-family:Verdana,Arial;line-height:19px;padding-top:13px"><img src="http://abharan.com/Frontend/assets/images/logo1.png" width="150px"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td colspan="2" height="25"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#fff">
                                            <tbody>
                                                <tr>
                                                    <td height="2" colspan="5" bgcolor="#f2e4cc"></td>
                                                </tr>
                                                <tr>
                                                    <td height="12" width="24"></td>
                                                    <td width="145"></td>
                                                    <td width="26"></td>
                                                    <td>&nbsp;</td>
                                                    <td width="24"></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="3"><font face="Verdana, Arial" color="#1a1a1a" style="font-size:12px"><b>Order# '.$order_no.'</b></font></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td style="background:none;border: solid 1px #f2e4cc;border-width:1px 0 0 0;height:1px;width: 100%;margin:0px 0px 0px 0px;padding-top:10px;padding-bottom:10px;" colspan="5">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="3">
                                                        <address>
                                                            <b>Shipped To:</b><br>'.$ship_address.'
                                                        </address>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="12"></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="3">
                                                        <address>
                                                            <b>Payment Status:</b><br>'.$order_mode.'
                                                        </address>
                                                    </td>
                                                    <td align="right">
                                                        <address>
                                                            <b>Order Date:</b><br>
                                                            '.$order_date.'<br><br>
                                                        </address>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="12"></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="12"></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="12"></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="3"><b>Order Summary</b></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="5">
                                                        <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#fff" style="
                                                            border-collapse: collapse;
                                                            ">
                                                            <thead>
                                                                <tr>
                                                                    <td><b>Sl no.</b></td>
                                                                    <td><b>SKU</b></td>
                                                                    <td><b>Name</b></td>
                                                                    <td align="right"><b>Price</b></td>
                                                                    <td align="right"><b>Quantity</b></td>
                                                                    <td align="right"><b>Total</b></td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>'.$table_items.'
                                                                ';
        if ($cart_gst) {
            $message_body .= '<tr>
                                                                    <td></td>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td align="right"><b>Subtotal</b></td>
                                                                    <td align="right"><b>'.$cart_subtotal.'</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td align="right"><b>GST('.$cart_gst.'%)</b></td>
                                                                    <td align="right"><b>'.$cart_tax.'</b></td>
                                                                </tr>';
        }
        $message_body .='<tr>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td ></td>
                                                                    <td align="right"><b>Total</b></td>
                                                                    <td align="right"><b>Rs. '.$cart_total.'/-</b></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="12"></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="25"></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="3"></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="27"></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td height="5" bgcolor="#f2e4cc"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="16" colspan="2"></td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                </tr>
                                <tr>
                                    <td height="15"></td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="-2" color="#9d9d9d">This is a computer-generated email, please do not reply to this message.</font></td>
                                </tr>
                                <tr>
                                    <td align="center" height="10"></td>
                                </tr>
                                <tr>
                                    <td height="16"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>';

        // file_put_contents('mail_body.txt', $message_body);
        $headers     = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'Reply-To: '. $from_email . "\r\n" ;
    	$headers .= 'X-Mailer: PHP/' . phpversion();
    	$headers .= "MIME-Version: 1.0\r\n";
        $headers .= 'To: <' . $to . '>' . "\r\n";
        $headers .= 'Bcc: '. implode(",", $bcc) . "\r\n";

        $headers .= 'From: ' . $sender_name . ' <' . $from_email . '>' . "\r\n";

        return mail($to, $message_subject, $message_body, $headers);
    }
    public function inrCurrency($num)
    {
        $num = preg_replace('/[^\d\.]+/', '', $num);
        $exp = '';
        if (strpos($num, '.') !== false) {
            $val_array = explode('.', $num);
            $num = $val_array[0];
            $exp = '.'.$val_array[1];
        }

        $explrestunits = "" ;

        if (strlen($num)>3) {
            $lastthree = substr($num, strlen($num)-3, strlen($num));
            $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
            $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
            $expunit = str_split($restunits, 2);
            for ($i=0; $i<sizeof($expunit); $i++) {
                // creates each of the 2's group and adds a comma to the end
                if ($i==0) {
                    $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
                } else {
                    $explrestunits .= $expunit[$i].",";
                }
            }
            return $explrestunits.$lastthree.$exp;
        } else {
            return $num.$exp;
        }
    }
}
