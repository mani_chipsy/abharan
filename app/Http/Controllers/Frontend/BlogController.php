<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\Blog;

class BlogController extends Controller
{
    public function index($get='view')
    {
        if ($get == 'view') {
            return view('Frontend.blog_list');            
        } else {
            $blogs = Blog::with('image')->orderBy('created_at','DESC')->paginate(10);

            foreach ($blogs as $blog) {
                $blog->created_str = $blog->created_at ? date('F d, Y', strtotime($blog->created_at)) : '-';
            }
            return $blogs;
        }
    }
}