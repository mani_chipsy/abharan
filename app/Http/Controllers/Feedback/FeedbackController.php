<?php
namespace App\Http\Controllers\Feedback;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cust_Feedback;
use App\FeedbackSuggestion;
use Auth;
use Hash;
use Validator;

class FeedbackController extends Controller
{
    public function index(Request $request)
    {
        $request->session()->put('popup', '1');
        return view('Feedback.index');
    }

    public function feedback(Request $request)
    {
        return view('Feedback.pages.feedback', ['popup' => $request->session()->get('popup')]);
    }

    public function post_feedback(Request $request)
    {
        $rules     = array(
            'mobile_no' => 'required|max:255','Staff' => 'required','Bill_number' => 'required',
            'name' => 'required',
            'quality_service' => 'required','exclusive' => 'required','service' => 'required',
            'convenience' => 'required','response' => 'required','quality' => 'required',
            'delivery' => 'required','showroom' => 'required','information' => 'required',
            'attending' => 'required','general1' => 'required','general2' => 'required',
            'mobile_bill' => 'required','comment' => 'required','mood' => 'required'

        );

        $validator = Validator::make($request->all(), $rules);

        $data = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }
        $Feedback=new Cust_Feedback;
    
    $Feedback->staff_code=$request->Staff;
      $Feedback->bill_number=$request->Bill_number;
    $Feedback->doc_no="MKT/F/01";
     $Feedback->rev_no="01";
        $Feedback->name=$request->name;
        $Feedback->landline=$request->land_line;
        $Feedback->mobile=$request->mobile_no;
        $Feedback->quality_of_service=$request->quality_service;
        $Feedback->exclusive_collection=$request->exclusive;
        $Feedback->service_sales_statff=$request->service;
        $Feedback->convenience_of_shopping=$request->convenience;
        $Feedback->response_to_enquiry=$request->response;
        $Feedback->quality_of_ornaments=$request->quality;
        $Feedback->delivery_time=$request->delivery;
        $Feedback->showroom_ambience=$request->showroom;
        $Feedback->info_on_prod_features=$request->information;
         $Feedback->attending_to_queries=$request->attending;
        $Feedback->place_id=$request->place;
        $Feedback->Ratings=$request->mood;
         $Feedback->save();



        $lastId = $Feedback->id;

          $FeedbackSuggestion=new FeedbackSuggestion;
           $FeedbackSuggestion->app_feedback_id=$lastId;
        $FeedbackSuggestion->is_abharan_first_choice=$request->general1;
        $FeedbackSuggestion->recommend_us_to_othrs=$request->general2;
        $FeedbackSuggestion->your_contact=$request->mobile_bill;
        $FeedbackSuggestion->more_suggestions=$request->comment;
        $FeedbackSuggestion->save();

       $request->session()->put('success', 'Thank you');
       // return Redirect::back()->with('success', '1');
        return response()->json(array(
            'success' => true,
                'message' => "success"
        ));
    }
}
