<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AbharanCollection extends Model
{
    protected $primaryKey = 'product_id';
    public $timestamps = false;

    public function main_category()
    {
        return $this->belongsTo('App\ProductCategory', 'm_category_id', 'm_category_id');
    }

    public function sub_category()
    {
        return $this->belongsTo('App\ProductSubCategory', 's_category_id', 's_category_id');
    }

    public function image()
    {
        return $this->hasOne('App\ImageMaster', 'global_name', 'sku');
    }
}
