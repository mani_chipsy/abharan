<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public function image()
    {
        return $this->hasOne('App\BlogImage', 'blog_id');
    }
}
