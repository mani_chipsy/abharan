<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentGatewayResponse extends Model
{
    protected $table = 'payment_gateway_response';

    protected $primaryKey = 'p_resp_id';

    protected $fillable = [
        'txn_ref_id',
        'response'
    ];
}
