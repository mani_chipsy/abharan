<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRates extends Model
{
    protected $table = '_current_rates';
    protected $primaryKey = 'p_rates_id';
    protected $fillable = ['purity', 'amount'];
}
