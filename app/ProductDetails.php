<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetails extends Model
{
    protected $table = 'product_details';

    protected $primaryKey = 'p_details_id';
    public $timestamps = false;

    protected $fillable = [
        'product_id',
        'category',
        'weight',
        'quote',
        'purity',
        'metal',
        'beeds',
        'beeds_value',
        'wastage',
        'making_amount',
        'cents',
        'daimond',
        'stone',
        'cost',
        'description',
        'production',
        'max',
        'certification',
        'cut',
        'colour',
        'clarity',
        'women',
        'men',
        'kids',
        'collection',
        'occasion'
    ];

    public function product()
    {
        return $this->belongsTo('App\ProductMaster');
    }
}
