<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    protected $table = 'user_details';

    protected $primaryKey = 'u_details_id';
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'alternate_mobile',
        'locality',
        'landmark',
        'pincode',
        'address',
        'city',
        'state'
    ];
}
