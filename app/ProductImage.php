<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_image';
    public $timestamps = false;
    protected $fillable = ['product_id', 'global_name'];
    
    //
}
