<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    protected $table = 'payment_transactions';

    protected $primaryKey = 'p_txn_id';

    protected $fillable = [
        'txn_ref_id',
        'order_id',
        'amount',
        'status'
    ];

    public function gateway_resp()
    {
        return $this->hasOne('App\PaymentGatewayResponse', 'txn_ref_id', 'txn_ref_id');
    }
}
