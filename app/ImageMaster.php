<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageMaster extends Model
{
    protected $table = 'image_master';

    protected $primaryKey = 'image_master_id';
    public $timestamps = false;
    
    protected $fillable = ['global_name', 'image_name', 'image_ext'];

    public function product()
    {
        return $this->belongsTo('App\ProductMaster');
    }
}
