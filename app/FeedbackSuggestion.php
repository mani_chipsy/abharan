<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedbackSuggestion extends Model
{
    public function feedback()
    {
        return $this->belongsTo('App\Cust_Feedback');
    }
}
