<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOffer extends Model
{
    protected $table = 'product_offers';
    protected $primaryKey = 'p_offer_id';
    protected $fillable = ['product_id', 'm_category_id', 's_category_id', 'discount'];
    

    public function product()
    {
        return $this->belongsTo('App\ProductMaster', 'product_id', 'product_id');
    }

    public function main_category()
    {
        return $this->belongsTo('App\ProductCategory', 'm_category_id', 'm_category_id');
    }

    public function sub_category()
    {
        return $this->belongsTo('App\ProductSubCategory', 's_category_id', 's_category_id');
    }
}
