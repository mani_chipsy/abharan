<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOrders extends Model
{
    protected $table = 'orders';

    protected $primaryKey = 'p_order_id';

    protected $fillable = [
        'order_no',
        'mode',
        'user_id',
        'alternate_mobile',
        'locality',
        'landmark',
        'pincode',
        'address',
        'city',
        'state'
    ];


    public function details()
    {
        return $this->hasMany('App\ProductOrderDetails', 'order_id', 'p_order_id');
    }

    public function transaction()
    {
        return $this->hasOne('App\PaymentTransaction', 'order_id');
    }
}
