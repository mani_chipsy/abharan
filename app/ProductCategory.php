<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = '_main_category';
    public $timestamps = false;
    public $primarykey = 'm_category_id';
    
    public function product()
    {
        return $this->hasMany('App\ProductMaster');
    }
}
