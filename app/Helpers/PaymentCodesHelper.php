<?php
namespace App\Helpers;

class PaymentCodesHelper
{
    /**
     * This function uses the QSI Response code retrieved from the Digital
     *
     * Receipt and returns an appropriate description for the QSI Response Code
     *
     * @param $responseCode String containing the QSI Response Code
     *
     * @return String containing the appropriate description
     */
    public static function getResultDescription($responseCode)
    {
        switch ($responseCode) {
        case "0":
            $result = "Transaction Successful";
            break;
        case "?":
            $result = "Transaction status is unknown";
            break;
        case "E":
            $result = "Referred";
            break;
        case "1":
            $result = "Transaction Declined";
            break;
        case "2":
            $result = "Bank Declined Transaction";
            break;
        case "3":
            $result = "No Reply from Bank";
            break;
        case "4":
            $result = "Expired Card";
            break;
        case "5":
            $result = "Insufficient funds";
            break;
        case "6":
            $result = "Error Communicating with Bank";
            break;
        case "7":
            $result = "Payment Server detected an error";
            break;
        case "8":
            $result = "Transaction Type Not Supported";
            break;
        case "9":
            $result = "Bank declined transaction (Do not contact Bank)";
            break;
        case "A":
            $result = "Transaction Aborted";
            break;
        case "B":
            $result = "Fraud Risk Blocked";
            break;
        case "C":
            $result = "Transaction Cancelled";
            break;
        case "D":
            $result = "Deferred transaction has been received and is awaiting processing";
            break;
        case "E":
            $result = "Transaction Declined - Refer to card issuer";
            break;
        case "F":
            $result = "3D Secure Authentication failed";
            break;
        case "I":
            $result = "Card Security Code verification failed";
            break;
        case "L":
            $result = "Shopping Transaction Locked (Please try the transaction again later)";
            break;
        case "M":
            $result = "Transaction Submitted (No response from acquirer)";
            break;
        case "N":
            $result = "Cardholder is not enrolled in Authentication scheme";
            break;
        case "P":
            $result = "Transaction has been received by the Payment Adaptor and is being processed";
            break;
        case "R":
            $result = "Transaction was not processed - Reached limit of retry attempts allowed";
            break;
        case "S":
            $result = "Duplicate SessionID (Amex Only)";
            break;
        case "T":
            $result = "Address Verification Failed";
            break;
        case "U":
            $result = "Card Security Code Failed";
            break;
        case "V":
            $result = "Address Verification and Card Security Code Failed";
            break;
        default:
            $result = "Unable to be determined";
        }
        return $result;
    }

    /**
     * This function uses the QSI AVS Result Code retrieved from the Digital
     * Receipt and returns an appropriate description for this code.
     * @param avsResultCode String containing the QSI AVS Result Code
     * @return description String containing the appropriate description
     */
    public function getAVSResultDescription($avsResultCode)
    {
        if ($avsResultCode != "") {
            switch ($avsResultCode) {
            case "Unsupported":
                $result = "AVS not supported or there was no AVS data provided";
                break;
            case "X":
                $result = "Exact match - address and 9 digit ZIP/postal code";
                break;
            case "Y":
                $result = "Exact match - address and 5 digit ZIP/postal code";
                break;
            case "W":
                $result = "9 digit ZIP/postal code matched, Address not Matched";
                break;
            case "S":
                $result = "Service not supported or address not verified (international transaction)";
                break;
            case "G":
                $result = "Issuer does not participate in AVS (international transaction)";
                break;
            case "C":
                $result = "Street Address and Postal Code not verified for International Transaction due to incompatible formats.";
                break;
            case "I":
                $result = "Visa Only. Address information not verified for international transaction.";
                break;
            case "A":
                $result = "Address match only";
                break;
            case "Z":
                $result = "5 digit ZIP/postal code matched, Address not Matched";
                break;
            case "R":
                $result = "Issuer system is unavailable";
                break;
            case "U":
                $result = "Address unavailable or not verified";
                break;
            case "E":
                $result = "Address and ZIP/postal code not provided";
                break;
            case "B":
                $result = "Street Address match for international transaction. Postal Code not verified due to incompatible formats.";
                break;
            case "N":
                $result = "Address and ZIP/postal code not matched";
                break;
            case "0":
                $result = "AVS not requested";
                break;
            case "D":
                $result = "Street Address and postal code match for international transaction.";
                break;
            case "M":
                $result = "Street Address and postal code match for international transaction.";
                break;
            case "P":
                $result = "Postal Codes match for international transaction but street address not verified due to incompatible formats.";
                break;
            case "K":
                $result = "Card holder name only matches.";
                break;
            case "F":
                $result = "Street address and postal code match. Applies to U.K. only.";
                break;
            default:
                $result = "Unable to be determined";
            }
        } else {
            $result = "null response";
        }
        return $result;
    }

    /**
     * This function uses the QSI CSC Result Code retrieved from the Digital
     * Receipt and returns an appropriate description for this code.
     * @param cscResultCode String containing the QSI CSC Result Code
     * @return description String containing the appropriate description
     */
    public function getCSCResultDescription($cscResultCode)
    {
        if ($cscResultCode != "") {
            switch ($cscResultCode) {
            case "Unsupported":
                $result = "CSC not supported or there was no CSC data provided";
                break;
            case "M":
                $result = "Exact code match";
                break;
            case "S":
                $result = "Merchant has indicated that CSC is not present on the card (MOTO situation)";
                break;
            case "P":
                $result = "Code not processed";
                break;
            case "U":
                $result = "Card issuer is not registered and/or certified";
                break;
            case "N":
                $result = "Code invalid or not matched";
                break;
            default:
                $result = "Unable to be determined";
                break;
            }
        } else {
            $result = "null response";
        }
        return $result;
    }
}
