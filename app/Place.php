<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    public $table = 'place';
    public $fillable = ['place_id','name'];
    
   
}