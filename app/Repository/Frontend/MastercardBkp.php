<?php
namespace App\Repository\Frontend;

use App\User;
use Validator;
use Illuminate\Http\Request;
use Redirect;
use App\PaymentTransaction;
use App\PaymentGatewayResponse;
use App\Helpers\PaymentCodesHelper;

use App\MastercardVPC\VPCPaymentConnection;

class MasterCard
{
    public static function redirectToVpc($data)
    {
        $payment_con = new VPCPaymentConnection();
        $mastercard = config('services.mastercard');

        // Set the Secure Hash Secret used by the VPC connection object
        $payment_con->setSecureSecret($mastercard['secure_secret']);

        $redirect_url = $mastercard['action_url'];
        $title = 'Abharan Jewellers';

        $payment_txn = new PaymentTransaction();
        $payment_txn->txn_ref_id = 'ABTXN'.date('YmdHis');
        $payment_txn->order_id = $data['order_id'];
        $payment_txn->amount = $data['amount'];
        $payment_txn->status = 0;

        $payment_txn->save();

        $params = $mastercard['params'];
        $params['vpc_Amount'] = $payment_txn->amount;
        $params['vpc_MerchTxnRef'] = $payment_txn->txn_ref_id;
        // echo '<pre>';
        // print_r($params);
        ksort($params);
        // echo '<pre>';
        // print_r($params);
        // die;
        // Add VPC post data to the Digital Order
        foreach ($params as $key => $value) {
            if (strlen($value) > 0) {
                $payment_con->addDigitalOrderField($key, $value);
            }
        }

        // Add original order HTML so that another transaction can be attempted.
        // $payment_con->addDigitalOrderField("AgainLink", $againLink);

        // Obtain a one-way hash of the Digital Order data and add this to the Digital Order
        $secureHash = $payment_con->hashAllFields();
        $payment_con->addDigitalOrderField("Title", $title);
        $payment_con->addDigitalOrderField("vpc_SecureHash", $secureHash);
        $payment_con->addDigitalOrderField("vpc_SecureHashType", "SHA256");

        // Obtain the redirection URL and redirect the web browser
        $redirect_url = $payment_con->getDigitalOrder($redirect_url);

        return Redirect::to($redirect_url);
    }

    public static function handleResponse($response)
    {
        if (!empty($response)) {
            if (($response['vpc_TxnResponseCode'] != "No Value Returned")) {
                $txnResponseCodeDesc = PaymentCodesHelper::getResultDescription($response['vpc_TxnResponseCode']);
            } else {
                $txnResponseCodeDesc = 'Unknown Error occured';
            }

            $txn_ref_id = $response['vpc_MerchTxnRef'];
            $payment_txn = PaymentTransaction::where('txn_ref_id', $txn_ref_id)->get();
            $payment_txn->status = 0;

            if ($txnResponseCodeDesc == 'Transaction Successful') {
                $payment_txn->status = 1; // Success
            } else {
                $payment_txn->status = 2; // Failed
            }

            $payment_txn->save();
            
            $payment_resp = new PaymentGatewayResponse();
            $payment_resp->txn_ref_id = $txn_ref_id;

            $response = json_encode($response);
            $payment_resp->response = $response;
            $payment_resp->save();

            return [
                'status' => $payment_txn->status,
                'order_id' => $payment_txn->order_id,
                'txn_ref_id' => $txn_ref_id,
            ];
        } else {
            return false;
        }
    }
}
