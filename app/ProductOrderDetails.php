<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOrderDetails extends Model
{
    protected $table = 'order_details';

    protected $primaryKey = 'o_details_id';
    public $timestamps = false;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    protected $fillable = [
        'order_id',
        'product_id',
        'qty',
        'price',
    ];

    public function order()
    {
        return $this->belongsTo('App\ProductOrders');
    }
}
