<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VpcError extends Model
{
    protected $fillable = [
        'vpc_TxnResponseCode',
        'description'
    ];
}
