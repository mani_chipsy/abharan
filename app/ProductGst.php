<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGst extends Model
{
    protected $table = '_current_tax';

    protected $primaryKey = 'p_tax_id';
    public $timestamps = false;
}
