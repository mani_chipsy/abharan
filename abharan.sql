-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 29, 2017 at 04:59 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abharan`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_feedbacks`
--

CREATE TABLE `app_feedbacks` (
  `id` int(11) UNSIGNED NOT NULL,
  `staff_code` varchar(50) DEFAULT NULL,
  `doc_no` varchar(50) DEFAULT NULL,
  `rev_no` varchar(50) DEFAULT NULL,
  `date` varchar(50) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `landline` varchar(30) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `quality_of_service` tinyint(1) UNSIGNED NOT NULL,
  `exclusive_collection` tinyint(1) UNSIGNED NOT NULL,
  `service_sales_statff` tinyint(1) UNSIGNED NOT NULL,
  `convenience_of_shopping` tinyint(1) UNSIGNED NOT NULL,
  `response_to_enquiry` tinyint(1) UNSIGNED NOT NULL,
  `quality_of_ornaments` tinyint(1) UNSIGNED NOT NULL,
  `delivery_time` tinyint(1) UNSIGNED NOT NULL,
  `showroom_ambience` tinyint(1) UNSIGNED NOT NULL,
  `info_on_prod_features` tinyint(1) UNSIGNED NOT NULL,
  `attending_to_queries` tinyint(1) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `app_feedbacks`
--

INSERT INTO `app_feedbacks` (`id`, `staff_code`, `doc_no`, `rev_no`, `date`, `name`, `landline`, `mobile`, `quality_of_service`, `exclusive_collection`, `service_sales_statff`, `convenience_of_shopping`, `response_to_enquiry`, `quality_of_ornaments`, `delivery_time`, `showroom_ambience`, `info_on_prod_features`, `attending_to_queries`, `created_at`, `updated_at`) VALUES
(1, 'STAFF-0001', 'MKT/F/01', '01', '01-10-2008', 'Deepthi Y', NULL, '8904600915', 3, 4, 1, 1, 2, 1, 4, 1, 2, 2, '2008-09-30 22:37:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `session` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `total_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `item_count` int(11) NOT NULL DEFAULT '0',
  `placed_at` timestamp NULL DEFAULT NULL,
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `session`, `name`, `status`, `total_price`, `item_count`, `placed_at`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, NULL, 'TQJwd7zOAyMhDpmZARFFcD9aJOWTojjwROG4J8qG', 'default', 'active', '0.00', 0, NULL, NULL, '2017-09-23 00:19:20', '2017-09-23 00:19:20');

-- --------------------------------------------------------

--
-- Table structure for table `cart_lines`
--

CREATE TABLE `cart_lines` (
  `id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `unit_price` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_feedbacks`
--

CREATE TABLE `customer_feedbacks` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `message` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_feedbacks`
--

INSERT INTO `customer_feedbacks` (`id`, `name`, `email`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Praveen', 'praslbaikady@gmail.com', 'dsfhdsfjsdjkfjksd', '2017-08-18 08:00:49', '2017-08-18 08:00:49'),
(2, 'Praveen', 'praslbaikady@gmail.com', 'dsfhdsfjsdjkfjksdfdsfd', '2017-08-18 08:00:50', '2017-08-18 08:00:50'),
(3, 'Praveen', 'praslbaikady@gmail.com', 'dsfhdsfjsdjkfjksdfdsfds', '2017-08-18 08:00:50', '2017-08-18 08:00:50'),
(4, 'Praveen', 'praslbaikady@gmail.com', 'dsfhdsfjsdjkfjksdfdsfds', '2017-08-18 08:00:51', '2017-08-18 08:00:51'),
(5, 'Praveen', 'praslbaikady@gmail.com', 'dsfhdsfjsdjkfjksdfdsfds', '2017-08-18 08:00:51', '2017-08-18 08:00:51'),
(6, 'Praveen', 'praslbaikady@gmail.com', 'dsfhdsfjsdjkfjksdfdsfds', '2017-08-18 08:00:52', '2017-08-18 08:00:52'),
(7, 'Praveen', 'praslbaikady@gmail.com', 'dsfhdsfjsdjkfjksdfdsfds', '2017-08-18 08:00:52', '2017-08-18 08:00:52'),
(8, 'Praveen', 'praslbaikady@gmail.com', 'dsfhdsfjsdjkfjksdfdsfds', '2017-08-18 08:00:52', '2017-08-18 08:00:52'),
(9, 'Praveen', 'praslbaikady@gmail.com', 'dsfhdsfjsdjkfjksdfdsfds', '2017-08-18 08:00:52', '2017-08-18 08:00:52'),
(10, 'Praveen', 'praslbaikady@gmail.com', 'dhfdjkshfjkdshfjkdhfjkhdkjfkdjfkdskfs', '2017-08-18 08:02:18', '2017-08-18 08:02:18'),
(11, 'rsdfsdf', 'sdff@gmsail.co', 'fgkfgl;;fdkgfd\'ga', '2017-08-18 08:04:00', '2017-08-18 08:04:00'),
(12, 'sdfhujh', 'tsh@gmail.com', 'jfsdjfkldjfsd', '2017-08-18 08:04:40', '2017-08-18 08:04:40');

-- --------------------------------------------------------

--
-- Table structure for table `feedback_suggestions`
--

CREATE TABLE `feedback_suggestions` (
  `id` int(11) UNSIGNED NOT NULL,
  `app_feedback_id` int(11) UNSIGNED NOT NULL,
  `is_abharan_first_choice` tinyint(1) UNSIGNED NOT NULL,
  `recommend_us_to_othrs` tinyint(1) UNSIGNED NOT NULL,
  `your_contact` tinyint(1) UNSIGNED NOT NULL,
  `more_suggestions` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feedback_suggestions`
--

INSERT INTO `feedback_suggestions` (`id`, `app_feedback_id`, `is_abharan_first_choice`, `recommend_us_to_othrs`, `your_contact`, `more_suggestions`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, 1, 'It takes more time in billing and in second floor their no proper service', '2008-09-30 22:37:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `image_master`
--

CREATE TABLE `image_master` (
  `image_master_id` int(10) NOT NULL,
  `global_name` varchar(20) NOT NULL,
  `image_name` varchar(50) NOT NULL,
  `image_ext` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image_master`
--

INSERT INTO `image_master` (`image_master_id`, `global_name`, `image_name`, `image_ext`) VALUES
(1, 'U43610', 'U43610_IN', 'JPG'),
(2, 'U43720', 'U43720_IN', 'JPG'),
(3, 'U43686', 'U43686_IN', 'JPG'),
(4, 'U43683', 'U43683_IN', 'JPG'),
(5, 'U43666', 'U43666_IN', 'JPG'),
(6, 'U43545', 'U43545_IN', 'JPG'),
(7, 'U43676', 'U43676_IN', 'JPG'),
(8, 'U43471', 'U43471_IN', 'JPG'),
(9, 'U43655', 'U43655_IN', 'JPG'),
(10, 'U43474', 'U43474_IN', 'JPG'),
(11, 'U40380', 'U40380_IN', 'JPG'),
(12, 'U40377', 'U40377_IN', 'JPG'),
(13, 'U40356', 'U40356_IN', 'JPG'),
(14, 'U40279', 'U40279_IN', 'JPG'),
(15, 'U40131', 'U40131_IN', 'JPG'),
(16, 'U39992', 'U39992_IN', 'JPG'),
(17, 'U39925', 'U39925_IN', 'JPG'),
(18, 'U39807', 'U39807_IN', 'JPG'),
(19, 'U44736', 'U44736_IN', 'JPG'),
(20, 'U45298', 'U45298_IN', 'JPG'),
(21, 'U44714', 'U44714_IN', 'JPG'),
(22, 'U44704', 'U44704_IN', 'JPG'),
(23, 'U45330', 'U45330_IN', 'JPG'),
(24, 'U45329', 'U45329_IN', 'JPG'),
(25, 'U45331', 'U45331_IN', 'JPG'),
(26, 'U45327', 'U45327_IN', 'JPG'),
(27, 'U46095', 'U46095_IN', 'JPG'),
(28, 'U46093', 'U46093_IN', 'JPG'),
(29, 'U43629', 'U43629_IN', 'JPG'),
(30, 'U46088', 'U46088_IN', 'JPG'),
(31, 'U43558', 'U43558_IN', 'JPG'),
(32, 'U46136', 'U46136_IN', 'JPG'),
(33, 'U40254', 'U40254_IN', 'JPG'),
(34, 'U43448', 'U43448_IN', 'JPG'),
(35, 'U46237', 'U46237_IN', 'JPG'),
(36, 'U39842', 'U39842_IN', 'JPG'),
(37, 'U40237', 'U40237_IN', 'JPG'),
(38, 'U46235', 'U46235_IN', 'JPG'),
(39, 'U46228', 'U46228_IN', 'JPG'),
(40, 'U39827', 'U39827_IN', 'JPG'),
(41, 'U46240', 'U46240_IN', 'JPG'),
(42, 'U46219', 'U46219_IN', 'JPG'),
(43, 'U45326', 'U45326_IN', 'JPG'),
(44, 'U46213', 'U46213_IN', 'JPG'),
(45, 'U43687', 'U43687_IN', 'JPG'),
(46, 'U45336', 'U45336_IN', 'JPG'),
(47, 'U45318', 'U45318_IN', 'JPG'),
(48, 'U43670', 'U43670_IN', 'JPG'),
(49, 'U46243', 'U46243_IN', 'JPG'),
(50, 'U43644', 'U43644_IN', 'JPG'),
(51, 'U43509', 'U43509_IN', 'JPG'),
(52, 'U45325', 'U45325_IN', 'JPG'),
(53, 'U43456', 'U43456_IN', 'JPG'),
(54, 'U43625', 'U43625_IN', 'JPG'),
(55, 'U43641', 'U43641_IN', 'JPG'),
(56, 'U41441', 'U41441_IN', 'JPG'),
(57, 'U41431', 'U41431_IN', 'JPG'),
(58, 'U40022', 'U40022_IN', 'JPG'),
(59, 'U38914', 'U38914_IN', 'JPG'),
(60, 'U43635', 'U43635_IN', 'JPG'),
(61, 'U36645', 'U36645_IN', 'JPG'),
(62, 'U43560', 'U43560_IN', 'JPG'),
(63, 'U36680', 'U36680_IN', 'JPG'),
(64, 'U39805', 'U39805_IN', 'JPG'),
(65, 'U39948', 'U39948_IN', 'JPG'),
(66, 'U43594', 'U43594_IN', 'JPG'),
(67, 'U39828', 'U39828_IN', 'JPG'),
(68, 'U43459', 'U43459_IN', 'JPG'),
(69, 'U43587', 'U43587_IN', 'JPG'),
(70, 'U44754', 'U44754_IN', 'JPG'),
(71, 'U45324', 'U45324_IN', 'JPG'),
(72, 'U46249', 'U46249_IN', 'JPG'),
(73, 'U43580', 'U43580_IN', 'JPG'),
(74, 'U43581', 'U43581_IN', 'JPG'),
(75, 'U46248', 'U46248_IN', 'JPG'),
(76, 'U46231', 'U46231_IN', 'JPG'),
(77, 'U46246', 'U46246_IN', 'JPG'),
(78, 'U39941', 'U39941_IN', 'JPG'),
(79, 'U46225', 'U46225_IN', 'JPG'),
(80, 'U46215', 'U46215_IN', 'JPG'),
(81, 'U46211', 'U46211_IN', 'JPG'),
(82, 'U43723', 'U43723_IN', 'JPG'),
(83, 'U47119', 'U47119_IN', 'JPG'),
(84, 'U39831', 'U39831_IN', 'JPG'),
(85, 'U43716', 'U43716_IN', 'JPG'),
(86, 'U47071', 'U47071_IN', 'JPG'),
(87, 'U43681', 'U43681_IN', 'JPG'),
(88, 'U47073', 'U47073_IN', 'JPG'),
(89, 'U43671', 'U43671_IN', 'JPG'),
(90, 'U47072', 'U47072_IN', 'JPG'),
(91, 'U47074', 'U47074_IN', 'JPG'),
(92, 'U47075', 'U47075_IN', 'JPG'),
(93, 'U47070', 'U47070_IN', 'JPG'),
(94, 'U43667', 'U43667_IN', 'JPG'),
(95, 'U44755', 'U44755_IN', 'JPG'),
(96, 'U43663', 'U43663_IN', 'JPG'),
(97, 'U47068', 'U47068_IN', 'JPG'),
(98, 'U43651', 'U43651_IN', 'JPG'),
(99, 'U45323', 'U45323_IN', 'JPG'),
(100, 'U47069', 'U47069_IN', 'JPG'),
(101, 'U47077', 'U47077_IN', 'JPG'),
(102, 'U47078', 'U47078_IN', 'JPG'),
(103, 'U43567', 'U43567_IN', 'JPG'),
(104, 'U47079', 'U47079_IN', 'JPG'),
(105, 'U47060', 'U47060_IN', 'JPG'),
(106, 'U47059', 'U47059_IN', 'JPG'),
(107, 'U40031', 'U40031_IN', 'JPG'),
(108, 'U43484', 'U43484_IN', 'JPG'),
(109, 'U47058', 'U47058_IN', 'JPG'),
(110, 'U43547', 'U43547_IN', 'JPG'),
(111, 'U47067', 'U47067_IN', 'JPG'),
(112, 'U47057', 'U47057_IN', 'JPG'),
(113, 'U47065', 'U47065_IN', 'JPG'),
(114, 'U47066', 'U47066_IN', 'JPG'),
(115, 'U47064', 'U47064_IN', 'JPG'),
(116, 'U47063', 'U47063_IN', 'JPG'),
(117, 'U43539', 'U43539_IN', 'JPG'),
(118, 'U47061', 'U47061_IN', 'JPG'),
(119, 'U47062', 'U47062_IN', 'JPG'),
(120, 'U43633', 'U43633_IN', 'JPG'),
(121, 'U43602', 'U43602_IN', 'JPG'),
(122, 'U43631', 'U43631_IN', 'JPG'),
(123, 'U42440', 'U42440_IN', 'JPG'),
(124, 'U47315', 'U47315_IN', 'JPG'),
(125, 'U42412', 'U42412_IN', 'JPG'),
(126, 'U43433', 'U43433_IN', 'JPG'),
(127, 'U45289', 'U45289_IN', 'JPG'),
(128, 'U43432', 'U43432_IN', 'JPG'),
(129, 'U40039', 'U40039_IN', 'JPG'),
(130, 'U47023', 'U47023_IN', 'JPG'),
(131, 'U39830', 'U39830_IN', 'JPG'),
(132, 'U46667', 'U46667_IN', 'JPG'),
(133, 'U45365', 'U45365_IN', 'JPG'),
(134, 'U40719', 'U40719_IN', 'JPG'),
(135, 'U47042', 'U47042_IN', 'JPG'),
(136, 'U47080', 'U47080_IN', 'JPG'),
(137, 'U45057', 'U45057_IN', 'JPG'),
(138, 'U45291', 'U45291_IN', 'JPG'),
(139, 'U44938', 'U44938_IN', 'JPG'),
(140, 'U44753', 'U44753_IN', 'JPG'),
(141, 'U44684', 'U44684_IN', 'JPG'),
(142, 'U44690', 'U44690_IN', 'JPG'),
(143, 'U42552', 'U42552_IN', 'JPG'),
(144, 'U41737', 'U41737_IN', 'JPG'),
(145, 'U43449', 'U43449_IN', 'JPG'),
(146, 'U45970', 'U45970_IN', 'JPG'),
(147, 'U47044', 'U47044_IN', 'JPG'),
(148, 'U45293', 'U45293_IN', 'JPG'),
(149, 'U44592', 'U44592_IN', 'JPG'),
(150, 'U43673', 'U43673_IN', 'JPG'),
(151, 'U43590', 'U43590_IN', 'JPG'),
(152, 'U47115', 'U47115_IN', 'JPG'),
(153, 'U43451', 'U43451_IN', 'JPG'),
(154, 'U43657', 'U43657_IN', 'JPG'),
(155, 'U46152', 'U46152_IN', 'JPG');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_07_10_110525_create_shoppingcart_table', 2),
(4, '2015_11_03_155308_create_cart_table', 3),
(5, '2015_11_03_155324_create_cart_lines_table', 3),
(6, '2017_09_09_075251_create_table_carts', 3),
(7, '2017_09_09_075316_create_table_cart_items', 3);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `p_order_id` int(10) NOT NULL,
  `order_no` varchar(50) NOT NULL,
  `mode` tinyint(1) NOT NULL COMMENT '1 - Via Mail, 2 - Online Payment',
  `user_id` int(10) NOT NULL,
  `recpnt_name` varchar(200) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `alternate_mobile` varchar(15) DEFAULT NULL,
  `locality` varchar(50) DEFAULT NULL,
  `landmark` varchar(100) DEFAULT NULL,
  `address` varchar(150) NOT NULL,
  `city` varchar(100) NOT NULL,
  `pincode` varchar(6) DEFAULT NULL,
  `state` varchar(100) NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`p_order_id`, `order_no`, `mode`, `user_id`, `recpnt_name`, `mobile`, `alternate_mobile`, `locality`, `landmark`, `address`, `city`, `pincode`, `state`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ORD20170911071437', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 1, '2017-09-11 01:44:37', '2017-09-11 01:51:48'),
(2, 'ORD20170911094252', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-09-11 04:12:52', '2017-09-11 04:12:52'),
(3, 'ORD20170920103901', 1, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 1, '2017-09-20 05:09:01', '2017-09-20 05:09:02'),
(4, 'ORD20170920103916', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-09-20 05:09:16', '2017-09-20 05:09:16'),
(5, 'ORD20170920104011', 1, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 1, '2017-09-20 05:10:11', '2017-09-20 05:10:11'),
(6, 'ORD20171125063222', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-25 01:02:22', '2017-11-25 01:02:22'),
(7, 'ORD20171125084015', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-25 03:10:15', '2017-11-25 03:10:15'),
(8, 'ORD20171125084116', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-25 03:11:16', '2017-11-25 03:11:16'),
(9, 'ORD20171125084341', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-25 03:13:41', '2017-11-25 03:13:41'),
(10, 'ORD20171125084551', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-25 03:15:51', '2017-11-25 03:15:51'),
(11, 'ORD20171125084659', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-25 03:16:59', '2017-11-25 03:16:59'),
(12, 'ORD20171127042508', 1, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 1, '2017-11-26 22:55:08', '2017-11-26 22:55:08'),
(13, 'ORD20171127042523', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 22:55:23', '2017-11-26 22:55:23'),
(14, 'ORD20171127044632', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:16:32', '2017-11-26 23:16:32'),
(15, 'ORD20171127044648', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:16:48', '2017-11-26 23:16:48'),
(16, 'ORD20171127044743', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:17:43', '2017-11-26 23:17:43'),
(17, 'ORD20171127045225', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:22:25', '2017-11-26 23:22:25'),
(18, 'ORD20171127045827', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:28:27', '2017-11-26 23:28:27'),
(19, 'ORD20171127050609', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:36:09', '2017-11-26 23:36:09'),
(20, 'ORD20171127050635', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:36:35', '2017-11-26 23:36:35'),
(21, 'ORD20171127050700', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:37:00', '2017-11-26 23:37:00'),
(22, 'ORD20171127051051', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:40:51', '2017-11-26 23:40:51'),
(23, 'ORD20171127051523', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:45:23', '2017-11-26 23:45:23'),
(24, 'ORD20171127051545', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:45:45', '2017-11-26 23:45:45'),
(25, 'ORD20171127051649', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:46:49', '2017-11-26 23:46:49'),
(26, 'ORD20171127052050', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-26 23:50:50', '2017-11-26 23:50:50'),
(27, 'ORD20171127052115', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 1, '2017-11-26 23:51:15', '2017-11-26 23:53:23'),
(28, 'ORD20171127055531', 1, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 1, '2017-11-27 00:25:31', '2017-11-27 00:25:32'),
(29, 'ORD20171127055720', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-27 00:27:20', '2017-11-27 00:27:20'),
(30, 'ORD20171127061857', 1, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 1, '2017-11-27 00:48:57', '2017-11-27 00:48:57'),
(31, 'ORD20171127061920', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-11-27 00:49:20', '2017-11-27 00:49:20'),
(32, 'ORD20171205063316', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-12-05 01:03:16', '2017-12-05 01:03:16'),
(33, 'ORD20171205063322', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-12-05 01:03:22', '2017-12-05 01:03:22'),
(34, 'ORD20171205063420', 2, 39, 'Praveen', '9066047177', '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka', 0, '2017-12-05 01:04:20', '2017-12-05 01:04:20');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `o_details_id` int(10) NOT NULL,
  `order_id` int(10) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` tinyint(1) NOT NULL,
  `price` float(10,2) NOT NULL,
  `gst_rate` float(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`o_details_id`, `order_id`, `product_id`, `qty`, `price`, `gst_rate`) VALUES
(1, 1, 170, 1, 1.00, 0.03),
(2, 2, 170, 1, 1.00, 0.03),
(3, 3, 170, 1, 1.00, 0.03),
(4, 4, 170, 1, 1.00, 0.03),
(5, 5, 118, 1, 141027.98, 0.03),
(6, 6, 214, 1, 1611015.38, 0.03),
(7, 7, 214, 1, 1611015.38, 0.03),
(8, 7, 261, 1, 66772.48, 0.03),
(9, 8, 214, 1, 1611015.38, 0.03),
(10, 8, 261, 1, 66772.48, 0.03),
(11, 9, 214, 1, 1611015.38, 0.03),
(12, 9, 261, 1, 66772.48, 0.03),
(13, 10, 214, 1, 1611015.38, 0.03),
(14, 10, 261, 1, 66772.48, 0.03),
(15, 11, 214, 1, 1611015.38, 0.03),
(16, 11, 261, 1, 66772.48, 0.03),
(17, 12, 259, 1, 36531.44, 0.03),
(18, 13, 259, 1, 36531.44, 0.03),
(19, 14, 259, 1, 36531.44, 0.03),
(20, 15, 259, 1, 36531.44, 0.03),
(21, 16, 259, 1, 36531.44, 0.03),
(22, 17, 259, 1, 36531.44, 0.03),
(23, 18, 259, 1, 36531.44, 0.03),
(24, 19, 259, 1, 36531.44, 0.03),
(25, 20, 259, 1, 36531.44, 0.03),
(26, 21, 259, 1, 36531.44, 0.03),
(27, 22, 259, 1, 36531.44, 0.03),
(28, 23, 259, 1, 36531.44, 0.03),
(29, 24, 259, 1, 36531.44, 0.03),
(30, 25, 259, 1, 36531.44, 0.03),
(31, 26, 259, 1, 36531.44, 0.03),
(32, 27, 259, 1, 36531.44, 0.03),
(33, 28, 170, 1, 1.00, 0.03),
(34, 29, 170, 1, 1.00, 0.03),
(35, 30, 170, 1, 1.00, 0.03),
(36, 31, 170, 1, 1.00, 0.03);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_gateway_response`
--

CREATE TABLE `payment_gateway_response` (
  `p_resp_id` int(10) NOT NULL,
  `txn_ref_id` varchar(25) NOT NULL,
  `response` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_gateway_response`
--

INSERT INTO `payment_gateway_response` (`p_resp_id`, `txn_ref_id`, `response`, `created_at`, `updated_at`) VALUES
(1, 'ABTXN20170911071437', '{"mihpayid":"6400599936","mode":"DC","status":"success","unmappedstatus":"captured","key":"yvRdJBMM","txnid":"ABTXN20170911071437","amount":"1.03","additionalCharges":"0.01","addedon":"2017-09-11 12:51:02","productinfo":"Online Purchase","firstname":"Praveen","lastname":null,"address1":null,"address2":null,"city":null,"state":null,"country":null,"zipcode":null,"email":"praslbaikady@gmail.com","phone":"9066047177","udf1":null,"udf2":null,"udf3":null,"udf4":null,"udf5":null,"udf6":null,"udf7":null,"udf8":null,"udf9":null,"udf10":null,"hash":"6ec9f860f677b4448f8919663a3d1797073f8e4d3424447ac0432c4341cb713d07aa6d07872b5bb3654bbb820416137e36e0070bd5609c16bd6bca11f070a1f9","field1":"117091135206451","field2":"128074","field3":"091117047766","field4":null,"field5":"0","field6":"Transaction Successful","field7":null,"field8":null,"field9":"Transaction Successful","PG_TYPE":"SBIPG","encryptedPaymentId":"8FBFD808F225E5A56E06BE6FB8C9C3BB","bank_ref_num":"091117047766","bankcode":"VISA","error":"E000","error_Message":"No Error","name_on_card":"payu","cardnum":"421492XXXXXX1624","cardhash":"This field is no longer supported in postback params.","amount_split":"{\\"PAYU\\":\\"1.04\\"}","payuMoneyId":"162808020","discount":"0.00","net_amount_debit":"1.04"}', '2017-09-11 01:51:48', '2017-09-11 01:51:48'),
(2, 'ABTXN20170911094252', '{"mihpayid":"403993715516590069","mode":"CC","status":"failure","unmappedstatus":"failed","key":"zS22aqxK","txnid":"ABTXN20170911094252","amount":"1.03","addedon":"2017-09-11 15:13:50","productinfo":"Online Purchase","firstname":"Praveen","lastname":null,"address1":null,"address2":null,"city":null,"state":null,"country":null,"zipcode":null,"email":"praslbaikady@gmail.com","phone":"9066047177","udf1":null,"udf2":null,"udf3":null,"udf4":null,"udf5":null,"udf6":null,"udf7":null,"udf8":null,"udf9":null,"udf10":null,"hash":"e7a304144ce078a75fabe24974a98f5f27c2439f716f3720f1710aa0c46d616e841d55467a286b8f7072fa1837cc784472436f6501437c2889ff5a3aeeeca94d","field1":"169897","field2":"115926","field3":"20170911","field4":"MC","field5":"68309684102","field6":"45","field7":"1","field8":"3DS","field9":"Verification of Secure Hash Failed: E700 -- Unspecified Failure -- Unknown Error -- Unable to be determined--E500","PG_TYPE":"AXISPG","bank_ref_num":"169897","bankcode":"CC","error":"E500","error_Message":"Bank failed to authenticate the customer","name_on_card":"test","cardnum":"400001XXXXXX0142","cardhash":"This field is no longer supported in postback params.","amount_split":"{\\"PAYU\\":\\"1.03\\"}","payuMoneyId":"1111372139","discount":"0.00","net_amount_debit":"0.00"}', '2017-09-11 04:14:14', '2017-09-11 04:14:14'),
(3, 'ABTXN20170911095044', '{"mihpayid":"1111372162","mode":null,"status":"failure","unmappedstatus":"userCancelled","key":"zS22aqxK","txnid":"ABTXN20170911095044","amount":"1.03","addedon":"2017-09-11 15:21:03","productinfo":"Online Purchase","firstname":"Praveen","lastname":null,"address1":null,"address2":null,"city":null,"state":null,"country":null,"zipcode":null,"email":"praslbaikady@gmail.com","phone":"9066047177","udf1":null,"udf2":null,"udf3":null,"udf4":null,"udf5":null,"udf6":null,"udf7":null,"udf8":null,"udf9":null,"udf10":null,"hash":"1a561eb5c6a375360dcd766b3a1c322fc60e35cbba0a649f6da9e0dd315eaf19d9ffcd9239bf3e9a1386e796ca269f4da350e313ee2cea17505fa7bf7ece3e64","field1":null,"field2":null,"field3":null,"field4":null,"field5":null,"field6":null,"field7":null,"field8":null,"field9":"Cancelled by user","PG_TYPE":"PAISA","bank_ref_num":"1111372162","bankcode":"PAYUW","error":"E000","error_Message":"No Error","amount_split":"{}","payuMoneyId":"1111372162"}', '2017-09-11 04:21:03', '2017-09-11 04:21:03'),
(4, 'ABTXN20170911100025', '{"mihpayid":"1111372192","mode":null,"status":"failure","unmappedstatus":"userCancelled","key":"zS22aqxK","txnid":"ABTXN20170911100025","amount":"1.03","addedon":"2017-09-11 15:35:33","productinfo":"Online Purchase","firstname":"Praveen","lastname":null,"address1":null,"address2":null,"city":null,"state":null,"country":null,"zipcode":null,"email":"praslbaikady@gmail.com","phone":"9066047177","udf1":null,"udf2":null,"udf3":null,"udf4":null,"udf5":null,"udf6":null,"udf7":null,"udf8":null,"udf9":null,"udf10":null,"hash":"7882a0fb0d3912acc156fa45f8e3553f2cc136e0fb77fb922da70a78215338d87e0b8e7aa096ce95dddf00ffe3a38ac5d2aa98f91adbc7b2d0c697d7592b90f5","field1":null,"field2":null,"field3":null,"field4":null,"field5":null,"field6":null,"field7":null,"field8":null,"field9":"Cancelled by user","PG_TYPE":"PAISA","bank_ref_num":"1111372192","bankcode":"PAYUW","error":"E000","error_Message":"No Error","amount_split":"{}","payuMoneyId":"1111372192"}', '2017-09-11 04:35:33', '2017-09-11 04:35:33'),
(5, 'ABTXN20170920103916', '{"mihpayid":"1111387150","mode":null,"status":"failure","unmappedstatus":"userCancelled","key":"zS22aqxK","txnid":"ABTXN20170920103916","amount":"1.03","addedon":"2017-09-20 16:09:31","productinfo":"Online Purchase","firstname":"Praveen","lastname":null,"address1":null,"address2":null,"city":null,"state":null,"country":null,"zipcode":null,"email":"praslbaikady@gmail.com","phone":"9066047177","udf1":null,"udf2":null,"udf3":null,"udf4":null,"udf5":null,"udf6":null,"udf7":null,"udf8":null,"udf9":null,"udf10":null,"hash":"26f5f5b1fe3184a76153f033f50e0819ae8852a892dc30d951e5059460adcb62d03709ace48d3a8700cdb845d23a371c007f03d8f584a76af0f095efb38baca8","field1":null,"field2":null,"field3":null,"field4":null,"field5":null,"field6":null,"field7":null,"field8":null,"field9":"Cancelled by user","PG_TYPE":"PAISA","bank_ref_num":"1111387150","bankcode":"PAYUW","error":"E000","error_Message":"No Error","amount_split":"{}","payuMoneyId":"1111387150"}', '2017-09-20 05:09:32', '2017-09-20 05:09:32'),
(6, 'ABTXN20171127045827', '{"Title":"Abharan Jewellers","vpc_Amount":"0","vpc_BatchNo":"0","vpc_Command":"pay","vpc_Locale":"en","vpc_MerchTxnRef":"ABTXN20171127045827","vpc_Merchant":"ABHRJWPMIGS","vpc_Message":"Field vpc_Amount value [37627.38] is invalid.","vpc_OrderInfo":"Some Comment","vpc_TransactionNo":"0","vpc_TxnResponseCode":"7","vpc_Version":"1"}', '2017-11-26 23:32:34', '2017-11-26 23:32:34'),
(8, 'ABTXN20171127050609', '{"Title":"Abharan Jewellers","vpc_Amount":"0","vpc_BatchNo":"0","vpc_Command":"pay","vpc_Locale":"en","vpc_MerchTxnRef":"ABTXN20171127050609","vpc_Merchant":"ABHRJWPMIGS","vpc_Message":"Field vpc_Amount value [37627.38] is invalid.","vpc_OrderInfo":"Some Comment","vpc_TransactionNo":"0","vpc_TxnResponseCode":"7","vpc_Version":"1"}', '2017-11-26 23:36:10', '2017-11-26 23:36:10'),
(10, 'ABTXN20171127050635', '{"Title":"Abharan Jewellers","vpc_Amount":"0","vpc_BatchNo":"0","vpc_Command":"pay","vpc_Locale":"en","vpc_MerchTxnRef":"ABTXN20171127050635","vpc_Merchant":"ABHRJWPMIGS","vpc_Message":"Field vpc_Amount value [37627.38] is invalid.","vpc_OrderInfo":"Some Comment","vpc_TransactionNo":"0","vpc_TxnResponseCode":"7","vpc_Version":"1"}', '2017-11-26 23:36:36', '2017-11-26 23:36:36'),
(11, 'ABTXN20171127050700', '{"Title":"Abharan Jewellers","vpc_Amount":"0","vpc_BatchNo":"0","vpc_Command":"pay","vpc_Locale":"en","vpc_MerchTxnRef":"ABTXN20171127050700","vpc_Merchant":"ABHRJWPMIGS","vpc_Message":"Field vpc_Amount value [37627.38] is invalid.","vpc_OrderInfo":"Some Comment","vpc_TransactionNo":"0","vpc_TxnResponseCode":"7","vpc_Version":"1"}', '2017-11-26 23:37:00', '2017-11-26 23:37:00'),
(12, 'ABTXN20171127052115', '{"Title":"Abharan Jewellers","vpc_3DSECI":"05","vpc_3DSXID":"wByH2JH92ZgbhaggPFmYxA+Qk8Y=","vpc_3DSenrolled":"Y","vpc_3DSstatus":"Y","vpc_AVSRequestCode":"Z","vpc_AVSResultCode":"Unsupported","vpc_AcqAVSRespCode":"Unsupported","vpc_AcqCSCRespCode":"M","vpc_AcqResponseCode":"00","vpc_Amount":"100","vpc_AuthorizeId":"550739","vpc_BatchNo":"20171127","vpc_CSCResultCode":"M","vpc_Card":"VC","vpc_Command":"pay","vpc_Currency":"INR","vpc_Locale":"en","vpc_MerchTxnRef":"ABTXN20171127052115","vpc_Merchant":"ABHRJWPMIGS","vpc_Message":"Approved","vpc_OrderInfo":"Some Comment","vpc_ReceiptNo":"733116641881","vpc_RiskOverallResult":"ACC","vpc_SecureHash":"8ED41071A6BD43817E2611CC1802F3684710DD19A74DA1466BFDBA4894973970","vpc_SecureHashType":"SHA256","vpc_TransactionNo":"1100000000","vpc_TxnResponseCode":"0","vpc_VerSecurityLevel":"05","vpc_VerStatus":"Y","vpc_VerToken":"AAABCFIAAGFAEnEDhwAAAAAAAAA=","vpc_VerType":"3DS","vpc_Version":"1"}', '2017-11-26 23:53:23', '2017-11-26 23:53:23');

-- --------------------------------------------------------

--
-- Table structure for table `payment_transactions`
--

CREATE TABLE `payment_transactions` (
  `p_txn_id` int(10) UNSIGNED NOT NULL,
  `txn_ref_id` varchar(25) NOT NULL,
  `order_id` int(10) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_transactions`
--

INSERT INTO `payment_transactions` (`p_txn_id`, `txn_ref_id`, `order_id`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ABTXN20170911071437', 1, 1.03, 1, '2017-09-11 01:44:37', '2017-09-11 01:51:48'),
(2, 'ABTXN20170911094252', 2, 1.03, 2, '2017-09-11 04:12:52', '2017-09-11 04:14:13'),
(3, 'ABTXN20170911095044', 2, 1.03, 2, '2017-09-11 04:20:44', '2017-09-11 04:21:03'),
(4, 'ABTXN20170911100025', 2, 1.03, 2, '2017-09-11 04:30:25', '2017-09-11 04:35:33'),
(5, 'ABTXN20170920103916', 4, 1.03, 2, '2017-09-20 05:09:16', '2017-09-20 05:09:31'),
(6, 'ABTXN20171125063222', 6, 1659345.88, 0, '2017-11-25 01:02:22', '2017-11-25 01:02:22'),
(7, 'ABTXN20171125084015', 7, 1728121.50, 0, '2017-11-25 03:10:15', '2017-11-25 03:10:15'),
(8, 'ABTXN20171125084116', 8, 1728121.50, 0, '2017-11-25 03:11:16', '2017-11-25 03:11:16'),
(9, 'ABTXN20171125084341', 9, 1728121.50, 0, '2017-11-25 03:13:41', '2017-11-25 03:13:41'),
(10, 'ABTXN20171125084552', 10, 1728121.50, 0, '2017-11-25 03:15:52', '2017-11-25 03:15:52'),
(11, 'ABTXN20171125084659', 11, 1728121.50, 0, '2017-11-25 03:16:59', '2017-11-25 03:16:59'),
(12, 'ABTXN20171127042523', 13, 37627.38, 0, '2017-11-26 22:55:23', '2017-11-26 22:55:23'),
(13, 'ABTXN20171127044632', 14, 37627.38, 0, '2017-11-26 23:16:32', '2017-11-26 23:16:32'),
(14, 'ABTXN20171127044648', 15, 37627.38, 0, '2017-11-26 23:16:48', '2017-11-26 23:16:48'),
(15, 'ABTXN20171127044743', 16, 37627.38, 0, '2017-11-26 23:17:43', '2017-11-26 23:17:43'),
(16, 'ABTXN20171127045225', 17, 37627.38, 0, '2017-11-26 23:22:25', '2017-11-26 23:22:25'),
(17, 'ABTXN20171127045827', 18, 37627.38, 2, '2017-11-26 23:28:27', '2017-11-26 23:32:33'),
(18, 'ABTXN20171127050609', 19, 37627.38, 2, '2017-11-26 23:36:09', '2017-11-26 23:36:10'),
(19, 'ABTXN20171127050635', 20, 37627.38, 2, '2017-11-26 23:36:35', '2017-11-26 23:36:36'),
(20, 'ABTXN20171127050700', 21, 37627.38, 2, '2017-11-26 23:37:00', '2017-11-26 23:37:00'),
(21, 'ABTXN20171127051051', 22, 37627.38, 0, '2017-11-26 23:40:51', '2017-11-26 23:40:51'),
(22, 'ABTXN20171127051523', 23, 37627.38, 0, '2017-11-26 23:45:23', '2017-11-26 23:45:23'),
(23, 'ABTXN20171127051545', 24, 37627.38, 0, '2017-11-26 23:45:45', '2017-11-26 23:45:45'),
(24, 'ABTXN20171127051649', 25, 37627.38, 0, '2017-11-26 23:46:49', '2017-11-26 23:46:49'),
(25, 'ABTXN20171127052050', 26, 37627.38, 0, '2017-11-26 23:50:50', '2017-11-26 23:50:50'),
(26, 'ABTXN20171127052115', 27, 37627.38, 1, '2017-11-26 23:51:15', '2017-11-26 23:53:23'),
(27, 'ABTXN20171127055720', 29, 1.03, 0, '2017-11-27 00:27:20', '2017-11-27 00:27:20'),
(28, 'ABTXN20171127061921', 31, 1.03, 0, '2017-11-27 00:49:21', '2017-11-27 00:49:21'),
(29, 'ABTXN20171205063320', 32, 0.00, 0, '2017-12-05 01:03:20', '2017-12-05 01:03:20'),
(30, 'ABTXN20171205063323', 33, 0.00, 0, '2017-12-05 01:03:23', '2017-12-05 01:03:23'),
(31, 'ABTXN20171205063420', 34, 0.00, 0, '2017-12-05 01:04:20', '2017-12-05 01:04:20');

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `p_details_id` int(11) NOT NULL,
  `product_id` int(10) NOT NULL,
  `category` varchar(10) NOT NULL,
  `weight` varchar(10) NOT NULL,
  `quote` int(10) NOT NULL,
  `purity` int(10) NOT NULL,
  `metal` varchar(20) NOT NULL,
  `beeds` float(10,4) NOT NULL,
  `beeds_value` float(10,4) NOT NULL,
  `wastage` float(10,4) DEFAULT NULL,
  `making_amount` float(10,4) DEFAULT NULL COMMENT 'Value will be percentage in case of Diamond and value in rupees to other metal',
  `mak_amt_percentage` float(10,4) DEFAULT NULL COMMENT 'Only in Siver, apply this value as percentage to the product of weight and per gram amt',
  `mak_amt_per_gram` float(10,4) DEFAULT NULL COMMENT 'Making amount per gram to calculate overall making amount',
  `cents` float(10,4) NOT NULL,
  `daimond` float(10,4) NOT NULL,
  `stone` float(10,4) NOT NULL,
  `cost` int(10) NOT NULL,
  `description` varchar(100) NOT NULL,
  `production` varchar(20) NOT NULL,
  `max` int(10) NOT NULL,
  `certification` int(10) NOT NULL,
  `cut` varchar(30) NOT NULL,
  `colour` varchar(20) NOT NULL,
  `clarity` varchar(20) NOT NULL,
  `women` int(1) NOT NULL,
  `men` int(1) NOT NULL,
  `kids` int(1) NOT NULL,
  `collection` varchar(100) NOT NULL,
  `occasion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`p_details_id`, `product_id`, `category`, `weight`, `quote`, `purity`, `metal`, `beeds`, `beeds_value`, `wastage`, `making_amount`, `mak_amt_percentage`, `mak_amt_per_gram`, `cents`, `daimond`, `stone`, `cost`, `description`, `production`, `max`, `certification`, `cut`, `colour`, `clarity`, `women`, `men`, `kids`, `collection`, `occasion`) VALUES
(1, 1, '0', '18.54', 1, 916, 'GOLD', 12.8400, 2750.0000, NULL, 22.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BIGST', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(2, 2, '0', '10.21', 1, 916, 'GOLD', 5.4900, 1270.0000, NULL, 22.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BIGST', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(3, 3, '0', '18.1', 1, 916, 'GOLD', 1.9900, 0.0000, NULL, 15.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'FANCY', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(4, 4, '0', '31.04', 1, 916, 'GOLD', 1.0000, 1250.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(5, 5, '0', '33.4', 1, 916, 'GOLD', 0.1000, 130.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(6, 6, '0', '35.65', 1, 916, 'GOLD', 1.7700, 1060.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'RUBY+EMEROLD', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(7, 7, '0', '32.63', 1, 916, 'GOLD', 1.0000, 1250.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(8, 8, '0', '43.15', 1, 916, 'GOLD', 0.5500, 690.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(9, 9, '0', '30.52', 1, 916, 'GOLD', 1.0000, 1250.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(10, 10, '0', '9.46', 1, 916, 'GOLD', 1.1000, 1380.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BINDIYA-ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(11, 11, '0', '59.42', 1, 916, 'GOLD', 2.4400, 1460.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BEEDSTONE+FNC', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(12, 12, '0', '17.25', 1, 916, 'GOLD', 1.1000, 1380.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(13, 13, '0', '74.73', 1, 916, 'GOLD', 0.4000, 500.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(14, 14, '0', '32.71', 1, 916, 'GOLD', 1.3000, 780.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BEED+FNC', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(15, 15, '0', '32.81', 1, 916, 'GOLD', 1.3500, 810.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BEED+FNC', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(16, 16, '0', '13.63', 1, 916, 'GOLD', 0.4000, 500.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(17, 17, '0', '40.91', 1, 916, 'GOLD', 1.8300, 2290.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(18, 18, '0', '41.38', 1, 916, 'GOLD', 4.2700, 2950.0000, NULL, 16.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'DAMBE HVL', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(19, 19, '0', '43.86', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'KASUMALA', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(20, 20, '0', '22.45', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PLAIN GOLD', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(21, 21, '0', '3.91', 1, 916, 'GOLD', 0.8000, 450.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ARC BEED/STONE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(22, 22, '0', '14.65', 1, 916, 'GOLD', 4.5600, 4100.0000, NULL, 15.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'HVL+MUSTI', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(23, 23, '0', '46.34', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PLAIN GOLD', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(24, 24, '0', '45.26', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PLAIN GOLD', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(25, 25, '0', '6.47', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PLAIN GOLD', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(26, 26, '0', '38.2', 1, 916, 'GOLD', 2.0100, 2510.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(27, 27, '0', '30.23', 1, 916, 'GOLD', 0.8900, 1110.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(28, 28, '0', '36.61', 1, 916, 'GOLD', 2.1900, 500.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ASTBD+GRAPE BN', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(29, 29, '0', '15.36', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PILGFNC+PG/ENM', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(30, 30, '0', '7.37', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PLAIN GOLD', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(31, 31, '0', '29.75', 1, 916, 'GOLD', 0.2100, 110.0000, NULL, 13.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BEED+FNC', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(32, 32, '0', '28.85', 1, 916, 'GOLD', 1.5900, 1990.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(33, 33, '0', '29.4', 1, 916, 'GOLD', 1.5900, 1990.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(34, 34, '0', '29.54', 1, 916, 'GOLD', 1.0600, 1320.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(35, 35, '0', '29.81', 1, 916, 'GOLD', 1.0600, 1320.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(36, 36, '0', '12.39', 1, 916, 'GOLD', 1.2600, 1580.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BINDIYA-ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(37, 37, '0', '14.5', 1, 916, 'GOLD', 1.9000, 2380.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(38, 38, '0', '10.94', 1, 916, 'GOLD', 1.2600, 1580.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BINDIYA-ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(39, 39, '0', '10.27', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'LADIES BT', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(40, 40, '0', '16.11', 1, 916, 'GOLD', 1.7500, 2190.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(41, 41, '0', '29.66', 1, 916, 'GOLD', 2.6400, 3300.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(42, 42, '0', '4.71', 1, 916, 'GOLD', 0.0300, 40.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'RUBY', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(43, 43, '0', '21.32', 1, 916, 'GOLD', 2.7100, 3390.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(44, 44, '0', '9.81', 1, 916, 'GOLD', 1.1800, 1480.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(45, 45, '0', '13.68', 1, 916, 'GOLD', 1.2200, 1530.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(46, 46, '0', '9.6', 1, 916, 'GOLD', 1.2600, 1580.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(47, 47, '0', '15.36', 1, 916, 'GOLD', 1.4100, 1760.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(48, 48, '0', '16', 1, 916, 'GOLD', 2.0200, 2530.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(49, 49, '0', '25.64', 1, 916, 'GOLD', 3.0100, 3760.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(50, 50, '0', '34.09', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PLAIN GOLD', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(51, 51, '0', '8.64', 1, 916, 'GOLD', 0.9400, 1180.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(52, 52, '0', '14.54', 1, 916, 'GOLD', 2.4000, 3000.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(53, 53, '0', '32.8', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PLAIN GOLD', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(54, 54, '0', '14.93', 1, 916, 'GOLD', 2.1100, 2640.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(55, 55, '0', '28.19', 1, 916, 'GOLD', 1.5400, 1000.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BEED/STONE+FNC', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(56, 56, '0', '28.46', 1, 916, 'GOLD', 1.3300, 870.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BEED/STONE+FNC', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(57, 57, '0', '36.69', 1, 916, 'GOLD', 1.6800, 1090.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BEED/STONE+FNC', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(58, 58, '0', '45.9', 1, 916, 'GOLD', 0.9900, 1240.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(59, 59, '0', '61.62', 1, 916, 'GOLD', 2.9900, 3740.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(60, 60, '0', '26.35', 1, 916, 'GOLD', 1.3500, 600.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BEED+FNC', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(61, 61, '0', '26.03', 1, 916, 'GOLD', 0.4500, 200.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BEED+FNC', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(62, 62, '0', '47.65', 1, 916, 'GOLD', 3.0300, 3790.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(63, 63, '0', '63.71', 1, 916, 'GOLD', 5.6500, 7060.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(64, 64, '0', '73.65', 1, 916, 'GOLD', 6.5400, 8180.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(65, 65, '0', '52.14', 1, 916, 'GOLD', 2.9900, 3740.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(66, 66, '0', '60.98', 1, 916, 'GOLD', 4.8500, 6060.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(67, 67, '0', '45.56', 1, 916, 'GOLD', 3.0900, 3860.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(68, 68, '0', '43.97', 1, 916, 'GOLD', 4.1600, 5200.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(69, 69, '0', '38.85', 1, 916, 'GOLD', 3.2500, 4060.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(70, 70, '0', '61.68', 1, 916, 'GOLD', 5.0100, 6260.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(71, 71, '0', '54.84', 1, 916, 'GOLD', 6.4100, 8010.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BINDIYA-ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(72, 72, '0', '46.99', 1, 916, 'GOLD', 4.1500, 5190.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(73, 73, '0', '125.99', 1, 916, 'GOLD', 10.4900, 13110.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(74, 74, '0', '55.96', 1, 916, 'GOLD', 1.6100, 2010.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(75, 75, '0', '49.4', 1, 916, 'GOLD', 4.3600, 5450.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(76, 76, '0', '53.13', 1, 916, 'GOLD', 3.7500, 4690.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(77, 77, '0', '41.88', 1, 916, 'GOLD', 0.8700, 1090.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(78, 78, '0', '97.77', 1, 916, 'GOLD', 3.1500, 3940.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(79, 79, '0', '45.01', 1, 916, 'GOLD', 3.3800, 2330.0000, NULL, 16.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'dambe hvl', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(80, 80, '0', '17.58', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PLAIN GOLD', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(81, 81, '0', '53.72', 1, 916, 'GOLD', 12.8700, 32700.0000, NULL, 13.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'KASHITHALI', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(82, 82, '0', '75.36', 1, 916, 'GOLD', 21.1100, 94750.0000, NULL, 13.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'KASHITHALI', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(83, 83, '0', '57.42', 1, 916, 'GOLD', 15.7200, 40990.0000, NULL, 13.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'KASHITHALI', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(84, 84, '0', '74.81', 1, 916, 'GOLD', 22.5600, 68350.0000, NULL, 13.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'KASHITHALI', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(85, 85, '0', '36.55', 1, 916, 'GOLD', 13.4100, 35870.0000, NULL, 13.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'KASHITHALI', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(86, 86, '0', '34.35', 1, 916, 'GOLD', 11.7400, 31220.0000, NULL, 13.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'KASHITHALI', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(87, 87, '0', '36.54', 1, 916, 'GOLD', 13.1700, 35170.0000, NULL, 13.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'KASHITHALI', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(88, 88, '0', '17.87', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ladies watch', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(89, 89, '0', '21.68', 1, 916, 'GOLD', 0.9900, 440.0000, NULL, 19.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'real stone', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(90, 90, '0', '71.83', 1, 916, 'GOLD', 8.1700, 12480.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(91, 91, '0', '62.1', 1, 916, 'GOLD', 1.8500, 970.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'beed/stone+fnc', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(92, 92, '0', '73.25', 1, 916, 'GOLD', 0.3000, 160.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'beed/stone+fnc', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(93, 93, '0', '35.51', 1, 916, 'GOLD', 0.2500, 130.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'beed/stone+fnc', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(94, 94, '0', '95.64', 1, 916, 'GOLD', 10.2100, 13130.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(95, 95, '0', '39.62', 1, 916, 'GOLD', 1.9800, 2080.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(96, 96, '0', '9.41', 1, 916, 'GOLD', 0.7500, 1350.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'anch-hvl', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(97, 97, '0', '14.72', 1, 916, 'GOLD', 0.8500, 370.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(98, 98, '0', '13.47', 1, 916, 'GOLD', 1.1500, 1300.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(99, 99, '0', '8.56', 1, 916, 'GOLD', 0.7500, 1350.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'anch-hvl', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(100, 100, '0', '10.96', 1, 916, 'GOLD', 1.0800, 1470.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(101, 101, '0', '11.28', 1, 916, 'GOLD', 0.9000, 1620.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'anch-hvl', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(102, 102, '0', '72.63', 1, 916, 'GOLD', 8.3400, 9140.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(103, 103, '0', '49.34', 1, 916, 'GOLD', 4.0700, 6720.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(104, 104, '0', '19.83', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'gents bracelet', 'YES', 15, 0, '0', '0', '0', 1, 1, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(105, 105, '0', '27.55', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 16.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'racket', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(106, 106, '0', '10.8', 1, 916, 'GOLD', 1.5800, 710.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'hair flower', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(107, 107, '0', '10.78', 1, 916, 'GOLD', 1.6900, 760.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'hair flower', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(108, 108, '0', '7.62', 1, 916, 'GOLD', 0.4300, 190.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'hair flower', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(109, 109, '0', '39.32', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 13.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'nerigundu', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(110, 110, '0', '37.5', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 13.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'nerigundu', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(111, 111, '0', '27.46', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 13.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PLAIN GOLD', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(112, 112, '0', '32.42', 1, 916, 'GOLD', 0.0600, 30.0000, NULL, 13.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PLAIN GOLD', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(113, 113, '0', '42.21', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 17.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PLAIN GOLD', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(114, 114, '0', '32.28', 1, 916, 'GOLD', 0.5500, 290.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'beed/stone', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(115, 115, '0', '49.52', 1, 916, 'GOLD', 0.6000, 320.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'beed/stone', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(116, 116, '0', '44.24', 1, 916, 'GOLD', 0.4000, 210.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'beed/stone', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(117, 117, '0', '35.27', 1, 916, 'GOLD', 0.6000, 320.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'beed/stone', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(118, 118, '0', '45.19', 1, 916, 'GOLD', 0.6500, 340.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'beed/stone', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(119, 119, '0', '48.27', 1, 916, 'GOLD', 0.6500, 340.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'beed/stone', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(120, 120, '0', '39.93', 1, 916, 'GOLD', 0.6500, 340.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'beed/stone', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(121, 121, '0', '48', 1, 916, 'GOLD', 0.6000, 320.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'beed/stone', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(122, 122, '0', '51.6', 1, 916, 'GOLD', 3.2800, 2130.0000, NULL, 18.5000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'nerigundu+fnc', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(123, 123, '0', '12.21', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 24.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(124, 124, '0', '38.17', 1, 916, 'GOLD', 0.8200, 1030.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(125, 125, '0', '36.22', 1, 916, 'GOLD', 1.1400, 1430.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(126, 126, '0', '23.34', 1, 916, 'GOLD', 0.3400, 430.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(127, 127, '0', '38.58', 1, 916, 'GOLD', 1.1600, 1450.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(128, 128, '0', '37.47', 1, 916, 'GOLD', 0.9100, 1140.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(129, 129, '0', '21.25', 1, 916, 'GOLD', 0.3600, 450.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(130, 130, '0', '20.08', 1, 916, 'GOLD', 0.4400, 550.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(131, 131, '0', '21.42', 1, 916, 'GOLD', 0.4600, 580.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(132, 132, '0', '19.33', 1, 916, 'GOLD', 0.3400, 430.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(133, 133, '0', '24.33', 1, 916, 'GOLD', 0.4400, 550.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(134, 134, '0', '13.76', 1, 916, 'GOLD', 0.0400, 50.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(135, 135, '0', '17.1', 1, 916, 'GOLD', 0.5400, 680.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(136, 136, '0', '16.82', 1, 916, 'GOLD', 0.4800, 600.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(137, 137, '0', '16.42', 1, 916, 'GOLD', 0.3300, 410.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(138, 138, '0', '19.84', 1, 916, 'GOLD', 0.3300, 410.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(139, 139, '0', '16.04', 1, 916, 'GOLD', 0.4600, 580.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(140, 140, '0', '14.38', 1, 916, 'GOLD', 0.2800, 350.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(141, 141, '0', '22.72', 1, 916, 'GOLD', 0.3000, 380.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(142, 142, '0', '15.53', 1, 916, 'GOLD', 0.4100, 510.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(143, 143, '0', '20.43', 1, 916, 'GOLD', 0.3300, 410.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(144, 144, '0', '16.26', 1, 916, 'GOLD', 0.3200, 400.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(145, 145, '0', '14.08', 1, 916, 'GOLD', 0.3200, 400.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(146, 146, '0', '15.69', 1, 916, 'GOLD', 0.1000, 130.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(147, 147, '0', '25.3', 1, 916, 'GOLD', 0.4400, 550.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(148, 148, '0', '13.46', 1, 916, 'GOLD', 0.3800, 480.0000, NULL, 21.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'CASUAL WEAR'),
(149, 149, '0', '14.62', 1, 916, 'GOLD', 0.8900, 1850.0000, NULL, 19.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'ANTIQUE', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(150, 150, '0', '6.8', 1, 916, 'GOLD', 0.5800, 1110.0000, NULL, 19.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'CAST+CZST', 'yes', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(151, 151, '0', '3.5', 1, 916, 'GOLD', 0.1800, 400.0000, NULL, 19.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'CAST+CZST', 'yes', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVALS', 'CASUAL WEAR'),
(152, 152, '0', '12.4', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'PLAIN GOLD VANKI', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'Wedding', 'Party Wear'),
(153, 153, '0', '51.81', 1, 916, 'GOLD', 1.1500, 600.0000, NULL, 0.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'BEED/STONE+FNC', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'Religious', 'Casual Wear'),
(154, 154, '0', '30.6', 1, 916, 'GOLD', 2.3500, 1240.0000, NULL, 0.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'DESIGN VANKI', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'Wedding', 'Party Wear'),
(155, 155, '0', '12.52', 1, 916, 'GOLD', 0.0000, 0.0000, NULL, 0.0000, NULL, NULL, 0.0000, 0.0000, 0.0000, 11, 'CASTING', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'New Arrivals', 'Party Wear'),
(156, 156, '0', '50', 1, 0, 'SILVER', 0.0000, 0.0000, NULL, 600.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, 'Plain Baby chain', 'yes', 15, 0, '0', '0', '0', 0, 0, 1, 'Religious', 'Daily wear'),
(157, 157, '0', '50', 1, 0, 'SILVER', 0.0000, 0.0000, NULL, 0.0000, 3.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, 'Plain Baby chain', 'yes', 15, 0, '0', '0', '0', 0, 0, 1, 'Religious', 'Daily wear'),
(158, 158, '0', '50', 1, 0, 'SILVER', 0.0000, 0.0000, NULL, 0.0000, 0.0000, 15.0000, 0.0000, 0.0000, 0.0000, 0, 'Plain Baby chain', 'yes', 15, 0, '0', '0', '0', 0, 0, 1, 'Religious', 'Daily wear'),
(159, 159, '0', '1.36', 1, 18, 'SILVER', 0.0000, 0.0000, NULL, 180.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, 'LEG CHAIN', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'DAILY WEAR'),
(160, 160, '0', '8.7', 1, 18, 'SILVER', 2.8000, 40.0000, NULL, 0.0000, 0.0000, 35.0000, 0.0000, 0.0000, 0.0000, 0, 'BABY  BANGLE', 'YES', 15, 0, '0', '0', '0', 0, 0, 1, 'NEW ARRIVAL', 'DAILY WEAR'),
(161, 161, '0', '4.8', 1, 18, 'SILVER', 0.7700, 20.0000, NULL, 0.0000, 0.0000, 35.0000, 0.0000, 0.0000, 0.0000, 0, 'BABY  BANGLE', 'YES', 15, 0, '0', '0', '0', 0, 0, 1, 'NEW ARRIVAL', 'DAILY WEAR'),
(162, 162, '0', '7.8', 1, 18, 'SILVER', 2.5200, 40.0000, NULL, 0.0000, 0.0000, 35.0000, 0.0000, 0.0000, 0.0000, 0, 'BABY  BANGLE', 'YES', 15, 0, '0', '0', '0', 0, 0, 1, 'NEW ARRIVAL', 'DAILY WEAR'),
(163, 163, '0', '7.8', 1, 18, 'SILVER', 1.2600, 40.0000, NULL, 0.0000, 0.0000, 35.0000, 0.0000, 0.0000, 0.0000, 0, 'BABY  BANGLE', 'YES', 15, 0, '0', '0', '0', 0, 0, 1, 'NEW ARRIVAL', 'DAILY WEAR'),
(168, 170, '0', '0', 1, 916, 'SILVER', 0.0000, 0.0000, NULL, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 1, 'FANCY KEY CHAIN', 'YES', 15, 0, '0', '0', '0', 1, 1, 0, 'NEW ARRIVAL', 'DAILY WEAR'),
(169, 165, '0', '3.86', 1, 916, 'Gold', 0.0000, 0.0000, 0.0000, 5500.0000, NULL, NULL, 0.4600, 29210.0000, 11.0000, 11, 'DIAMOND', 'YES', 15, 0, 'ROUND', 'GH', 'VVS', 1, 0, 0, 'Wedding ', 'Casual wear'),
(170, 171, '0', '3.86', 1, 18, 'Gold', 0.0000, 0.0000, 0.0000, 5500.0000, NULL, NULL, 0.4600, 29210.0000, 11.0000, 11, 'DIAMOND', 'YES', 15, 0, 'ROUND', 'GH', 'VVS', 1, 0, 0, 'Wedding ', 'Casual wear'),
(171, 172, '0', '3.86', 1, 18, 'Gold', 0.0000, 0.0000, 0.0000, 5500.0000, NULL, NULL, 0.4600, 29210.0000, 11.0000, 11, 'DIAMOND', 'YES', 15, 0, 'ROUND', 'GH', 'VVS', 1, 0, 0, 'Wedding ', 'Casual wear'),
(172, 173, '0', '4.15', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.1300, 10725.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'PARTY WEAR'),
(173, 174, '0', '5.05', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 5000.0000, NULL, NULL, 0.1300, 10725.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'PARTY WEAR'),
(174, 175, '0', '3.53', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 2500.0000, NULL, NULL, 0.1800, 17910.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'PARTY WEAR'),
(175, 176, '0', '4.68', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.1300, 10725.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'PARTY WEAR'),
(176, 177, '0', '4.67', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.1300, 10725.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'PARTY WEAR'),
(177, 178, '0', '2.47', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.2500, 20625.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'PARTY WEAR'),
(178, 179, '0', '6.96', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 7000.0000, NULL, NULL, 1.8700, 171105.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(179, 180, '0', '4.62', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.3100, 25575.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(180, 181, '0', '6.94', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 7000.0000, NULL, NULL, 0.2700, 20115.0000, 4.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(181, 182, '0', '4.28', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.1000, 8250.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(182, 183, '0', '3.42', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 3500.0000, NULL, NULL, 0.0800, 5960.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(183, 184, '0', '3.2', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 3500.0000, NULL, NULL, 0.1000, 8250.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(184, 185, '0', '3.85', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.1200, 9900.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(185, 186, '0', '4', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.1000, 8250.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(186, 187, '0', '7.19', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 7000.0000, NULL, NULL, 0.3100, 25575.0000, 3.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(187, 188, '0', '4.47', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.1200, 9900.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(188, 189, '0', '3.95', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.1000, 8250.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(189, 190, '0', '3.66', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 3500.0000, NULL, NULL, 0.1100, 9075.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(190, 191, '0', '7.19', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 7000.0000, NULL, NULL, 0.2900, 21605.0000, 4.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(191, 192, '0', '3.73', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 3500.0000, NULL, NULL, 0.1100, 9075.0000, 1.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(192, 193, '0', '6.69', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 6500.0000, NULL, NULL, 1.5400, 127050.0000, 14.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(193, 194, '0', '7.38', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 7500.0000, NULL, NULL, 1.6900, 139425.0000, 14.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(194, 195, '0', '4.43', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 5500.0000, NULL, NULL, 0.4400, 30580.0000, 18.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(195, 196, '0', '3.34', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.2700, 18765.0000, 8.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(196, 197, '0', '9.03', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 9000.0000, NULL, NULL, 1.6000, 112100.0000, 34.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(197, 198, '0', '7.76', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 8000.0000, NULL, NULL, 1.0700, 75265.0000, 34.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(198, 199, '0', '7.75', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 8000.0000, NULL, NULL, 1.4100, 97995.0000, 38.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(199, 200, '0', '9.49', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 9500.0000, NULL, NULL, 1.1600, 80620.0000, 50.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'CASUAL WEAR'),
(200, 201, '0', '10.82', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 11000.0000, NULL, NULL, 1.2900, 89655.0000, 50.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'NEW ARRIVAL', 'DAILY WEAR'),
(201, 202, '0', '5.76', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 6500.0000, NULL, NULL, 1.0300, 71585.0000, 34.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'DAILY WEAR'),
(202, 203, '0', '8.12', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 8000.0000, NULL, NULL, 1.0700, 75165.0000, 34.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'DAILY WEAR'),
(203, 204, '0', '9.14', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 9000.0000, NULL, NULL, 1.2400, 88780.0000, 34.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'DAILY WEAR'),
(204, 205, '0', '7.62', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 7500.0000, NULL, NULL, 1.0500, 75575.0000, 34.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'DAILY WEAR'),
(205, 206, '0', '8.67', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 8500.0000, NULL, NULL, 0.9600, 67520.0000, 34.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'DAILY WEAR'),
(206, 207, '0', '8.38', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 8500.0000, NULL, NULL, 1.7600, 129120.0000, 30.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'DAILY WEAR'),
(207, 208, '0', '5.23', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 6000.0000, NULL, NULL, 0.9300, 64635.0000, 26.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'DAILY WEAR'),
(208, 209, '0', '9.5', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 9500.0000, NULL, NULL, 1.1400, 79230.0000, 50.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'DAILY WEAR'),
(209, 210, '0', '3.77', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.1500, 10425.0000, 3.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'DAILY WEAR'),
(210, 211, '0', '5.07', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 5500.0000, NULL, NULL, 0.2400, 16680.0000, 4.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'DAILY WEAR'),
(211, 212, '0', '3.02', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.1500, 10425.0000, 3.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'DAILY WEAR'),
(212, 213, '0', '8.76', 1, 916, 'GOLD', 0.0000, 0.0000, 0.0000, 8500.0000, NULL, NULL, 1.1200, 77840.0000, 29.0000, 111, 'DIAMOND', 'YES', 15, 0, '0', '0', '0', 1, 0, 0, 'WEDDING', 'DAILY WEAR'),
(213, 214, '0', '9.42', 1, 916, 'GOLD', 0.2500, 1500.0000, 25.0000, 6269.0000, NULL, NULL, 0.0800, 6333.0000, 2.0000, 111, 'NAVARATHNA', 'YES', 15, 0, '0', '0', '0', 0, 1, 0, 'RELIGIOUS', 'DAILY WEAR'),
(214, 215, '0', '18.51', 1, 916, 'Gold', 0.8310, 0.0000, 0.0000, 18500.0000, NULL, NULL, 4.1550, 288772.0000, 78.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(215, 216, '0', '18.71', 1, 916, 'Gold', 0.8390, 0.0000, 0.0000, 18500.0000, NULL, NULL, 4.1950, 291552.0000, 78.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(216, 217, '0', '4.19', 1, 916, 'Gold', 0.0780, 0.0000, 0.0000, 5000.0000, NULL, NULL, 0.3900, 29055.0000, 5.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(217, 218, '0', '7.03', 1, 916, 'Gold', 0.0440, 0.0000, 0.0000, 7000.0000, NULL, NULL, 0.2200, 16390.0000, 3.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 0, 1, 0, 'New Arrivals', 'Casual Wear'),
(218, 219, '0', '5.37', 1, 916, 'Gold', 0.0420, 0.0000, 0.0000, 5500.0000, NULL, NULL, 0.2100, 15645.0000, 3.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 0, 1, 0, 'New Arrivals', 'Casual Wear'),
(219, 220, '0', '3.8', 1, 916, 'Gold', 0.0200, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.1000, 8250.0000, 1.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 0, 1, 0, 'New Arrivals', 'Casual Wear'),
(220, 221, '0', '4.19', 1, 916, 'Gold', 0.0160, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.0800, 5960.0000, 1.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 0, 1, 0, 'New Arrivals', 'Casual Wear'),
(221, 222, '0', '2.92', 1, 916, 'Gold', 0.0220, 0.0000, 0.0000, 3500.0000, NULL, NULL, 0.1100, 9075.0000, 1.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(222, 223, '0', '2.51', 1, 916, 'Gold', 0.0320, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.1600, 11920.0000, 2.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(223, 224, '0', '2.88', 1, 916, 'Gold', 0.0400, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.2000, 16500.0000, 2.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(224, 225, '0', '2.51', 1, 916, 'Gold', 0.0320, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.1600, 11920.0000, 2.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'Envy', 'Casual Wear'),
(225, 226, '0', '5.92', 1, 916, 'Gold', 0.2620, 0.0000, 0.0000, 6000.0000, NULL, NULL, 1.3100, 108075.0000, 14.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(226, 227, '0', '2.56', 1, 916, 'Gold', 0.0420, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.2100, 17325.0000, 2.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'Envy', 'Casual Wear'),
(227, 228, '0', '2.08', 1, 916, 'Gold', 0.0360, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.1800, 13410.0000, 2.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'Envy', 'Casual Wear'),
(228, 229, '0', '3.28', 1, 916, 'Gold', 0.0680, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.3400, 23630.0000, 6.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'Envy', 'Casual Wear'),
(229, 230, '0', '2.61', 1, 916, 'Gold', 0.0580, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.2900, 20155.0000, 10.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 0, 1, 0, 'Envy', 'Casual Wear'),
(230, 231, '0', '3.38', 1, 916, 'Gold', 0.0880, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.4400, 30580.0000, 8.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 0, 1, 0, 'Envy', 'Casual Wear'),
(231, 232, '0', '3.24', 1, 916, 'Gold', 0.0360, 0.0000, 0.0000, 5500.0000, NULL, NULL, 0.1800, 12510.0000, 4.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(232, 233, '0', '7.16', 1, 916, 'Gold', 0.0620, 0.0000, 0.0000, 7000.0000, NULL, NULL, 0.3100, 21545.0000, 6.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 0, 1, 0, 'New Arrivals', 'Casual Wear'),
(233, 234, '0', '4.24', 1, 916, 'Gold', 0.0500, 0.0000, 0.0000, 5500.0000, NULL, NULL, 0.2500, 18625.0000, 4.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 0, 1, 0, 'New Arrivals', 'Casual Wear'),
(234, 235, '0', '8.24', 1, 916, 'Gold', 0.2280, 0.0000, 0.0000, 8500.0000, NULL, NULL, 1.1400, 79230.0000, 29.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'Wedding', 'Party Wear'),
(235, 236, '0', '6.3', 1, 916, 'Gold', 0.1620, 0.0000, 0.0000, 6500.0000, NULL, NULL, 0.8100, 56295.0000, 21.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'Wedding', 'Party Wear'),
(236, 237, '0', '6.38', 1, 916, 'Gold', 0.1620, 0.0000, 0.0000, 6500.0000, NULL, NULL, 0.8100, 56295.0000, 21.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'Wedding', 'Party Wear'),
(237, 238, '0', '6.93', 1, 916, 'Gold', 0.1620, 0.0000, 0.0000, 7000.0000, NULL, NULL, 0.8100, 56295.0000, 21.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'Wedding', 'Party Wear'),
(238, 239, '0', '8.71', 1, 916, 'Gold', 0.2240, 0.0000, 0.0000, 8500.0000, NULL, NULL, 1.1200, 77840.0000, 29.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'Wedding', 'Party Wear'),
(239, 240, '0', '4.09', 1, 916, 'Gold', 0.0120, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.0600, 4170.0000, 1.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(240, 241, '0', '3.96', 1, 916, 'Gold', 0.0120, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.0600, 4170.0000, 1.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(241, 242, '0', '3.78', 1, 916, 'Gold', 0.0360, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.0900, 6705.0000, 1.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(242, 243, '0', '47.48', 1, 18, 'Gold', 1.8160, 0.0000, 0.0000, 46500.0000, NULL, NULL, 4.5400, 283750.0000, 480.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'Wedding', 'Party Wear'),
(243, 244, '0', '3.69', 1, 18, 'Gold', 0.1440, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.3600, 23460.0000, 18.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(244, 245, '0', '3.76', 1, 18, 'Gold', 0.1080, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.2700, 18765.0000, 7.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(245, 246, '0', '3.64', 1, 18, 'Gold', 0.1320, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.3300, 20625.0000, 78.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(246, 247, '0', '1.88', 1, 18, 'Gold', 0.0920, 0.0000, 0.0000, 3000.0000, NULL, NULL, 0.2300, 14375.0000, 14.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(247, 248, '0', '2.79', 1, 18, 'Gold', 0.2760, 0.0000, 0.0000, 3500.0000, NULL, NULL, 0.6900, 45325.0000, 67.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(248, 249, '0', '4.73', 1, 18, 'Gold', 0.2040, 0.0000, 0.0000, 5000.0000, NULL, NULL, 0.5100, 34275.0000, 42.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(249, 250, '0', '3.03', 1, 18, 'Gold', 0.2160, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.5400, 37530.0000, 14.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(250, 251, '0', '2.87', 1, 18, 'Gold', 0.1160, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.2900, 18125.0000, 22.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'Envy', 'Casual Wear'),
(251, 252, '0', '4.78', 1, 18, 'Gold', 0.4240, 0.0000, 0.0000, 5000.0000, NULL, NULL, 1.0600, 73670.0000, 38.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(252, 253, '0', '5.96', 1, 18, 'Gold', 0.5760, 0.0000, 0.0000, 6000.0000, NULL, NULL, 1.4400, 91800.0000, 134.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(253, 254, '0', '3.82', 1, 18, 'Gold', 0.1640, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.4100, 25625.0000, 34.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(254, 255, '0', '5.31', 1, 18, 'Gold', 0.3080, 0.0000, 0.0000, 5500.0000, NULL, NULL, 0.7700, 48125.0000, 176.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(255, 256, '0', '2.42', 1, 18, 'Gold', 0.1040, 0.0000, 0.0000, 3500.0000, NULL, NULL, 0.2600, 16250.0000, 14.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'Envy', 'Casual Wear'),
(256, 257, '0', '5.44', 1, 18, 'Gold', 0.2160, 0.0000, 0.0000, 5500.0000, NULL, NULL, 0.5400, 33750.0000, 82.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(257, 258, '0', '4.36', 1, 18, 'Gold', 0.2720, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.6800, 42500.0000, 70.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(258, 259, '0', '2.96', 1, 18, 'Gold', 0.1520, 0.0000, 0.0000, 4000.0000, NULL, NULL, 0.3800, 26410.0000, 18.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear'),
(259, 260, '0', '4.42', 1, 18, 'Gold', 0.4040, 0.0000, 0.0000, 4500.0000, NULL, NULL, 1.0100, 63125.0000, 74.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear');
INSERT INTO `product_details` (`p_details_id`, `product_id`, `category`, `weight`, `quote`, `purity`, `metal`, `beeds`, `beeds_value`, `wastage`, `making_amount`, `mak_amt_percentage`, `mak_amt_per_gram`, `cents`, `daimond`, `stone`, `cost`, `description`, `production`, `max`, `certification`, `cut`, `colour`, `clarity`, `women`, `men`, `kids`, `collection`, `occasion`) VALUES
(260, 261, '0', '4.64', 1, 18, 'Gold', 0.3040, 0.0000, 0.0000, 4500.0000, NULL, NULL, 0.7600, 52820.0000, 32.0000, 111, 'Diamond', 'YES', 15, 0, 'Round', 'EF', 'VVS', 1, 0, 0, 'New Arrivals', 'Casual Wear');

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE `product_image` (
  `product_id` int(10) NOT NULL,
  `global_name` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`product_id`, `global_name`) VALUES
(1, 'U36645'),
(2, 'U36680'),
(3, 'U38914'),
(4, 'U39805'),
(5, 'U39807'),
(6, 'U39827'),
(7, 'U39828'),
(8, 'U39830'),
(9, 'U39831'),
(10, 'U39842'),
(11, 'U39925'),
(12, 'U39941'),
(13, 'U39948'),
(14, 'U39992'),
(15, 'U40022'),
(16, 'U40031'),
(17, 'U40039'),
(18, 'U40131'),
(19, 'U40237'),
(20, 'U40254'),
(21, 'U40279'),
(22, 'U40356'),
(23, 'U40377'),
(24, 'U40380'),
(25, 'U40719'),
(26, 'U41431'),
(27, 'U41441'),
(28, 'U41737'),
(29, 'U42412'),
(30, 'U42440'),
(31, 'U42552'),
(32, 'U43432'),
(33, 'U43433'),
(34, 'U43448'),
(35, 'U43449'),
(36, 'U43451'),
(37, 'U43456'),
(38, 'U43459'),
(39, 'U43471'),
(40, 'U43474'),
(41, 'U43484'),
(42, 'U43509'),
(43, 'U43539'),
(44, 'U43545'),
(45, 'U43547'),
(46, 'U43558'),
(47, 'U43560'),
(48, 'U43567'),
(49, 'U43581'),
(50, 'U43587'),
(51, 'U43590'),
(52, 'U43594'),
(53, 'U43602'),
(54, 'U43610'),
(55, 'U43625'),
(56, 'U43629'),
(57, 'U43631'),
(58, 'U43633'),
(59, 'U43635'),
(60, 'U43641'),
(61, 'U43644'),
(62, 'U43651'),
(63, 'U43655'),
(64, 'U43657'),
(65, 'U43663'),
(66, 'U43666'),
(67, 'U43667'),
(68, 'U43670'),
(69, 'U43671'),
(70, 'U43673'),
(71, 'U43676'),
(72, 'U43681'),
(73, 'U43683'),
(74, 'U43686'),
(75, 'U43687'),
(76, 'U43716'),
(77, 'U43720'),
(78, 'U43723'),
(79, 'U44592'),
(80, 'U44684'),
(81, 'U44690'),
(82, 'U44704'),
(83, 'U44714'),
(84, 'U44736'),
(85, 'U44753'),
(86, 'U44754'),
(87, 'U44755'),
(88, 'U44938'),
(89, 'U45057'),
(90, 'U45289'),
(91, 'U45291'),
(92, 'U45293'),
(93, 'U45298'),
(94, 'U45318'),
(95, 'U45323'),
(96, 'U45324'),
(97, 'U45325'),
(98, 'U45326'),
(99, 'U45327'),
(100, 'U45329'),
(101, 'U45330'),
(102, 'U45331'),
(103, 'U45336'),
(104, 'U45365'),
(105, 'U45970'),
(106, 'U46088'),
(107, 'U46093'),
(108, 'U46095'),
(109, 'U46136'),
(110, 'U46152'),
(111, 'U46211'),
(112, 'U46213'),
(113, 'U46228'),
(114, 'U46231'),
(115, 'U46235'),
(116, 'U46237'),
(117, 'U46240'),
(118, 'U46243'),
(119, 'U46246'),
(120, 'U46248'),
(121, 'U46249'),
(122, 'U46667'),
(123, 'U47023'),
(124, 'U47042'),
(125, 'U47044'),
(126, 'U47057'),
(127, 'U47058'),
(128, 'U47059'),
(129, 'U47060'),
(130, 'U47061'),
(131, 'U47062'),
(132, 'U47063'),
(133, 'U47064'),
(134, 'U47065'),
(135, 'U47066'),
(136, 'U47067'),
(137, 'U47068'),
(138, 'U47069'),
(139, 'U47070'),
(140, 'U47071'),
(141, 'U47072'),
(142, 'U47073'),
(143, 'U47074'),
(144, 'U47075'),
(145, 'U47077'),
(146, 'U47078'),
(147, 'U47079'),
(148, 'U47080'),
(149, 'U47115'),
(150, 'U47119'),
(151, 'U47315'),
(152, 'U37263'),
(153, 'U37266'),
(154, 'U37267'),
(155, 'U37280'),
(156, 'U432145'),
(157, 'U432146'),
(158, 'U432147'),
(159, 'U98092'),
(160, 'U98581'),
(161, 'U98676'),
(162, 'U98786'),
(163, 'U98923'),
(164, '155426'),
(166, 'U100941'),
(168, 'U100941'),
(169, 'U100941'),
(170, 'U100941'),
(165, '155426'),
(231, 'U130085'),
(230, 'U130066'),
(229, 'U130042'),
(228, 'U129858'),
(227, 'U129856'),
(226, 'U129855'),
(225, 'U129854'),
(224, 'U129848'),
(223, 'U129845'),
(222, 'U129795'),
(221, 'U129773'),
(220, 'U129769'),
(219, 'U129762'),
(218, 'U129742'),
(217, 'U129726'),
(216, 'U103044'),
(215, 'U103041'),
(214, 'U130740'),
(213, 'U130287'),
(212, 'U130224'),
(211, 'U130222'),
(210, 'U130220'),
(209, 'U130081'),
(208, 'U130077'),
(207, 'U130075'),
(206, 'U130070'),
(205, 'U130069'),
(204, 'U130062'),
(203, 'U130058'),
(202, 'U130052'),
(201, 'U130050'),
(200, 'U130047'),
(199, 'U130022'),
(198, 'U129988'),
(197, 'U129959'),
(196, 'U129957'),
(195, 'U129932'),
(194, 'U129864'),
(193, 'U129837'),
(192, 'U129764'),
(191, 'U129669'),
(190, 'U129666'),
(189, 'U129663'),
(188, 'U129662'),
(187, 'U129661'),
(186, 'U129660'),
(185, 'U129659'),
(184, 'U129658'),
(183, 'U129656'),
(182, 'U129655'),
(181, 'U129653'),
(180, 'U129652'),
(179, 'U129622'),
(178, 'U129618'),
(177, 'U129615'),
(176, 'U129609'),
(175, 'U129605'),
(174, 'U129602'),
(173, 'U129598'),
(172, '155426'),
(171, '155426'),
(232, 'U130229'),
(233, 'U130233'),
(234, 'U130238'),
(235, 'U130264'),
(236, 'U130268'),
(237, 'U130273'),
(238, 'U130279'),
(239, 'U130294'),
(240, 'U131288'),
(241, 'U131292'),
(242, 'U137551'),
(243, 'U137626'),
(244, 'U141421'),
(245, 'U141423'),
(246, 'U141424'),
(247, 'U141425'),
(248, 'U141426'),
(249, 'U141440'),
(250, 'U141444'),
(251, 'U141446'),
(252, 'U141447'),
(253, 'U141448'),
(254, 'U141457'),
(255, 'U141466'),
(256, 'U141472'),
(257, 'U141474'),
(258, 'U141475'),
(259, 'U141476'),
(260, 'U141486'),
(261, 'U141491');

-- --------------------------------------------------------

--
-- Table structure for table `product_master`
--

CREATE TABLE `product_master` (
  `product_id` int(10) NOT NULL,
  `SKU` varchar(20) CHARACTER SET utf8 NOT NULL,
  `name` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `m_category_id` int(10) NOT NULL,
  `s_category_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_master`
--

INSERT INTO `product_master` (`product_id`, `SKU`, `name`, `m_category_id`, `s_category_id`, `created_at`, `updated_at`) VALUES
(1, 'U36645', 'BIGST', 3, 10, '2017-08-17 05:57:19', '2017-08-17 05:57:19'),
(2, 'U36680', 'BIGST', 3, 10, '2017-08-17 05:57:20', '2017-08-17 05:57:20'),
(3, 'U38914', 'FANCY', 3, 7, '2017-08-17 05:57:20', '2017-08-17 05:57:20'),
(4, 'U39805', 'ANTIQUE', 3, 5, '2017-08-17 05:57:20', '2017-08-17 05:57:20'),
(5, 'U39807', 'ANTIQUE', 3, 5, '2017-08-17 05:57:20', '2017-08-17 05:57:20'),
(6, 'U39827', 'RUBY+EMEROLD', 3, 5, '2017-08-17 05:57:20', '2017-08-17 05:57:20'),
(7, 'U39828', 'ANTIQUE', 3, 5, '2017-08-17 05:57:20', '2017-08-17 05:57:20'),
(8, 'U39830', 'ANTIQUE', 3, 5, '2017-08-17 05:57:20', '2017-08-17 05:57:20'),
(9, 'U39831', 'ANTIQUE', 3, 5, '2017-08-17 05:57:20', '2017-08-17 05:57:20'),
(10, 'U39842', 'BINDIYA-ANTIQUE', 3, 5, '2017-08-17 05:57:20', '2017-08-17 05:57:20'),
(11, 'U39925', 'BEEDSTONE+FNC', 3, 2, '2017-08-17 05:57:21', '2017-08-17 05:57:21'),
(12, 'U39941', 'ANTIQUE', 3, 6, '2017-08-17 05:57:21', '2017-08-17 05:57:21'),
(13, 'U39948', 'ANTIQUE', 3, 2, '2017-08-17 05:57:21', '2017-08-17 05:57:21'),
(14, 'U39992', 'BEED+FNC', 3, 6, '2017-08-17 05:57:21', '2017-08-17 05:57:21'),
(15, 'U40022', 'BEED+FNC', 3, 6, '2017-08-17 05:57:21', '2017-08-17 05:57:21'),
(16, 'U40031', 'ANTIQUE', 3, 10, '2017-08-17 05:57:21', '2017-08-17 05:57:21'),
(17, 'U40039', 'ANTIQUE', 3, 6, '2017-08-17 05:57:21', '2017-08-17 05:57:21'),
(18, 'U40131', 'DAMBE HVL', 3, 5, '2017-08-17 05:57:21', '2017-08-17 05:57:21'),
(19, 'U40237', 'KASUMALA', 3, 2, '2017-08-17 05:57:21', '2017-08-17 05:57:21'),
(20, 'U40254', 'PLAIN GOLD', 3, 6, '2017-08-17 05:57:21', '2017-08-17 05:57:21'),
(21, 'U40279', 'ARC BEED/STONE', 3, 10, '2017-08-17 05:57:22', '2017-08-17 05:57:22'),
(22, 'U40356', 'HVL+MUSTI', 3, 7, '2017-08-17 05:57:22', '2017-08-17 05:57:22'),
(23, 'U40377', 'PLAIN GOLD', 3, 2, '2017-08-17 05:57:22', '2017-08-17 05:57:22'),
(24, 'U40380', 'PLAIN GOLD', 3, 2, '2017-08-17 05:57:22', '2017-08-17 05:57:22'),
(25, 'U40719', 'PLAIN GOLD', 3, 10, '2017-08-17 05:57:22', '2017-08-17 05:57:22'),
(26, 'U41431', 'ANTIQUE', 3, 6, '2017-08-17 05:57:22', '2017-08-17 05:57:22'),
(27, 'U41441', 'ANTIQUE', 3, 6, '2017-08-17 05:57:22', '2017-08-17 05:57:22'),
(28, 'U41737', 'ASTBD+GRAPE BN', 3, 5, '2017-08-17 05:57:22', '2017-08-17 05:57:22'),
(29, 'U42412', 'PILGFNC+PG/ENM', 3, 10, '2017-08-17 05:57:22', '2017-08-17 05:57:22'),
(30, 'U42440', 'PLAIN GOLD', 3, 10, '2017-08-17 05:57:22', '2017-08-17 05:57:22'),
(31, 'U42552', 'BEED+FNC', 3, 6, '2017-08-17 05:57:22', '2017-08-17 05:57:22'),
(32, 'U43432', 'ANTIQUE', 3, 5, '2017-08-17 05:57:22', '2017-08-17 05:57:22'),
(33, 'U43433', 'ANTIQUE', 3, 5, '2017-08-17 05:57:23', '2017-08-17 05:57:23'),
(34, 'U43448', 'ANTIQUE', 3, 5, '2017-08-17 05:57:23', '2017-08-17 05:57:23'),
(35, 'U43449', 'ANTIQUE', 3, 5, '2017-08-17 05:57:23', '2017-08-17 05:57:23'),
(36, 'U43451', 'BINDIYA-ANTIQUE', 3, 5, '2017-08-17 05:57:23', '2017-08-17 05:57:23'),
(37, 'U43456', 'ANTIQUE', 3, 3, '2017-08-17 05:57:23', '2017-08-17 05:57:23'),
(38, 'U43459', 'BINDIYA-ANTIQUE', 3, 5, '2017-08-17 05:57:23', '2017-08-17 05:57:23'),
(39, 'U43471', 'LADIES BT', 3, 5, '2017-08-17 05:57:23', '2017-08-17 05:57:23'),
(40, 'U43474', 'ANTIQUE', 3, 3, '2017-08-17 05:57:23', '2017-08-17 05:57:23'),
(41, 'U43484', 'ANTIQUE', 3, 1, '2017-08-17 05:57:23', '2017-08-17 05:57:23'),
(42, 'U43509', 'RUBY', 3, 8, '2017-08-17 05:57:23', '2017-08-17 05:57:23'),
(43, 'U43539', 'ANTIQUE', 3, 10, '2017-08-17 05:57:23', '2017-08-17 05:57:23'),
(44, 'U43545', 'ANTIQUE', 3, 10, '2017-08-17 05:57:23', '2017-08-17 05:57:23'),
(45, 'U43547', 'ANTIQUE', 3, 10, '2017-08-17 05:57:24', '2017-08-17 05:57:24'),
(46, 'U43558', 'ANTIQUE', 3, 10, '2017-08-17 05:57:24', '2017-08-17 05:57:24'),
(47, 'U43560', 'ANTIQUE', 3, 10, '2017-08-17 05:57:24', '2017-08-17 05:57:24'),
(48, 'U43567', 'ANTIQUE', 3, 10, '2017-08-17 05:57:24', '2017-08-17 05:57:24'),
(49, 'U43581', 'ANTIQUE', 3, 10, '2017-08-17 05:57:24', '2017-08-17 05:57:24'),
(50, 'U43587', 'PLAIN GOLD', 3, 2, '2017-08-17 05:57:24', '2017-08-17 05:57:24'),
(51, 'U43590', 'ANTIQUE', 3, 10, '2017-08-17 05:57:24', '2017-08-17 05:57:24'),
(52, 'U43594', 'ANTIQUE', 3, 10, '2017-08-17 05:57:24', '2017-08-17 05:57:24'),
(53, 'U43602', 'PLAIN GOLD', 3, 2, '2017-08-17 05:57:24', '2017-08-17 05:57:24'),
(54, 'U43610', 'ANTIQUE', 3, 10, '2017-08-17 05:57:25', '2017-08-17 05:57:25'),
(55, 'U43625', 'BEED/STONE+FNC', 3, 2, '2017-08-17 05:57:25', '2017-08-17 05:57:25'),
(56, 'U43629', 'BEED/STONE+FNC', 3, 2, '2017-08-17 05:57:25', '2017-08-17 05:57:25'),
(57, 'U43631', 'BEED/STONE+FNC', 3, 2, '2017-08-17 05:57:25', '2017-08-17 05:57:25'),
(58, 'U43633', 'ANTIQUE', 3, 2, '2017-08-17 05:57:25', '2017-08-17 05:57:25'),
(59, 'U43635', 'ANTIQUE', 3, 2, '2017-08-17 05:57:25', '2017-08-17 05:57:25'),
(60, 'U43641', 'BEED+FNC', 3, 6, '2017-08-17 05:57:25', '2017-08-17 05:57:25'),
(61, 'U43644', 'BEED+FNC', 3, 6, '2017-08-17 05:57:25', '2017-08-17 05:57:25'),
(62, 'U43651', 'ANTIQUE', 3, 6, '2017-08-17 05:57:25', '2017-08-17 05:57:25'),
(63, 'U43655', 'ANTIQUE', 3, 6, '2017-08-17 05:57:26', '2017-08-17 05:57:26'),
(64, 'U43657', 'ANTIQUE', 3, 6, '2017-08-17 05:57:26', '2017-08-17 05:57:26'),
(65, 'U43663', 'ANTIQUE', 3, 2, '2017-08-17 05:57:26', '2017-08-17 05:57:26'),
(66, 'U43666', 'ANTIQUE', 3, 2, '2017-08-17 05:57:26', '2017-08-17 05:57:26'),
(67, 'U43667', 'ANTIQUE', 3, 6, '2017-08-17 05:57:26', '2017-08-17 05:57:26'),
(68, 'U43670', 'ANTIQUE', 3, 2, '2017-08-17 05:57:27', '2017-08-17 05:57:27'),
(69, 'U43671', 'ANTIQUE', 3, 6, '2017-08-17 05:57:27', '2017-08-17 05:57:27'),
(70, 'U43673', 'ANTIQUE', 3, 2, '2017-08-17 05:57:27', '2017-08-17 05:57:27'),
(71, 'U43676', 'BINDIYA-ANTIQUE', 3, 5, '2017-08-17 05:57:27', '2017-08-17 05:57:27'),
(72, 'U43681', 'ANTIQUE', 3, 2, '2017-08-17 05:57:27', '2017-08-17 05:57:27'),
(73, 'U43683', 'ANTIQUE', 3, 2, '2017-08-17 05:57:27', '2017-08-17 05:57:27'),
(74, 'U43686', 'ANTIQUE', 3, 2, '2017-08-17 05:57:27', '2017-08-17 05:57:27'),
(75, 'U43687', 'ANTIQUE', 3, 2, '2017-08-17 05:57:27', '2017-08-17 05:57:27'),
(76, 'U43716', 'ANTIQUE', 3, 2, '2017-08-17 05:57:27', '2017-08-17 05:57:27'),
(77, 'U43720', 'ANTIQUE', 3, 2, '2017-08-17 05:57:27', '2017-08-17 05:57:27'),
(78, 'U43723', 'ANTIQUE', 3, 2, '2017-08-17 05:57:27', '2017-08-17 05:57:27'),
(79, 'U44592', 'dambe hvl', 3, 5, '2017-08-17 05:57:28', '2017-08-17 05:57:28'),
(80, 'U44684', 'PLAIN GOLD', 3, 6, '2017-08-17 05:57:28', '2017-08-17 05:57:28'),
(81, 'U44690', 'KASHITHALI', 3, 7, '2017-08-17 05:57:28', '2017-08-17 05:57:28'),
(82, 'U44704', 'KASHITHALI', 3, 7, '2017-08-17 05:57:28', '2017-08-17 05:57:28'),
(83, 'U44714', 'KASHITHALI', 3, 7, '2017-08-17 05:57:28', '2017-08-17 05:57:28'),
(84, 'U44736', 'KASHITHALI', 3, 7, '2017-08-17 05:57:28', '2017-08-17 05:57:28'),
(85, 'U44753', 'KASHITHALI', 3, 7, '2017-08-17 05:57:28', '2017-08-17 05:57:28'),
(86, 'U44754', 'KASHITHALI', 3, 7, '2017-08-17 05:57:28', '2017-08-17 05:57:28'),
(87, 'U44755', 'KASHITHALI', 3, 7, '2017-08-17 05:57:28', '2017-08-17 05:57:28'),
(88, 'U44938', 'ladies watch', 3, 5, '2017-08-17 05:57:29', '2017-08-17 05:57:29'),
(89, 'U45057', 'real stone', 3, 6, '2017-08-17 05:57:29', '2017-08-17 05:57:29'),
(90, 'U45289', 'ANTIQUE', 3, 6, '2017-08-17 05:57:29', '2017-08-17 05:57:29'),
(91, 'U45291', 'beed/stone+fnc', 3, 2, '2017-08-17 05:57:29', '2017-08-17 05:57:29'),
(92, 'U45293', 'beed/stone+fnc', 3, 2, '2017-08-17 05:57:29', '2017-08-17 05:57:29'),
(93, 'U45298', 'beed/stone+fnc', 3, 2, '2017-08-17 05:57:29', '2017-08-17 05:57:29'),
(94, 'U45318', 'ANTIQUE', 3, 6, '2017-08-17 05:57:29', '2017-08-17 05:57:29'),
(95, 'U45323', 'ANTIQUE', 3, 6, '2017-08-17 05:57:29', '2017-08-17 05:57:29'),
(96, 'U45324', 'anch-hvl', 3, 10, '2017-08-17 05:57:29', '2017-08-17 05:57:29'),
(97, 'U45325', 'ANTIQUE', 3, 3, '2017-08-17 05:57:29', '2017-08-17 05:57:29'),
(98, 'U45326', 'ANTIQUE', 3, 3, '2017-08-17 05:57:30', '2017-08-17 05:57:30'),
(99, 'U45327', 'anch-hvl', 3, 10, '2017-08-17 05:57:30', '2017-08-17 05:57:30'),
(100, 'U45329', 'ANTIQUE', 3, 5, '2017-08-17 05:57:30', '2017-08-17 05:57:30'),
(101, 'U45330', 'anch-hvl', 3, 10, '2017-08-17 05:57:30', '2017-08-17 05:57:30'),
(102, 'U45331', 'ANTIQUE', 3, 5, '2017-08-17 05:57:30', '2017-08-17 05:57:30'),
(103, 'U45336', 'ANTIQUE', 3, 1, '2017-08-17 05:57:30', '2017-08-17 05:57:30'),
(104, 'U45365', 'gents bracelet', 3, 5, '2017-08-17 05:57:30', '2017-08-17 05:57:30'),
(105, 'U45970', 'racket', 3, 5, '2017-08-17 05:57:30', '2017-08-17 05:57:30'),
(106, 'U46088', 'hair flower', 3, 10, '2017-08-17 05:57:30', '2017-08-17 05:57:30'),
(107, 'U46093', 'hair flower', 3, 10, '2017-08-17 05:57:31', '2017-08-17 05:57:31'),
(108, 'U46095', 'hair flower', 3, 10, '2017-08-17 05:57:31', '2017-08-17 05:57:31'),
(109, 'U46136', 'nerigundu', 3, 5, '2017-08-17 05:57:31', '2017-08-17 05:57:31'),
(110, 'U46152', 'nerigundu', 3, 5, '2017-08-17 05:57:31', '2017-08-17 05:57:31'),
(111, 'U46211', 'PLAIN GOLD', 3, 5, '2017-08-17 05:57:31', '2017-08-17 05:57:31'),
(112, 'U46213', 'PLAIN GOLD', 3, 5, '2017-08-17 05:57:31', '2017-08-17 05:57:31'),
(113, 'U46228', 'PLAIN GOLD', 3, 5, '2017-08-17 05:57:31', '2017-08-17 05:57:31'),
(114, 'U46231', 'beed/stone', 3, 5, '2017-08-17 05:57:32', '2017-08-17 05:57:32'),
(115, 'U46235', 'beed/stone', 3, 5, '2017-08-17 05:57:32', '2017-08-17 05:57:32'),
(116, 'U46237', 'beed/stone', 3, 5, '2017-08-17 05:57:32', '2017-08-17 05:57:32'),
(117, 'U46240', 'beed/stone', 3, 5, '2017-08-17 05:57:32', '2017-08-17 05:57:32'),
(118, 'U46243', 'beed/stone', 3, 5, '2017-08-17 05:57:32', '2017-08-17 05:57:32'),
(119, 'U46246', 'beed/stone', 3, 5, '2017-08-17 05:57:32', '2017-08-17 05:57:32'),
(120, 'U46248', 'beed/stone', 3, 5, '2017-08-17 05:57:32', '2017-08-17 05:57:32'),
(121, 'U46249', 'beed/stone', 3, 5, '2017-08-17 05:57:32', '2017-08-17 05:57:32'),
(122, 'U46667', 'nerigundu+fnc', 3, 2, '2017-08-17 05:57:33', '2017-08-17 05:57:33'),
(123, 'U47023', 'ANTIQUE', 3, 6, '2017-08-17 05:57:33', '2017-08-17 05:57:33'),
(124, 'U47042', 'ANTIQUE', 3, 6, '2017-08-17 05:57:33', '2017-08-17 05:57:33'),
(125, 'U47044', 'ANTIQUE', 3, 6, '2017-08-17 05:57:33', '2017-08-17 05:57:33'),
(126, 'U47057', 'ANTIQUE', 3, 3, '2017-08-17 05:57:33', '2017-08-17 05:57:33'),
(127, 'U47058', 'ANTIQUE', 3, 3, '2017-08-17 05:57:33', '2017-08-17 05:57:33'),
(128, 'U47059', 'ANTIQUE', 3, 3, '2017-08-17 05:57:33', '2017-08-17 05:57:33'),
(129, 'U47060', 'ANTIQUE', 3, 3, '2017-08-17 05:57:33', '2017-08-17 05:57:33'),
(130, 'U47061', 'ANTIQUE', 3, 3, '2017-08-17 05:57:33', '2017-08-17 05:57:33'),
(131, 'U47062', 'ANTIQUE', 3, 3, '2017-08-17 05:57:33', '2017-08-17 05:57:33'),
(132, 'U47063', 'ANTIQUE', 3, 3, '2017-08-17 05:57:33', '2017-08-17 05:57:33'),
(133, 'U47064', 'ANTIQUE', 3, 3, '2017-08-17 05:57:34', '2017-08-17 05:57:34'),
(134, 'U47065', 'ANTIQUE', 3, 3, '2017-08-17 05:57:34', '2017-08-17 05:57:34'),
(135, 'U47066', 'ANTIQUE', 3, 3, '2017-08-17 05:57:34', '2017-08-17 05:57:34'),
(136, 'U47067', 'ANTIQUE', 3, 3, '2017-08-17 05:57:34', '2017-08-17 05:57:34'),
(137, 'U47068', 'ANTIQUE', 3, 3, '2017-08-17 05:57:34', '2017-08-17 05:57:34'),
(138, 'U47069', 'ANTIQUE', 3, 3, '2017-08-17 05:57:34', '2017-08-17 05:57:34'),
(139, 'U47070', 'ANTIQUE', 3, 3, '2017-08-17 05:57:34', '2017-08-17 05:57:34'),
(140, 'U47071', 'ANTIQUE', 3, 3, '2017-08-17 05:57:34', '2017-08-17 05:57:34'),
(141, 'U47072', 'ANTIQUE', 3, 3, '2017-08-17 05:57:34', '2017-08-17 05:57:34'),
(142, 'U47073', 'ANTIQUE', 3, 3, '2017-08-17 05:57:34', '2017-08-17 05:57:34'),
(143, 'U47074', 'ANTIQUE', 3, 3, '2017-08-17 05:57:34', '2017-08-17 05:57:34'),
(144, 'U47075', 'ANTIQUE', 3, 3, '2017-08-17 05:57:34', '2017-08-17 05:57:34'),
(145, 'U47077', 'ANTIQUE', 3, 3, '2017-08-17 05:57:35', '2017-08-17 05:57:35'),
(146, 'U47078', 'ANTIQUE', 3, 3, '2017-08-17 05:57:35', '2017-08-17 05:57:35'),
(147, 'U47079', 'ANTIQUE', 3, 3, '2017-08-17 05:57:35', '2017-08-17 05:57:35'),
(148, 'U47080', 'ANTIQUE', 3, 3, '2017-08-17 05:57:35', '2017-08-17 05:57:35'),
(149, 'U47115', 'CAST+CZST', 3, 10, '2017-08-17 05:57:35', '2017-08-17 05:57:35'),
(150, 'U47119', 'CAST+CZST', 3, 10, '2017-08-17 05:57:35', '2017-08-17 05:57:35'),
(151, 'U47315', 'CAST+CZST', 3, 10, '2017-08-17 05:57:35', '2017-08-17 05:57:35'),
(152, 'U37263', 'PLAIN GOLD VANKI', 3, 1, '2017-08-24 00:31:51', '2017-08-24 00:31:51'),
(153, 'U37266', 'BEED/STONE+FNC', 3, 2, '2017-08-24 00:31:52', '2017-08-24 00:31:52'),
(154, 'U37267', 'DESIGN VANKI', 2, 1, '2017-08-24 00:31:52', '2017-08-24 00:31:52'),
(155, 'U37280', 'CASTING', 3, 3, '2017-08-24 00:31:52', '2017-08-24 00:31:52'),
(156, 'U432145', 'Baby Chain', 6, 11, '2017-08-24 00:33:41', '2017-08-24 00:33:41'),
(157, 'U432146', 'Baby Chain2', 6, 11, '2017-08-24 00:33:41', '2017-08-24 00:33:41'),
(158, 'U432147', 'Baby Chain3', 6, 11, '2017-08-24 00:33:41', '2017-08-24 00:33:41'),
(159, 'U98092', 'LEG CHAIN', 6, 5, '2017-08-28 07:29:04', '2017-08-28 07:29:04'),
(160, 'U98581', 'BABY BANGLE', 6, 5, '2017-08-28 07:29:04', '2017-08-28 07:29:04'),
(161, 'U98676', 'BABY BANGLE', 6, 5, '2017-08-28 07:29:05', '2017-08-28 07:29:05'),
(162, 'U98786', 'BABY BANGLE', 6, 5, '2017-08-28 07:29:05', '2017-08-28 07:29:05'),
(163, 'U98923', 'BABY BANGLE', 6, 5, '2017-08-28 07:29:05', '2017-08-28 07:29:05'),
(170, 'U100941', 'FANCY KEY CHAIN', 6, 10, '2017-08-31 05:02:06', '2017-08-31 05:02:06'),
(172, '155426', 'DIAMOND', 1, 9, '2017-10-03 02:07:07', '2017-10-03 02:07:07'),
(173, 'U129598', 'DIAMOND', 3, 9, '2017-10-30 00:31:11', '2017-10-30 00:31:11'),
(174, 'U129602', 'DIAMOND', 3, 9, '2017-10-30 00:31:12', '2017-10-30 00:31:12'),
(175, 'U129605', 'DIAMOND', 3, 9, '2017-10-30 00:31:12', '2017-10-30 00:31:12'),
(176, 'U129609', 'DIAMOND', 3, 9, '2017-10-30 00:31:12', '2017-10-30 00:31:12'),
(177, 'U129615', 'DIAMOND', 3, 9, '2017-10-30 00:31:12', '2017-10-30 00:31:12'),
(178, 'U129618', 'DIAMOND', 3, 3, '2017-10-30 00:31:12', '2017-10-30 00:31:12'),
(179, 'U129622', 'DIAMOND', 1, 3, '2017-10-30 00:31:12', '2017-10-30 00:47:23'),
(180, 'U129652', 'DIAMOND', 3, 9, '2017-10-30 00:31:12', '2017-10-30 00:31:12'),
(181, 'U129653', 'DIAMOND', 3, 9, '2017-10-30 00:31:13', '2017-10-30 00:31:13'),
(182, 'U129655', 'DIAMOND', 3, 9, '2017-10-30 00:31:13', '2017-10-30 00:31:13'),
(183, 'U129656', 'DIAMOND', 3, 9, '2017-10-30 00:31:13', '2017-10-30 00:31:13'),
(184, 'U129658', 'DIAMOND', 3, 9, '2017-10-30 00:31:13', '2017-10-30 00:31:13'),
(185, 'U129659', 'DIAMOND', 3, 9, '2017-10-30 00:31:13', '2017-10-30 00:31:13'),
(186, 'U129660', 'DIAMOND', 3, 9, '2017-10-30 00:31:13', '2017-10-30 00:31:13'),
(187, 'U129661', 'DIAMOND', 3, 9, '2017-10-30 00:31:13', '2017-10-30 00:31:13'),
(188, 'U129662', 'DIAMOND', 3, 9, '2017-10-30 00:31:13', '2017-10-30 00:31:13'),
(189, 'U129663', 'DIAMOND', 3, 9, '2017-10-30 00:31:14', '2017-10-30 00:31:14'),
(190, 'U129666', 'DIAMOND', 3, 9, '2017-10-30 00:31:14', '2017-10-30 00:31:14'),
(191, 'U129669', 'DIAMOND', 3, 9, '2017-10-30 00:31:14', '2017-10-30 00:31:14'),
(192, 'U129764', 'DIAMOND', 3, 9, '2017-10-30 00:31:14', '2017-10-30 00:31:14'),
(193, 'U129837', 'DIAMOND', 3, 3, '2017-10-30 00:31:14', '2017-10-30 00:31:14'),
(194, 'U129864', 'DIAMOND', 3, 3, '2017-10-30 00:31:14', '2017-10-30 00:31:14'),
(195, 'U129932', 'DIAMOND', 3, 3, '2017-10-30 00:31:14', '2017-10-30 00:31:14'),
(196, 'U129957', 'DIAMOND', 3, 3, '2017-10-30 00:31:14', '2017-10-30 00:31:14'),
(197, 'U129959', 'DIAMOND', 3, 3, '2017-10-30 00:31:14', '2017-10-30 00:31:14'),
(198, 'U129988', 'DIAMOND', 3, 3, '2017-10-30 00:31:15', '2017-10-30 00:31:15'),
(199, 'U130022', 'DIAMOND', 3, 3, '2017-10-30 00:31:15', '2017-10-30 00:31:15'),
(200, 'U130047', 'DIAMOND', 3, 3, '2017-10-30 00:31:15', '2017-10-30 00:31:15'),
(201, 'U130050', 'DIAMOND', 3, 3, '2017-10-30 00:31:15', '2017-10-30 00:31:15'),
(202, 'U130052', 'DIAMOND', 3, 3, '2017-10-30 00:31:15', '2017-10-30 00:31:15'),
(203, 'U130058', 'DIAMOND', 3, 3, '2017-10-30 00:31:15', '2017-10-30 00:31:15'),
(204, 'U130062', 'DIAMOND', 3, 3, '2017-10-30 00:31:15', '2017-10-30 00:31:15'),
(205, 'U130069', 'DIAMOND', 3, 3, '2017-10-30 00:31:15', '2017-10-30 00:31:15'),
(206, 'U130070', 'DIAMOND', 3, 3, '2017-10-30 00:31:15', '2017-10-30 00:31:15'),
(207, 'U130075', 'DIAMOND', 3, 3, '2017-10-30 00:31:16', '2017-10-30 00:31:16'),
(208, 'U130077', 'DIAMOND', 3, 3, '2017-10-30 00:31:16', '2017-10-30 00:31:16'),
(209, 'U130081', 'DIAMOND', 3, 3, '2017-10-30 00:31:16', '2017-10-30 00:31:16'),
(210, 'U130220', 'DIAMOND', 3, 9, '2017-10-30 00:31:16', '2017-10-30 00:31:16'),
(211, 'U130222', 'DIAMOND', 3, 9, '2017-10-30 00:31:16', '2017-10-30 00:31:16'),
(212, 'U130224', 'DIAMOND', 3, 9, '2017-10-30 00:31:16', '2017-10-30 00:31:16'),
(213, 'U130287', 'DIAMOND', 3, 9, '2017-10-30 00:31:16', '2017-10-30 00:31:16'),
(214, 'U130740', 'NAVARATHNA', 3, 9, '2017-10-30 00:31:16', '2017-10-30 00:31:16'),
(215, 'U103041', 'Diamond', 1, 5, '2017-11-17 01:09:19', '2017-11-17 01:09:19'),
(216, 'U103044', 'Diamond', 1, 5, '2017-11-17 01:09:20', '2017-11-17 01:09:20'),
(217, 'U129726', 'Diamond', 1, 9, '2017-11-17 01:09:20', '2017-11-17 01:09:20'),
(218, 'U129742', 'Diamond', 1, 9, '2017-11-17 01:09:20', '2017-11-17 01:09:20'),
(219, 'U129762', 'Diamond', 1, 9, '2017-11-17 01:09:21', '2017-11-17 01:09:21'),
(220, 'U129769', 'Diamond', 1, 9, '2017-11-17 01:09:21', '2017-11-17 01:09:21'),
(221, 'U129773', 'Diamond', 1, 9, '2017-11-17 01:09:21', '2017-11-17 01:09:21'),
(222, 'U129795', 'Diamond', 1, 9, '2017-11-17 01:09:21', '2017-11-17 01:09:21'),
(223, 'U129845', 'Diamond', 1, 3, '2017-11-17 01:09:21', '2017-11-17 01:09:21'),
(224, 'U129848', 'Diamond', 1, 3, '2017-11-17 01:09:21', '2017-11-17 01:09:21'),
(225, 'U129854', 'Diamond', 1, 3, '2017-11-17 01:09:22', '2017-11-17 01:09:22'),
(226, 'U129855', 'Diamond', 1, 3, '2017-11-17 01:09:22', '2017-11-17 01:09:22'),
(227, 'U129856', 'Diamond', 1, 3, '2017-11-17 01:09:22', '2017-11-17 01:09:22'),
(228, 'U129858', 'Diamond', 1, 3, '2017-11-17 01:09:22', '2017-11-17 01:09:22'),
(229, 'U130042', 'Diamond', 1, 3, '2017-11-17 01:09:22', '2017-11-17 01:09:22'),
(230, 'U130066', 'Diamond', 1, 3, '2017-11-17 01:09:23', '2017-11-17 01:09:23'),
(231, 'U130085', 'Diamond', 1, 3, '2017-11-17 01:09:23', '2017-11-17 01:09:23'),
(232, 'U130229', 'Diamond', 1, 9, '2017-11-17 01:09:23', '2017-11-17 01:09:23'),
(233, 'U130233', 'Diamond', 1, 9, '2017-11-17 01:09:23', '2017-11-17 01:09:23'),
(234, 'U130238', 'Diamond', 1, 9, '2017-11-17 01:09:23', '2017-11-17 01:09:23'),
(235, 'U130264', 'Diamond', 1, 9, '2017-11-17 01:09:23', '2017-11-17 01:09:23'),
(236, 'U130268', 'Diamond', 1, 9, '2017-11-17 01:09:23', '2017-11-17 01:09:23'),
(237, 'U130273', 'Diamond', 1, 9, '2017-11-17 01:09:23', '2017-11-17 01:09:23'),
(238, 'U130279', 'Diamond', 1, 9, '2017-11-17 01:09:23', '2017-11-17 01:09:23'),
(239, 'U130294', 'Diamond', 1, 9, '2017-11-17 01:09:24', '2017-11-17 01:09:24'),
(240, 'U131288', 'Diamond', 1, 9, '2017-11-17 01:09:24', '2017-11-17 01:09:24'),
(241, 'U131292', 'Diamond', 1, 9, '2017-11-17 01:09:24', '2017-11-17 01:09:24'),
(242, 'U137551', 'Diamond', 1, 9, '2017-11-17 01:09:24', '2017-11-17 01:09:24'),
(243, 'U137626', 'Diamond', 1, 5, '2017-11-17 01:09:25', '2017-11-17 01:09:25'),
(244, 'U141421', 'Diamond', 1, 9, '2017-11-17 01:09:25', '2017-11-17 01:09:25'),
(245, 'U141423', 'Diamond', 1, 9, '2017-11-17 01:09:25', '2017-11-17 01:09:25'),
(246, 'U141424', 'Diamond', 1, 9, '2017-11-17 01:09:25', '2017-11-17 01:09:25'),
(247, 'U141425', 'Diamond', 1, 9, '2017-11-17 01:09:25', '2017-11-17 01:09:25'),
(248, 'U141426', 'Diamond', 1, 9, '2017-11-17 01:09:25', '2017-11-17 01:09:25'),
(249, 'U141440', 'Diamond', 1, 9, '2017-11-17 01:09:26', '2017-11-17 01:09:26'),
(250, 'U141444', 'Diamond', 1, 9, '2017-11-17 01:09:26', '2017-11-17 01:09:26'),
(251, 'U141446', 'Diamond', 1, 3, '2017-11-17 01:09:26', '2017-11-17 01:09:26'),
(252, 'U141447', 'Diamond', 1, 3, '2017-11-17 01:09:26', '2017-11-17 01:09:26'),
(253, 'U141448', 'Diamond', 1, 3, '2017-11-17 01:09:26', '2017-11-17 01:09:26'),
(254, 'U141457', 'Diamond', 1, 3, '2017-11-17 01:09:27', '2017-11-17 01:09:27'),
(255, 'U141466', 'Diamond', 1, 3, '2017-11-17 01:09:27', '2017-11-17 01:09:27'),
(256, 'U141472', 'Diamond', 1, 3, '2017-11-17 01:09:28', '2017-11-17 01:09:28'),
(257, 'U141474', 'Diamond', 1, 3, '2017-11-17 01:09:28', '2017-11-17 01:09:28'),
(258, 'U141475', 'Diamond', 1, 3, '2017-11-17 01:09:28', '2017-11-17 01:09:28'),
(259, 'U141476', 'Diamond', 1, 3, '2017-11-17 01:09:29', '2017-11-17 01:09:29'),
(260, 'U141486', 'Diamond', 1, 3, '2017-11-17 01:09:29', '2017-11-17 01:09:29'),
(261, 'U141491', 'Diamond', 1, 3, '2017-11-17 01:09:29', '2017-11-17 01:09:29');

-- --------------------------------------------------------

--
-- Table structure for table `product_offers`
--

CREATE TABLE `product_offers` (
  `p_offer_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) DEFAULT NULL,
  `m_category_id` int(10) DEFAULT NULL,
  `s_category_id` int(10) DEFAULT NULL,
  `discount` int(5) UNSIGNED NOT NULL COMMENT 'offer in rupees',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0 - Expired, 1 - Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0 - Mobile Not Verified, 1 - Active',
  `power` tinyint(1) UNSIGNED NOT NULL COMMENT '1 - Admin, 2- Others',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `password`, `status`, `power`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Abharan Admin', 'admin1', '9191919191', '$2y$10$gVoMgBorSE0OZ/54.qzUbOAb1BcXVW/Us7p30PF/HJlxQC1EU3QVa', 1, 1, 'xLpRpc2hauPEqupKcqVQmfHqCMoauDPXbLKK6FRAfsc1sNuQKz6Q46zL7y3S', NULL, '2017-09-26 07:16:37'),
(39, 'Praveen', 'praslbaikady@gmail.com', '9066047177', '$2y$10$gVoMgBorSE0OZ/54.qzUbOAb1BcXVW/Us7p30PF/HJlxQC1EU3QVa', 1, 2, 'Kzb0f0EEV55hLKlP9w2bto2levuPlkJemx7hQ5i8IQbKRDcoXsREe3Wnw5lC', '2017-08-17 05:39:07', '2017-08-24 01:17:38'),
(40, 'Praveen', 'test@gmail.com', '9066047172', '$2y$10$udsr94m4e6xLg0.jhn2VbuxkguJEqXnI.z0bLb9p1gA6h06.YCmPu', 1, 2, 'XGtK7gRqJZN2sFapAR1aCwQDO3hIwCe2bDSkzkR8jYooPkYrHtSOv0eFH2Oo', '2017-08-17 05:39:07', '2017-08-17 05:42:51');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `u_details_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `alternate_mobile` varchar(15) DEFAULT NULL,
  `locality` varchar(50) DEFAULT NULL,
  `landmark` varchar(100) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `pincode` varchar(6) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`u_details_id`, `user_id`, `alternate_mobile`, `locality`, `landmark`, `address`, `city`, `pincode`, `state`) VALUES
(1, 39, '9066047177', NULL, 'Baliga Hospital', 'Gandhinagar Baikady', 'Brahmavara', '576213', 'Karnataka');

-- --------------------------------------------------------

--
-- Table structure for table `user_orders`
--

CREATE TABLE `user_orders` (
  `id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `order_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_current_rates`
--

CREATE TABLE `_current_rates` (
  `p_rates_id` int(11) NOT NULL,
  `purity` int(10) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_current_rates`
--

INSERT INTO `_current_rates` (`p_rates_id`, `purity`, `amount`, `created_at`, `updated_at`) VALUES
(1, 916, 2745.00, NULL, '2017-11-17 07:19:03'),
(2, 18, 2180.00, NULL, '2017-07-11 05:33:02'),
(3, 0, 39.00, NULL, '2017-08-05 00:41:57');

-- --------------------------------------------------------

--
-- Table structure for table `_current_tax`
--

CREATE TABLE `_current_tax` (
  `p_tax_id` int(10) NOT NULL,
  `gst` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_current_tax`
--

INSERT INTO `_current_tax` (`p_tax_id`, `gst`) VALUES
(1, '3%');

-- --------------------------------------------------------

--
-- Table structure for table `_main_category`
--

CREATE TABLE `_main_category` (
  `m_category_id` int(10) NOT NULL,
  `m_category_name` varchar(30) NOT NULL,
  `image_name` varchar(50) NOT NULL,
  `ordering` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_main_category`
--

INSERT INTO `_main_category` (`m_category_id`, `m_category_name`, `image_name`, `ordering`) VALUES
(1, 'Diamond', 'diamond.png', 5),
(2, 'Gems', 'gems.png', 6),
(3, 'Gold', 'gold.png', 1),
(4, 'Pearl', 'pearl.png', 4),
(6, 'Silver', 'silver.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `_sub_category`
--

CREATE TABLE `_sub_category` (
  `s_category_id` int(10) NOT NULL,
  `s_category_name` varchar(30) NOT NULL,
  `image_name` varchar(50) NOT NULL,
  `hover_img` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_sub_category`
--

INSERT INTO `_sub_category` (`s_category_id`, `s_category_name`, `image_name`, `hover_img`) VALUES
(1, 'Waist Chain', '1.png', '1_h.png'),
(2, 'Haram', '2.png', '2_h.png'),
(3, 'Studs', '3.png', '3_h.png'),
(5, 'Bangles', '5.png', '5_h.png'),
(6, 'Necklace', '6.png', '6_h.png'),
(7, 'Kanti', '7.png', '7_h.png'),
(8, 'Ear Rings', '8.png', '8_h.png'),
(9, 'Finger Ring', '9.png', '9_h.png'),
(10, 'Pendent', '10.png', '10_h.png'),
(11, 'Chain', '11.png', '11_h.png'),
(12, 'Bracelet', '12.png', '12_h.png'),
(13, 'Articles', '13.png', '13_h.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_feedbacks`
--
ALTER TABLE `app_feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_name_index` (`name`),
  ADD KEY `cart_user_id_status_index` (`user_id`,`status`),
  ADD KEY `cart_session_status_index` (`session`,`status`);

--
-- Indexes for table `cart_lines`
--
ALTER TABLE `cart_lines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_lines_cart_id_foreign` (`cart_id`);

--
-- Indexes for table `customer_feedbacks`
--
ALTER TABLE `customer_feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_suggestions`
--
ALTER TABLE `feedback_suggestions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_master`
--
ALTER TABLE `image_master`
  ADD PRIMARY KEY (`image_master_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`p_order_id`),
  ADD UNIQUE KEY `user_id` (`p_order_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`o_details_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `payment_gateway_response`
--
ALTER TABLE `payment_gateway_response`
  ADD PRIMARY KEY (`p_resp_id`),
  ADD UNIQUE KEY `txn_ref_id` (`txn_ref_id`);

--
-- Indexes for table `payment_transactions`
--
ALTER TABLE `payment_transactions`
  ADD PRIMARY KEY (`p_txn_id`),
  ADD UNIQUE KEY `txn_ref_id` (`txn_ref_id`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD PRIMARY KEY (`p_details_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_master`
--
ALTER TABLE `product_master`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `SKU` (`SKU`),
  ADD KEY `m_category_id` (`m_category_id`),
  ADD KEY `s_category_id` (`s_category_id`);

--
-- Indexes for table `product_offers`
--
ALTER TABLE `product_offers`
  ADD PRIMARY KEY (`p_offer_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `m_category_id` (`m_category_id`),
  ADD KEY `s_category_id` (`s_category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mobile` (`mobile`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`u_details_id`);

--
-- Indexes for table `user_orders`
--
ALTER TABLE `user_orders`
  ADD KEY `id` (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `_current_rates`
--
ALTER TABLE `_current_rates`
  ADD PRIMARY KEY (`p_rates_id`);

--
-- Indexes for table `_current_tax`
--
ALTER TABLE `_current_tax`
  ADD PRIMARY KEY (`p_tax_id`);

--
-- Indexes for table `_main_category`
--
ALTER TABLE `_main_category`
  ADD PRIMARY KEY (`m_category_id`);

--
-- Indexes for table `_sub_category`
--
ALTER TABLE `_sub_category`
  ADD PRIMARY KEY (`s_category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_feedbacks`
--
ALTER TABLE `app_feedbacks`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cart_lines`
--
ALTER TABLE `cart_lines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_feedbacks`
--
ALTER TABLE `customer_feedbacks`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `feedback_suggestions`
--
ALTER TABLE `feedback_suggestions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `image_master`
--
ALTER TABLE `image_master`
  MODIFY `image_master_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `p_order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `o_details_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `payment_gateway_response`
--
ALTER TABLE `payment_gateway_response`
  MODIFY `p_resp_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `payment_transactions`
--
ALTER TABLE `payment_transactions`
  MODIFY `p_txn_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `product_details`
--
ALTER TABLE `product_details`
  MODIFY `p_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;
--
-- AUTO_INCREMENT for table `product_master`
--
ALTER TABLE `product_master`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=262;
--
-- AUTO_INCREMENT for table `product_offers`
--
ALTER TABLE `product_offers`
  MODIFY `p_offer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `u_details_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `_current_rates`
--
ALTER TABLE `_current_rates`
  MODIFY `p_rates_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `_current_tax`
--
ALTER TABLE `_current_tax`
  MODIFY `p_tax_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `_main_category`
--
ALTER TABLE `_main_category`
  MODIFY `m_category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `_sub_category`
--
ALTER TABLE `_sub_category`
  MODIFY `s_category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart_lines`
--
ALTER TABLE `cart_lines`
  ADD CONSTRAINT `cart_lines_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
