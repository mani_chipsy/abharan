@extends('Feedback.layouts.main_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('Assets/parsley/parsley.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/jquery-confirm/jquery-confirm.css')}}">
<div ng-controller='FeedbackController'>
<div class="container" >

  
      <div class="col-sm-12 col-xs-12 text-center" style="padding-bottom: 15px">
        <div class="">
         <header id="header">

        <h2 class="head">
           ಗ್ರಾಹಕರ ಅಭಿಪ್ರಾಯ ಪತ್ರ / CUSTOMER FEEDBACK FORM
          </h2>
          </header>
          <br><br>
          <img src="logo1.png" class="" alt="">
          
          
        </div>
      </div>
     
      <div class="col-sm-6 col-xs-6 ">
        <label style=""> Staff Code:
        <input type="text" readonly=""  name="Staff" id="Staff" style="   width: fit-content;
    border: 0px;
    background: transparent;">
       <!--  <lable id="Staff" name="Staff"></lable></label> -->
      </div>
      <div class="col-sm-12 col-xs-12 ">
        <label style=""> Bill Number:
        <input type="text" readonly=""  name="show_bill_number" id="show_bill_number" style="   width: fit-content;
    border: 0px;
    background: transparent;">
       <!--  <lable id="Staff" name="Staff"></lable></label> -->
      </div>
      <div class="col-sm-6 col-xs-6 pull-right " style="    text-align: right;    margin-top: -72px;">
        <div class="row-sm-3"> <label>Doc.NO. : MKT/F/01 </label></div>
        <div class="row-sm-3"> <label>Rev.NO. : 00 </label></div>
        <div class="row-sm-3">   <label>Date :
         <?php
  date_default_timezone_set("Asia/Kolkata");
  echo  date("d-m-Y") ;
  ?>  </label></div>
      </div>
    </div>
    <section id="">
      <section id="">
        <div class="container">
       
        <input type="hidden"  name="_token" value="{{ csrf_token() }}" >

          <div class="card">
            <div class="card-body card-padding">  
          
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
            <p>Feedback 1 of 4</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
            <p>Feedback 2 of 4</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
            <p>Feedback 3 of 4</p>
        </div>
         <div class="stepwizard-step">
            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
            <p>Feedback 4 of 4</p>
        </div>
    </div>
</div>

 
  <div class="row setup-content"  id="step-1">
  <form name="feedback-1"  >
        <div class="col-xs-12" style="font-size: 14px;">
            <div class="col-md-12">
            <hr>
                     <h4 style="margin-left: -6px;">ನಿಮ್ಮ ಪರಿಚಯ / About You</h4>
            <hr>


              <div class="w3-row w3-section form-group" style="    margin-left: -6px;">
                <div class="w3-col" style="width:75px">ಹೆಸರು<br>Name</div>
                <div class="w3-rest fg-line ">
                  <input  type="text" required="required"  placeholder="Enter Name" class="form-control in" id="name"  name="name" required autofocus>
                </div>
              </div>
              <div class="w3-row w3-section " style="    margin-left: -6px;">
                <div class="w3-col" style="width:75px">ಲ್ಯಾಂಡ್ ಲೈನ್
<br>Land Line</div>
                <div class="w3-rest fg-line">

                  <input type="text" class="form-control parsley-validated"  data-parsley-type="digits" data-parsley-type-message="only numbers" id="land_line" name="land_line" placeholder="Landline number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' >
                <!--   <input  type="number" class="form-control in" id="land_line"  name="land_line" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /> -->
                  <!-- <input class="w3-input w3-border" name="last" type="text" placeholder="Last Name"> -->
                
                </div>
              </div>

                <div class="w3-row w3-section form-group"  style="    margin-left: -6px;" >
                <div class="w3-col" style="width:75px">ಮೊಬೈಲ್<br>Mobile</div>
                <div class="w3-rest fg-line">
                  <input type="text" class="form-control parsley-validated" name="mobile_no" id="mobile_no" placeholder="Enter mobile Number" data-parsley-type="digits" data-parsley-type-message="only numbers" data-parsley-pattern="^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$" data-parsley-pattern-message="Number is invalid "  required="required" onKeyPress="if(this.value.length==10) return false;"> 
                <!--   <input  type="number"  class="form-control in" id="mobile_no"  name="mobile_no" required="required"  ng-model="number" onKeyPress="if(this.value.length==10) return false;" /> -->
                  
                </div>
              </div>
              
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" style="margin-top: 10px" >Next</button>

              </div>
              </div>
              </form>
              </div>
              <!--first step end-->


    <div class="row setup-content" id="step-2">
    <form name="feedback-2"  >
        <div class="col-xs-12">
            <div class="col-md-12">
              <hr>
               <h4>
ಸೂಕ್ತವಾದ ಅಂಕಣದಲ್ಲಿ <b><i class="zmdi zmdi-check zmdi-hc-fw"></i></b> ಗುರುತು ಹಾಕಿ</h4> 
<small><h5>(Please tick in the boxes provided below)</h5></small>
  <div class="">
              <table class="table table-bordered quality" style="width:100%">
                <tr>
                  <td    style=" width: 25%;"><b>ಮಾಪಕ/Criteria</b></td>

 
                  <td ><b>ಉತ್ಕೃಷ್ಟ Excellent</b></td>
                  <td  ><b>ಉತ್ತಮ Good</b></td>
                  <td  ><b>ಸಾಮಾನ್ಯ Average</b></td>
                  <td ><b>ತೀರಾ ಸಾಮಾನ್ಯ  Poor</b></td>
                </tr>
                <!--       <tbody> -->
                <tr id="qualitys">
                  <td class="cen">1.ಸೇವೆಯ ಗುಣಮಟ್ಟ<br>&nbsp&nbspQuality&nbspof&nbspService</td>
                  <td>
                    <div class="checkbox form-group"> 
                    <div class="checkbox"> <label>
                       <input required="required" type="checkbox" name="qualitys" value="1"  />
                      <i class="input-helper"></i></label> 
                    </div>
                    </div>

                  </td>
                  <td>
                    <div class="checkbox form-group"> <label>
                       <input required="required" type="checkbox" name="qualitys" value="2"  />
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox form-group"> <label>
                      <input required="required" type="checkbox" name="qualitys"  value="3" />
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox form-group"> <label>
                      <input required="required" type="checkbox" name="qualitys" value="4" />
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                </tr>
                <tr id="exclusive">
                  <td class="cen">2.ವೈವಿಧ್ಯಮಯ ಆಭರಣಗಳ ಸಂಗ್ರಹ<br>&nbsp&nbspExclusive&nbspCollection</td>
                  <td>
                    <div class="checkbox form-group"> <label>
                      <input type="checkbox" name="exclusive" value="1" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="exclusive" value="2" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="exclusive" value="3" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="exclusive" value="4" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                </tr>
                <tr id="service">
                  <td class="cen">3.ಮಾರಾಟ ಸಿಬ್ಬಂದಿಗಳ ಸೇವೆ<br>&nbsp&nbspService&nbspOf&nbspsales&nbspStaff</td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="service" value="1" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="service" value="2" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="service" value="3" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="service" value="4" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                </tr>
                <tr id="convenience">
                  <td class="cen">4.ಶೋರೂಂನ&nbspಒಳಗಿನ&nbspಪರಿಸರ<br>&nbsp&nbspಅನುಕೂಲಕರವಾಗಿದೆಯೇ ?<br>&nbsp&nbspConvenience&nbspof&nbspshopping&nbspin <br>&nbsp the&nbspShow&nbsproom.</td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox"  name="convenience" value="1" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="convenience" value="2" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox"  name="convenience" value="3" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="convenience" value="4" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                </tr>
                <tr id="response">
                  <td class="cen">5.ನಿಮ್ಮ&nbspವಿಚಾರಣೆಗಳಿಗೆ&nbspಪ್ರತಿಕ್ರಿಯೆ<br>&nbsp&nbspResponse to enquiry</td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="response" value="1" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="response" value="2" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="response" value="3" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="response" value="4" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                </tr>
                <tr id="quality">
                  <td class="cen">6.ನಮ್ಮಲ್ಲಿಯ&nbspಆಭರಣಗಳ&nbspಗುಣಮಟ್ಟ<br>&nbsp&nbspQuality of ornaments</td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="quality" value="1" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="quality" value="2" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="quality" value="3" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="quality" value="4" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                </tr>
                <tr id="delivery">
                  <td class="cen">7.ಸರಿಯಾದ&nbspಸಮಯಕ್ಕೆ&nbspನೀವು&nbspಸೂಚಿಸಿದ <br>&nbsp&nbsp&nbspಆಭರಣಗಳು ದೊರಕಿವೆಯೇ?<br>&nbsp&nbspDelivery Time</td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="delivery" value="1" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="delivery" value="2" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="delivery" value="3" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="delivery" value="4" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                </tr>
                <tr id="showroom">
                  <td class="cen">8.ಶೋರೂಂನ&nbspಪರಿಸರ<br>&nbsp&nbspShowroom Ambience.</td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="showroom" value="1" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="showroom" value="2" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="showroom" value="3" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="showroom" value="4" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                </tr>
                <tr id="information">
                  <td class="cen">9.ನಮ್ಮ&nbspಉತ್ಪನ್ನಗಳ&nbspಕುರಿತು&nbspನೀಡಿದ&nbspಮಾಹಿತಿ<br>&nbsp&nbspInformation on Product<br>&nbsp&nbsp&nbspFeatures</td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="information" value="1" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="information" value="2" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="information" value="3" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="information" value="4" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                </tr>
                <tr id="attending">
                  <td class="cen">10.ನಿಮ್ಮ&nbspಸಮಸ್ಯೆಗಳಿಗೆ&nbspನಮ್ಮ &nbspಪರಿಹಾರ <br>&nbsp&nbsp&nbsp&nbsp&nbspನಿಮಗೆ&nbspತೃಪ್ತಿಕರವಾಗಿದೆಯೇ?<br>&nbsp&nbsp&nbsp&nbspAttending to Queries</td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="attending" value="1" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="attending" value="2" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="attending" value="3" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                  <td>
                    <div class="checkbox"> <label>
                      <input type="checkbox" name="attending" value="4" required="required">
                      <i class="input-helper"></i></label> 
                    </div>
                  </td>
                </tr>
                <!-- </tbody> -->
              </table>
            </div>
            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" style="    margin-top: 14px;" >Next</button>
            </div>
            </div>
            </form>
            </div>
            <!--second end-->
             <div class="row setup-content" id="step-3">
               <form name="feedback-3"  >
        <div class="col-xs-12">
            <div class="col-md-12" style="font-size: 14px;">
            <hr>
                <h5><ul style="    list-style-type: disc;
    margin-left: 17px;">
                <li><b> ನಿಮ್ಮ ಅಭಿಪ್ರಾಯ ಸಾಮಾನ್ಯ ಅಥವಾ ಅದಕ್ಕಿಂತ ಕೆಳಗಿನದ್ದಾಗಿದ್ದಲ್ಲಿ ನಮ್ಮ ಸೇವೆಯನ್ನು  ಉತ್ತಮಗೊಳಿಸಲು ನಿಮ್ಮ ಸಲಹೆಗಳನ್ನು ದಯವಿಟ್ಟು ಕೊಡಿ.</b></li>
                <li style="padding-top: 4px"></i><b> If the Rating is Average or Below, Please Provide additional information for improvement of our Services.</b></li></ul>
                </h6>
             
            <table class="table table-bordered pad1"  >
              <tr>
                <th style="width:25%">ಸಾಮಾನ್ಯ: General :</th>
                <th >ಹೌದು <br>Yes</th>


                <th >ಇಲ್ಲ<br>No</th>
                <th > ಹೇಳಲು ಸಾಧ್ಯವಿಲ್ಲ Can't&nbspSay</th>
              </tr>
              <!--       <tbody> -->
              <tr id="general1">
                <th class="general">
                ಅಭರಣ ಜುವೆಲ್ಲರ್ಸ್‌   ಪ್ರಥಮ  ಆಯ್ಕೆಯೇ?
                <br>Is&nbspthe&nbspselection&nbspof&nbspour showroom&nbspyour&nbspfirst choice?</th>
                <th>
                  <div class="checkbox"> <label>
                    <input type="checkbox" name="general1" value="1" required="required">
                    <i class="input-helper"></i></label> 
                  </div>
                </th>
                <th>
                  <div class="checkbox"> <label>
                    <input type="checkbox" name="general1" value="2" required="required">
                    <i class="input-helper"></i></label> 
                  </div>
                </th>
                <th>
                  <div class="checkbox"> <label>
                    <input type="checkbox" name="general1" value="3" required="required">
                    <i class="input-helper"></i></label> 
                  </div>
                </th>
              </tr>
              <tr id="general2">
                <th class="general">
                  ನಮ್ಮಲ್ಲಿ  ಆಭರಣಗಳನ್ನು ಖರೀದಿಸುವಂತೆ ನೀವು ಇತರರಿಗೆ ಶಿಫಾರಸು ಮಾಡುತ್ತಿರಾ?
                <br>Will&nbspyou&nbsprecommend&nbspour services/&nbspproducts&nbspto others</th>
                <th>
                  <div class="checkbox"> <label>
                    <input type="checkbox" name="general2" value="1" required="required">
                    <i class="input-helper"></i></label> 
                  </div>
                </th>
                <th>
                  <div class="checkbox"> <label>
                    <input type="checkbox" name="general2" value="2" required="required">
                    <i class="input-helper"></i></label> 
                  </div>
                </th>
                <th>
                  <div class="checkbox"> <label>
                    <input type="checkbox" name="general2" value="3" required="required">
                    <i class="input-helper"></i></label> 
                  </div>
                </th>
              </tr>
            </table>
           
            <div class=" border">
              <h5 class="div1"><b>
                ಬಿಲ್ ಮಾಡುವಾಗ ನೀವು ಕೊಟ್ಟಿರುವ ಫೋನ್ ನಂಬರ್ ನಿಮ್ಮದೆಯೇ?:
              <br>Whether&nbspthe&nbspphone&nbspnumber&nbspgiven&nbspby&nbspyou 
                while&nbspbilling&nbspbelogs&nbspto&nbspyou? :</b>
              </h5>
              <table class="tab" border="0" >
                <tr id="mobile_bill">
                  <th class="pull-right" width="50%"> <br> 
                    <label class="checkbox checkbox-inline m-r-20">
                    <input type="checkbox" name="mobile_bill" value="option1" required="required">
                    <i class="input-helper"></i>    
                    No
                    </label>
                  </th>
                  <th class="pull-right"> <br> <label class="checkbox checkbox-inline m-r-20">
                    <input type="checkbox" name="mobile_bill" value="option2" required="required">
                    <i class="input-helper"></i>    
                    Yes
                    </label>
                  </th>
                </tr>
                <!--  <tr ><th class="pull-right"width="50%">
                  </th>
                  <th class="pull-right">   &nbsp/</th></tr> -->
              </table>
            </div>
         
            <div class=" border">
              <h5 class="div1"><b>
              ನಮ್ಮ ಸೇವೆಯನ್ನು ಇನ್ನಷ್ಟು ಉತ್ತಮಗೊಳಿಸುವ ನಿಟ್ಟಿನಲ್ಲಿ ನಿಮ್ಮ ಸಲಹೆ ಸೂಚನೆಗಳು:<br>
                Your valuable suggestions for improvement our customer&nbspservice.:</b>
              </h5>
              <br>
              <br>
              <div class="form-group cen mar" style="">
                <div class="fg-line ">
                  <textarea class="form-control" id="comment" maxlength="110" name="comment" placeholder="Enter Comment" ></textarea>

                </div>
              </div>
              </div>
            <br>
            <div class=" border">
              <table class="tab mar" border="0" >
                <tr>
                  <th width="8%">
                    <h5>Show&nbspRoom</h5>
                  </th>
                  <th>
                     <div class="col-sm-4">
                    <select class=" mySelect  form-control" required="required"  id="mySelect" name="city" style="    border: 1px solid #e0e0e0;">
                           <option value='' selected style="padding-left: 2px">Show&nbspRoom</option>
                         @foreach(App\Place::orderBy('name')->get() as $city)
                          <!-- $selected = '';
                          if($city->id == 1)    // Any Id
                          {
                              $selected = 'selected="selected"';
                          } -->

                          <option class="state"   value='{{ $city->place_id }}' > {{ $city->name}}</option>

                          @endforeach
                          </select>

                

                          </div>

                   <!--  <div class="dropdown">
                      <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select Place
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li><a href="#">Udupi</a></li>
                        <li><a href="#">CSS</a></li>
                        <li><a href="#">JavaScript</a></li>
                      </ul>
                    </div> -->
                  </th>
                </tr>
              </table>

             
            </div>


         
 <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" style="    margin-top: 14px;" >Next</button>
        
            </div>
      </div>
        </form>

          </div>

<div class="row setup-content"  id="step-4">
  <form name="feedback-4"  >
        <div class="col-xs-12">
            <div class="col-md-12  text-center">
            <hr>
                     <h4 style="margin-left: -6px;">Please Rate The Quality Of Our Service</h4>
            <hr>


             <table class="tab mar" border="0" >
                <tr id="mood">
                  <td style="text-align: center;">
                  <div id="buttons">
                  <ul class="nav  mood" required="required" >
                  

                    <li required="required"  value="1">
                    <div class="col-xs-2 col-sm-2 col-md-2 ">
                      <a href="javascript:void(0)" id="1"><span  class="em" style="color: red">&nbsp&#128547; </span></a><br>
                     &nbsp&nbsp&nbspHate&nbspit
                    </div></li>
                    <li  required="required" value="2">
                    <div class="col-sm-2 col-xs-2 col-md-2">
                    <a href="javascript:void(0)" id="2">  <span  class="em" style="color: orange">&nbsp&#128528; </span></a><br>
                      Dont&nbspLike
                    </div>
                    </li>
                    <li required="required" value="3">
                    <div class="col-sm-2 col-xs-2 col-md-2">
                <a href="javascript:void(0)" id="3">      <span  class="em" style="color: dodgerblue;">&nbsp&#128527;</span></a><br>
                      &nbsp&nbsp&nbspIts&nbspOK
                    </div>
                    </li>
                    <li required="required" value="4">
                    <div class="col-sm-2 col-xs-2 col-md-2">
                <a href="javascript:void(0)" id="4">      <span  class="em" style="color: yellowgreen;">&nbsp&nbsp&#128515; </span></a><br>
                      &nbspLooks&nbspGood
                    </div>
                    </li>
                    <li required="required" value="5">
                    <div class="col-sm-2 col-xs-2 col-md-2">
                   <a href="javascript:void(0)" id="5">   <span  class="em" style="color: #175f05">&nbsp&#128525; </span></a><br>
                     &nbsp&nbspLove&nbspit 
                    </div>
                    </li>
                    
                    </ul>
                  </td>
                  </tr>
              </table>
             </div>
              
    <div class="parsley-required error1"  style="color:red;    text-align: center;">&nbsp&nbsp</div>
               <br>
                <button class="btn btn-success btn-lg pull-right " ng-click="save()" type="button">Finish!</button>
              </div>
              </div>
              </form>
              </div>
              <!--first step end-->
    


</div>
</div>
</div>
</section>
</section>



    <div class="modal fade  " id="modalDefault" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog dialog" style="text-align: center;width:80%">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title">Enter&nbspthe&nbspStaff&nbspcode</h3>
              </div>
              <div class="modal-body">
              <div class="form-group">
                <h3 ><label>Staff&nbspId:&nbsp 
                <br/> <br/><input type="text" name="Staff_id" id="Staff_id" required autofocus style="border: 1px solid ;"></label></h3>

              </div>
                   
              </div>
                
              <div class="modal-body">
              <div class="form-group">
                <h3 style="    margin-top: -35px;
    margin-bottom: -20px;"><label>Bill&nbspNumber:&nbsp 
                <br/> <br/><input type="text" name="Bill_number" id="Bill_number" required autofocus style="border: 1px solid ;"></label></h3>

              </div>
                   
              </div>
              <div class="modal-footer">
                 <button type="button" id="btnSubmit" name="subbmit" class="btn btn-info" style=" width:75%">Submit</button>  
               <!--    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
              </div>
          </div>
      </div>
  </div> 
 
   <script src="{{asset('Feedback/feedback.js')}}"></script>
   <script src="{{asset('Assets/parsley/parsley.js')}}"></script>
<script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script>
 
   <script type="text/javascript">
     $("input:checkbox").on('click', function() {
  // in the handler, 'this' refers to the box clicked on
  var $box = $(this);
  if ($box.is(":checked")) {
    // the name of the box is retrieved using the .attr() method
    // as it is assumed and expected to be immutable
    var group = "input:checkbox[name='" + $box.attr("name") + "']";
    // the checked state of the group/box on the other hand will change
    // and the current value is retrieved using .prop() method
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});
   </script>
    <script type="text/javascript">
      if ({{$popup}}) {
        $(window).on('load',function(){
            $('#modalDefault').modal('show');
            
        });
      }
  
          $("#btnSubmit").button().click(function(){
             var procede=true;
             if($('[name=Bill_number]').val()=="")
            {
               $('#Bill_number').css('border-color', 'red');  
               procede=false;
               return false;
             
              
             
              // $('#modalDefault').modal('show');
               
            }else {
                procede=true;
                var data=$('[name=Bill_number]').val();
                   $('#show_bill_number').val(data);
            }
            
            if($('[name=Staff_id]').val()=="")
            {
               $('#Staff_id').css('border-color', 'red');  
               procede=false;
               return false;
             
              
             
              // $('#modalDefault').modal('show');
               
            }
            else
            {
            procede=true;
            var data=$('[name=Staff_id]').val();

            $('#Staff').val(data);
           
          }
          if(procede){ 
           alert($('#show_bill_number').val());
           $('#modalDefault').modal('hide');
           }
           
           
             });


$(function() {
      $( 'ul.nav li' ).on( 'click', function() {
            $( this ).parent().find( 'li.active' ).removeClass( 'active' );
            $( this ).addClass( 'active' );
      });
});




    </script>


@endsection
