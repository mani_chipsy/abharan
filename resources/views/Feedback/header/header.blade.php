<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf_token" content="{{ csrf_token() }}" />
	<title>{{config('app.name')}}</title>
	<!-- Vendor CSS -->
	<link href="/Feedback/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
	<link href="/Feedback/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
	<link href="/Feedback/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/Feedback/css/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- CSS -->
	<link href="/Feedback/css/bootstrap.css" rel="stylesheet">
	<link href="/Feedback/css/main.css" rel="stylesheet">
	<link href="/Feedback/css/app.min.1.css" rel="stylesheet">
	<link href="/Feedback/css/app.min.2.css" rel="stylesheet">
		<link href="/Feedback/css/app.css" rel="stylesheet">
	<style>
		table, th, td {
		/* border: 1px solid black;*/
		}
	</style>
<!-- <link rel="stylesheet" href="{{asset('Assets/parsley/parsley.css')}}"> -->
	<link href="{{asset('Assets/preloader/preloader.css')}}" rel="stylesheet">
	
	<link href="{{asset('Backend/assets/css/generic.css')}}" rel="stylesheet">
	
	<style type="text/css">
	.active{
   background-color: #2d9af4;
    color: white;

}
</style>
	<script src="{{asset('Assets/jquery/jquery-3.2.1.min.js')}}"></script>
	<script src="{{asset('Assets/angular/angular.js')}}"></script>
	<script src="{{asset('Assets/angular/route.js')}}"></script>
	<script src="{{asset('Assets/angular/ui-bootstrap-tpls-2.5.0.min.js')}}"></script>
	<script src="{{asset('Assets/bootstrap/js/bootstrap.js')}}"></script>
<!-- 	<script src="{{asset('Assets/parsley/parsley.js')}}"></script> -->
	<script src="{{asset('Assets/preloader/preloader.js')}}"></script>
	
	<script src="/Feedback/vendors/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="/Feedback/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

</head>