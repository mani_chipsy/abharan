<script src="/Feedback/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
<script src="/Feedback/vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="/Feedback/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
<script src="/Feedback/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
<script src="/Feedback/vendors/bower_components/autosize/dist/autosize.min.js"></script>

<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
<script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
<![endif]-->

<script src="/Feedback/js/functions.js"></script>
<script src="/Feedback/js/demo.js"></script>
<script src="/Feedback/js/app.js"></script>
