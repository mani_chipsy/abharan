<head>
  <meta charset="UTF-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <link href='https://fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Raleway:400,600,500,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Belleza' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel='stylesheet' type='text/css'>
  <meta name="description" content="Abharan, abharan, udupi, Jewellers, Jeweller, Jevellers, Jewelers, Jeveler, Jevelers, gold, silver, bangles, necklaces, platinum, chain, ring, rings, gems, pearl, mangalore, diamond, daimond, online, coa,cod, purchase, best, good, cheap, better, eshop, onlineshop, trust, daily, rates  " />
 
  <title>Abharan Jewellers</title>
  <link rel="icon" href="/favicon.ico" sizes="16x16" type="image/ico">
  <link href="{{asset('Assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('Frontend/assets/css/jquery.camera.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('Frontend/assets/css/jquery.fancybox-buttons.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('Frontend/assets/css/cs.animate.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('Frontend/assets/css/application.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('Frontend/assets/css/swatch.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('Frontend/assets/css/jquery.owl.carousel.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('Frontend/assets/css/jquery.bxslider.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('Frontend/assets/css/bootstrap.min.3x.css')}}" rel="stylesheet" type="text/css" media="all" />
  <!-- <link rel="stylesheet" href="{{asset('Assets/bootstrap/css/bootstrap.css')}}" > -->

  <!-- <link rel="stylesheet" href="{{asset('Assets/bootstrap/css/bootstrap-theme.css')}}"> -->
  
  <link href="{{asset('Frontend/assets/css/cs.bootstrap.3x.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('Frontend/assets/css/cs.global.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('Frontend/assets/css/cs.style.css?v=125')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('Frontend/assets/css/cs.media.3x.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('Frontend/assets/css/generic.css')}}" rel="stylesheet" type="text/css" media="all" />
  <!-- <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> -->
  <link href="{{asset('Assets/preloader/preloader.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('Assets/parsley/parsley.css')}}">

  <!-- Newly Added -->
  <link rel="stylesheet" href="{{asset('Frontend/assets/css/tabmenu.css')}}">


  <script src="{{asset('Frontend/assets/js/jquery-1.9.1.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/jquery.imagesloaded.min.js')}}" type="text/javascript"></script>
  <!-- <script src="{{asset('Assets/bootstrap/js/bootstrap.js')}}" type="text/javascript"></script> -->
  <script src="{{asset('Frontend/assets/js/bootstrap.min.3x.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/jquery.easing.1.3.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/jquery.camera.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/jquery.mobile.customized.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/cookies.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/modernizr.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/cs.optionSelect.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/cs.customSelect.js')}}" type="text/javascript"></script>  
  <script src="{{asset('Frontend/assets/js/application.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/jquery.owl.carousel.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/jquery.bxslider.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/skrollr.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/jquery.fancybox-buttons.js')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/jquery.zoom.js')}}" type="text/javascript"></script>
  <!-- <script src="{{asset('Frontend/assets/js/jquery.currencies.min.js')}}" type="text/javascript"></script> -->
  <script src="{{asset('Frontend/assets/js/cs.script.js?v=111')}}" type="text/javascript"></script>
  <script src="{{asset('Frontend/assets/js/cs.global.js')}}" type="text/javascript"></script>
  <script src="{{asset('Assets/angular/angular.js')}}"></script>
  <script src="{{asset('Assets/angular/route.js')}}"></script>
  <script src="{{asset('Assets/angular/ui-bootstrap-tpls-2.5.0.min.js')}}"></script>
  <script src="{{asset('Assets/angular/angular-sanitize.js')}}" type="application/javascript"></script>

  <script async src="{{asset('Frontend/assets/js/analytics.js')}}"></script>
  <script type="text/javascript" async src="{{asset('Frontend/assets/js/trekkie.storefront.min.js')}}"></script>
  <script async src="{{asset('Frontend/assets/js/fbds.js')}}"></script>
  <script src="{{asset('Frontend/assets/js/blowup.js')}}"></script>
  <script src="{{asset('Assets/preloader/preloader.js')}}"></script>
  <script src="{{asset('Assets/parsley/parsley.js')}}"></script>
 <link href="/css/atstyle.css" rel="stylesheet">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-105612285-1', 'auto');
  ga('send', 'pageview');

</script>




</head>