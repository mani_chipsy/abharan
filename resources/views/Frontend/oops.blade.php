@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent" ng-controller='OopsController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Oops!</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content">
				<div class="container">
					<div class="row ">
						<div id="page-header" class="col-md-24">
							<h1 id="page-title" class="text-center text-danger">Oops!</h1>
						</div>
						<div id="col-main" class="col-md-24 cart-page content">
			            	<p class="def-height text-center text-danger">
			            		Sorry, online payment service is not available right now.
			            	</p>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection