@extends('Frontend.layouts.home_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('Frontend/assets/css/custom.css')}}">
<link href="{{asset('Assets/lightbox/ekko-lightbox.min.css')}}" rel="stylesheet">
<script src="{{asset('Assets/lightbox/ekko-lightbox.js')}}"></script>
<div id="content-wrapper-parent" >
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Collections</span>
						</div>
					</div>
				</div>
			</div>
			<section class="pb-95">
		      <div class="home-newproduct back_pin chg-back_pin">
		        <div class="container">
          <div class="group_home_products row">
            <div class="col-md-24">
              <div class="home_products">
                <h6 class="general-title">Pritty Collections</h6>
                  <p> <i class="fa fa-phone" aria-hidden="true"></i> FOR ENQUIRIES CALL OUR TOLL FREE NUMBER &nbsp;1800 5999 777 &nbsp;<a class="clik-ank" href="/contact-us"> Click Here To Order Now</a> </p>
                
                <div class="home_products_wrapper">
                  <div id="sandBox-wrapper" class="group-product-item row collection-full">
                    <ul id="sandBox" class="list-unstyled">
                      <div class="about-detail">
                        <div class="row">
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/0210.jpg?t=123" data-toggle="lightbox">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/0210.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/0200.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/0200.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/0186.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/0186.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/0044.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/0044.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/0041.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/0041.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/0020.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/0020.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HU34.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HU34.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HU24.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HU24.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT988.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT988.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT983.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT983.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT926.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT926.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT822.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT822.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT812.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT812.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT772.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT772.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT731.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT731.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT710.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT710.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT584.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT584.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT566.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT566.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT555.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT555.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT546.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT546.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT503.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT503.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT494.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT494.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT493.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT493.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT465.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT465.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT292.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT292.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT256.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT256.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT225.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT225.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT218.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT218.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT169.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT169.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT140.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT140.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HT129.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HT129.jpg?t=123"  >
                            </a>
                          </div>
                          <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                            <a class="thumbnail"  data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/PRITTY COLLECTION/5HS997.jpg?t=123" data-target="#image-gallery">
                            <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/Pritty/5HS997.jpg?t=123"  >
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close"  data-dismiss="modal">
                              <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                              <h4 class="modal-title" id="image-gallery-title"></h4>
                              <ul class="pager">
                                <li class="previous" id="show-previous-image"><a href="#">Previous</a></li>
                                <li class="next" id="show-next-image"><a href="#">Next</a></li>
                              </ul>
                            </div>
                            <div class="modal-body">
                              <img id="image-gallery-image" class="img-responsive" src="">
                            </div>
                          </div>
                        </div>
                      </div>
                    </ul>
                     <p style="text-align: center;">
                          <button type="button" class="btn btn-info" onclick="location.href='/contact-us';"> Click Here To Order Now</button>
                          </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
		      </div>
		    </section>
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
          // If you can't/don't want to set the href property of an element
          // data-remote="http://www...."
          remote: '',
          // For grouping elements
          // data-gallery="galleryname"
          gallery: '',
          // If you have multiple galleries per page, this will restrict the gallery items to the parent that matches this selector.
          gallery_parent_selector: 'document.body',
          // CSS classes for navigation arrows
          left_arrow_class: '.glyphicon .glyphicon-chevron-left',
          right_arrow_class: '.glyphicon .glyphicon-chevron-right',
          // Enable the navigation arrows
          directional_arrows: true,
          // Force the lightbox into image/YouTube mode.
          // image|youtube|vimeo
          // data-type="(image|youtube|vimeo)"
          type: null,
          // Always show the close button, even if no title is present
          always_show_close: true,
          // Don't show related videos when finished playing
          no_related: false,
          // Scale the height as well as width
          scale_height: true,
          // Message injected for loading
          // loadingMessage: 'Loading...',           
          // Callback functions
          onShow: function() {},
          onShown: function() {},
          onHide: function() {},
          onHidden: function() {},
          onNavigate: function() {},
          onContentLoaded: function() {}
      });
  });
</script>
@endsection