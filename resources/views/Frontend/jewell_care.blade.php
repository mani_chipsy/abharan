@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent">
  <div id="content-wrapper">
    <div id="content" class="clearfix">
      <div id="breadcrumb" class="breadcrumb">
        <div itemprop="breadcrumb" class="container">
          <div class="row">
            <div class="col-md-24">
              <a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
              <span>/</span>
              <span class="page-title">Jewellery Care</span>
            </div>
          </div>
        </div>
      </div>

      <section class="content">
        <div class="container">
          <div class="row">
            <div id="page-header" class="col-md-24">
              <h1 id="page-title">JEWELLERY CARE</h1>
            </div>
            <div id="col-main" class="blog blog-page col-xs-24 col-sm-24 col-content col-content ">
              <!-- <h2 class="subhdng01">JEWELLERY CARE</h2> -->
              <hr>
              <div class="col-sm-6" style="border-right:1px solid #ddd; padding-right:20px;  margin-right: 26px;">
     <!--          <aside id="column-left" class="col-sm-6 hidden-xs"> -->
    <div class="list-group">
 <ul class="nav tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab"><span style="color:#a07936">Gold Jewellery Care</a></li>
                    <hr>
                    <li class=""><a href="#tab2" data-toggle="tab"><span style="color:#a07936">Diamond Care</span></a></li>
                    <hr>
                    <li class=""><a href="#tab3" data-toggle="tab"><span style=" color:#a07936">Coloured Gemstone Care</span></a></li>
                    <hr>
                    <li class=""><a href="#tab4" data-toggle="tab"><span style=" color:#a07936">Platinum Care</span></a></li>
                    <hr>
                    <li class=""><a href="#tab5" data-toggle="tab"><span style=" color:#a07936">Cultured Pearl Care</span></a></li>
                    <hr>
                  </ul>

        </div>
       <!--  </aside> -->
               <!--  <nav class="nav-sidebar">
                  <ul class="nav tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab"><span style="font-size:12px; color:#a07936">Gold Jewellery Care</a></li>
                    <hr>
                    <li class=""><a href="#tab2" data-toggle="tab"><span style="font-size:12px; color:#a07936">Diamond Care</span></a></li>
                    <hr>
                    <li class=""><a href="#tab3" data-toggle="tab"><span style="font-size:12px; color:#a07936">Coloured Gemstone Care</span></a></li>
                    <hr>
                    <li class=""><a href="#tab4" data-toggle="tab"><span style="font-size:12px; color:#a07936">Platinum Care</span></a></li>
                    <hr>
                    <li class=""><a href="#tab5" data-toggle="tab"><span style="font-size:12px; color:#a07936">Cultured Pearl Care</span></a></li>
                    <hr>
                  </ul>
                </nav> -->
              </div>
              <!-- tab content 
                <h2 class="subhdng01">JEWELLERY CARE</h2>-->
              <div class="tab-content product_center" style="float: none">
                <div class="tab-pane active text-style price_section" id="tab1">
                  <hr>
                  <h3 class="price_section_inner">GOLD JEWELLERY CARE</h3>
                  <hr>
                  <div class="col-sm-17">
                    <div class="txt_contents_01 clearfix">
                      <p style=" font-size:14px;">
                        Though we have various variety of jewellery, general cleaning is common for all of them.
                      </p>
                      <br>
                    <!--   <h5 >Jewellery Efficiently:</h5> -->
                      <strong>Though Gold jewellery is available in various Karats the general cleaning is common for all varieties.
                      Following are some tips to be taken to handle the gold jewellery efficiently: </strong>
                      <br>
                      <br>
                      <p><strong> •    </strong><i class="non-italic">Gold jewellery should be removed before a shower. This is because soap can cause a film to form on gold jewellery, making it appear dull and dingy.</i></p>
                      <p><strong> •    </strong><i class="non-italic">For home cleaning of gold jewellery, we can remove tarnish by using soap and water mixed with a few drops of ammonia followed by careful brushing with a soft bristle brush. After the brushing, simply rinse with lukewarm water and allow drying.</i></p>
                      <p><i class="non-italic"><strong> •   </strong> There are many brands available in the market which serves to clean the gold jewellery. Soft chamois cloth is also a good option. One can retrieve complete information from the Jeweller regarding gold cleaning.</i></p>
                      <p><i class="non-italic"><strong> •    </strong> Your Gold jewellery can be permanently damaged and discoloured especially at high temperatures. Also, care should be taken not to use gold jewellery while using chlorine bleach or while in a swimming pool or hot bath tub.</i></p>
                      <p><i class="non-italic"><strong>•    </strong> Grease from gold jewellery can be removed by dipping the jewellery into plain rubbing alcohol. However care should be taken while cleaning gold jewellery with coloured gemstone pieces and consultation with Jeweller will be of help.</i></p>
                      </p> 
                    </div>
                  </div>
                  <hr>
                </div>
                <div class="tab-pane text-style  price_section" id="tab2">
                  <hr>
                 <h3 class="price_section_inner ">DIAMOND CARE</h3>
                  <hr>
                  <br>
                  <div class="col-sm-17">
                    <strong>A diamond is the hardest form of carbon with incredible durability. However, a strong blow to an edge can still chip it. Hence it is advised not to wear diamond during strenous physical activities.</strong>
                    <br>
                    <br>
                    <p><strong>•    </strong>Diamond is susceptible to Chlorine. Hence diamond should be kept away from bleach or other household chemicals to avoid discolouration and damage. Also it is advised not to wear diamond jewellery while entering into pool or hot bath tub.</p>
                    <p><i class="non-italic"> <strong>•    </strong>Diamonds should be regularly cleaned using commercial jewellery cleaner, a mix of ammonia and water, or a mild detergent. Use a soft brush to wipe dust or dirt from under the setting.</i></p>
                    <p><i class="non-italic"><strong> •   </strong>Diamonds should be handled by its edges. Also it is better to avoid touching clean diamonds with fingers.
                    </i>
                    </p>
                  </div>
                </div>
                <div class="tab-pane text-style price_section" id="tab3">
                  <hr>
                 <h3 class="price_section_inner "> COLOURED GEMSTONE CARE</h3>
                  <hr>
                  <br>
                  <div class="col-sm-17">
                    <p><i class="non-italic"><strong>Listed below are some of the general care and cleaning rules that apply to all coloured gemstone jewellery. </strong></i></p>
                    <p><i class="non-italic"><strong> •    </strong>After use, the precious gemstone jewellery should be wiped thoroughly with a clean soft, slightly damp cloth. This is because the gemstone's luster will be enhanced if it is ensured that jewellery is clean before storage.</i></p>
                    <p><i class="non-italic"><strong> •    </strong>Gemstone or Gemstone studded jewellery should be stored individually in soft pouches provided by the Jeweller.</i></p>
                    <p><i class="non-italic"><strong> •    </strong>Exposure to salt water or harsh chemicals, such as chlorine or detergents deteriorates the quality of the jewellery. These chemicals will eventually erode the finish and polish of gemstones.</i></p>
                    <p><i class="non-italic"><strong> •    </strong>Care should be taken to wear jewellery after application of cosmetics, hair spray, perfume, etc. Perspiration may cause jewellery to become dull.</i></p>
                    <p><i class="non-italic"><strong> •    </strong>Also gemstone jewellery should not be subjected to sudden temperature changes.</i></p>
                    <p><i class="non-italic"><strong> •    </strong>Extra precautions should be taken while having active lifestyle. For example, Emeralds are brittle and should be avoided when doing household chores or any other activity where the stone could be hit or damaged.</i></p>
                    <p>Careful precautions should be taken while using ultrasonic cleaners. Some gemstones are fragile and can be damaged by ultrasonic cleaners. One can retrieve complete information from the Jeweller regarding coloured gemstone care.</p>
                    </p>
                  </div>
                </div>
                <div class="tab-pane text-style price_section" id="tab4">
                  <hr>
                   <h3 class="price_section_inner "> PLATINUM CARE</h3>
                  <hr>
                  <br>
                  <div class="col-sm-17">
                    <p> 
                      <i class="non-italic"><strong>Platinum, unlike gold is more durable, and does not get easily tarnished and discoloured by chlorine and other chemicals. However, proper care will ensure the natural sheen of the metal. Following are some tips to be taken to handle the Platinum.  </strong>
                      </i>
                    </p>
                    <p> 
                      <i class="non-italic"><strong> •    </strong>Platinum jewellery can be cleaned with the conventional ways of cleaning gold jewellery. However, a professional cleaning every six months is always advisable for platinum jewellery.
                      </i>
                    </p>
                    <p> 
                      <i class="non-italic"><strong> •    </strong>Platinum jewellery should be treated with care and stored separately. Care should be taken not to allow pieces to touch each other to avoid scratches.
                      </i>
                    </p>
                    <p> <i class="non-italic"><strong> •   </strong>Special care should be rendered if platinum is set with diamonds or other precious stones, as these materials can be more susceptible to damage.
                      </i>
                    </p>
                  </div>
                </div>
                <div class="tab-pane text-style price_section" id="tab5">
                  <hr>
                 <h3 class="price_section_inner "> CULTURED PEARL CARE
                  </h3>
                  <hr>
                  <br>
                  <div class="col-sm-17">
                    <p> 
                      <i class="non-italic">
                      <strong> •    </strong>Pearl Jewellery should always be used after the application of cosmetics. After usage the pearl jewellery should be wiped with soft damp cloth.
                      </i>
                    </p>
                    <p> 
                      <i class="non-italic">
                      <strong> •    </strong>Pearl Jewellery can also be washed with mild soap and water. Chemicals or any other solvents should be avoided as these substances can damage the pearls.
                      </i>
                    </p>
                    <p> <i class="non-italic">
                      <strong> •   </strong>Also care should be taken not to toss pearl jewellery carelessly into a purse, bag or jewel box.
                      <br>
                      <br>
                      <strong> •   </strong>A pearl's surface is soft and can be scratched by hard metal edges or by harder gemstones of other jewellery pieces. Hence care must be taken to store it separately.</i>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
@endsection