@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent">
      <div id="content-wrapper">
        <!-- Content -->
        <div id="content" class="clearfix">
          <div id="breadcrumb" class="breadcrumb">
            <div itemprop="breadcrumb" class="container">
              <div class="row">
                <div class="col-md-24">
                  <a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
                  <span>/</span>
                  <span class="page-title">FAQ'S
                  Policies
                  </span>
                </div>
              </div>
            </div>
          </div>
          <section class="content">
            <div class="container">
              <div class="row">
                <div class="container">
                  <div class="row">
                    <div id="page-header" class="col-md-24">
                      <h1 id="page-title">FAQ's</h1>
                    </div>
                  </div>
                </div>
                <div id="col-main" class="contact-page clearfix">
                  <div class="group-faq clearfix">
                    <div class="content">
                      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                          <div class="panel-heading active" role="tab" id="headingTwo">
                            <h4 class="panel-titlee">
                              <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                              1. Unique properties of gold.
                              </a>
                            </h4>
                          </div>
                          <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="true">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Gold is a metal. Its scientific name is Aurum (Au).<br /> 
                                Au = Aurum,<br /> 
                                Atomic No. 79,<br /> 
                                Melting Point 1064.43°C,<br /> 
                                Boiling Point 2807°C.<br /> 
                                Gold was the first metal widely known to our species. It was first discovered thousands of years ago as shining, yellow nuggets. Gold became a part of every human culture as it is associated with God, immortality, and wealth. Gold is extracted from the mines that are found in different parts of the world. South Africa is one such country where Gold is available in abundance. In India, Kolar and Raichur gold mines are famous places where gold is mined.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              2. How was gold associated with dignity and worship?
                              </a>
                            </h4>
                          </div>
                          <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Gold, right from prehistoric days was valued high. Many battles were fought for gold. This clearly indicates that gold was owned by the powerful and well-connected, or made into objects of worship, or used to decorate sacred locations.</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="headingFour">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                              3. Which substances are used in purification of gold?
                              </a>
                            </h4>
                          </div>
                          <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">A combination of Hydrochloric Acid and Nitric Acid (Aqua regia – Royal water) is used for purifying the gold. For polishing gold Potassium Cyanide is used.</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="headingFive">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                              4. Why do gold prices keep fluctuating?
                              </a>
                            </h4>
                          </div>
                          <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Gold is traded at International Market. Hence according to the International Market situation, the prices of gold keep fluctuating.<br /> 
                                More Demand and Scarcity of supply leads to price hike and vice-a-versa.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="headingSix">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                              5. What is KDM with reference to gold?
                              </a>
                            </h4>
                          </div>
                          <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">KDM is a metal known as Cadmium (Kadium). The scientific code is 48 Cd 112.41.<br /> 
                                Cd = Cadmium,<br /> 
                                Atomic No. 48,<br /> 
                                Melting Point 320.9°C,<br /> 
                                Boiling Point 765°C.<br /> 
                                <br /> 
                                Some of the gold jewellery in the market has a KDM stamp on it, which means that it was soldered with Cadmium. Cadmium (Kadium, KDM) is used in soldering gold jewellery for liquidity and melting at lower temperatures. However, cadmium when melted releases toxic fumes and affects the purity of jewellery and many countries have banned use of cadmium solder. <br /> 
                                <br /> 
                                Nowadays, Zinc (branded ZNC) solders are used in America as well as European countries for soldering gold. Zinc too evaporates during summer just like KDM. But it does not cause any harm to the artisans. Research is underway for using Zinc as a replacement for KDM. <br /> 
                                <br /> 
                                Zinc is a metal with scientific code 30 Zn 65.38.<br /> 
                                Zn = Zinc,<br /> 
                                Atomic No. 30,<br /> 
                                Melting Point 419.58°C,<br /> 
                                Boiling Point 907°C.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="headingSeven">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                              6. How do we measure the purity of gold?
                              </a>
                            </h4>
                          </div>
                          <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">The carat/karat (abbreviation – ct) is a measure of purity of gold and platinum alloys. In the United States and Canada, the spelling karat is now solely used for the measure of purity, while carat solely refers to the measure of mass weight. As a measure of purity, one karat is purity by mass:<br /> 
                                <br /> 
                                X=24 Mg/Mm<br /> 
                                <br /> 
                                Where,<br /> 
                                X is the karat rating of the material, Mg is the mass of pure gold or platinum the material, and Mm is the total mass of the material. <br /> 
                                <br /> 
                                Therefore 24 karat gold is pure (100% Au w/w), 18 karat gold is 75% gold, 12 karat gold is 50% gold, and so forth.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading8">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapseSeven">
                              7. What is millesimal fineness?
                              </a>
                            </h4>
                          </div>
                          <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">The karat system is increasingly being complemented or superseded by the millesimal fineness system in which the purity of precious metals is denoted by parts per thousand of pure metal in the alloy.<br /> 
                                <br /> 
                                Historically, in England the karat was divisible into four grains, and the grain was divisible into four quarts. For example, a gold alloy of 381/384 fineness (that is, 99.2% purity) could have been described as being 23 karat, 3 grain, 1 quart gold.
                              </p>
                              The most common karats used for gold in bullion, jewellery and goldsmiths are: <br /> 
                              <p style="font-size:16px; text-align:justify; line-height:25px">• 24 karat (millesimal fineness 999) <br /> 
                                • 22 karat (millesimal fineness 916) <br /> 
                                • 20 karat (millesimal fineness 833) <br /> 
                                • 18 karat (millesimal fineness 750) <br /> 
                                • 15 karat (millesimal fineness 625) <br /> 
                                • 14 karat (millesimal fineness 585) <br /> 
                                • 10 karat (millesimal fineness 417) <br /> 
                                • 9 karat (millesimal fineness 375)  
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading9">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapseSeven">
                              8. Distinguish between 22-karat (22k) and 24-karat (24k) gold.
                              </a>
                            </h4>
                          </div>
                          <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">24-karat is often referred to as 'fine gold'. It is 99.0% to 99.9% pure gold depending on a country's laws. When we say 24kt or 24k gold jewelry, we mean that all the 24 parts in the gold are just pure gold without traces of any other metal(s) or 99.9 percent pure.</p>
                              <p style="font-size:16px; text-align:justify; line-height:25px">22-karat or &lsquo;Skin purity&rsquo; of gold jewellery signifies that 22 parts of the jewellery is gold and the balance 2 parts are some other metal(s) or after melting, the purity of the gold jewellery will be 22k (22-karat) or 91.67% of pure gold. This symbol or stamp is very popular on the gold jewellery business in Asian countries like India, Pakistan, Bangladesh, Nepal, Yemen, and Gulf Countries.</p>
                              There are few ways to find out the difference between 22 and 24-karat gold. <br /> 
                              They are: <br /> 
                              <p style="font-size:16px; text-align:justify; line-height:25px">* Difference in weight (24karat gold&rsquo;s weigh 19.4gm/cm3)<br /> 
                                * Using Karat machine. 
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading10">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="false" aria-controls="collapseSeven">
                              9. What does &lsquo;wastage&rsquo; in jewellery making mean?</h2>
                              </a>
                            </h4>
                          </div>
                          <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Goldsmiths prepare jewellery from gold bars. During the process of jewellery making some gold particles will be leftover as residue. This residual gold is known as wastage in gold making.<br /> 
                                <br /> 
                                For example, by using 115g of gold, the goldsmith will be able to prepare 100g of jewellery. The 15g of residual gold is wastage. Goldsmiths consider this gold as the reward for the work done and uses it for the future requirements. <br /> 
                                <br /> 
                                There is no relation between wastage and purify of the gold. We come across wastage even in KDM.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading11">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="false" aria-controls="collapseSeven">
                              10. Why gold is found in different colours?
                              </a>
                            </h4>
                          </div>
                          <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">The metal component in the alloy mix used in the gold determines the colour of gold. The most popular is yellow gold, followed by white gold and rose gold, while rarely gold is also available in colours such as bronze, red and lime gold. <br /> 
                                <br /> 
                                Yellow gold is made by mixing pure gold with alloy metals such as copper and zinc. Rose gold is made using a mix of pure gold with alloys including copper which provide the rose-reddish colour. Due to the properties of the alloy mix, the colour of yellow gold and rose gold will not chip, fade or wear off with usage. White gold is an alloy of gold and some white metals such as silver and palladium. Traditionally, nickel is used in white gold. Natural white gold is light grey in colour. New white gold is given a Rhodium coating which will make gold look whiter. To keep a white gold ring at its best, it should be rhodium plated in 12 to 18 month periods.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading12">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="false" aria-controls="collapseSeven">
                              11. What are the methods of gold polishing?
                              </a>
                            </h4>
                          </div>
                          <div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Gold can be polished in two different methods.<br /> 
                                1.   Hot Process: Hot process is also known as electroplating process. In this process 24-karat gold sheets are attached to the anode end and gold ornament which is to be polished is attached to the cathode end. When these electrodes (cathode and anode) are dipped in a container filled with potassium cyanide, 24-karat gold in the anode melts. Because of attraction, the gold gets attached to the surface of gold ornament in the cathode. It means 5 to 6 microns of gold layer gets attached on the ornament&rsquo;s surface. In this way gold ornaments are polished.<br /> 
                                <br /> 
                                In this process the weight of the ornament remains the same.
                              </p>
                              <p style="font-size:16px; text-align:justify; line-height:25px">     1 Milligram = 1000 micros. </p>
                              <br /> 
                              <p style="font-size:16px; text-align:justify; line-height:25px">2.  Cold Process: When a gold ornament which is to be polished is dipped in a liquid called Aqua regia, the gold melts in the liquid. The outer layer of the gold melts and we can see the shiny inner layer.</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading13">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse13" aria-expanded="false" aria-controls="collapseSeven">
                              12. Why is gold allergic to some people?
                              </a>
                            </h4>
                          </div>
                          <div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Gold is believed to be good for health. People do use it as energy booster. During the Coating process the gold jewellery is dipped in Potassium Cyanide Solution. After this process it is cleaned properly. But in rare cases, Potassium Cyanide would be left out in the jewellery which may cause allergic reaction in some people.</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading14">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse14" aria-expanded="false" aria-controls="collapseSeven">
                              13. What is &lsquo;Karang&rsquo; or discolouration of gold? 
                              </a>
                            </h4>
                          </div>
                          <div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14" aria-expanded="false">
                            <div class="panel-body">
                              <p class="first-p"> 
                                After the purchase, over a period of time gold loses its natural sheen due to geographical and climatic reasons. This phenomenon is termed as &lsquo;Karang&rsquo;. Similarly, when gold comes into contact with body sweat (acidic in nature) and soap water, results in oxidation and subsequent discolouration of gold turning it black in colour.
                              </p>
                              </p> 
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading14">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse14" aria-expanded="false" aria-controls="collapseSeven">
                              14. Does daily usage of gold jewellery lead to corrosion of the metal? 
                              </a>
                            </h4>
                          </div>
                          <div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14" aria-expanded="false">
                            <div class="panel-body">
                              <p class="first-p"> 
                                All metals corrode due to divergence and gold is no exception. Over a period of time, gold loses some of its weight due to corrosion. Chemical agents like soap water, bleaching powder, perfumes, etc. also enhance the corrosion of gold. 
                              </p>
                              <p class="first-p"> 
                                After the purchase, over a period of time gold loses its natural sheen due to geographical and climatic reasons. This phenomenon is termed as &lsquo;Karang&rsquo;. Similarly, when gold comes into contact with body sweat (acidic in nature) and soap water, results in oxidation and subsequent discolouration of gold turning it black in colour.
                              </p>
                              </p> 
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading15">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse15" aria-expanded="false" aria-controls="collapseSeven">
                              15. How do we differentiate between an Imitation and Gold Jewellery?</h2> 
                              </a>
                            </h4>
                          </div>
                          <div id="collapse15" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">We can easily distinguish gold from imitation by using karat machine. Generally, imitation is lightweight and loses its colour while checking against tone.</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading16">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse16" aria-expanded="false" aria-controls="collapseSeven">
                              16. Unique properties of silver              </a>
                            </h4>
                          </div>
                          <div id="collapse16" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Silver is a metal. Scientific name for silver (Latin argentum) is 47 Ag 107.868. <br /> 
                                Atomic No. 47,<br /> 
                                Melting Point 961.93°C,<br /> 
                                Boiling Point 2212°C.<br /> 
                                Silver is available in mines. The cost involved in its production and transportation is trivial and hence the price of this metal is less when compared with gold. 
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading17">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse17" aria-expanded="false" aria-controls="collapseSeven">
                              17. What is electroplating of silver? 
                              </a>
                            </h4>
                          </div>
                          <div id="collapse17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17" aria-expanded="false">
                            <div class="panel-body">
                              Electroplating is the mechanism used to give silver a golden polish.            
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading18">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse18" aria-expanded="false" aria-controls="collapseSeven">
                              18. How is diamond formed? Why it is very costly?              </a>
                            </h4>
                          </div>
                          <div id="collapse18" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">The formation of natural diamond requires very specific conditions—exposure of carbon-bearing materials to high pressure, but at a comparatively low temperature range. In particular, these conditions are prevalent under oceanic plates and at the site of a meteorite strike. Diamonds formed hundreds of miles below the sea level are forced upwards to the earth&rsquo;s surface by volcanic explosions. After the cooling of the magma, it is solidified into a blue mass, or kimberlite, where the precious rough diamonds are still found. <br /> 
                                <br /> 
                                Rated 10 on the Moh&rsquo;s scale of hardness, diamonds are the hardest substances on earth, but their appeal goes far beyond durability. Approximately 250 tons of ore must be mined and processed in order to produce a single, one karat, polished, gem-quality diamond. This is what makes them so valuable and unique.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading19">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse19" aria-expanded="false" aria-controls="collapseSeven">
                              19. Unique properties of Ruby, Emerald and Sapphire 
                              </a>
                            </h4>
                          </div>
                          <div id="collapse19" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Ruby<br /> 
                                <br /> 
                                Ruby is one of the most expensive gems. Largest ruby weighing 400 karat and was found in Burma. Many rubies are important part of royal enigma and other famous jewellery. The Bohemian St. Wenzel's crown (Prague), for instance, holds a non-faceted ruby of about 250 karat. The colouring agent of ruby is chrome, sometimes iron. The colour is very stable against light and heat. The colour distribution is often uneven, in stripes and spots. Cut ruby luster can reach that of a diamond. Often the ruby is clouded by inclusions. These are not classified as faults, but as evidence to the genuineness of the ruby.
                              </p>
                              <div id="hd2">Physical Properties of Ruby:</div>
                              Colour: Varying Red; <br /> 
                              Moh's Hardness: 9.0;<br /> 
                              Density: 3.97-4.05;<br /> 
                              Chemical Composition: Aluminium Oxide;<br /> 
                              Transparency: Transparent to Opaque; <br /> 
                              Refractive Index: 1.762-1.778;<br /> 
                              Dispersion: 0.018; <br /> 
                              Pleochroism: Strong; yellow-red, deep carmine red. </p> 
                              <br /> 
                              <br /> 
                              <p style="font-size:16px; text-align:justify; line-height:25px">Emerald<br /> 
                                <br /> 
                                Green beryls are called emerald. Green emerald expresses faithfulness and continuity. The colouring agent is chrome, sometimes vanadium. The colour is very stable against light and heat. The colour distribution is often irregular; a dark green is most desired. Only the fine specimens are transparent. Often the emerald is clouded by inclusions. These are not classified as faults, but as evidence to the genuineness of the emerald as compared with synthetics. 
                              </p>
                              <div id="hd2">Physical Properties of Emerald:</div>
                              Colour: Emerald Green, green and yellowish green;<br /> 
                              Moh's Hardness: 7.5-8;<br /> 
                              Density: 2.67-2.78;<br /> 
                              Chemical Composition: Aluminium Berrylium Silicate;<br /> 
                              Transparency: Transparent to Opaque;<br /> 
                              Refractive Index: 1.565-1.602;<br /> 
                              Dispersion: 0.014;<br /> 
                              Pleochroism: Definite, green, blue, blue green to yellow green. </p> 
                              <br /> 
                              <br /> 
                              <p style="font-size:16px; text-align:justify; line-height:25px">Sapphire <br /> 
                                <br /> 
                                Today gemstone quality of all colours except red are called sapphire. Red varieties are called rubies. The various colours of sapphires are qualified by description, green sapphire or yellow sapphire. The colouring agents in blue sapphire are iron and titanium. The colour is very stable against light and heat. The most desired colour is a cornflower-blue. There is no definite demarcation between ruby and sapphire. 
                              </p>
                              <div id="hd2">Physical Properties of Sapphire:</div>
                              Sapphire Colour: Blue in various hues, colourless, pink, orange, yellow, green, purple and black;<br /> 
                              Moh's Hardness: 9.0;<br /> 
                              Density: 3.95-4.03;<br /> 
                              Chemical Composition: Aluminium Oxide;<br /> 
                              Transparency: Transparent to Opaque;<br /> 
                              Refractive Index: 1.762-1.788;<br /> 
                              Dispersion: 0.018;<br /> 
                              Pleochroism: Blue; Definite; dark blue, green blue. </p> 
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading20">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse20" aria-expanded="false" aria-controls="collapseSeven">
                              20. What are Red, Green and Blue stones? 
                              </a>
                            </h4>
                          </div>
                          <div id="collapse20" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Red Stone, Blue Stone and Green Stones are manufactured gems. These are available at very less cost and these stones lose their colour and shining over a period of time. Therefore, while returning the jewellery that is embedded with these stones, their original rates are not considered. Further, there is a chance of these stones getting damaged while removing it from the jewellery set.</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading21">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse21" aria-expanded="false" aria-controls="collapseSeven">
                              21. Distinguish between AD and Diamond? 
                              </a>
                            </h4>
                          </div>
                          <div id="collapse21" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading21" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">AD stands for American Diamond and it resembles natural diamond. We can observe the difference between AD and diamond when they are seen through eye glass. <br /> 
                                <br /> 
                                When posed to sunlight, usually AD shines reflecting 7 colours whereas diamond shines with white as reflecting colour. Moreover, there is difference in terms of their weight (diamond weighs 3.50 – 3.53gm/cm3). <br /> 
                                <br /> 
                                One more distinctive feature of diamond is that it has got cutting in both layers i.e., inner layer and outer layer, whereas, AD has got only outer layer cutting. <br /> 
                                <br /> 
                                The following experiment helps us to identify the difference between the two. Take a paper and mark it using black ink. Now place AD on this mark such that the pointed edge of AD is on the mark. When observed through paper the other edge the mark can be seen whereas the mark cannot be seen in diamond.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading22">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse22" aria-expanded="false" aria-controls="collapseSeven">
                              22. How do Pearls originate?              </a>
                            </h4>
                          </div>
                          <div id="collapse22" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading22" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">The formation of a natural pearl begins when a foreign substance slips into the oyster between the mantle and the shell, which irritates the mantle. The oyster's natural reaction is to cover up that irritant to protect itself. The mantle covers the irritant with layers of the same nacre substance that is used to create the shell. This eventually forms a pearl. So a pearl is a foreign substance covered with layers of nacre. Pearls, as you've probably noticed, come in a variety of colours, including white, black, gray, red, blue and green. While most pearls can be found all over the world, black pearls are indigenous to the South Pacific.</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading23">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse23" aria-expanded="false" aria-controls="collapseSeven">
                              23. What are baroque pearls?
                              </a>
                            </h4>
                          </div>
                          <div id="collapse23" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading23" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Most pearls that we see in jewellery stores are nicely rounded objects, which are cherished as valuable ones. Not all pearls turn out so well. Some pearls form in an uneven shape and are called baroque pearls.</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading24">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse24" aria-expanded="false" aria-controls="collapseSeven">
                              24. How do Corals originate? 
                              </a>
                            </h4>
                          </div>
                          <div id="collapse24" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading24" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Coral is a plant-like-shape formed by millions of unicellular creatures which live at the bottom of the sea and are called coelenterate (coelenterates). Their name is derived from the long tube which crosses their body; its name is in fact celenterio. <br /> 
                                <br /> 
                                The colony of coelenterates releases a deposit of carbonato di calcio (calcium carbonate) which solidifies and takes various shapes. The most common one is that of a little tree. Centuries ago, the Mediterranean Sea, along the Sicily, Sardinia and Naples coastline, were filled with coral reefs. In fact it was in Sicily and Naples that the art of carving and modeling coral was initially started. There are two types of coral. First type is the false coral, which grows at a depth of about 30/35 metres. At that depth the sun still influences the light and the warmth of the water, so the coral remains whitish and soft; and the real coral, which gets a bull-blood-like colour and can be found at a depth of about 70/80 metres. The deeper we go the darker and harder the coral becomes and is more precious. <br /> 
                                <br /> 
                                As both Coral and Pearl are made up of living substances, the calcium element is huge in both gems. Human body temperature and perspiration causes a chemical reaction which makes both the Pearl and Coral erode gradually. Hence they lose shine and colour over a period of time.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading25">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse25" aria-expanded="false" aria-controls="collapseSeven">
                              25. What are birthstones? How are they significant? 
                              </a>
                            </h4>
                          </div>
                          <div id="collapse25" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading25" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Birthstones are various gems associated with the particular calendar months of the year, and are considered lucky to people who are born in those months. </p>
                              <br /> 
                              <p style="font-size:16px; text-align:justify; line-height:25px">Birth stones can be of different types. Along with the traditional ones, there are Astrological, Mystical, Ayurvedic and a Modern list of stones. While the traditional ones reflect the 15th century traditions, the mystical stones are of Tibetan origin and the Ayurvedic stones, associated with 'Ayurveda' the Indian system of Medicine.</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading26">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse26" aria-expanded="false" aria-controls="collapseSeven">
                              26. Sri Navaratna - The Nine Gems
                              </a>
                            </h4>
                          </div>
                          <div id="collapse26" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading26" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">A &quot;Navaratna&quot; talisman set is a jewellery piece embedded with nine perfect gems. In the centre is a 95% flawless &quot;red lotus&quot; coloured 1 karat Burmese Ruby surrounded (clockwise from the top) by flawless diamond, natural pearl, red coral, hessonite, blue sapphire, cat's eye, yellow sapphire, and emerald.</p>
                              <p style="font-size:16px; text-align:justify; line-height:25px">Translation:</p>
                              <p style="font-size:16px; text-align:justify; line-height:25px">1) Ruby for the Sun <br /> 
                                2) Pearl for the Moon <br /> 
                                3) Coral for Mars <br /> 
                                4) Emerald for Mercury <br /> 
                                5) Yellow sapphire for Jupiter <br /> 
                                6) Diamond for Venus <br /> 
                                7) Blue sapphire for Saturn <br /> 
                                8) Hessonite for Rahu (the ascending node of the Moon) <br /> 
                                9) Cat's eye for Ketu (the descending node of the Moon)
                              </p>
                              <p style="font-size:16px; text-align:justify; line-height:25px">Note: These gems must be high-born (top quality) and pure in quality and variety. According to bona fide Vedic authority only &quot;sujatyam-amalam&quot; (sujati = high-born, and amala = completely pure or flawless), clean top quality gems are considered to be auspicious.</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading27">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse27" aria-expanded="false" aria-controls="collapseSeven">
                              27. Unique properties of platinum
                              </a>
                            </h4>
                          </div>
                          <div id="collapse27" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading27" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Apart from gold and silver, platinum is also being used in jewellery making. It is very expensive when compared to the former two metals. The scientific name of platinum is 78 Pt 195.09.<br /> 
                                Pt = Platinum.<br /> 
                                Atomic No. 78,<br /> 
                                Melting Point 1772°C,<br /> 
                                Boiling Point 3827°C.
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading28">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse28" aria-expanded="false" aria-controls="collapseSeven">
                              28. What is Elephant Hair in Jewellery?</h2>
                              </a>
                            </h4>
                          </div>
                          <div id="collapse28" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading28" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">As the name indicates the skin hair or the tail hair of the elephant is termed as elephant hair. Since it has been used as decorative item, it is being used in designing jewels. In Kerala and many parts of Africa we get it in abundance.</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading29">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse29" aria-expanded="false" aria-controls="collapseSeven">
                              29. What does Enamel Paint stand for? 
                              </a>
                            </h4>
                          </div>
                          <div id="collapse29" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading29" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">The artificial colour used in jewellery is usually referred to as Enamel Paint. It is being used in designing on the jewellery. Its purpose is to add attraction on the jewels just like girl&rsquo;s applying nail enamel for their nails.</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading30">
                            <h4 class="panel-titlee">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse30" aria-expanded="false" aria-controls="collapseSeven">
                              30. What are colour beads?
                              </a>
                            </h4>
                          </div>
                          <div id="collapse30" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading30" aria-expanded="false">
                            <div class="panel-body">
                              <p style="font-size:16px; text-align:justify; line-height:25px">Colour beads are artificial beads made up of glass, plastic and fibre. They are available in cities like Delhi, Hyderabad and Mumbai.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <script>
                        jQuery(function ($) {
                           var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
                           $active.find('a').prepend(' ');
                           $('#accordion .panel-heading').not($active).find('a').prepend(' ');
                           $('#accordion').on('show.bs.collapse', function (e) {
                               $('#accordion .panel-heading.active').removeClass('active').find('.fa-icon').toggleClass('fa fa-plus fa fa-minus');
                               $(e.target).prev().addClass('active').find('.fa-icon').toggleClass('fa fa-plus fa fa-minus');
                           })
                        });
                      </script>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
@endsection