@extends('Frontend.layouts.home_layout')
@section('content')
<style type="text/css">
	video {
    width: 100% !important;
    height: auto !important;
}
</style>
<link rel="stylesheet" type="text/css" href="{{asset('Frontend/assets/css/custom.css')}}">
<div id="content-wrapper-parent" >
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Video</span>
						</div>
					</div>
				</div>
			</div>
			<section class="pb-95">
		      <div class="home-newproduct ">
		        <div class="container">
		          <div class="group_home_products row"><div class="col-md-24">
		              <div class="home_products">
		                <h6 class="general-title">Pure Mystery Commercial</h6>
		                <div class="home_products_wrapper text-center">
		                  <div class="row">
                      <div class="col-md-6"></div>
                      <div class="col-md-12"><video  autoplay controls='true' controlsList="nodownload">
                     					 <source src="Frontend/assets/videos/pure-mystery.mp4" type="video/mp4" />
						  <source src="Frontend/assets/videos/pure-mystery.webm" type="video/webm">
						  Your browser does not support HTML5 video.
						</video></div>
                      <div class="col-md-6"></div>
		                  </div>
		                </div>
		              </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </section>
		    
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
          // If you can't/don't want to set the href property of an element
          // data-remote="http://www...."
          remote: '',
          // For grouping elements
          // data-gallery="galleryname"
          gallery: '',
          // If you have multiple galleries per page, this will restrict the gallery items to the parent that matches this selector.
          gallery_parent_selector: 'document.body',
          // CSS classes for navigation arrows
          left_arrow_class: '.glyphicon .glyphicon-chevron-left',
          right_arrow_class: '.glyphicon .glyphicon-chevron-right',
          // Enable the navigation arrows
          directional_arrows: true,
          // Force the lightbox into image/YouTube mode.
          // image|youtube|vimeo
          // data-type="(image|youtube|vimeo)"
          type: null,
          // Always show the close button, even if no title is present
          always_show_close: true,
          // Don't show related videos when finished playing
          no_related: false,
          // Scale the height as well as width
          scale_height: true,
          // Message injected for loading
          // loadingMessage: 'Loading...',           
          // Callback functions
          onShow: function() {},
          onShown: function() {},
          onHide: function() {},
          onHidden: function() {},
          onNavigate: function() {},
          onContentLoaded: function() {}
      });
  });
</script>
@endsection