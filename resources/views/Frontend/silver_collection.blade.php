@extends('Frontend.layouts.home_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('Frontend/assets/css/custom.css')}}">
<link href="{{asset('Assets/lightbox/ekko-lightbox.min.css')}}" rel="stylesheet">
<script src="{{asset('Assets/lightbox/ekko-lightbox.js')}}"></script>
<div id="content-wrapper-parent" >
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Collections</span>
						</div>
					</div>
				</div>
			</div>
			<section class="pb-95">
		      <div class="home-newproduct back_pin chg-back_pin">
		        <div class="container">
          <div class="group_home_products row">
            <div class="col-md-24">
              <div class="home_products">
                <h6 class="general-title">Silver Collection</h6>
                  <p> <i class="fa fa-phone" aria-hidden="true"></i> FOR ENQUIRIES CALL OUR TOLL FREE NUMBER &nbsp;1800 5999 777 &nbsp;<a class="clik-ank" href="/contact-us"> Click Here To Order Now</a>  </p>
                
                <div class="home_products_wrapper">
                  <div id="sandBox-wrapper" class="group-product-item row collection-full">
                    <div class="about-detail">
                      <div class="row">
                        <div class="heading-part heading-bg mb-30">
                          <h2 class="heading m-10"> Jewellery</h2>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19937.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19937.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19945.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19945.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19950.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19950.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20112.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20112.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20119.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20119.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20121.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20121.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20129.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20129.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20179.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20179.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20183.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20183.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20185.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20185.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20188.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20188.jpg?t=123"  >
                          </a>
                        </div>
                        <hr style="clear: both; padding-bottom: 10px;">
                        <div class="heading-part heading-bg mb-30">
                          <h2 class="heading m-0">Articles</h2>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19815.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19815.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19851.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19851.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19857.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19857.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19860.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19860.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19869.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19869.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19885.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19885.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19890.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19890.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19893.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19893.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19894.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19894.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19897.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19897.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19902.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19902.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19908.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19908.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19911.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19911.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/19913.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/19913.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20037.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20037.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20039.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20039.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20043.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20043.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20049.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20049.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20052.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20052.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20055.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20055.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20063.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20063.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20064.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20064.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20069.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20069.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20071.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20071.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20072.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20072.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20077.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20077.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20079.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20079.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20085.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20085.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20086.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20086.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/silver_collection/20094.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/silver_collection/20094.jpg?t=123"  >
                          </a>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close"  data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="image-gallery-title"></h4>
                            <ul class="pager">
                              <li class="previous" id="show-previous-image"><a href="#">Previous</a></li>
                              <li class="next" id="show-next-image"><a href="#">Next</a></li>
                            </ul>
                          </div>
                          <div class="modal-body">
                            <img id="image-gallery-image" class="img-responsive" src="">
                          </div>
                        </div>
                      </div>
                    </div>
                    </ul>
                     <p style="text-align: center;">
                          <button type="button" class="btn btn-info" onclick="location.href='/contact-us';"> Click Here To Order Now</button>
                          </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
		      </div>
		    </section>
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
          // If you can't/don't want to set the href property of an element
          // data-remote="http://www...."
          remote: '',
          // For grouping elements
          // data-gallery="galleryname"
          gallery: '',
          // If you have multiple galleries per page, this will restrict the gallery items to the parent that matches this selector.
          gallery_parent_selector: 'document.body',
          // CSS classes for navigation arrows
          left_arrow_class: '.glyphicon .glyphicon-chevron-left',
          right_arrow_class: '.glyphicon .glyphicon-chevron-right',
          // Enable the navigation arrows
          directional_arrows: true,
          // Force the lightbox into image/YouTube mode.
          // image|youtube|vimeo
          // data-type="(image|youtube|vimeo)"
          type: null,
          // Always show the close button, even if no title is present
          always_show_close: true,
          // Don't show related videos when finished playing
          no_related: false,
          // Scale the height as well as width
          scale_height: true,
          // Message injected for loading
          // loadingMessage: 'Loading...',           
          // Callback functions
          onShow: function() {},
          onShown: function() {},
          onHide: function() {},
          onHidden: function() {},
          onNavigate: function() {},
          onContentLoaded: function() {}
      });
  });
</script>
@endsection