@extends('Frontend.layouts.home_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('Frontend/assets/css/custom.css')}}">
<link href="{{asset('Assets/lightbox/ekko-lightbox.min.css')}}" rel="stylesheet">
<script src="{{asset('Assets/lightbox/ekko-lightbox.js')}}"></script>
<div id="content-wrapper-parent" >
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Collections</span>
						</div>
					</div>
				</div>
			</div>
			<section class="pb-95">
		      <div class="home-newproduct back_pin chg-back_pin">
		        <div class="container">
            <div class="group_home_products row">
              <div class="col-md-24">
                <div class="home_products">
                  <h6 class="general-title">GANESH CHATURTHI COLLECTION</h6>
                  <p> <i class="fa fa-phone" aria-hidden="true"></i> FOR ENQUIRIES CALL OUR TOLL FREE NUMBER &nbsp;1800 5999 777 &nbsp;<a class="clik-ank" href="/contact-us"> Click Here To Order Now</a> </p>
                  
                  <div class="home_products_wrapper">
                    <div id="sandBox-wrapper" class="group-product-item row collection-full">
                      <div class="about-detail">
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/1.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/1.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/2.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/2.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/3.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/3.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/4.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/4.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/5.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/5.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/6.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/6.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/7.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/7.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/8.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/8.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/9.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/9.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/10.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/10.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/11.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/11.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/12.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/12.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/13.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/13.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/14.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/14.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/15.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/15.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/16.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/16.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/17.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/17.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/18.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/18.jpg?t=123"  >
                          </a>
                        </div>
                        <div class=" col-lg-4 col-md-6 col-sm-8 col-xs-12">
                          <a class="thumbnail"   data-toggle="lightbox" href="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/19.jpg?t=123" data-target="#image-gallery">
                          <img class="img-responsive" src="/Frontend/assets/images/scheme-collections/GANESH CHATURTHI COLLECTION/19.jpg?t=123"  >
                          </a>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close"  data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="image-gallery-title"></h4>
                            <!--<div class="col-md-12 ">-->
                            <ul class="pager">
                              <li class="previous" id="show-previous-image"><a href="#">Previous</a></li>
                              <li class="next" id="show-next-image"><a href="#">Next</a></li>
                            </ul>
                            <!--<button type="button" class="btn btn-primary" id="show-previous-image">Previous</button>
                              <button type="button" id="show-next-image" class="btn btn-default">Next</button>
                                        </div>-->
                          </div>
                          <div class="modal-body">
                            <img id="image-gallery-image" class="img-responsive" src="">
                          </div>
                        </div>
                      </div>
                    </div>
                    </ul>
                     <p style="text-align: center;">
                          <button type="button" class="btn btn-info" onclick="location.href='/contact-us';"> Click Here To Order Now</button>
                          </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
		      </div>
		    </section>
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
          // If you can't/don't want to set the href property of an element
          // data-remote="http://www...."
          remote: '',
          // For grouping elements
          // data-gallery="galleryname"
          gallery: '',
          // If you have multiple galleries per page, this will restrict the gallery items to the parent that matches this selector.
          gallery_parent_selector: 'document.body',
          // CSS classes for navigation arrows
          left_arrow_class: '.glyphicon .glyphicon-chevron-left',
          right_arrow_class: '.glyphicon .glyphicon-chevron-right',
          // Enable the navigation arrows
          directional_arrows: true,
          // Force the lightbox into image/YouTube mode.
          // image|youtube|vimeo
          // data-type="(image|youtube|vimeo)"
          type: null,
          // Always show the close button, even if no title is present
          always_show_close: true,
          // Don't show related videos when finished playing
          no_related: false,
          // Scale the height as well as width
          scale_height: true,
          // Message injected for loading
          // loadingMessage: 'Loading...',           
          // Callback functions
          onShow: function() {},
          onShown: function() {},
          onHide: function() {},
          onHidden: function() {},
          onNavigate: function() {},
          onContentLoaded: function() {}
      });
  });
</script>
@endsection