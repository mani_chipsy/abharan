@extends('Frontend.layouts.home_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('Frontend/assets/css/custom.css')}}">

<div id="content-wrapper-parent" >
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Collections</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content">
		      <div class="container">
		      <br >
		      <div class="row">
		      <div class="col-md-6 col-sm-4" style="margin-top: 20px;">
		        <div class="account-sidebar account-tab mb-xs-30">
		          <div class="account-tab-inner">
		            <a class="btn btn-1 enable hidden-xs" href="javascript:void(0)">COLLECTIONS</a>
		            <!--  <li id="step2">
		              <a href="javascript:void(0)">VIDEOS<i class="fa fa-angle-right"></i>
		              </a>
		              </li>
		              
		              <li id="step3">
		              <a href="javascript:void(0)">EXHIBITIONS<i class="fa fa-angle-right"></i>
		              </a>
		              </li>
		              <li id="step4">
		              <a href="javascript:void(0)">EVENTS<i class="fa fa-angle-right"></i>
		              </a>
		              </li>
		              <li id="step5">
		              <a href="javascript:void(0)">SHOWROOMS<i class="fa fa-angle-right"></i>
		              </a>
		              </li>
		              <li id="step6">
		              <a href="javascript:void(0)">CALENDAR<i class="fa fa-angle-right"></i>
		              </a>
		              </li>-->

		          </div>
		        </div>
		      </div>
		      <div class="col-md-18 col-sm-8">
		      <div id="data-step1" class="account-content" data-temp="tabdata">
		        <div class="row">
		          <div class="col-md-24">
		            <div class="heading-part heading-bg mb-30">
		              <h3 id="page-title">Collections</h3>
		            </div>
		          </div>
		        </div>
		        <div class="mb-30">
		          <div class="row">
		            <div class="col-md-24">
		              <div class="row">
		                <div class="col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="pritty-silver-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/pritty_silver.jpg')}}" alt="Collection" class="image">
		                        <!--<img src="images/pritty_silver/pritty_silver.jpg" alt="Collection" class="image">-->
		                        <div class="overlay">
		                          <div class="text">Pritty Silver Collection</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  
		                  <div class="col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="/pritty-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/pritty_collection.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Pritty Collection</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class="col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="/aasthana-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/thumb.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Aasthana Collection</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class="col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="silver-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/silver_collection.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Silver Collection</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class="col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="shezmin-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/shezmin_collection.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Shezmin Collection</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class=" col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="azva-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/azva_collection.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Azva Collection</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class=" col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="/celesta-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/celesta_collection.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Celesta collection</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class=" col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="/monsoon-mystica">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/monsoon_mystica.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Monsoon Mystica</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class=" col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="/taarika-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/taarika_collection.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Taarika Collection</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class=" col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="/blue-pottery-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/blue_pottery_collection.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Blue Pottery Collection</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class=" col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="/utsav-temple-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/temple-collection.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Utsav Temple Collection </div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class=" col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="/tanvi-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/tanvi_collection.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Tanvi Collection</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class=" col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="/mantra-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/mantra_collection.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Mantra Collection</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class=" col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="/ganesh-chaturthi-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/ganesh_chaturthi_collection.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text" style="text-align: center;">Ganesh Chaturthi<br>
		                            Collection
		                          </div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		                  <div class=" col-lg-6 col-md-6 col-sm-24 col-xs-24 ">
		                    <a href="/noorie-collection">
		                      <div class="gcontainer">
		                        <img src="{{asset('Frontend/assets/images/index-collection/noorie_collection.jpg')}}" alt="Collection" class="image">
		                        <div class="overlay">
		                          <div class="text">Noorie Collection</div>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		              </div>
		            <hr style="clear: both; padding-top: 58px;">
		          </div>
		        </div>
		      </div>
		      </div>
		    </section>
		</div>
	</div>
</div>
@endsection