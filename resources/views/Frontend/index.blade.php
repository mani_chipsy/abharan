@extends('Frontend.layouts.home_layout')
@section('content')
<link href="{{asset('Frontend/assets/css/mainstyle.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('Frontend/assets/css/custom.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('Frontend/assets/css/custom-1.css')}}">
<link href="{{asset('Assets/lightbox/ekko-lightbox.min.css')}}" rel="stylesheet">
<script src="{{asset('Assets/lightbox/ekko-lightbox.js')}}"></script>
<div id="content-wrapper-parent">
  <div id="content-wrapper">
    <div class="home-slider-wrapper clearfix">
      <div class="camera_wrap" id="home-slider">
        <div data-src="{{asset('http://abharan.com/Frontend/assets/images/bg/banner/abharan_a.jpg')}}">
        </div>
        <div data-src="{{asset('http://abharan.com/Frontend/assets/images/bg/banner/abharan_b.jpg')}}">
        </div>
        <div data-src="{{asset('http://abharan.com/Frontend/assets/images/bg/banner/abharan_c.jpg')}}">
        </div>
        <div data-src="{{asset('http://abharan.com/Frontend/assets/images/bg/banner/abharan_d.jpg')}}">
        </div>
        <div data-src="{{asset('http://abharan.com/Frontend/assets/images/bg/banner/abharan_e.jpg')}}">
        </div>
        <div data-src="{{asset('http://abharan.com/Frontend/assets/images/bg/banner/abharan_f.jpg')}}">
        </div>
        <div data-src="{{asset('http://abharan.com/Frontend/assets/images/bg/banner/abharan_g.jpg')}}">
        </div>
        {{--<div data-src="{{asset('http://abharan.com/Frontend/assets/images/bg/banner/banner-1.jpg')}}">
        </div>--}}
        <div data-src="{{asset('http://abharan.com/Frontend/assets/images/bg/banner/banner-2.jpg')}}">
        </div>
        <div data-src="{{asset('http://abharan.com/Frontend/assets/images/bg/banner/banner-3.jpg')}}">
        </div>
      </div>
    </div>
    <!-- Content -->
  <div id="content" class="clearfix">
      <section class="content" ng-controller='HomeCollectionsController'>
        <div id="col-main" class="clearfix">
            <div class="home-popular-collections " >
            <div class="container">
              <div class="group_home_collections row">
                <div class="col-md-24">
                  <div class="home_collections">
                    <h6 class="general-title">Categories</h6>
                    <div class="home_collections_wrapper">
                      <!-- <div id="home_collections"> -->
                      <data-owl-carousel class="owl-carousel"  id="home_collections">
                          <div owl-carousel-item ng-repeat='p_collection in pop_collections'  class="category_item home_collections_item">
                            <div class="home_collections_item_inner">
                              <div class="collection-details">
                                <a href="/product/collection?m_category_id=<%p_collection.m_category_id%>" title="Browse our <%p_collection.m_category_name%>" >
                                <img ng-if='p_collection.image_name' ng-src="{{asset('Frontend/assets/images/Main_cata/<%p_collection.image_name%>')}}" alt="<%p_collection.m_category_name%>" class="img-responsive"/>
                                <img ng-if='!p_collection.image_name' ng-src="{{asset('/images/NO_IMAGE_IN.png')}}" alt="No Preview" />
                                </a>
                              </div>
                             <!--  <div class="hover-overlay">
                                <span class="col-name"><a href="/product/collection?m_category_id=<%p_collection.m_category_id%>" style="color: #ddd499;font-size: 20px;"><%p_collection.m_category_name%></a></span>
                                <div class="collection-action">
                                  <a href="/product/collection?m_category_id=<%p_collection.m_category_id%>" style="color: #fc8b1c!important;">See the Collection</a>
                                </div>
                              </div> -->
                            </div>
                          </div>
                      </data-owl-carousel>
                      <script>
                        $(document).ready(function() {
                          $('.collection-details').hover(
                            function() {
                              $(this).parent().addClass("collection-hovered");
                            },
                            function() {
                            $(this).parent().removeClass("collection-hovered");
                            });
                        });
                        
                      </script>
                     <!--  <div id="home_collections">
                        <div class="home_collections_item" ng-repeat='p_collection in pop_collections'>
                          <div class="home_collections_item_inner">
                            <div class="collection-details">
                              <a href="/product/collection?m_category_id=<%p_collection.m_category_id%>" title="Browse our <%p_collection.m_category_name%>">
                              <img ng-src="{{asset('Frontend/assets/images/Main_cata/<%p_collection.image_name%>')}}" alt="<%p_collection.m_category_name%>" />
                              </a>
                            </div>
                          </div>
                        </div>
                      </div> -->
                      <!-- </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="home-popular-collections" style="background-color: #f7f7f7;padding-bottom: 40px">
            <div class="container">
                <div class="col-md-24 " >
                 

                  <div class="home_products">
                   <h6 class="general-title" style="margin-bottom: 35px;">Video</h6>
                    <div class="home_products_wrapper">
                      <div id="sandBox-wrapper" class="group-product-item row collection-full">
                        <div class="row" style="padding-bottom:20px">
                          <div class="col-md-24 ">
                            <div class="sub-banner sub-banner2" style="visibility: visible; animation-name: zoomIn;">
                              <a href="/videos">
                                <img alt="Abharan" src="{{asset('Frontend/assets/images/sub-banner/video_backdrop.png')}}">
                               <!--  <div class="sub-banner-detail">
                                  <div class="sub-banner-title">Videos</div>
                                  <div class="sub-banner-subtitle">Pure Mystery Commercial</div>
                                </div> -->
                              </a>
                            </div>
                          </div>
                        <!--   <div class="col-md-10">
                            <div class="sub-banner sub-banner3" style="visibility: visible; animation-name: zoomIn;">
                              <a href="/events">
                                <img alt="Abharan" src="{{asset('Frontend/assets/images/sub-banner/2.jpg')}}">
                                <div class="sub-banner-detail">
                                  <div class="sub-banner-title">Events</div>
                                  <div class="sub-banner-subtitle">Women's Day 2017</div>
                                </div>
                              </a>
                            </div>
                          </div> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <div class="home-newproduct" >
            <!-- catagories -->
   <!--          <section class="section-wrapper ja_latest_sec"  >
              <div class="container">
                <div class="ja_latestsec_inner">
                  <h6 class="general-title">Category</h6>
                  <div class="col-lg-24 col-xl-24 text-center">
                    <ul class="ja_latest_cir_lst" >
                      <li class="active" ng-repeat='w_collection in weekly_collections track by $index'>
                        <a href="/product/collection?s_category_id=<%w_collection.s_category_id%>" title="Browse our <%w_collection.s_category_name%>">
                        <img ng-src="<%w_collection.imgsrc_out%>" alt="<%w_collection.s_category_name%>" class="ja_latest_slider_icons ">
                        <span class=""><%w_collection.s_category_name%></span>
                        </a>                  
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </section> -->
            <div class="container">
              <div class="group_home_products row">
                <div class="col-md-24">
                  <div class="home_products">
                      <h6 class="general-title">Our Collection</h6>
                      <div class="container">
                        <div class="home-promotion-blog row">
                          <h5 class="general-title"></h5>
                          <div class="home-bottom_banner_wrapper ">
                            <div id="sandBox-wrapper" class="group-product-item row collection-full">
                              <ul id="sandBox" class="list-unstyled">
                                <li class="element no_full_widths" style="    width: 25%;">
                                  <ul class="row-container list-unstyled clearfix" style="box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.52);" >
                                    <li class="row-left">
                                      <a href="/pritty-silver-collection">
                                        <div class="gcontainer">
                                          <img  class="img-responsive" src="{{asset('Frontend/assets/images/index-collection/pritty_silver.jpg')}}" alt="Collection" class="image">
                                          <!--<img src="images/pritty_silver/pritty_silver.jpg" alt="Collection" class="image">-->
                                          <div class="overlay">
                                            <div class="text">Pritty Silver Collection</div>
                                          </div>
                                        </div>
                                      </a>
                                    </li>
                                  </ul>
                                </li>
                                <li class="element no_full_widths" style="    width: 25%;" >
                                  <ul class="row-container list-unstyled clearfix" style="box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.52);" >
                                    <li class="row-left">
                                      <a href="/pritty-collection">
                                        <div class="gcontainer">
                                          <img  class="img-responsive" src="{{asset('Frontend/assets/images/index-collection/pritty_collection.jpg')}}" alt="Collection" class="image">
                                          <!--<img src="images/pritty_silver/pritty_silver.jpg" alt="Collection" class="image">-->
                                          <div class="overlay">
                                            <div class="text">Pritty Collection</div>
                                          </div>
                                        </div>
                                      </a>
                                    </li>
                                  </ul>
                                </li>
                                <li class="element no_full_widths" style="    width: 25%;">
                                  <ul class="row-container list-unstyled clearfix" style="box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.52);" >
                                    <li class="row-left">
                                      <a href="/silver-collection">
                                        <div class="gcontainer">
                                          <img  class="img-responsive" src="{{asset('Frontend/assets/images/index-collection/silver_collection.jpg')}}" alt="Collection" class="image">
                                          <!--<img src="images/pritty_silver/pritty_silver.jpg" alt="Collection" class="image">-->
                                          <div class="overlay">
                                            <div class="text"> Silver Collection</div>
                                          </div>
                                        </div>
                                      </a>
                                    </li>
                                  </ul>
                                </li>
                                <li class="element no_full_widths" style="    width: 25%;" >
                                  <ul class="row-container list-unstyled clearfix" style="box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.52);" >
                                    <li class="row-left">
                                      <a href="/aasthana-collection">
                                        <div class="gcontainer">
                                          <img  class="img-responsive" src="{{asset('Frontend/assets/images/index-collection/thumb.jpg')}}" alt="Collection" class="image">
                                          <div class="overlay">
                                            <div class="text">Aasthana Collection</div>
                                          </div>
                                        </div>
                                      </a>
                                    </li>
                                  </ul>
                                </li>
                                <!--one row-->
                              <!--   <li class="element no_full_widths" style="    width: 25%;" >
                                  <ul class="row-container list-unstyled clearfix" style="box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.52);" >
                                    <li class="row-left">
                                      <a href="/shezmin-collection">
                                        <div class="gcontainer">
                                          <img  class="img-responsive" src="{{asset('Frontend/assets/images/index-collection/shezmin_collection.jpg ')}}" alt="Collection" class="image">
                                          <div class="overlay">
                                            <div class="text">Shezmin Collection</div>
                                          </div>
                                        </div>
                                      </a>
                                    </li>
                                  </ul>
                                </li>
                                <li class="element no_full_widths" style="    width: 25%;" >
                                  <ul class="row-container list-unstyled clearfix" style="box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.52);" >
                                    <li class="row-left">
                                      <a href="/azva-collection">
                                        <div class="gcontainer">
                                          <img  class="img-responsive" src="{{asset('Frontend/assets/images/index-collection/azva_collection.jpg ')}}" alt="Collection" class="image">
                                          <div class="overlay">
                                            <div class="text">Azva Collection</div>
                                          </div>
                                        </div>
                                      </a>
                                    </li>
                                  </ul>
                                </li>
                                <li class="element no_full_widths" style="    width: 25%;" >
                                  <ul class="row-container list-unstyled clearfix" style="box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.52);" >
                                    <li class="row-left">
                                      <a href="/celesta-collection">
                                        <div class="gcontainer">
                                          <img  class="img-responsive" src="{{asset('Frontend/assets/images/index-collection/celesta_collection.jpg ')}}" alt="Collection" class="image">
                                          <div class="overlay">
                                            <div class="text">Celesta Collection</div>
                                          </div>
                                        </div>
                                      </a>
                                    </li>
                                  </ul>
                                </li>
                                <li class="element no_full_widths" style="    width: 25%;" >
                                  <ul class="row-container list-unstyled clearfix" style="box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.52);" >
                                    <li class="row-left">
                                      <a href="/monsoon-mystica">
                                        <div class="gcontainer">
                                          <img  class="img-responsive" src="{{asset('Frontend/assets/images/index-collection/monsoon_mystica.jpg')}}" alt="Collection" class="image">
                                          <div class="overlay">
                                            <div class="text">Monsoon Collection</div>
                                          </div>
                                        </div>
                                      </a>
                                    </li>
                                  </ul>
                                </li> -->
                              </ul>
                              <div class="top-buffer col-lg-24 col-md-24 col-sm-24 col-xs-24 text-center">
                                <button class="btn btn-primary btn-lg button-cart pull-right" data-parent=".product-information" name="add" onclick="location.href='/collections';" style="min-width: 200px">View more</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                   
                      {{--<h6 class="general-title">Online Products</h6>
                      <div class="home_products_wrapper">

                        <div id="sandBox-wrapper" class="group-product-item row collection-full">
                          <ul id="sandBox" class="list-unstyled">
                            <li ng-repeat="collection in collections track by $index" class="element first no_full_width" data-alpha="Curabitur cursus dignis" data-price="25900">
                              <ul class="row-container list-unstyled clearfix" style="border: solid 1px #ababab;">
                                <li class="row-left" >
                                  <a href="/product-details?product_id=<%collection.product_id%>" class="container_item">
                                  <img ng-if='collection.image' ng-src="/product/storage/IN/<%collection.image.global_name%>_IN.<%collection.image.image_ext%>?width=350&height=281" class="img-responsive" alt="<%collection.name%>" />  
                                  <img ng-if='!collection.image' ng-src="/product/storage/IN/NO_IMAGE_IN.png?width=350&height=281" alt="No Preview" />
                                  </a>
                                  <div class="hbw">
                                    <span class="hoverBorderWrapper"></span>
                                  </div>
                                </li>
                                <li class="row-right parent-fly animMix">
                                  <div class="product-content-left">
                                    <a class="title-5" href="/product-details?product_id=<%collection.product_id%>" title="<%collection.SKU%>">
                                      <%collection.SKU%>
                                      <!--  - <%collection.sub_category.s_category_name%> -->
                                    </a>
                                    <span class="shopify-product-reviews-badge" data-id="1293238211"></span>
                                  </div>
                                  <div class="product-content-right">
                                    <div class="product-price" ng-if='!collection.offer_value'>    
                                      <span class="price"><span class='money'>Rs.<%collection.gross_value%></span></span>
                                    </div>
                                    <div class="product-price" ng-if='collection.offer_value'>
                                      <span class="price_sale"><span class="money">Rs.<%collection.offer_value%></span></span>
                                      <del class="price_compare"> <span class="money">Rs.<%collection.gross_value%></span></del>
                                    </div>
                                  </div>
                                  <div class="hover-appear">
                                    <div class="effect-ajax-cart">
                                      <form method="POST" action="{{url('/product/cart/add')}}">
                                        <input type="hidden" name="product_id" value="<%collection.product_id%>">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="task" value="add">
                                        <button class="add-to-cart" type="submit" name="add"><i class="fa fa-shopping-cart"></i><span class="list-mode">Add to Cart</span></button>
                                      </form>
                                    </div>
                                    <div class="product-ajax-qs">
                                      <div data-handle="curabitur-cursus-dignis-1" ng-click='quickView($index)' class="quick_shop" >
                                        <i class="fa fa-eye" title="Quick view"></i><span class="list-mode">Quick View</span>
                                      </div>
                                    </div>
                                    <!-- <a class="wish-list" href="/account/login" title="wish list"><i class="fa fa-heart"></i><span class="list-mode">Add to Wishlist</span></a> -->
                                  </div>
                                </li>
                              </ul>
                            </li>
                          </ul>
                          <div class="camera_loader" style="display: none;"></div>
                          <div ng-hide='collections.length' class="text-center"><b> No collections found!</b></div>
                        </div>
                        <!-- <div class="row"> -->
                        <!-- <div class="col-md-16">&nbsp;</div> -->
                        <!-- <div class="col-md-8"> -->
                        <ul ng-show='collections.length' uib-pagination total-items="totalCollections" ng-model="currentPage" class="pagination-sm chng-pagination" boundary-links="true" rotate="true" max-size="maxSize" items-per-page="collectionsPerPage"></ul>
                        <!-- </div> -->
                        <!-- </div> -->
                      </div>--}}
                  </div>
                </div>
              </div>
            </div>         
          </div>
          <section class="section-wrapper home-banner-wrapper  "  style="background: url(/images/bg-2.png) no-repeat;">
            <div class="container">
           <!--    <div id="home-banner" class="text-center clearfix">
                <img class="pulse img-banner-caption" src="{{asset('Frontend/assets/images/bg/aa-logo.png')}}" alt="" />
                <div class="home-banner-caption">
                  <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br/>
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                  </div>
                  <div class="home-banner-action">
                  <a href="/collections">Shop Now</a>
                  </div> 
              </div> -->
                <div class="container html3module">



  


  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 remove-padding15-right normal-padding15-right htmlblock3">



  <div class="">



  <p><br></p>


   </div> 



  </div>



  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 remove-padding15-left normal-padding15-left htmlblock3">



  <div class="">



  <div class="htmlcontentouter">
<h4>Accessorize Yourself With</h4>
<h1 class="f-42"> Best Collection</h1>
<div class="htmlcontent">Abharan is an ISO 9001: 2008 certified jeweller selling 'Hallmark' jewellery. In fact, we are the first to introduce the Karatometer in the country. Abharan has an online purchase window enabling customers to choose and purchase jewellery through the internet. With uncompromising quality and customer satisfaction measures. Abharan has won customers the world over. </div>
<div class="htmlbutton">
<!-- <a class="button" href="#">BUY NOW</a> -->
<a class="button" href="/product/collection">VIEW ALL</a>
</div>
</div>


   </div>



  </div>



 </div>
        </section>
        <div class="container-fluid remove-padding15-left remove-padding15-right htmlmodule1outer" style="margin-bottom: 0px">
<div class="container html1module" >

<div class="row">



  


  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 banner1html">



  <div class="promo-banner space5">



  <a href="#"><span class="html1-icon"><img src="/images/icons/Icon-1.png" alt="shipping"></span>                </a><div class="html1-content"><a href="#">                    <h4 class="html1-main">shipping All Over India</h4>





                              <h5 class="html1-sub">vary based on your location</h5>





                        </a></div><a href="#">





</a>


   </div> 



  </div>



<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 banner1html">



  <div class="promo-banner space5">

<div class="banner-overlay"></div>

  <a href="#">                <span class="html1-icon"><img src="/images/icons/Icon-2.png" alt="30dayreturn"></span>                <div class="html1-content">                    <h4 class="html1-main">Value For Money</h4>





                              <h5 class="html1-sub">All Products go through strict quality check</h5>





                        </div>





</a>


   </div>



  </div>



  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 banner1html">



  <div class="promo-banner space5">

<div class="banner-overlay"></div>

   <a href="#">                <span class="html1-icon"><img src="/images/icons/Icon-3.png" alt="customersupport"></span>                <div class="html1-content">                    <h4 class="html1-main">Call Support</h4>




                              <h5 class="html1-sub">Call Our Toll Free </h5>




                        </div>




</a>


    </div>



   </div>



</div>

</div>
</div>
          {{--<div class="home-blog">
            <div class="container">
              <div class="home-promotion-blog row">
                <h6 class="general-title">Our SCHEMES</h6>
                <div class="home-blog-wrapper col-md-24">
                  <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24 " >
                    <a href="{{asset('Frontend/assets/images/schema/SCHEME 50-50.jpg')}}" data-toggle="lightbox">
                      <div class="gcontainer">
                        <img style="width:100%" src="{{asset('Frontend/assets/images/schema/SCHEME 50-50.jpg')}}" alt="Abharan" /> 
                        <div class="overlay">
                          <div class="text">Click to view</div>
                        </div>
                      </div>
                    </a>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24 ">
                    <a href="{{asset('Frontend/assets/images/schema/SCHEMEpendant_plan.jpg')}}" data-toggle="lightbox">
                      <div class="gcontainer">
                        <img style="width:100%" src="{{asset('Frontend/assets/images/schema/SCHEMEpendant_plan.jpg')}}" alt="Abharan" />  
                        <div class="overlay">
                          <div class="text">Click to view</div>
                        </div>  
                      </div>
                    </a>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-24 col-xs-24 ">
                    <a href="{{asset('Frontend/assets/images/schema/SCHEMESmart_savings (1).jpg')}}" data-toggle="lightbox">
                      <div class="gcontainer">
                       <img style="width:100%" src="{{asset('Frontend/assets/images/schema/SCHEMESmart_savings (1).jpg')}}" alt="Abharan" />
                       <div class="overlay">
                          <div class="text">Click to view</div>
                        </div> 
                      </div>
                    </a>
                  </div>
                  </div>
                </div>
              </div>
            </div>--}}
          </div>

           


          <h6 class="general-title">Get To Know Us</h6>
          <div class="home-banner-wrapper">
            <div class="row m-0">
              <div class="col-md-8 p-0">
                <div class="cate-banner">
                  <div class="cate-banner-img">
                    <img src="/Frontend/assets/images/category1.jpg" style="width: 100%;" alt="">
                  </div>
                  <div class="cate-banner-bg">
                    <img src="/Frontend/assets/images/category4.jpg" style="width: 100%;" alt="">
                    <div class="cate-banner-detail align-center ">
                      <div class="cate-banner-inner">
                        <div class="cate-banner-info">
                          <div class="cate-banner-title"> About Us</div>
                          <div class="cate-banner-desc" style=" color: #878787;">It all began in 1930. Late Sri Burde Sadananda Kamath, a general merchant, in his travels to Malnad, discovered the exquisite nature of jewellery made in Udupi. Soon he began purchasing jewellery from the</div>
                          <div class="cate-banner-more mt-30 mt-xs-20">
                            <a href="/about-us">Read more</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-8 p-0">
                <div class="cate-banner cate-banner2">
                  <div class="cate-banner-img">
                    <img src="/Frontend/assets/images/category2.jpg" style="width: 100%;" alt="">
                  </div>
                  <div class="cate-banner-bg">
                    <img src="/Frontend/assets/images/category4.jpg" style="width: 100%;" alt="">
                    <div class="cate-banner-detail align-center ">
                      <div class="cate-banner-inner">
                        <div class="cate-banner-info">
                          <div class="cate-banner-title">Jewellery Care</div>
                          <div class="cate-banner-desc"style=" color: #878787;">Though Gold jewellery is available in various Karats the general cleaning is common for all varieties. Following are some tips to be taken to handle the gold jewellery efficiently: </div>
                          <div class="cate-banner-more mt-30 mt-xs-20">
                            <a href="/jewellery-care">Read more</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-8 p-0">
                <div class="cate-banner">
                  <div class="cate-banner-img">
                    <img src="/Frontend/assets/images/category3.jpg" style="width: 100%;" alt="">
                  </div>
                  <div class="cate-banner-bg">
                    <img src="/Frontend/assets/images/category4.jpg" style="width: 100%;" alt="">
                    <div class="cate-banner-detail align-center ">
                      <div class="cate-banner-inner">
                        <div class="cate-banner-info">
                          <div class="cate-banner-title">About Jewellery </div>
                          <div class="cate-banner-desc"style=" color: #878787;">India is unique in its culture and traditions. The concept of 'beauty' was well entrenched in this culture. In fact, our ancient remedies for enhancement of beauty by way of herbal treatments and oils are well known</div>
                          <div class="cate-banner-more mt-30 mt-xs-20">
                            <a href="/about-jewellry">Read more</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="home-feature" ng-controller='NewThisWeekController'>
            <div class="container">
              <div class="group_featured_products row">
                <div class="col-md-24">
                  <div class="home_fp">
                    <h6 class="general-title">New this week</h6>
                    <div class="home_fp_wrapper">
                      <data-owl-carousel class="owl-carousel">
                        <div owl-carousel-item class="element no_full_width not-animated" ng-repeat="collection in new_colctns track by $index">
                          <ul class="row-container list-unstyled clearfix">
                            <li class="row-left">
                              <a href="/product-details?product_id=<%collection.product_id%>" class="container_item">
                              <img ng-if='collection.image' ng-src="/product/storage/IN/<%collection.image.global_name%>_IN.<%collection.image.image_ext%>?width=278&height=209" class="img-responsive" alt="<%collection.name%>" />
                              <img ng-if='!collection.image' ng-src="/product/storage/IN/NO_IMAGE_IN.png?width=278&height=209" alt="No Preview" />
                              </a>
                              <div class="hbw">
                                <span class="hoverBorderWrapper"></span>
                              </div>
                            </li>
                            <li class="row-right parent-fly animMix">
                              <div class="product-content-left">
                                <a class="title-5" href="/product-details?product_id=<%collection.product_id%>" title="   <%collection.SKU%>">   <%collection.SKU%><!--  - <%collection.sub_category.s_category_name%> --></a>
                                <span class="shopify-product-reviews-badge" data-id="1293238211"></span>
                              </div>
                              <div class="product-content-right">
                                <div class="product-price" ng-if='!collection.offer_value'>    
                                  <span class="price"><span class='money' >Rs.<%collection.gross_value%></span></span>
                                </div>
                                <div class="product-price" ng-if='collection.offer_value'>
                                <span class="price_sale"><span class="money" style="color: #fc8b1c!important;">Rs.<%collection.offer_value%></span></span>
                                  <del class="price_compare"> <span class="money">Rs.<%collection.gross_value%></span></del>
                                </div>
                              </div>
                              <div class="hover-appear">
                                <div class="effect-ajax-cart">
                                  <form method="POST" action="{{url('/product/cart/add')}}">
                                    <input type="hidden" name="product_id" value="<%collection.product_id%>">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="task" value="add">
                                    <button class="add-to-cart" type="submit" name="add"><i class="fa fa-shopping-cart"></i><span class="list-mode">Add to Cart</span></button>
                                  </form>
                                </div>
                                <div class="product-ajax-qs">
                                  <div data-handle="curabitur-cursus-dignis-1" ng-click='quickView($index)' class="quick_shop" >
                                    <i class="fa fa-eye" title="Quick view"></i><span class="list-mode">Quick View</span>
                                  </div>
                                </div>
                                <!-- <a class="wish-list" href="/account/login" title="wish list"><i class="fa fa-heart"></i><span class="list-mode">Add to Wishlist</span></a> -->
                              </div>
                            </li>
                          </ul>
                        </div>
                      </data-owl-carousel>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
          // If you can't/don't want to set the href property of an element
          // data-remote="http://www...."
          remote: '',
          // For grouping elements
          // data-gallery="galleryname"
          gallery: '',
          // If you have multiple galleries per page, this will restrict the gallery items to the parent that matches this selector.
          gallery_parent_selector: 'document.body',
          // CSS classes for navigation arrows
          left_arrow_class: '.glyphicon .glyphicon-chevron-left',
          right_arrow_class: '.glyphicon .glyphicon-chevron-right',
          // Enable the navigation arrows
          directional_arrows: true,
          // Force the lightbox into image/YouTube mode.
          // image|youtube|vimeo
          // data-type="(image|youtube|vimeo)"
          type: null,
          // Always show the close button, even if no title is present
          always_show_close: true,
          // Don't show related videos when finished playing
          no_related: false,
          // Scale the height as well as width
          scale_height: true,
          // Message injected for loading
          // loadingMessage: 'Loading...',           
          // Callback functions
          onShow: function() {},
          onShown: function() {},
          onHide: function() {},
          onHidden: function() {},
          onNavigate: function() {},
          onContentLoaded: function() {}
      });
  });
</script>

@endsection