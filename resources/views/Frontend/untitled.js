modal_content += '</div><br>'+
			'</div>'+

			'<div class="col-md-12 product-information">'+
			'<div id="quick-shop-image" class="product-image-wrapper">'+
			'<div style="font-size:18px;font-weight:bold;">'+
			'<span class="productname1"><strong>'+collection.name+' - '+collection.sub_category.s_category_name+'</strong></span>'+
			'</div>'+
			'<hr class="stylehr">'+
			'<div class="section group">'+
			'<div class="col span_3_of_3">'+
			'<span class="style3">Item Code &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;'+ collection.SKU+'</span>'+
			'</div>'+
			'<div class="col span_3_of_3">'+
			'<span class="style3">Gross Weight&nbsp;&nbsp;:&nbsp;&nbsp;'+collection.details.weight+''+
			'</span>'+
			'</div>'+
			'<div class="col span_3_of_3">'+
			'<span class="style3">Beeds Weight&nbsp;&nbsp;:&nbsp;&nbsp;'+collection.details.beeds+'</span>'+
			'</div>'+
			'<div class="col span_3_of_3">'+
			'<span class="style3">Cents&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;'+collection.details.cents+
			'</span>'+
			'</div>'+
			'<div class="col span_3_of_3">'+
			'<span class="style3">Metal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;'+collection.details.metal+
			'</span>'+
			'</div>'+
			'<div class="col span_3_of_3">'+
			'<span class="style3">Purity&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+collection.details.purity+
			'</span>'+
			'</div>'+
			// '<div class="col span_3_of_3">'+
			// '<span class="style3">Availability&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;Unknown'+
			// '</span>'+
			// '</div>'+
			'</div>'+
			'<hr class="stylehr">'+
			'<div class="section group">'+
			'<div class="col span_1_of_1">'+
			'<span class="style2"> <strong>';
			
			if (!collection.offer_value) {
				modal_content += '<div class="product-price abharan-compare"> '+
					'<span class="price_sale"><span class="money">Rs.'+collection.gross_value+'</span></span>';
			} else {
				modal_content += '<div class="product-price abharan-compare">'+
					'<span class="price_sale"><span class="money">Rs.'+collection.offer_value+'</span></span>'+
					'<del class="price_compare"> <span class="money">Rs.'+collection.gross_value+'</span></del>';
			}			
			
			modal_content += ((collection.gst)? (' (Inclusive of '+collection.gst+'% GST: '+collection.gst_amount+')') : '')+' (Starting price)&nbsp;&nbsp;  Web Orders Only '+
			'</div></strong></span>'+
			'<span class="WebOrderTextMobile" style="display:none">Web Orders Only</span>'+
			'</div>'+
			'</div>'+
			'<h1 id="quick-shop-title"></h1>'+
			'<div id="quick-shop-infomation" class="description">'+
			'<div id="quick-shop-description" class="text-left"></div>'+
			'</div>'+
			'<div id="quick-shop-container">'+
			'<div id="quick-shop-relative" class="relative text-left">'+
			'<ul class="list-unstyled">'+
			'<li class="control-group vendor">'+
			'<span class="control-label">Vendor :</span>'+
			'</li>'+
			'<li class="control-group type">'+
			'<span class="control-label">Type :</span>'+
			'</li>'+
			'</ul>'+
			'</div>'+
			'<div class="others-bottom">'+
			// '<input id="quick-shop-add" class="btn small add-to-cart" type="submit" name="add" value="View More" />'+
			'<a id="quick-shop-add" class="btn small add-to-cart" href="/product-details?product_id='+collection.product_id+'" >View More</a>'+
			'</div>'+
			'</div>'+
			'</div>'+
			'</div>'+
			'</div>';