@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent" ng-controller='AuthController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Login</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content register-section">
				<div class="container">
					<form method="post" ng-enter="loginUser()" name="customer_login" >
					<div class="row">
						<div id="page-header" ><div class="clear-fix signup-error"></div></div>
						<div id="col-main" class="col-md-7 register-page ">
							<h1 id="page-header" >
								Login
							</h1>
								<ul id="register-form" class="row list-unstyled">
									<li id="first_name">
										<label for="customer_mobile_box" class="control-label">Mobile No.<span class="req">*</span></label>
                             			<input type="number" value="" name="mobile" id="customer_mobile_box" class="form-control" required="required" data-parsley-error-message="Mobile no. is required"/>
									</li>
									<li class="clear-fix"></li>
		                            <li class="clearfix">
		                              <label for="customer_password_box" class="control-label">Password <span class="req">*</span></label>
		                              <input type="password" value="" name="password" id="customer_password_box" class="form-control password" required="required" data-parsley-error-message="Password is required"/>
		                            </li>
		                            <li class="clear-fix"></li>
		                            <li class="clearfix text-right"><a href="/forgot-password">Forgot Password?</a></li>
		                            <li class="clearfix  action-last block-btns">
		                              <input type="hidden" name="_token" value="{{csrf_token()}}">
		                              <button class="btn" type="button" ng-click='loginUser()'>Login</button>
		                            </li>
		                            <li class="clearfix action-last block-btns">
		                              <a class="btn" href="/register">Create an account</a>
		                            </li>
								</ul>
						</div>
					</div>
					</form>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection