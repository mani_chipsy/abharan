<!doctype html> 

<html ng-app="abharan" ng-cloak>
  @include('Frontend.header.home_header')
  <body itemscope itemtype="http://schema.org/WebPage" class="@if (Request::is('/')) templateIndex @else templateProduct @endif" >
    <div id="preloader">
      <div id="spinner">
        <img style="width:100%" src="{{asset('Frontend/assets/images/loader.gif')}}" alt="">
      </div>
      <div id="disable-preloader" class=""></div>
    </div>
    <!-- Header -->
    <header id="top" class="fadeInDown clearfix">
      <!--top-->
      <div class="container">
        <div class="top row">
          <div class="col-md-8 phone-shopping">
            <span><!-- PHONE SHOPING (01) 123 456 UJ -->Toll Free: 1800 5999 777 / Office Hours 10:30 A.M to 6:30 P.M
</span>
          </div>
          <div class="col-md-16">
            <ul class="text-right">
              <li class="customer-links hidden-xs">
                <ul id="accounts" class="list-inline">
                  <li><i class="fa fa-truck" aria-hidden="true"></i> Shipping All Over India  </li>   
                  <li>/</li>
                  @if (Auth::check())
                  <li>
                  <div class="currency_group">
                    <div class="currencies-switcher hidden-xs">
                      <div class="currency btn-group uppercase">
                        <a class="currency_wrapper dropdown-toggle" data-toggle="dropdown">
                        <i class="sub-dropdown1"></i>
                        <i class="sub-dropdown"></i>
                        <span class="heading hidden-xs">Hi {{Auth::user()->name}}! </span>
                        <span class="currency_code visible-xs">USD</span>
                        <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="abh-my-account dropdown-menu" >
                          <li>
                            <a href="/product/order-history" >My Account </a>
                          </li>
                          <li>
                            <a href="/settings" >Settings </a>
                          </li>
                          <li>
                            <a href="/signout" >Signout </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  </li>
                  @else 
                  @if (!Request::is('login'))
                  <li class="login" >
                    <span id="loginButton" uib-dropdown-toggle >
                    Login
                    <i class="sub-dropdown1"></i>
                    <i class="sub-dropdown"></i>
                    </span>
                    <!-- Customer Account Login -->
                    <div id="loginBox" class="dropdown-menu text-left" uib-dropdown-menu ng-controller='AuthController'>
                      <form method="post" ng-enter='loginUser()' name="customer_login" accept-charset="UTF-8">
                        <div id="bodyBox">
                          <ul class="control-container customer-accounts list-unstyled">
                            <li class="clear-fix signup-error" ></li>
                            <li class="clearfix">
                              <label for="customer_mobile_box" class="control-label">Mobile No.<span class="req">*</span></label>
                              <input type="number" value="" name="mobile" id="customer_mobile_box" class="form-control" required="required" data-parsley-error-message="Mobile no. is required"/>
                            </li>
                            <li class="clear-fix"></li>
                            <li class="clearfix">
                              <label for="customer_password_box" class="control-label">Password <span class="req">*</span></label>
                              <input type="password" value="" name="password" id="customer_password_box" class="form-control password" required="required" data-parsley-error-message="Password is required"/>
                            </li>
                            <li class="clearfix text-right"><a href="/forgot-password">Forgot Password?</a></li>
                            <li class="clear-fix"></li>
                            <li class="clearfix">
                              <input type="hidden" name="_token" value="{{csrf_token()}}">
                              <button class="btn" type="button" ng-click='loginUser()'>Login</button>
                            </li>
                            <li class="clearfix">
                              <a class="action btn btn-1" href="/register">Create an account</a>
                            </li>
                          </ul>
                        </div>
                      </form>
                    </div>
                  </li>
                  <li>/</li>
                  @endif
                  <li class="register">
                    <a href="/register" id="customer_register_link">Create an account</a>
                  </li>
                  @endif
                </ul>
              </li>
              <li class="li-currency" ng-controller='DailyRatesController'>
                <div class="currency_group">
                  <div class="currencies-switcher">
                    <div class="currency btn-group uppercase">
                      <a class="currency_wrapper dropdown-toggle" data-toggle="dropdown">
                      <i class="sub-dropdown1"></i>
                      <i class="sub-dropdown"></i>
                      <span class="heading hidden-xs"><b style="color:blue">Daily Rates</b></span>
                      <span class="currency_code visible-xs"><i class="fa fa-inr fa-lg" style="margin-top: 11px;"></i></span>
                      <i class="fa fa-caret-down hidden-xs"></i>
                      </a>
                      <ul class="currencies dropdown-menu text-left">
                        <li class="currency-USD active">
                          <a href="javascript:;"><strong>Metal Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Per Gram</strong></a>
                          <input type="hidden" value="USD" />
                        </li>
                        <li ng-repeat="metal in metal_rates">
                          <a href="javascript:;" >
                          <span ng-if='metal.purity && metal.purity == 916'>Gold 22 kt (916)&nbsp;&nbsp;&nbsp;&nbsp;</span>
                          <span ng-if='metal.purity && metal.purity == 18'>Gold 18 kt  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                          <span ng-if='metal.purity == 0'>Silver&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                          Rs. <%metal.amount%>
                          </a>
                        </li>
                        <!-- 
                          <li >
                            <a href="javascript:;">Gold 18 kt (750) &nbsp;&nbsp;&nbsp;&nbsp;  Rs. 2230 </a>
                            <input type="hidden" value="GBP" />
                          </li>
                          <li >
                            <a href="javascript:;">Silver  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rs. 38.9</a>
                            <input type="hidden" value="GBP" />
                          </li> -->
                      </ul>
                      <select class="currencies_src hide" name="currencies">
                        <option value="USD" selected="selected">USD</option>
                        <option value="EUR">EUR</option>
                        <option value="GBP">GBP</option>
                      </select>
                    </div>
                  </div>
                </div>
              </li>
              <li id="widget-social">
                <ul class="list-inline">
                  <li><a target="_blank" href="https://www.facebook.com/abharanudupi" class="btooltip swing" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook-official"></i></a></li>
                  <!-- <li><a target="_blank" href="https://twitter.com/AbharanUdupi" class="btooltip swing" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a></li> -->
                  <!-- <li><a target="_blank" href="http://www.youtube.com/user/abharanjewellers" class="btooltip swing" data-toggle="tooltip" data-placement="bottom" title="YouTube"><i class="fa fa-youtube"></i></a></li> -->
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!--End top-->
      <div class="line"></div>
      <!-- Navigation -->
      <div class="container">
        <div class="top-navigation">
          <ul class="list-inline">
            <li class="top-logo">
              <a id="site-title" href="/" title="Abharan">
              <img class="img-responsive" src="{{asset('Frontend/assets/images/logo1.png')}}" alt="Abharan" />
              </a>
            </li>
            <li class="navigation">
              <nav class="navbar" role="navigation" ng-controller='FiltersController'>
                <div class="clearfix">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle main navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                  </div>
                  <div class="is-mobile visible-xs">
                    <ul class="list-inline">
                      <li class="is-mobile-menu">
                        <div class="btn-navbar" data-toggle="collapse" data-target=".navbar-collapse">
                          <span class="icon-bar-group">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          </span>
                        </div>
                      </li>
                      <li class="is-mobile-login">
                        <div class="btn-group">
                          <div class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i>
                          </div>
                          <ul class="customer dropdown-menu">
                          @if (Auth::check())
                              <li>
                                <a href="/product/order-history" >My Account </a>
                              </li>
                              <li>
                                <a href="/settings" >Settings </a>
                              </li>
                              <li>
                                <a href="/signout" >Signout </a>
                              </li>
                          @else
                            @if (!Request::is('login'))
                              <li class="logout">
                                <a href="/login">Login</a>
                              </li>
                              <li class="account">
                                <a href="/register">Register</a>
                              </li>
                            @else
                              <li class="account">
                                <a href="/register">Register</a>
                              </li>
                            @endif
                          @endif
                            </ul>
                        </div>
                      </li>
                      <!-- <li class="is-mobile-wl">
                        <a href="javascript:void(0)"><i class="fa fa-heart"></i></a>
                      </li> -->
                      <li class="is-mobile-cart">
                        <a href="/product/cart"><i class="fa fa-shopping-cart"></i></a>
                      </li>
                    </ul>
                  </div>
                  <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav hoverMenuWrapper">
                      <li class="nav-item active">
                        <a href="/">
                        <span>HOME</span>
                        </a>
                      </li>
                      <li class="dropdown mega-menu">
                        <a href="/product/collection?m_category_id=3" class="dropdown-toggle dropdown-link" data-toggle="dropdown">
                        <span>GOLD</span>
                        <i class="fa fa-caret-down"></i>
                        <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>
                        <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                        </a>
                        <div class="megamenu-container megamenu-container-1 dropdown-menu banner-bottom mega-col-4">
                          <ul class="sub-mega-menu">
                            <li ng-repeat="column in g_filter_cat track by $index">
                              <ul>
                                <li ng-repeat="(filtr, filtr_txt) in column" class="list-title"><a href="/product/collection?m_category_id=3&filter_cat=<%filtr%>" class='list-title-a'><%filtr_txt%></a></li>
                              </ul>
                            </li>
                            <li ng-repeat="column in g_sub_cat track by $index">
                              <ul>
                                <li ng-repeat="filtr in column" class="list-title"><a href="/product/collection?m_category_id=3&s_category_id=<%filtr.s_category_id%>" class='list-title-a'><%filtr.s_category_name%></a></li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </li>
                      <!-- SILVER -->
                      <li class="dropdown mega-menu">
                        <a href="/product/collection?m_category_id=6" class="dropdown-toggle dropdown-link" data-toggle="dropdown">
                        <span>SILVER</span>
                        <i class="fa fa-caret-down"></i>
                        <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>
                        <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                        </a>
                        <div class="megamenu-container megamenu-container-1 dropdown-menu banner-bottom mega-col-4">
                          <ul class="sub-mega-menu">
                            <li ng-repeat="column in s_filter_cat track by $index">
                              <ul>
                                <li ng-repeat="(filtr, filtr_txt) in column" class="list-title"><a href="/product/collection?m_category_id=6&filter_cat=<%filtr%>" class='list-title-a'><%filtr_txt%></a></li>
                              </ul>
                            </li>
                            <li ng-repeat="column in s_sub_cat track by $index">
                              <ul>
                                <li ng-repeat="filtr in column" class="list-title"><a href="/product/collection?m_category_id=6&s_category_id=<%filtr.s_category_id%>" class='list-title-a'><%filtr.s_category_name%></a></li>
                              </ul>
                            </li>
                          </ul>

                        </div>
                      </li>
                      <!-- END SILVER -->
                      <!-- DIAMOND -->
                      <li class="dropdown mega-menu">
                        <a href="/product/collection?m_category_id=1" class="dropdown-toggle dropdown-link" data-toggle="dropdown">
                        <span>DIAMOND</span>
                        <i class="fa fa-caret-down"></i>
                        <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>
                        <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                        </a>
                        <div class="megamenu-container megamenu-container-1 dropdown-menu banner-bottom mega-col-4">
                          <ul class="sub-mega-menu">
                            <li ng-repeat="column in d_filter_cat track by $index">
                              <ul>
                                <li ng-repeat="(filtr, filtr_txt) in column" class="list-title"><a href="/product/collection?m_category_id=1&filter_cat=<%filtr%>" class='list-title-a'><%filtr_txt%></a></li>
                              </ul>
                            </li>
                            <li ng-repeat="column in d_sub_cat track by $index">
                              <ul>
                                <li ng-repeat="filtr in column" class="list-title"><a class='list-title-a' href="/product/collection?m_category_id=1&s_category_id=<%filtr.s_category_id%>"><%filtr.s_category_name%></a></li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </li>
                      <li class="nav-item">
                        <a href="/collections">
                        <span>COLLECTIONS</span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="/our-schemes">
                        <span>SCHEMES</span>
                        </a>
                      </li>
                      <!-- End Diamond -->   
                      <li class="nav-item">
                        <a href="/contact-us">
                        <span>CONTACT</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>
            </li>
            <!-- <li class="top-search hidden-xs">
              <div class="header-search">
                <a href="#">
                <span data-toggle="dropdown">
                <i class="fa fa-search"></i>
                <i class="sub-dropdown1"></i>
                <i class="sub-dropdown"></i>
                </span>
                </a>
                <form id="header-search" class="search-form dropdown-menu" action="javascript:void(0);" method="get">
                  <input type="hidden" name="type" value="product" />
                  <input type="text" name="q" value="" accesskey="4" autocomplete="off" placeholder="Search something..." />
                  <button type="submit" class="btn">Search</button>
                </form>
              </div>
            </li> -->
            <!-- <li class="umbrella hidden-xs">
              <div id="umbrella" class="list-inline unmargin" ng-controller='CartListController'>
                <div class="cart-link" name='cart-dropdown'>
                  <a href="{{url('/product/cart')}}" class="dropdown-toggle dropdown-link" data-toggle="dropdown">
                    <i class="sub-dropdown1"></i>
                    <i class="sub-dropdown"></i>              
                    <div class="num-items-in-cart">
                      <span class="icon">
                      Cart
                      <span class="number">{{Cart::content()->count()}}</span>
                      </span>
                    </div>
                  </a>
                  <div id="cart-info" class="dropdown-menu">
                    <div id="cart-content"> -->
                      <!-- <div class="loading">
                        <img src="/s/files/1/0908/7252/t/2/assets/loader.gif" alt="" />
                      </div> -->
                    <!-- </div>
                  </div>
                </div>
              </div>
            </li> -->
            <li class="umbrella hidden-xs">
              <div id="umbrella" class="list-inline unmargin" ng-controller='CartListController'>
                <div class="cart-link">
                  <a href="{{url('/product/cart')}}" class="dropdown-toggle dropdown-link" data-toggle="dropdown">
                    <i class="sub-dropdown1"></i>
                    <i class="sub-dropdown"></i>              
                    <div class="num-items-in-cart">
                      <span class="icon">
                      Cart
                      <span class="number"><%cartCount%></span>
                      </span>
                    </div>
                  </a>
                  <div id="cart-info" class="dropdown-menu" style="display: none;">
                    <div id="cart-content" >
                      <div ng-show='cartItems|keylength'>
                        <div class="items control-container" ng-repeat='item in cartItems|cartlimit track by $index'>
                          <div class="row items-wrapper">
                            <button ng-click='deleteItem(item.id)' class="cart-close"><i class="fa fa-times"></i></button>
                            <div class="col-md-6 cart-left">
                              <a class="cart-image" href="{{url('/product-details?product_id=<%item.id%>')}}">
                                <img ng-src="/product/storage/TH/<%item.options.th_image%>?width=53&height=40" alt="<%item.name%>" title="">
                              </a>
                            </div>
                            <div class="col-md-18 cart-right">
                              <div class="cart-title">
                                <a href="{{url('/product-details?product_id=<%item.id%>')}}"><%item.name%>                                
                                </a>
                              </div>
                              <div class="cart-price"><span class="money" data-currency-usd="$259.00" data-currency="USD"><%item.price | INR %></span></div>
                            </div>
                          </div>
                        </div>
                        <div class="subtotal">
                          <span>Total:</span>
                          <span class="cart-total-right">
                          <span class="money" ><%cartTotal| INR %></span>
                          </span>
                        </div>
                        <div class="action">
                          <!-- <button class="btn" onclick="window.location='/checkout'">CHECKOUT</button> -->
                          <a class="btn btn-1" href="{{url('/product/cart')}}">View Cart</a>
                        </div>
                      </div>
                      <div ng-hide='cartItems|keylength'>Cart is empty</div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <!-- <li class="mobile-search visible-xs">
              <form id="mobile-search" class="search-form" action="javascript:void(0);" method="get">
                <input type="hidden" name="type" value="product" />
                <input type="text" class="" name="q" value="" accesskey="4" autocomplete="off" placeholder="Search something..." />
                <button type="submit" class="search-submit" title="search"><i class="fa fa-search"></i></button>
              </form>
            </li> -->
          </ul>
        </div>
      </div>
      <!--End Navigation-->
      <script>
        function addaffix(scr){
          if($(window).innerWidth() >= 1024){
            if(scr > $('#top').innerHeight()){
              if(!$('#top').hasClass('affix')){
                $('#top').addClass('affix').addClass('animated');
              }
            }
            else{              
              if($('#top').hasClass('affix')){
                $('#top').prev().remove();
                $('#top').removeClass('affix').removeClass('animated');
              }
            }
          }
          else $('#top').removeClass('affix');
        }
        $(window).scroll(function() {
          var scrollTop = $(this).scrollTop();
          addaffix(scrollTop);
        });
        $( window ).resize(function() {
          var scrollTop = $(this).scrollTop();
          addaffix(scrollTop);
        });
      </script>
    </header>
    @yield('content')
    <script src="{{asset('Frontend/jscontrols/home_controls.js')}}"></script>
    <!--Androll-->
    <!-- Google Code -->
    <footer id="footer">
      <div id="footer-content">
        <div class="footer-content footer-content-top clearfix">
          <div class="container">
            <div class="footer-link-list col-md-6">
              <div class="group">
               <h4 class="footer-title footer_padding">About Us</h4>
                 <hr class="footer_border">
                <ul class="list-unstyled">
                  <li >
                    <a href="/about-us">Who we are</a>
                  </li>
                  <!-- <li class="list-unstyled">
                    <a href="/">Whosesalers</a>
                    </li>
                    <li class="list-unstyled">
                    <a href="/">Map Site</a>
                    </li> -->
                  <li >
                    <a href="/contact-us">Contact Us</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="footer-link-list col-md-6">
              <div class="group">
                <h4 class="footer-title footer_padding">Information</h4>
                <hr class="footer_border">
                 <ul class="list-unstyled">
                  <li class="list-unstyled">
                    <a href="/faqs">Help &amp; FAQs</a>
                  </li>
                  <li class="list-unstyled">
                    <a href="/jewellery-care">Jewellery Care</a>
                  </li>
                  <!-- <li class="list-unstyled">
                    <a href="/">Advance Search</a>
                    </li>
                    <li class="list-unstyled">
                    <a href="/">Gift Cards</a>
                    </li>
                    <li class="list-unstyled">
                    <a href="/">Shop By Brands</a>
                    </li> -->
                </ul>
              </div>
            </div>
            <div class="footer-link-list col-md-6">
              <div class="group">
              <!--   <h4 class="footer-title"></h4> -->
                

        <h4 class="footer-title footer_padding">Account</h4>
        <hr class="footer_border">
        <ul class="list-unstyled">
        <li><a href="/product/cart">Cart Page</a></li>
        <li><a href="/login">Sign In</a></li>
        </ul>
              </div>
            </div>
            <div class="footer-link-list col-md-6">
               <div class="group">
              <!--   <h4 class="footer-title"></h4> -->
                

        <h4 class="footer-title footer_padding">Customer</h4>
        <hr class="footer_border">
        <ul class="list-unstyled">
        <li><a href="/return-policy">Return Policy</a></li>
        <li><a href="/contact-us">Help &amp; Contact</a></li>
        </ul>
              </div>



            </div>
          </div>
        </div>
        <div class="footer-content footer-content-bottom clearfix">
          <div class="container">
            
            
            <div class="copyright col-md-8">© 2018 <a href="/">Abharan</a>. All Rights Reserved.</div>
            <div id="widget-payment" class="col-md-16">
              <ul id="payments" class="list-inline animated">

            <li class="" data-toggle="tooltip" data-placement="top" title="GoDaddy"><span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=F3H6V72V6TD9cN7wXyEjzPgroTtwb4VW3iO8YI7RzSC42WX2I2PTJHFHvFak"></script></span></li>
                <li  title="BIS Hallmark and IGI" name="certification"><a href="#"><img src="{{asset('Frontend/assets/images/bg/footer/BIS_Hallmark_and_IGI.png')}}"></a></li>
                <li class="btooltip tada" data-toggle="tooltip" data-placement="top" title="Visa"><a href="#" class="icons visa"></a></li>
                  <li class="btooltip tada" data-toggle="tooltip" data-placement="top" title="Mastercard"><a href="#" class="icons mastercard"></a></li> 
<li class="btooltip tada" data-toggle="tooltip" data-placement="top" title="Maestro"><a href="#" class="icons mastercard"></a></li> 

                <li class="copyright" data-toggle="tooltip" data-placement="top" title="Chipsy">Designed &amp; Developed by <a href="http://chipsy.in/" target="
                  _blank">Chipsy</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>

    {{--<footer id="footer">
      <div id="footer-content">
        <div class="footer-content footer-content-top clearfix">
          <div class="container">
            <div class="footer-link-list col-md-6">
              <div class="group">
                <h5 class="general-title">About Us</h5>
                <ul class="list-unstyled list-styled">
                  <li class="list-unstyled">
                    <a href="/about-us">Who we are</a>
                  </li>
                  <!-- <li class="list-unstyled">
                    <a href="/">Whosesalers</a>
                    </li>
                    <li class="list-unstyled">
                    <a href="/">Map Site</a>
                    </li> -->
                  <li class="list-unstyled">
                    <a href="/contact-us">Contact Us</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="footer-link-list col-md-6">
              <div class="group">
                <h5 class="general-title">Information</h5>
                <ul class="list-unstyled list-styled">
                  <li class="list-unstyled">
                    <a href="/faqs">Help &amp; FAQs</a>
                  </li>
                  <li class="list-unstyled">
                    <a href="/jewellery-care">Jewellery Care</a>
                  </li>
                  <!-- <li class="list-unstyled">
                    <a href="/">Advance Search</a>
                    </li>
                    <li class="list-unstyled">
                    <a href="/">Gift Cards</a>
                    </li>
                    <li class="list-unstyled">
                    <a href="/">Shop By Brands</a>
                    </li> -->
                </ul>
              </div>
            </div>
            <div class="footer-link-list col-md-6">
              <div class="group">
                <h5 class="general-title">Account</h5>
                <ul class="list-unstyled list-styled">
                  <!-- <li class="list-unstyled">
                    <a href="/">Preferences</a>
                    </li>
                    <li class="list-unstyled">
                    <a href="/">Order History</a>
                    </li> -->
                  <li class="list-unstyled">
                    <a href="/product/cart">Cart Page</a>
                  </li>
                  @if (!Auth::check())
                  <li class="list-unstyled">
                    <a href="/login">Sign In</a>
                  </li>
                  @else
                  <li class="list-unstyled">
                    <a href="/product/order-history">My Account</a>
                  </li>
                  @endif
                </ul>
              </div>
            </div>
            <div class="footer-link-list col-md-6">
              <div class="group">
                <h5 class="general-title">Customer</h5>
                <ul class="list-unstyled list-styled">
                  <!-- <li class="list-unstyled">
                    <a href="/">Search Advanced</a>
                    </li> -->
                  <li class="list-unstyled">
                    <a href="/return-policy">Return Policy</a>
                  </li>
                  <!-- <li class="list-unstyled">
                    <a href="/">Privacy Policy</a>
                    </li> -->
                  <li class="list-unstyled">
                    <a href="/contact-us">Help &amp; Contact</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-content footer-content-bottom clearfix">
          <div class="container">--}}
            {{--<div class="row">
              <div class="col-md-6 f-col">
                <span class="opener plus"></span>
                <ul class="col-md-24">
                  <div style="color: #FFBD2F;padding-bottom: 3px;font-weight: bold;">Udupi Showroom</div>
                  <p style="color: #878787;">Corporation Bank Road Udupi 576 101<br>
                    Email: info@abharan.com<br>
                    Ph: 0820 429 7777, 252 1267<br>
                    Business Hours (Mon - Sat): <br>
                    10.30 – 6.30<br>
                    (no lunch break)<br>
                  </p>
                  <div style="color: #FFBD2F;padding-bottom: 3px;font-weight: bold;">Udupi Silver Showroom</div>
                  <p style="color: #878787;">Sharada,Corporation Bank Road<br>
                    Udupi 576 101<br>
                    Email: info@abharan.com<br>
                    Ph: 0820 4297710, 252 1267<br>
                    Business Hours (Mon - Sat):<br>
                    10.30 – 6.30<br>
                    (no lunch break)<br>
                  </p>
                </ul>
              </div>
              <div class="col-md-6 f-col">
                <span class="opener plus"></span>
                <ul class="col-md-24">
                  <div style="color: #FFBD2F;padding-bottom: 3px;font-weight: bold;">Shivamogga Showroom</div>
                  <p style="color: #878787;">Kshiti Complex, J.P.N. Road<br>
                    Shivamogga 577 201<br>
                    Email: abjsmg@abharan.com<br>
                    Ph: 08182 270 458, 224 848<br>
                    Business Hours (Mon - Sat): <br>
                    11.00 – 7.00<br>
                    (no lunch break)<br>
                  </p>
                  <div style="color: #FFBD2F;padding-bottom: 3px;font-weight: bold;">Mangaluru Showroom</div>
                  <p style="color: #878787;">Central Park, Shivbagh Road<br>
                    Kadri, Mangaluru 575 002<br>
                    Email: mng@abharan.com<br>
                    Ph: 0824 221 6111, 221 8111<br>
                    Business Hours (Mon - Sat):<br>
                    10.30 – 7.30<br> 
                    (no lunch break)<br>
                  </p>
                </ul>
              </div>
              <div class="col-md-6 f-col">
                <span class="opener plus"></span>
                <ul class="col-md-24">
                  <div style="color: #FFBD2F;padding-bottom: 3px;font-weight: bold;">Karkala Showroom</div>
                  <p style="color: #878787;">Near Sri Venkataramana Temple<br>
                    Main Road, Karkala 574 104<br>
                    Email: krk@abharan.com<br>
                    Ph: 08258 234 444<br>
                    Business Hours (Mon - Sat): <br>
                    10.30 – 6.30 <br>
                    (no lunch break)<br>
                  </p>
                  <div style="color: #FFBD2F;padding-bottom: 3px;font-weight: bold;">Kundapur Showroom</div>
                  <p style="color: #878787;">S.V. Temple Road<br>
                    Kundapur 576 201<br>
                    Email: knd@abharan.com<br>
                    Ph: 08254 234 444<br>
                    Business Hours (Mon - Sat):<br>
                    10.30 – 6.30<br>
                    (no lunch break)<br>
                  </p>
                </ul>
              </div>
              <div class="col-md-6 f-col">
                <span class="opener plus"></span>
                <ul class="col-md-24">
                  <div style="color: #FFBD2F;padding-bottom: 3px;font-weight: bold;">Hebri Showroom</div>
                  <p style="color: #878787;">Sudhindra Krupa Towers<br>
                    Near Bus Stand<br>
                    Hebri 576112<br>
                    Ph: 08253 250 444<br>
                    Business Hours (Mon - Sat):<br>  
                    10.30 – 6.30,<br>  
                    (no lunch break)
                    <br>
                  </p>
                  <div style="color: #FFBD2F;padding-bottom: 3px;font-weight: bold;">Byndoor Showroom</div>
                  <p style="color: #878787;">Sowparnika Complex<br>
                    N.H. 66, Byndoor <br>
                    Email: bnd@abharan.com <br>
                    Ph: 08254 252 222<br> 
                    Business Hours (Mon - Sat):<br>
                    10.30 – 6.30<br>
                    (no lunch break)<br>
                  </p>
                </ul>
              </div>
            </div>--}}
            {{--<hr style="margin-bottom: 0px;
              margin-top: 0px;
              border-top: 1px solid #dedede;
              border-bottom: none;
              border-left: none;
              border-right: none;
              border-color: #444444;
              ">--}}
          {{--  <div class="copyright col-md-8">&copy; 2018 <a href="/">Abharan</a>. All Rights Reserved.</div>
            <div id="widget-payment" class="col-md-16">
              <ul id="payments" class="list-inline animated">
              <li class="" data-toggle="tooltip" data-placement="top" title="GoDaddy"><span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=F3H6V72V6TD9cN7wXyEjzPgroTtwb4VW3iO8YI7RzSC42WX2I2PTJHFHvFak"></script></span></li>
                <li  title="BIS Hallmark and IGI" name="certification"><a href="#"><img src="{{asset('Frontend/assets/images/bg/footer/BIS_Hallmark_and_IGI.png')}}"></a></li>
                <li class="btooltip tada" data-toggle="tooltip" data-placement="top" title="Visa"><a href="#" class="icons visa"></a></li>
                  <li class="btooltip tada" data-toggle="tooltip" data-placement="top" title="Mastercard"><a href="#" class="icons mastercard"></a></li> 
<li class="btooltip tada" data-toggle="tooltip" data-placement="top" title="Maestro"><a href="#" class="icons mastercard"></a></li> 

                <li class="copyright" data-toggle="tooltip" data-placement="top" title="Chipsy">Designed &amp; Developed by <a href="http://chipsy.in/" target="
                  _blank">Chipsy</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>--}}
    <div id="quick-shop-modal" class="modal" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true" tabindex="-1" data-width="800">
      <div class="modal-dialog new-modal-width rotateInDownLeft">
        <div class="modal-content">
          <div class="modal-header">
            <i class="close fa fa-times btooltip" data-toggle="tooltip" data-placement="top" title="Close" data-dismiss="modal" aria-hidden="true"></i>
          </div>
          <div class="modal-body">
          </div>
        </div>
      </div>
    </div>
  <!--    <div class="newsletter-popup" style="display: none;">   -->
  <!--<form action="https://codespot.us5.list-manage.com/subscribe/post?u=ed73bc2d2f8ae97778246702e&amp;id=c63b4d644d" method="post" name="mc-embedded-subscribe-form" target="_blank">
    <h4>-50% Deal</h4>
    <p class="tagline">subscribe for newsletter and get the item for 50% off</p>
    <div class="group_input">
      <input class="form-control" type="email" name="EMAIL" placeholder="YOUR EMAIL" />
      <button class="btn" type="submit"><i class="fa fa-paper-plane"></i></button>
    </div>
  </form>
  <div id="popup-hide">
    <input type="checkbox" id="mc-popup-hide" value="1" checked="checked"> <label for="mc-popup-hide">Never show this message again</label>
  </div> -->
<!--  </div>  -->
 <script type="text/javascript">
    jQuery(document).ready(function($) {
  
  /* Cokkies Popup */
  // if ({{(int)\Request::is('/')}} && !$.cookie('mycookie') || !{{(int)\Request::is('/')}}) {
    // $.fancybox(
      // $('.newsletter-popup'),
      // {
      //   'autoDimensions'    : false,
      //   'width'             : 970,
      //   'height'            : 486,
      //   'autoSize' : false,
      //   'transitionIn'      : 'none',
      //   'transitionOut'     : 'none',
      //   afterLoad: function(){
      //     setTimeout( function() {$.fancybox.close(); },500000);
      //   }
      // }
    // );
    // it hasn't been one days yet
  /*}*//* else {
    $.fancybox(
      $('.newsletter-popup'),
      {
        'autoDimensions'    : false,
        'width'             : 970,
        'height'            : 486,
        'autoSize' : false,
        'transitionIn'      : 'none',
        'transitionOut'     : 'none',
        afterLoad: function(){
          setTimeout( function() {$.fancybox.close(); },500000);
        }
      }
    );
  }*/
  // $.cookie('mycookie', 'true', { expires: 1});
});
 </script>
  </body>
</html>