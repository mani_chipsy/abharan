@extends('Frontend.layouts.home_layout')
@section('content')
<style type="text/css">

@media (min-width:320px) and (max-width: 640px)
{
  .media-body {
    padding-left: 20px;
  }
}
.post-header-line { border-top:1px solid #fff;border-bottom:1px solid #fff;padding:5px 0px 5px 15px;font-size: 12px; margin-bottom: 10px}
.block-description { 
    padding-top: 10px;
    text-align: justify;
  }
</style>
<link rel="stylesheet" type="text/css" href="{{asset('Frontend/assets/css/custom.css')}}">
<link href="{{asset('Assets/lightbox/ekko-lightbox.min.css')}}" rel="stylesheet">
<script src="{{asset('Assets/lightbox/ekko-lightbox.js')}}"></script>
<style type="text/css">
.mod-gen-title {
    margin-top: 30px!important;
    margin-bottom: 45px!important;
  }
  .main-list li::after {
   content: '';
    width: 90%;
    margin-top: 10px;
    border-bottom: solid 1px #fff;
    position: absolute;
  }
  .main-list li:last-child::after {
    width:0;
  }
  .feat-art-bg {
    border: 1px dotted #cac4c4;
  }
  .feat-art-cont {
    padding: 2px 30px;
  }
  .feat-art-disc {
    padding:0;
  }
  .feat-art-padd {
    width: 0px;
  }
  .feat-art-rec {
    padding: 14px;
  }
  .mod-media-head {
    text-transform: none;
  }
</style>
<div id="content-wrapper-parent" >
  <div id="content-wrapper">
    <!-- Content -->
    <div id="content" class="clearfix">
      <div id="breadcrumb" class="breadcrumb">
        <div itemprop="breadcrumb" class="container">
          <div class="row">
            <div class="col-md-24">
              <a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
              <span>/</span>
              <span class="page-title">Events</span>
            </div>
          </div>
        </div>
      </div>
      <section class="pb-95" ng-controller="BlogController">
          <div class="home-newproduct ">
            <div class="container">
            <div class="group_home_products row">
              <div class="col-md-24">
                <div class="home_products">
                  <h6 class="general-title mod-gen-title">Latest News</h6>
                  
                  <div class="row" ng-show='articles.length'>
                    <div class="col-md-14 col-lg-14 feat-art-cont feat-art-bg" >
                      <!-- artigo em destaque -->
                      <div class="featured-article" id='featured-article' >
                        <div class="row">
                            <h2><%featured.title%></h2>
                            <div class="col-md-24 post-header-line">
                                <i class="fa fa-user"></i> by Abharan | <i class="fa fa-calendar">
                                </i> <%featured.created_str%>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-md-24 feat-art-disc" >
                            <a href="#">
                              <img ng-src="/api/get-image/<%featured.image.year%>/<%featured.image.month%>/lg/<%featured.image.img_name%>.<%featured.image.ext%>" alt="" class="thumb">
                            </a>                        
                            <div class="block-description" ng-bind-html='trustAsHtml(featured.description)'>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- /.featured-article -->
                    </div>
                    <div class="col-md-1 col-lg-1 feat-art-padd" >&nbsp;</div>
                    <div class="col-md-9 col-lg-9 feat-art-rec feat-art-bg" >
                      <ul class="media-list main-list">
                        <li class="media" ng-repeat="article in articles">
                          <div class="row" ng-click="getArticle($index)">
                            <div class="col-lg-10 col-md-10 col-sm-24">
                              <a class="pull-left" href="javascript:void(0);">
                                <img class="media-object" ng-src="/api/get-image/<%article.image.year%>/<%article.image.month%>/th/<%article.image.img_name%>.<%article.image.ext%>" alt="...">
                              </a>
                            </div>
                            <div class="col-lg-14 col-md-14 col-sm-24" style="margin-left: -50px">
                              <div class="media-body">
                                <h4  class="media-heading mod-media-head"><%article.title%></h4>
                                <p class="by-author">By Abharan</p>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                      <div class="col-md-24 col-lg-24" style="padding:0" ng-show='totalArticles > 10'>
                        <ul  uib-pagination total-items="totalArticles" ng-model="currentPage" class="pagination-sm chng-pagination" boundary-links="true" rotate="true" max-size="maxSize" items-per-page="articlesPerPage"></ul>
                      </div>
                    </div>
                  </div>

                  <div class="row" ng-hide='articles.length'>
                    <div class="col-md-24 col-lg-24 text-center">
                     <b>No blog post to display</b>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </section>
    </div>
  </div>
</div>
<script type="application/javascript" src="/Frontend/jscontrols/home_controls.js"></script>
<script type="text/javascript">
  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
          // If you can't/don't want to set the href property of an element
          // data-remote="http://www...."
          remote: '',
          // For grouping elements
          // data-gallery="galleryname"
          gallery: '',
          // If you have multiple galleries per page, this will restrict the gallery items to the parent that matches this selector.
          gallery_parent_selector: 'document.body',
          // CSS classes for navigation arrows
          left_arrow_class: '.glyphicon .glyphicon-chevron-left',
          right_arrow_class: '.glyphicon .glyphicon-chevron-right',
          // Enable the navigation arrows
          directional_arrows: true,
          // Force the lightbox into image/YouTube mode.
          // image|youtube|vimeo
          // data-type="(image|youtube|vimeo)"
          type: null,
          // Always show the close button, even if no title is present
          always_show_close: true,
          // Don't show related videos when finished playing
          no_related: false,
          // Scale the height as well as width
          scale_height: true,
          // Message injected for loading
          // loadingMessage: 'Loading...',           
          // Callback functions
          onShow: function() {},
          onShown: function() {},
          onHide: function() {},
          onHidden: function() {},
          onNavigate: function() {},
          onContentLoaded: function() {}
      });
  });
</script>
@endsection