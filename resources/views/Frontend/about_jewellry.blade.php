@extends('Frontend.layouts.home_layout')
@section('content')
<style>


/*  bhoechie tab */
div.bhoechie-tab-container{
  z-index: 10;
  background-color: #ffffff;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;
  margin-top: 20px;
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.bhoechie-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.bhoechie-tab-menu div.list-group{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a .glyphicon,
div.bhoechie-tab-menu div.list-group>a .fa {
  color: #a07936;
}
div.bhoechie-tab-menu div.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a.active,
div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
div.bhoechie-tab-menu div.list-group>a.active .fa{
  background-color: #a07936;
  background-image: #a07936;
  color: #ffffff;
}
div.bhoechie-tab-menu div.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #a07936;
}

div.bhoechie-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
  padding-top: 10px;
}

div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
</style>
<link href="{{asset('Frontend/assets/css/custom.css')}}" rel="stylesheet" type="text/css" media="all" />
<div id="content-wrapper-parent">
  <div id="content-wrapper">
    <div id="content" class="clearfix">
      <div id="breadcrumb" class="breadcrumb">
        <div itemprop="breadcrumb" class="container">
          <div class="row">
            <div class="col-md-24">
              <a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
              
              <span>/</span>
              <span class="page-title">About Jewellery</span>
               
            </div>
          </div>
        </div>
      </div>
      <section class="content">
        <div class="container">
          
	   
	   
	 
	<div class="row pt-85">
        <div class="col-md-24 bhoechie-tab-container">
            <div class="  col-md-8   bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item active text-center">
              UNIQUE INDIAN JEWELLERY
                </a>
                <a href="#" class="list-group-item text-center">
                GLITTERING DIAMONDS
                </a>
                <a href="#" class="list-group-item text-center">
                 GOLD JEWELLERY
                </a>
                <a href="#" class="list-group-item text-center">
             SILVER JEWELLERY
                </a>
                <a href="#" class="list-group-item text-center">
          PLATINUM JEWELLERY
                </a>
				 <a href="#" class="list-group-item text-center">
          PEARL JEWELLERY
                </a>
              </div>
            </div>
            <div class="col-md-16   bhoechie-tab">
                <!-- flight section -->
                <div class="bhoechie-tab-content active">
                    <p>India is unique in its culture and traditions. The concept of 'beauty' was well entrenched in this culture. In fact, our ancient remedies for enhancement of beauty by way of herbal treatments and oils are well known and well documented. So too were the enhancements to beautify in the form of ornaments, jewellery and other accessories. From discoveries made, there is enough evidence to prove that our culture promoted beautiful jewellery and craftsmanship. These include excavated jewellery made from ivory tusks, feathers, leaves, nuts, berries, flowers, bones and even teeth of animals.</p>
<p>Jewellery as an art form goes back to our early ancestors. It's roughly estimated that Indian jewellery designs had its beginning nearly 5000 years ago.</p>
<p>At first, jewellery was made from what was found in nature and its immediate surroundings. Today, we have sophisticated processes, refining and plating techniques to transform metals and gemstones into beautiful eye-catching jewellery.</p>
                </div>
                <!-- train section -->
                <div class="bhoechie-tab-content">
                  <p>Diamond is a much adorned gemstone by most of the jewellery lovers. It is also considered as the birthstone for those born in the month of April. A cherished gemstone for couples celebrating their tenth and their sixtieth year of wedding anniversary. The original name of the diamond is derived from the Greek word called "adamas", which refers to the quality of unconquerable. This gemstone also depicts a true value for love and eternity. Various types of rings, necklaces, bands and bracelets are being designed using this precious gemstone.</p>
                </div>
    
                <!-- hotel search -->
                <div class="bhoechie-tab-content">
                   <p>Gold is considered as the first mined metal. Gold is the base metal for any type of jewellery. Most of the jewellery items made up of gold are long-lasting and easy to maintain. These days a variety of gemstones are embedded along with the various pieces of gold items to enhance its look and dignity. Without gold any function or occasion is considered incomplete. Especially in India, during weddings the bride is adorned with gold jewellery enhancing the look and beauty of the bride.</p>
                </div>
                <div class="bhoechie-tab-content">
                    <p>In India silver holds the second place in jewellery and ornaments. Considered holy and spiritual due to its healing and magical properties, the metal is said to be auspicious. This is the main reason that most of the Indian household prefer to have silver items in the form of jewellery pieces such as ornaments and utensils. Anklets, bracelets are the common items among silver jewellery. Born with a silver spoon is a common notion to depict the sanctity and celebrity of silver on any occasion.</p>
                </div>
                <div class="bhoechie-tab-content">
                   <p>The name Platinum is derived from the Spanish term platina, which literally translates into "little silver". Platinum's resistance to wear and tarnish is well suited for making fine jewellery. And its rarity as a metal is often been associated with exclusivity and wealth. This jewellery item has made a popular mode and approach in its various designs of ring forms and ring settings. Due to its brilliance and shine, the sale of platinum jewellery is expected to overmatch the sale of other jewellery items.</p>
                </div>
				 <div class="bhoechie-tab-content">
                   <p>Similar to gold, pearl also holds a unique place in Indian jewellery. The origin of pearl is itself an interesting story. The formation of pearl is a tough job for the pearl oyster, which carries the pearl formation in its primary shell. Whenever a foreign substance enters the body of the pearl oyster, the defense mechanism helps to overlap the foreign substance by secreting nacre around the same. These layers of nacre are thereby formed into an oval shape thus giving form to the new pearl. The formation of natural pearl is a very painful task to this organism. Though pearl is naturally occurring, it can also be artificially induced as done in various pearl producing farms.</p>
                </div>
            </div>
        </div>
  </div>
 
	<script>   
	$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});   
	   </script>
	   
	   
	   
	   
	   
	   
	   
	  
	  
        </div>
      </section>
    </div>
  </div>
</div>
@endsection