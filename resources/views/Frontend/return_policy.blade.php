@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent">
      <div id="content-wrapper">
        <!-- Content -->
        <div id="content" class="clearfix">
          <div id="breadcrumb" class="breadcrumb">
            <div itemprop="breadcrumb" class="container">
              <div class="row">
                <div class="col-md-24">
                  <a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
                  <span>/</span>
                  <span class="page-title">Abharan Return Policies
                  </span>
                </div>
              </div>
            </div>
          </div>
          <section class="content">
            <div class="container">
              <div class="container">
                <div class="row">
                  <div id="page-header" class="col-md-24">
                    <h1 id="page-title">Abharan Return Policies</h1>
                  </div>
                  <div id="col-main" class="blog blog-page col-xs-24 col-sm-24 col-content col-content ">
                    <div class="txtbx01 clearfix" >
                      <hr>
                      <h3 style="text-transform:capitalize; font-weight:normal;">Return Policies</h3>
                      <p>Abharan is committed to making sure that you are completely satisfied with the quality of our products and the service that you receive.</p>
                      <h3 style="text-transform:capitalize; font-weight:normal;">Returns and Exchanges
                      </h3>
                      <p>Your complete shopping satisfaction is our No.1 Priority. If an item you ordered from abharan.com does not meet your expectations, please contact us within fifteen (15) days of purchase to obtain a return authorization and instructions. The product must be returned in original condition in the original packaging. Upon arrival and inspection, product refunds will be promptly credited to the original purchase account, shipping excluded. Certain items may have additional specific return conditions published on the respective product web page.</p>
                      <h3 style="text-transform:capitalize; font-weight:normal;">Warranty and Defect Claims</h3>
                      <p>Abharan takes takes complete responsibility for return and replacement shipping costs if any product is determined to be defective. Please contact us within the warranty period to obtain instructions.</p>
                    </div>
                    <hr>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
@endsection