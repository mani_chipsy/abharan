@extends('Frontend.layouts.home_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('Frontend/assets/css/custom.css')}}">
<link href="{{asset('Assets/lightbox/ekko-lightbox.min.css')}}" rel="stylesheet">
<script src="{{asset('Assets/lightbox/ekko-lightbox.js')}}"></script>

<div id="content-wrapper-parent" >
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Schemes</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content">
		      <div class="container">
		      <br >
		      <div class="row">
		      <div class="col-md-6 col-sm-4" style="margin-top: 20px;">
		        <div class="account-sidebar account-tab mb-xs-30">
		          <div class="account-tab-inner">
		            <a class="btn btn-1 enable hidden-xs" href="javascript:void(0)">SCHEMES</a>
		            <!--  <li id="step2">
		              <a href="javascript:void(0)">VIDEOS<i class="fa fa-angle-right"></i>
		              </a>
		              </li>
		              
		              <li id="step3">
		              <a href="javascript:void(0)">EXHIBITIONS<i class="fa fa-angle-right"></i>
		              </a>
		              </li>
		              <li id="step4">
		              <a href="javascript:void(0)">EVENTS<i class="fa fa-angle-right"></i>
		              </a>
		              </li>
		              <li id="step5">
		              <a href="javascript:void(0)">SHOWROOMS<i class="fa fa-angle-right"></i>
		              </a>
		              </li>
		              <li id="step6">
		              <a href="javascript:void(0)">CALENDAR<i class="fa fa-angle-right"></i>
		              </a>
		              </li>-->
		          </div>
		        </div>
		      </div>
		      <div class="col-md-18 col-sm-8">
		      <div id="data-step1" class="account-content" data-temp="tabdata">
		        <div class="row">
		          <div class="col-md-24">
		            <div class="heading-part heading-bg mb-30">
		              <h3 id="page-title">SCHEMES</h3>
		            </div>
		          </div>
		        </div>
		        <div class="mb-30">
		          <div class="row">
		            <div class="col-md-24">
		              <div class="row">
		              	<div class="col-lg-8 col-md-8 col-sm-24 col-xs-24 " >
		              		<a href="{{asset('Frontend/assets/images/schema/SCHEME 50-50.jpg')}}" data-toggle="lightbox">
		                      <div class="gcontainer">
		                        <img style="width:100%" src="{{asset('Frontend/assets/images/schema/SCHEME 50-50.jpg')}}" alt="Abharan" /> 
		                        <div class="overlay">
		                          <div class="text">Click to view</div>
		                        </div>
		                      </div>
		                    </a>
	                  	</div>
	                  	<div class="col-lg-8 col-md-8 col-sm-24 col-xs-24 ">
		                    <a href="{{asset('Frontend/assets/images/schema/SCHEMEpendant_plan.jpg')}}" data-toggle="lightbox">
		                      <div class="gcontainer">
		                        <img style="width:100%" src="{{asset('Frontend/assets/images/schema/SCHEMEpendant_plan.jpg')}}" alt="Abharan" />  
		                        <div class="overlay">
		                          <div class="text">Click to view</div>
		                        </div>  
		                      </div>
		                    </a>
	                  	</div>
	                  	<div class="col-lg-8 col-md-8 col-sm-24 col-xs-24 ">
		                    <a href="{{asset('Frontend/assets/images/schema/SCHEMESmart_savings (1).jpg')}}" data-toggle="lightbox">
		                      <div class="gcontainer">
		                       <img style="width:100%" src="{{asset('Frontend/assets/images/schema/SCHEMESmart_savings (1).jpg')}}" alt="Abharan" />
		                       <div class="overlay">
		                          <div class="text">Click to view</div>
		                        </div> 
		                      </div>
		                    </a>
	                  	</div>
		              </div>
		            <hr style="clear: both; padding-top: 58px;">
		          </div>
		        </div>
		      </div>
		      </div>
		    </section>
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
          // If you can't/don't want to set the href property of an element
          // data-remote="http://www...."
          remote: '',
          // For grouping elements
          // data-gallery="galleryname"
          gallery: '',
          // If you have multiple galleries per page, this will restrict the gallery items to the parent that matches this selector.
          gallery_parent_selector: 'document.body',
          // CSS classes for navigation arrows
          left_arrow_class: '.glyphicon .glyphicon-chevron-left',
          right_arrow_class: '.glyphicon .glyphicon-chevron-right',
          // Enable the navigation arrows
          directional_arrows: true,
          // Force the lightbox into image/YouTube mode.
          // image|youtube|vimeo
          // data-type="(image|youtube|vimeo)"
          type: null,
          // Always show the close button, even if no title is present
          always_show_close: true,
          // Don't show related videos when finished playing
          no_related: false,
          // Scale the height as well as width
          scale_height: true,
          // Message injected for loading
          // loadingMessage: 'Loading...',           
          // Callback functions
          onShow: function() {},
          onShown: function() {},
          onHide: function() {},
          onHidden: function() {},
          onNavigate: function() {},
          onContentLoaded: function() {}
      });
  });
</script>
@endsection