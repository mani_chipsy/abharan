@extends('Frontend.layouts.home_layout')
@section('content')
<style type="text/css">
	.def-height {
		font-size: 18px;
		color: red;
	}
	 .mod-titl {
	 	text-transform: none!important;
	 }

	 .toll-link {
	 	color: blue;
	 	font-weight: normal;
	 	font-size: 18px;
	 }
</style>
<div id="content-wrapper-parent" >
	<div id="content-wrapper">
		<!-- Content -->

		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Service not available</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content">
				<div class="container">
					<div class="row">
						<div id="page-header" class="col-md-24">
							<h1 id="page-title" class=" mod-titl">Service not available!</h1>
						</div>
						<div id="col-main" class="col-md-24 cart-page content">
							<div >
			            		<p class="def-height"  > Online orders temporarily stopped. We apologize for the inconvenience. <br>For any other query please contact our toll free number <a href="tel:18005999777" class="toll-link" >  1800 5999 777 </a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection