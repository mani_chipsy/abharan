@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent" ng-controller='AuthController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Forgot Password</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content register-section">
				<div class="container">
					<form method="post" ng-enter="sendResetOtp()" name="forgot_password">
						<div class="row">
							<div id="page-header" ><div class="clear-fix signup-error"></div></div>
							<div id="col-main" class="col-md-7 register-page">
								<p id="page-header" >
									Can't sign in? Forget your password?<br>
									Enter your mobile number below and we'll send you One Time Password (OTP) to reset your password in next step
								</p>
									<ul id="register-form" class="row list-unstyled " style="height:180px">
										<li id="first_name">
											<label for="customer_mobile_box" class="control-label">Enter your mobile number <span class="req">*</span></label>
	                             			<input type="number" tabindex='10' autofocus value="" name="mobile" id="customer_mobile_box" class="form-control" required="required" data-parsley-error-message="Mobile no. is required"/>
										</li>
										<li class="clear-fix"></li>
			                            <li class="clearfix  action-last block-btns">
			                              <input type="hidden" name="_token" value="{{csrf_token()}}">
			                              <button class="btn" type="button" tabindex='15' ng-click="sendResetOtp()">Get OTP</button>
			                            </li>
									</ul>
							</div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection