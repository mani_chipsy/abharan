@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent">
  <div id="content-wrapper">
    <div id="content" class="clearfix">
      <div id="breadcrumb" class="breadcrumb">
        <div itemprop="breadcrumb" class="container">
          <div class="row">
            <div class="col-md-24">
              <a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
              
              <span>/</span>
              <span class="page-title">About Us</span>
              
            </div>
          </div>
        </div>
      </div>
      <section class="content">
        <div class="container">
          <div class="row">
            <div id="page-header" class="col-md-24">
              <h1 id="page-title">About Us</h1>
            </div>
            <div id="col-main" class="blog blog-page col-xs-24 col-sm-24 col-content col-content ">
              <div class="txtbx01 clearfix" >
                <hr>
                <h3>Founder<br>
                  Late Sri Burde Sadananda Kamath
                </h3>
              </div>
              <hr>
              <div class="txt_contents_01 clearfix">
                <br>
                <p style="text-align:justify; ">
                  <img src="/Frontend/assets/images/others/Burde_Sadananda_Kamath.jpg" style="float:left; padding-right:20px; padding-bottom:5px;">
                  <p>
                    It all began in 1930. Late Sri Burde Sadananda Kamath, a general merchant, in his travels to Malnad, discovered the exquisite nature of jewellery made in Udupi. Soon he began purchasing jewellery from the local craftsmen and selling them to the customers in other places. In 1935, Kamath opened "Neo Jewellery Mart", the first jewellery shop in Udupi. 
                </p></p><p>
                <p style="text-align:justify; ">
                A man of principles, Kamath only sold pure, quality gold to customers. The Gold Control Act of 1962 restricted jewellers from selling 22 karat gold. They were allowed to sell only 14 karat gold. Utterly dejected Kamath closed his shop. </p>
                <p style="text-align:justify; ">
                Years later the government withdrew its restrictions. And in 1979 Sri Madhukar S. Kamath, the founder's son, started a new gold showroom 'Abharan Jewellers' in Udupi. From then on, Abharan grew into a retail chain with 9 showrooms in Karnataka  under the skillful management of Sri Subhas M. Kamath and Sri Mahesh M. Kamath, sons of Sri Madhukar S. Kamath, and the guiding force of their uncle Sri Dayanand Kamath. Abharan also has separate silver showrooms in Udupi and Mangalore. </p>
                </p><p style="text-align:justify; "><p>
                Abharan is an ISO 9001: 2008 certified jeweller selling 'Hallmark' jewellery. In fact, Abharan was the first to introduce the Karatometer in the country. Abharan has an online purchase window enabling customers to choose and purchase jewellery through the internet. With uncompromising quality and customer satisfaction measures Abharan has won customers the world over. </p>
                </p>
                <hr>
                <br>
              </div>
         
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
@endsection