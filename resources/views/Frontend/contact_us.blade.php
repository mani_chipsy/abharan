@extends('Frontend.layouts.home_layout')
@section('content')
<style type="text/css">
  .iframe-container{
    position: relative;
    width: 100%;
    padding-bottom: 56.25%; /* Ratio 16:9 ( 100%/16*9 = 56.25% ) */
}
.iframe-container > *{
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    margin: 0;
    padding: 0;
    height: 100%;
    width: 100%;
}
</style>
<div id="content-wrapper-parent">
  <div id="content-wrapper">
    <!-- Content -->
    <div id="content" class="clearfix">
      <div id="breadcrumb" class="breadcrumb">
        <div itemprop="breadcrumb" class="container">
          <div class="row">
            <div class="col-md-24">
              <a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
              <span>/</span>
              <span class="page-title">Contact</span>
            </div>
          </div>
        </div>
      </div>
      <section class="content">
        <div class="container">
          <div class="row">
            <div id="page-header" class="col-md-24">
              <h1 id="page-title" style="text-align: center;"><b>Contact Information</b></h1>
            </div>
          </div>
        </div>
        <div id="col-main" class="contact-page clearfix" ng-controller="FeedbackController">
          <div class="group-contact clearfix">
            <div class="container">
          
              <div class="row">

              <div id="page-header" ><div class="clear-fix signup-error"></div></div>
                <div class="left-block col-md-9">
                 <form method="post" ng-enter="submitFeedback()" id="contact_form" class="contact-form" accept-charset="UTF-8">
                    <input type="hidden" value="contact" name="form_type"><input type="hidden" name="utf8" value="✓">
                    <ul id="contact-form" class="list-unstyled">
                      <li class="">
                        <h3>DROP US A LINE</h3>
                      </li>
                      <li class="">
                        <label class="control-label" for="name">Your Name</label>
                        <input type="text" id="name" value="" class="form-control" name="contact[name]">
                      </li>
                      <li class="clear-fix"></li>
                      <li class="">
                        <label class="control-label" for="email">Your Email <span class="req">*</span></label>
                        <input type="email" id="email" value="" class="form-control email" name="contact[email]">
                      </li>
                      <li class="clear-fix"></li>
                      <li class="">
                        <label class="control-label" for="message">Your Message <span class="req">*</span></label>
                        <textarea id="message" rows="5" class="form-control" name="contact[body]"></textarea>
                      </li>
                    <!--   <li class="clear-fix"></li> -->
                        
                      <li class="unpadding-top">
                      <input type="hidden"  name="_token" value="{{ csrf_token() }}" >
                        <button type="button" ng-click="submitFeedback()" class="btn btn-primary btn-lg button-cart" style="min-width: 200px;">Submit Contact</button></li></ul>

                                                                                                                                                                                                                                                                   
                    
                  </form>
                </div>
                <div class="right-block contact-content col-md-15">
               {{-- <div class="right-block contact-content col-md-12">
                  <ul class="right-content">
                    <li class="title">
                      <h6>Follow Us on</h6>
                    </li>
                    <li class="facebook"><a href="https://www.facebook.com/abharanudupi" target="_blank"><span class="fa-stack fa-lg btooltip" title="" data-original-title="Facebook"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-facebook fa-inverse fa-stack-1x"></i> </span></a></li>
                    <!-- <li class="twitter"><a href="#"><span class="fa-stack fa-lg btooltip" title="" data-original-title="Twitter"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-twitter fa-inverse fa-stack-1x"></i> </span></a></li>
                    <li class="google-plus"><a href="#"><span class="fa-stack fa-lg btooltip" title="" data-original-title="Google plus"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-google-plus fa-inverse fa-stack-1x"></i> </span></a></li>
                    <li class="pinterest"><a href="#"><span class="fa-stack fa-lg btooltip" title="" data-original-title="Pinterest"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-pinterest fa-inverse fa-stack-1x"></i> </span></a></li> -->
                  </ul>
                </div>--}}
                <div class="right-block contact-content col-md-7 product_center card1">
                
                 <div class="price_section ">
                   <h3 class="price_section_inner" >Udupi Showroom</h3>
             
               <!--                  <h4>Udupi Showroom</h4> -->
                                  <ul class="right-content ">
                                <li class="address">
                                  <p><i class="fa fa-address-card-o" aria-hidden="true"></i> Corporation Bank Road,Udupi 576 101 </p>
                                </li>
                                <br>
                                <li class="phone">
                                  <p><i class="fa fa-phone" aria-hidden="true"></i> 0820 429 7777, 252 1267 </p>
                                </li>
                                <br>
                                <li class="email">
                                  <p> <i class="fa fa-envelope"></i>info@abharan.com </p>
                                </li>
                                <br>
                                <li class="address">
                                  <p><i class="fa fa-clock-o" aria-hidden="true"></i> Business Hours (Mon - Sat): 10.30 – 6.30 (no lunch break) </p>
                                </li>
                              </ul>
                              </div>
                </div>
                          <div class="right-block contact-content col-md-7 product_center card1">
                
                 <div class="price_section">
                   <h3 class="price_section_inner">Udupi Silver Showroom</h3>
                     <ul class="right-content">
                                <li class="address">
                                  <p><i class="fa fa-address-card-o" aria-hidden="true"></i> Sharada, Corporation Bank Road ,Udupi 576 101 </p>
                                </li>
                                <br>
                                <li class="phone">
                                  <p><i class="fa fa-phone" aria-hidden="true"></i> 0820 429 7777, 252 1267 </p>
                                </li>
                                <br>
                                <li class="email">
                                  <p> <i class="fa fa-envelope"></i>info@abharan.com </p>
                                </li>
                                <br>
                                <li class="address">
                                  <p><i class="fa fa-clock-o" aria-hidden="true"></i> Business Hours (Mon - Sat): 10.30 – 6.30 (no lunch break) </p>
                                </li>
                              </ul>
                  </div>
                  </div>
                      <div class="right-block contact-content col-md-7 product_center card1">
                
                 <div class="price_section">
                   <h3 class="price_section_inner">Shimoga Showroom</h3>
 <ul class="right-content">
                              <li class="address">
                                <p><i class="fa fa-address-card-o" aria-hidden="true"></i> Kshiti Complex, J.P.N. Road ,Shimoga 577 201 </p>
                              </li>
                              <br>
                              <li class="phone">
                                <p><i class="fa fa-phone" aria-hidden="true"></i> 08182 270 458, 224 848 </p>
                              </li>
                              <br>
                              <li class="email">
                                <p> <i class="fa fa-envelope"></i>abjsmg@abharan.com </p>
                              </li>
                              <br>
                              <li class="address">
                                <p><i class="fa fa-clock-o" aria-hidden="true"></i> Business Hours (Mon - Sat): 11.00 – 7.00 (no lunch break) </p>
                              </li>
                              </ul>
                 </div>
                 </div>
                        <div class="right-block contact-content col-md-7 product_center card1">
                
                 <div class="price_section">
                   <h3 class="price_section_inner">Karkala Showroom</h3>
                      <ul class="right-content">
                                <li class="address">
                                  <p><i class="fa fa-address-card-o" aria-hidden="true"></i> Near Sri Venkataramana Temple Main Road, Karkala 574 104 </p>
                                </li>
                                <br>
                                <li class="phone">
                                  <p><i class="fa fa-phone" aria-hidden="true"></i> 08258 234 444 </p>
                                </li>
                                <br>
                                <li class="email">
                                  <p> <i class="fa fa-envelope"></i>krk@abharan.com </p>
                                </li>
                                <br>
                                <li class="address">
                                  <p><i class="fa fa-clock-o" aria-hidden="true"></i> Business Hours (Mon - Sat): 10.30 – 6.30 (no lunch break) </p>
                                </li>
                              </ul>

                  </div>

                  </div>


             </div>
              <hr>
            <!--   <h6 class="sb-title"><i class="fa fa-home"></i> Contact Information</h6> -->
            <!--   <div class="row"> -->
                <div id="col-main" class=" col-md-24  ">
                  <div class="blog-content-wrapper">
                    
                      <div class="right-block contact-content col-md-7 product_center card">
                
                 <div class="price_section">
                   <h3 class="price_section_inner">Mangalore Showroom</h3>
                   <ul class="right-content">
                    <li class="address" >
                                  <p><i class="fa fa-address-card-o" aria-hidden="true"></i> Abharan Jewellers Pvt Ltd, Central Park, Shivbagh Kadri, Mangalore-575002 </p>
                                </li>
                                <br>
                                <li class="phone">
                                  <p><i class="fa fa-phone" aria-hidden="true"></i> 0824-2216111, 2218111 </p>
                                </li>
                                <br>
                                <li class="address">
                                  <p><i class="fa fa-clock-o" aria-hidden="true"></i> Timings: 10:30 AM - 7:30 PM</p>
                                </li>
                              </ul>
                           
                    </div>
                    </div>


    <div class="right-block contact-content col-md-7 product_center card">
                
                 <div class="price_section">
                   <h3 class="price_section_inner">Kundapur Showroom</h3>
                             
                              <ul class="right-content">
                                <li class="address">
                                  <p><i class="fa fa-address-card-o" aria-hidden="true"></i> S.V. Temple Road Kundapur 576 201 </p>
                                </li>
                                <br>
                                <li class="phone">
                                  <p><i class="fa fa-phone" aria-hidden="true"></i> 08254 234 444 </p>
                                </li>
                                <br>
                                <li class="email">
                                  <p> <i class="fa fa-envelope"></i>knd@abharan.com </p>
                                </li>
                                <br>
                                <li class="address">
                                  <p><i class="fa fa-clock-o" aria-hidden="true"></i> Business Hours (Mon - Sat): 10.30 – 6.30 (no lunch break) </p>
                                </li>
                              </ul>
                            </div>
                    </div>
                   <div class="right-block contact-content col-md-7 product_center card">
                
                 <div class="price_section">
                   <h3 class="price_section_inner">Hebri Showroom</h3>
                               
                              <ul class="right-content">
                                <li class="address">
                                  <p><i class="fa fa-address-card-o" aria-hidden="true"></i> 1st Floor Sudhindra Krupa Towers Near Bus Stand Hebri 576112 </p>
                                </li>
                                <br>
                                <li class="phone">
                                  <p><i class="fa fa-phone" aria-hidden="true"></i> 08253 250 444 </p>
                                </li>
                                <br>
                                <li class="address">
                                  <p><i class="fa fa-clock-o" aria-hidden="true"></i> Business Hours (Mon - Sat): 10.30 – 6.30 (no lunch break) </p>
                                </li>
                              </ul>
                            </div>
                         
                    </div>
          <div class="right-block contact-content col-md-8 product_center card">
                
                 <div class="price_section">
                   <h3 class="price_section_inner">Byndoor Showroom</h3>
                               
                              
                              <ul class="right-content">
                                <li class="address">
                                  <p><i class="fa fa-address-card-o" aria-hidden="true"></i> 1st Floor, Sowparnika Complex N.H. 66, Byndoor  </p>
                                </li>
                                <br>
                                <li class="phone">
                                  <p><i class="fa fa-phone" aria-hidden="true"></i> 08254 252 222 </p>
                                </li>
                                <br>
                                <li class="email">
                                  <p> <i class="fa fa-envelope"></i>bnd@abharan.com </p>
                                </li>
                                <br>
                                <li class="address">
                                  <p><i class="fa fa-clock-o" aria-hidden="true"></i> Business Hours (Mon - Sat): 10.30 – 6.30 (no lunch break) </p>
                                </li>
                              </ul>
                            </div>
                          
                    </div>
          
          <div class="right-block contact-content col-md-8 product_center card">
                
                 <div class="price_section">
                   <h3 class="price_section_inner">Brahmawar Showroom</h3>
                             
                              <ul class="right-content">
                                <li class="address">
                                  <p><i class="fa fa-address-card-o" aria-hidden="true"></i> Shri Tirumala Tower,Old Bus Stand Road  </p>
                                </li>
                                <br>
                                <li class="phone">
                                  <p><i class="fa fa-phone" aria-hidden="true"></i> 0820 2561377 </p>
                                </li>
                                <br>
                                <li class="email">
                                  <p> <i class="fa fa-envelope"></i>brm@abharan.com </p>
                                </li>
                                <br>
                                <li class="address">
                                  <p><i class="fa fa-clock-o" aria-hidden="true"></i> Business Hours (Mon - Sat): 10.30 – 6.30 (no lunch break) </p>
                                </li>
                              </ul>
                            </div>
                        
                    </div>  
					
		  <div class="right-block contact-content col-md-8 product_center card">
                
                 <div class="price_section">
                   <h3 class="price_section_inner">Chikmangalore Showroom</h3>
                              
                             
                              <ul class="right-content">
                                <li class="address">
                                  <p><i class="fa fa-address-card-o" aria-hidden="true"></i> MPCCW Complex, Market Road Chikmangalore-577101 </p>
                                </li>
                                <br>
                                <li class="phone">
                                  <p><i class="fa fa-phone" aria-hidden="true"></i> 08262 233 077 </p>
                                </li>
                               <br>
                                <li class="email">
                                  <p> <i class="fa fa-envelope"></i>ckm@abharan.com </p>
                                </li>
                                <br>
                                <li class="address">
                                  <p><i class="fa fa-clock-o" aria-hidden="true"></i> Business Hours (Mon - Sat): 10.30 – 6.30 (no lunch break) </p>
                                </li>
                              </ul>
                         </div>
                    </div>
          
          
         
          
                  </div>
                </div>
                <!-- End of layout -->
              </div>
               
           <br>
           <div class="row">
          
     <div id="col-main" class=" col-sm-24 col-md-24  ">
                  <iframe class="col-md-24" src="https://www.google.com/maps/d/embed?mid=1uKhk9Kz2FwIMHTkj15tYq9pC73oX2cJD" width="640" height="480" style="    border: none;min-height: 250px;"></iframe>
                </div>
                </div>

      </section>
    </div>
  </div>
</div>
<style type="text/css">
  .product_center .price_section .price_section_inner {
    color: #fc8b1c!important;
   font-size: 20px;
    margin-right: 10px;
    font-weight: 600;
    margin-top: 10px;
}
p {
    font-size: 13px;

      line-height: 1.0em;
    }

</style>
@endsection