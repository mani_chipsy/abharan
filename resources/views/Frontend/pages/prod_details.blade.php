@extends('Frontend.layouts.home_layout')
@section('content')
         <!--  <base href="http://127.0.0.1:8000/" />
          <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
            <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
            <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript" defer="defer"></script>
            <script src="catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
            <link href="catalog/view/javascript/jquery/owl-carousel/owl.carousel.css" rel="stylesheet" media="screen" /> -->
           <!--  <link href="catalog/view/javascript/jquery/owl-carousel/owl.transitions.css" rel="stylesheet" media="screen" /> -->
    
      
            <link href="/css/atstyle.css" rel="stylesheet">

<div id="content-wrapper-parent" ng-controller='ProductDetailsController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<a href="/product/collection?m_category_id={{$prod_det->main_category->m_category_id}}" title="">{{$prod_det->main_category->m_category_name}}</a>
							<span>/</span>
							<span class="page-title" style="    text-transform: capitalize;">{{$prod_det->sub_category->s_category_name}}</span>
						</div>
					</div>
				</div>
			</div>

            <div class="container">
               <div class="product_title">
                  <h3>{{$prod_det->sub_category->s_category_name}}</h3>
                </div>
               <div class="row">
                  <div id="content" class="col-sm-18">
                     <div class="row">
                        <div class="col-sm-12 product_left">
                           <div class="thumbnails">
                              <div>
                             <!--  <img src="https://www.abharan.com/product/storage/PH/U230145_PH.jpg?width=500&height=500" title="Lorem Ipsum Passages" alt="Lorem Ipsum Passages" id="atzoom" data-zoom-image="https://www.abharan.com/product/storage/PH/U230145_PH.jpg?width=800&height=800"/> -->
                              @if ($prod_det->image)
													<img src="/api/product/storage/PH/{{$prod_det->image->global_name}}_PH.{{$prod_det->image->image_ext}}?width=500&height=500"  alt="{{$prod_det->name}}"  title="{{$prod_det->name}}"  id="atzoom" data-zoom-image="/api/product/storage/PH/{{$prod_det->image->global_name}}_PH.{{$prod_det->image->image_ext}}?width=800&height=800" class="thumbnail " />
												@else
									<img src="/api/product/storage/PH/NO_IMAGE_PH.png?width=520&height=390" alt="No Preview" />
												@endif</div>
                              
                           </div>
                        </div>
                        <div class="col-sm-12 product_center">
                           <!--  <h2>Lorem Ipsum Passages</h2>-->
                           <ul class="list-unstyled price_section">
                        <!--    <li>
                                 <h3 class="price_section_inner">{{$prod_det->name}}</h3>
                              </li> -->
                              
                           @if (!$prod_det->offer_value)
                              <li>
                                 <h2 class="price_section_inner">Rs.{{$prod_det->gross_value}}</h2>
                              </li>
                              <li>
                                 Ex Tax:                                                                                                    {{$prod_det->gst_amount}}                                                                                                                            
                              </li>
                              @elseif ($prod_det->offer_value)

                              <li>
                                 <h2 class="price_section_inner">Rs.{{$prod_det->offer_value}}</h2>
                              </li>
                              <li>
                                 Ex Tax:                                                                                                    {{$prod_det->gst_amount}}                                                                                                                            
                              </li>
                              @endif
                           </ul>
                        <style type="text/css">li.pad { width: 50%; display: inline-block; }
                        li.pad span{float: right;
   margin-right: 30%;}
li.pad:nth-last-child(1):nth-child(odd) { float: right; }</style>

                           <ul class="list-unstyled" style="    border-top: 1px solid rgb(221, 221, 221);">
                           <li class="pad"> Item Code    <span>:</span>  </li><li class="pad"> {{$prod_det->SKU}}  </li>
                           <li class="pad">Gross Weight   <span>:</span>  </li><li class="pad"> {{$prod_det->details->weight}} gm </li>
                           <li class="pad">Stone Weight   <span>:</span> </li><li class="pad"> {{$prod_det->details->beeds}} </li>
                           <li class="pad">Stone Value   <span>:</span>  </li><li class="pad">  {{$prod_det->details->beeds_value}}</li>
                           <li class="pad">Stone&nbsp;Details <span>:</span></li><li class="pad"> @if($prod_det->details->beeds_description)
						   {{$prod_det->details->beeds_description}}										@else NA
						   @endif</li>
						   @if($prod_det->main_category->m_category_name=="Diamond")<li class="pad">
						   clarity: </li><li class="pad"> {{$prod_det->details->clarity}}  <span>:</span> </li>  @endif

						  @if($prod_det->details->colour)  <li class="pad">Color <span>:</span></li><li class="pad"> {{$prod_det->details->colour}}</li> @endif
                           <li class="pad">Cents  <span>:</span>   </li><li class="pad">     {{$prod_det->details->cents}} </li>
                           <li class="pad">Metal <span>:</span>         </li><li class="pad">{{$prod_det->details->metal}} </li>
                           <li class="pad">Purity  <span>:</span>    </li><li class="pad">  {{$prod_det->details->purity}} </li>
                                         
                           </ul>
                           <div id="product" style="    border-top: 1px solid rgb(221, 221, 221);">
                           <div class="btn-group addcart_group" style="    min-width: 200px;">
                           <form method="POST" action="{{url('/product/cart/add')}}">
				                                            <input type="hidden" name="product_id" value="{{$prod_det->product_id}}">
				                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
				                                            <input type="hidden" name="task" value="add">
                           <button  name="add" type="submit" id="add-to-cart" data-loading-text="Loading..." class="btn btn-primary btn-lg button-cart" style="min-width: 200px;">
                                Add to Cart                                                                                                                                                                                                                                    </button>
                                </form>
                        
                              </div>
                           </div>
                           <!-- <hr> -->
                           <!-- AddThis Button BEGIN -->
                           <div class="addthis_toolbox addthis_default_style">
                              <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                              <a class="addthis_button_tweet"></a>
                              <a class="addthis_button_pinterest_pinit"></a>
                              <a class="addthis_counter addthis_pill_style"></a>
                           </div>
                           <!-- AddThis Button END -->
                        </div>
                     </div>
                  </div>
                  <!--right column-->
                  <div class="column_right_outer col-sm-6 hidden-xs">
                     <aside id="column-right" class="col-sm-6 hidden-xs">
                        <div>
                           <div class="pro_static_content_main">
                              <div class="pro_cms_img"><img src="/images/bis-logo.jpg" alt="brand"></div>
                             <!--  <div class="pro_static_content">This is a static CMS block edited from theme admin panel.You can insert ant content (text, images, HTML)) here. Lorem ipsum dolor sit amet, consectetur adipiscing elit porta.</div> -->
                              <div class="pro_static_outer">
                                 <div class="pro_cms_icon"><i class="fa fa-tag f-24"></i></div>
                                 <div class="pro_static_content_inner"><span class="content_cms_heading">Shipping charges vary based on your location</span></div>
                              </div>
                              <div class="pro_static_outer">
                                <a ng-click='returnPolicy()' href="javascript:void(0);" style="font-weight: 400;"> <div class="pro_cms_icon"><i class="fa fa-tag f-24"></i></div>
                                 <div class="pro_static_content_inner"><span class="content_cms_heading">Return policy</span></div></a>                              </div>
                              <div class="pro_static_outer">
                                 <div class="pro_cms_icon"><i class="fa fa-tag f-24"></i></div>
                                 <div class="pro_static_content_inner"><span class="content_cms_heading">Toll Free:1800 5999 777</div>
                              </div>
                           </div>
                        </div>
                     </aside>
                     <div class="pro_tag">
                     </div>
                  </div>
                  </div>
                  </div>

          <script type="text/javascript">
               
               $(document).ready(function() {
               
                   /*	$('.thumbnails').magnificPopup({
               
                   		type:'image',
               
                   		delegate: 'a',
               
                   		gallery: {
               
                   			enabled:true
               
                   		}
               
                   	});
               
                   }); */
               
                   // $("#related_product").owlCarousel({
               
                   //     slideSpeed: 500,
               
                   //     items: 4,
               
                   //     itemsDesktop: [1199, 4],
               
                   //     itemsDesktopSmall: [979, 3],
               
                   //     itemsTablet: [992, 3],
               
                   //     itemsMobile: [767, 2],
               
                   //     itemsMobileSmall: [480, 1],
               
                   //     autoWidth: true,
               
                   //     loop: true,
               
                   //     pagination: false,
               
                   //     navigation: true,
               
                   //     navigationText: [
               
                   //         "<i class='fa fa-caret-left'></i>",
               
                   //         "<i class='fa fa-caret-right'></i>"
               
                   //     ],
               
                   //     stopOnHover: true
               
                   // });
               
                   // $("#additional-images").owlCarousel({
               
                   //     navigation: true,
               
                   //     pagination: false,
               
                   //     navigationText: [
               
                   //         "<i class='fa fa-angle-left'></i>",
               
                   //         "<i class='fa fa-angle-right'></i>"
               
                   //     ],
               
                   //     items: 4,
               
                   //     itemsDesktop: [1199, 3],
               
                   //     itemsDesktopSmall: [979, 3],
               
                   //     itemsTablet: [992, 3],
               
                   //     itemsMobile: [767, 4],
               
                   //     itemsMobileSmall: [480, 3],
               
                   // });
               
                   if ($(window).width() > 767) {
               
                       $("#atzoom").elevateZoom({
               
                           gallery: 'image_additional_outer',
               
                           //inner zoom				 
               
                           zoomType: "inner",
               
                           cursor: "crosshair"
               
                           /*//tint
               
                           tint:true, 
               
                           tintColour:'#F90', 
               
                           tintOpacity:0.5
               
                           //lens zoom
               
                           zoomType : "lens", 
               
                           lensShape : "round", 
               
                           lensSize : 200 
               
                           //Mousewheel zoom
               
                           scrollZoom : true*/
               
                       });
               
                       var z_index = 0;
               
                       $(document).on('click', '.thumbnail', function() {
               
                           $('.thumbnails').magnificPopup('open', z_index);
               
                           $('.thumbnails div:first').addClass('zoom-box');
               
               
                           return false;
               
                       });
               
                       $('.image_additional_outer a').click(function() {
               
                           var smallImage = $(this).attr('data-image');
               
                           var largeImage = $(this).attr('data-zoom-image');
               
                           var ez = $('#atzoom').data('elevateZoom');
               
               $('.zoom-box .thumbnail').attr('data-image', largeImage);
               
                           $(".zoom-box #atzoom").attr("src",smallImage);
               
                           $(".zoom-box #atzoom").attr("data-zoom-image",smallImage);
               
                           ez.swaptheimage(smallImage, largeImage);
               
                           z_index = $(this).index('.image_additional_outer a');
               
                           return false;
               
                       });
               
                   } else {
               
                       $(document).on('click', '.thumbnail', function() {
               
                           $('.thumbnails').magnificPopup('open', 0);
               
                           return false;
               
                       });
               
                   }
               
                   $('.thumbnails').magnificPopup({
               
                       delegate: 'a.elevatezoom-gallery',
               
                       type: 'image',
               
                       tLoading: 'Loading image #%curr%...',
               
                       mainClass: 'mfp-with-zoom',
               
                       gallery: {
               
                           enabled: true,
               
                           navigateByImgClick: true,
               
                           preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
               
                       },
               
                       image: {
               
                           tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
               
                           titleSrc: function(item) {
               
                               return item.el.attr('title');
               
                           }
               
                       }
               
                   });
               
               });
               
               //-->
            </script>
    
@endsection