@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent" ng-controller='ShoppingCartController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">{{$checkout_resp}}
							</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content">
				<div class="container">
					<div class="row text-center">
				        <div class="col-sm-24">
				        <br><br> <h2 class="text-success">{{$checkout_resp}}</h2>
				        <i class="fa @if (isset($order_no)) fa-check-circle @else fa-times-circle-o @endif fa-4x" aria-hidden="true"></i>
				        <h3>Dear, {{Auth::user()->name}}</h3>
				        @if (isset($order_no))
				        <p style="font-size:20px;color:#5C5C5C;">Thank you for shopping with us. You order has been placed successfully with order no. {{$order_no}}</p>
				        @else 
				        <p style="font-size:20px;color:#ff0000;">Checkout failed</p>
				        @endif
						<br><br>
				        </div>				        
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection