@extends('Frontend.layouts.home_layout')
@section('content')

<div id="content-wrapper-parent" ng-controller='AuthController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Change Password</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content register-section">
				<div class="container">
					<div class="row">
						<div id="page-header" ><div class="clear-fix signup-error"></div></div>
						<div id="col-main" class="col-md-7 register-page ">
							<h1 id="page-header" >
								Change Password
							</h1>
							<form method="post" ng-enter="changePass()" name="change_pass" >
								<ul id="register-form" class="row list-unstyled">
									<li>
										<label for="old-password">Old Password <span class="req">*</span></label>
										<input type="password" id="old-password" name="old_password" class="form-control" required="required" >
									</li>
									<li class="clear-fix"></li>
									<li>
										<label for="password">New Password <span class="req">*</span></label>
										<input type="password" id="password" name="password" class="form-control" required="required">
									</li>
									<li class="clear-fix"></li>
									<li>
										<label for="password2">Repeat Password <span class="req">*</span></label>
										<input type="password" id="password2" name="password2" class="form-control" required="required" data-parsley-equalto="#password" data-parsley-required-message="This value is required" data-parsley-equalto-message="Password Confirmation should match the Password">
									</li>
									<li class="clear-fix"></li>
									<hr class='hr-dotted' />
									<li class="unpadding-top action-last text-center">
			        					<input type="hidden"  name="_token" value="{{ csrf_token() }}" >
										<button class="btn" type="button" ng-click="changePass()">Change Password</button>
									</li>
								</ul>
							</form>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection