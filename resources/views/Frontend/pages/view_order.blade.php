@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent" ng-controller='ViewOrderController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">View Order</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content">
				<div class="container">
					<div class="row">
						<div id="page-header" class="col-md-24">
							<h1 id="page-title"># {{$order->order_no}}</h1>
							<h1>Mode: {{$order->mode_str}} <span class="pull-right">Ordered On: {{$order->created_str}}</span></h1>
							
						</div>
						<div id="col-main" class="col-md-24 cart-page content">
							<div name="cartform" class="clearfix">
								<div class="row table-cart">
									<div class="wrap-table">
										<table class="cart-items haft-border">
											<colgroup>
												<col class="checkout-image" />
												<col class="checkout-info" />
												<col class="checkout-price" />
												<col class="checkout-quantity" />
												<col class="checkout-totals" />
											</colgroup>
											<thead>
												<tr class="top-labels">
													<th width="20%">Items</th>
													<th width="50%"></th>
													<th width="10%" class="text-left">Qty</th>
													<th width="15%" class="text-left">SubTotal</th>
												</tr>
											</thead>
											<tbody name="ord_history">
											@foreach ($order->details as $item)
											<tr class="item"> 
											<td class="text-left item-img">
												<a href="{{url('/product-details?product_id='.$item->product_id)}}">
													@if ($item->product->image)
													<img ng-src="/product/storage/TH/{{$item->product->image->global_name}}_TH.{{$item->product->image->image_ext}}" alt="" />
													@else
													<img ng-src="/product/storage/TH/NO_IMAGE_TH.png" alt="" />
													@endif
													</a>
											</td>
											<td class="title text-left">
												<ul>
													<li class="link">
														<a href="{{url('/product-details?product_id='.$item->product_id)}}">
														<span class="title-5">
														{{$item->product->name}} </span>
														</a>
													</li>
													<li style="padding-left:15px">
														<p class='money'>Rs.{{$item->price}}</p>
													</li>
												</ul>
											</td>
											<td class="text-right">
											<ul>
												<li >
												{{$item->qty}}
												</li>
											</ul></td>
											<td class="total text-right title-1">
											<ul>
												<li >
												<span class='money'>Rs.{{$item->subtotal}}</span>
												</li>
											</ul></td>
												
											</tr>
											@endforeach
											</tbody>
											<tfoot>
												<tr ng-if ='cartTax != 0.00' class="bottom-summary">
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td class="update-quantities"><b>Sub total</b></td>
													<td name="cart-total" class="subtotal title-1"><span class='money'>Rs.{{$order->subtotal}}</span></td>
												</tr>
												<tr ng-if ='cartTax != 0.00' class="bottom-summary">
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td class="update-quantities"><b>GST ({{$order->gst}}%)</b></td>
													<td name="cart-total" class="subtotal title-1"><span class='money'>Rs.{{$order->tax}}</span></td>
												</tr>
												<tr class="bottom-summary">
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td class="update-quantities"><b>Total</b></td>
													<td name="cart-total" class="subtotal title-1"><span class='money'>Rs.{{$order->total}}</span></td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
								<div class="row">      
							      <div id="checkout-addnote" class="col-md-24 text-right">
							        <div class="wrapper-title">
							          <span style="color:#a07936">Delivery Address:</span>
							        </div>
							        <address>{!!$order->ship_address !!}</address>
							      </div>							      
							    </div>
							</div>
							<script src="http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js" type="text/javascript"></script>
							<script src="http://cdn.shopify.com/s/assets/themes_support/shopify_common-560aead139dc90d812ab2864e525084f7410876b146af25d903c665092c98dc0.js" type="text/javascript"></script>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection