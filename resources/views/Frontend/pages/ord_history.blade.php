@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent" ng-controller='OrdHistoryController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Order History</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content">
				<div class="container">
					<div class="row">
						<div id="page-header" class="col-md-24">
							<h1 id="page-title">Order History</h1>
						</div>
						<div id="col-main" class="col-md-24 cart-page content">
							<!-- <div ng-if='cartCount'> -->
								<div name="cartform" class="clearfix">
									<div class="row table-cart">
										<div class="wrap-table">
											<table class="cart-items haft-border">
												<colgroup>
													<col class="checkout-image" />
													<col class="checkout-info" />
													<col class="checkout-price" />
													<col class="checkout-quantity" />
												</colgroup>
												<thead>
													<tr class="top-labels">
														<th class="text-left">Order No</th>
														<th class="text-left">Mode</th>
														<th class="text-left">Total</th>
														<th class="text-left">View Order</th>
														<th class="text-left">Ordered On.</th>
													</tr>
												</thead>
												<tbody>
												<tr ng-repeat='history in histories track by $index' class="item"> 
													<td class="title text-left">
														<%history.order_no%>
													</td>
													<td class="title"><%history.mode_str%>
													</td>
													<td class="title"><%history.total | INR%></td>
													<td class="title"><a href="/product/order-history/<%history.p_order_id%>">View</a></td>
													<td class="total title"><%history.created_str%></td>
												</tr>
												<tr ng-show='histories.length'>
										            <td colspan="5" class='text-right'>
										            	<ul uib-pagination total-items="totalHistory" ng-model="currentPage" class="pagination-sm chng-pagination" boundary-links="true" rotate="true" max-size="maxSize" items-per-page="historyPerPage" ></ul>
										            </td>
										          </tr>
										          <tr class='text-center' ng-hide='histories.length'>
										            <td colspan="5">No order history found</td>
										          </tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<script src="http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js" type="text/javascript"></script>
								<script src="http://cdn.shopify.com/s/assets/themes_support/shopify_common-560aead139dc90d812ab2864e525084f7410876b146af25d903c665092c98dc0.js" type="text/javascript"></script>
							<!-- </div> -->
			            	<!-- <p ng-hide='cartCount' class="def-height">You have no items in the shopping cart</p> -->
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection