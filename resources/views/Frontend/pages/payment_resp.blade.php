@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent" ng-controller='ShoppingCartController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">@if ($payment_resp) Payment Success @else Failed @endif
							</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content">
				<div class="container">
					<div class="row text-center">
				        <div class="col-sm-24">
				        <br><br> <h2 class="text-success">@if ($payment_resp) Payment Success @else Failed @endif</h2>
				        <i class="fa @if ($payment_resp) fa-check-circle @else fa-times-circle-o @endif fa-4x" aria-hidden="true"></i>
				        <h3>Dear, {{Auth::user()->name}}</h3>
				        @if ($payment_resp)
				        <p style="font-size:20px;color:#5C5C5C;">Thank you for shopping with us. You order has been placed successfully with order no. {{$order_no}} and Payment Transaction Id. {{$txn_ref_id}}</p>
				        @else 
				        <p style="font-size:20px;color:#ff0000;">Your payment failed with Transaction Id. {{$txn_ref_id}}</p>
				        <a href="/product/checkout"> Checkout Again</a>
				        @endif
						<br><br>
				        </div>				        
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection