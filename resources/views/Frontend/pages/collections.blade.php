@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent" ng-controller='CollectionsController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Collections</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content">
				<div class="container">
					<div class="row">
						<div id="collection-content">
							<!-- Tags loading -->
							<div id="tags-load" style="display:none;"><i class="fa fa-spinner fa-pulse fa-2x"></i></div>
							<div class="collection-main-content">
								<div id="col-main" class="collection collection-page col-xs-24 col-sm-18 no_full_width have-left-slidebar">
									<div class="container-nav clearfix">
									<div id="options" class="container-nav ">
										<div class="row">
											<div class="col-xs-14">
												<div name='filter-tags'></div>
											</div>
											<div class="col-xs-10">
												<input type="text" name="search-collection" value="" accesskey="4" autocomplete="off" class="form-control" placeholder="Search collection...">
											</div>
										</div>
								    </div>
									</div>
									<div id="sandBox-wrapper" class="group-product-item row collection-full">
										<ul id="sandBox" class="list-unstyled">
											<li class="element first no_full_width" data-alpha="Curabitur cursus dignis" data-price="20000" ng-repeat='(key, collection) in collections track by $index'>
												<ul class="row-container list-unstyled clearfix">
													<li class="row-left">
														<a href="/product-details?product_id=<%collection.product_id%>" class="container_item">
														<img ng-if='collection.image' ng-src="/product/storage/IN/<%collection.image.global_name%>_IN.<%collection.image.image_ext%>?width=270&height=203" class="img-responsive" alt="<%collection.name%>" />   
														<img ng-if='!collection.image' ng-src="/product/storage/IN/NO_IMAGE_IN.png?width=270&height=203" class="img-responsive" alt="No Preview" />   
														</a>
														<div class="hbw">
															<span class="hoverBorderWrapper"></span>
														</div>
													</li>
													<li class="row-right parent-fly animMix">
														<div class="product-content-left">
															<a class="title-5" href="/product-details?product_id=<%collection.product_id%>" title="<%collection.SKU%>"><%collection.SKU%></a>
															<span class="shopify-product-reviews-badge" data-id="1293239619"></span>
														</div>
														<div class="product-content-right">
															<div class="product-price" ng-if='!collection.offer_value'>    
							                                    <span class="price"><span class='money'>Rs.<%collection.gross_value%></span></span>
							                                  </div>
							                                  <div class="product-price" ng-if='collection.offer_value'>
							                                  <span class="price_sale"><span class="money">Rs.<%collection.offer_value%></span></span>
							                                    <del class="price_compare"> <span class="money">Rs.<%collection.gross_value%></span></del>
							                                  </div>
														</div>
														<div class="list-mode-description">
															Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis amet voluptas assumenda est, omnis dolor repellendus quis nostrum. Temporibus autem quibusdam et aut officiis debitis aut rerum dolorem necessitatibus saepe eveniet ut et neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed...
														</div>
														<div class="hover-appear">
															<div class="hide clearfix">
																<select name="id" >
																	<option selected="selected" value="5141875779">Default Title</option>
																</select>
															</div>
															<div class="effect-ajax-cart">
																<form method="POST" action="{{url('/product/cart/add')}}">
						                                            <input type="hidden" name="product_id" value="<%collection.product_id%>">
						                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
						                                            <input type="hidden" name="task" value="add">
																	<button class="add-to-cart" type="submit" name="add"><i class="fa fa-shopping-cart"></i><span class="list-mode">Add to Cart</span></button>
																</form>
															</div>
															<div class="product-ajax-qs ">
																<div  class="quick_shop" ng-click='quickView(key)'>
																	<i class="fa fa-eye" title="Quick view"></i><span class="list-mode">Quick View</span>
																</div>
															</div>
															<!-- <a class="wish-list" href="#" title="wish list"><i class="fa fa-heart"></i><span class="list-mode">Add to Wishlist</span></a> -->
														</div>
													</li>
												</ul>
											</li>
										</ul>
			            				<div ng-show="IsButtonClick"><p ng-hide='totalCollections'>No collections found!</p></div>
			            				<div class="text-center" ng-show="!IsButtonClick && !totalCollections">Loading...</div>
									</div>
									<div class="row">
										<div class="col-xs-24">
											<ul ng-show='totalCollections' uib-pagination total-items="totalCollections" ng-model="currentPage" class="pagination-sm chng-pagination" boundary-links="true" rotate="true" max-size="maxSize" items-per-page="collectionsPerPage"></ul>
										</div>
									</div>									
								</div>
								<div id="prodcoll" class="left-slidebar col-xs-24 col-sm-6">
									<div class="group_sidebar">
										<div class="sb-wrapper">
											<!-- filter tags group -->
											<div class="filter-tag-group">
												<h6 class="sb-title">Filters</h6>
												<!-- tags groupd 3 -->
												<div class="home-collection-wrapper sb-wrapper clearfix" id="coll-filter-3">
													<ul class="list-unstyled sb-content list-styled" name='filter-cat'>
														<li>
															<div class="boxes">
															  <input class='filter-cat' type="checkbox" value="men" id="filter-box-men" @if (isset($filter_cat) && $filter_cat == 'men') checked @endif>
															  <label for="filter-box-men">Men</label>
															</div>
														</li>
														<li>
															<div class="boxes">
															  <input class='filter-cat' type="checkbox" value="women" id="filter-box-women" @if (isset($filter_cat) && $filter_cat == 'women') checked @endif>
															  <label for="filter-box-women">Women</label>
															</div>
														</li>
														<li>
															<div class="boxes">
															  <input class='filter-cat' type="checkbox" value="kids" id="filter-box-kids" @if (isset($filter_cat) && $filter_cat == 'kids') checked @endif>
															  <label for="filter-box-kids">Kids</label>
															</div>
														</li>
													</ul>
												</div>
												<!-- tags groupd 3 -->
											</div>
										</div>
										<div class="sb-wrapper">
											<!-- filter tags group -->
											<div class="filter-tag-group">
												<h6 class="sb-title">Prices</h6>
												<!-- tags groupd 3 -->
												<div class="home-collection-wrapper sb-wrapper clearfix" id="coll-filter-3">
													<ul class="list-unstyled sb-content list-styled">
														<li>
															<div class="radio radio-abharan">
															  <input name='price-filter' type="radio" value="" id="all-price" >
															  <label for="all-price">All Prices</label>
															</div>
														</li>
														<li>
															<div class="radio radio-abharan">
															  <input name='price-filter' type="radio" value="below-10000" id="below-10000" >
															  <label for="below-10000">Below 10,000</label>
															</div>
														</li>
														<li>
															<div class="radio radio-abharan">
															  <input name='price-filter' type="radio" value="10000-25000" id="below-25000" >
															  <label for="below-25000">10,000 - 25,000</label>
															</div>
														</li>
														<li>
															<div class="radio radio-abharan">
															  <input name='price-filter' type="radio" value="25000-50000" id="below-50000" >
															  <label for="below-50000">25,000 - 50,000</label>
															</div>
														</li>
														<li>
															<div class="radio radio-abharan">
															  <input name='price-filter' type="radio" value="50000-75000" id="below-75000" >
															  <label for="below-75000">50,000 - 75,000</label>
															</div>
														</li>
														<li>
															<div class="radio radio-abharan">
															  <input name='price-filter' type="radio" value="75000-100000" id="below-100000" >
															  <label for="below-100000">75,000 - 1,00,000</label>
															</div>
														</li>
														<li>
															<div class="radio radio-abharan">
															  <input name='price-filter' type="radio" value="100000-150000" id="below-150000" >
															  <label for="below-150000">1,00,000 - 1,50,000</label>
															</div>
														</li>
														<li>
															<div class="radio radio-abharan">
															  <input name='price-filter' type="radio" value="150000-300000" id="below-300000" >
															  <label for="below-300000">1,50,000 - 3,00,000</label>
															</div>
														</li>
														<li>
															<div class="radio radio-abharan">
															  <input name='price-filter' type="radio" value="300000-above" id="above-300000" >
															  <label for="above-300000">Above 3,00,000</label>
															</div>
														</li>
													</ul>
												</div>
												<!-- tags groupd 3 -->
											</div>
										</div>
										<div class="sb-wrapper">
											<!-- filter tags group -->
											<div class="filter-tag-group">
												<h6 class="sb-title">Main Categories</h6>
												<!-- tags groupd 3 -->
												<div class="home-collection-wrapper sb-wrapper clearfix" id="coll-filter-3">
													<ul class="list-unstyled sb-content list-styled" name='main-cat'>
													@foreach ($main_cat as $id => $option)
														<li>
															<div class="boxes">
															  <input class='main-cat' type="checkbox" value="{{$id}}" id="main-box-{{$id}}" @if ($m_category_id == $id) checked @endif>
															  <label for="main-box-{{$id}}">{{$option}}</label>
															</div>
														</li>
													@endforeach
													</ul>
												</div>
												<!-- tags groupd 3 -->
											</div>
										</div>
										<div class="home-collection-wrapper sb-wrapper clearfix">
											<h6 class="sb-title">Product Categories</h6>
											<ul class="list-unstyled sb-content list-styled" name='sub-cat'>
												@foreach ($sub_cat as $id => $option)
													<li>
														<div class="boxes">
														  <input class='sub-cat' type="checkbox" value="{{$id}}" id="sub-box-{{$id}}" @if ($s_category_id == $id) checked @endif>
														  <label for="sub-box-{{$id}}">{{$option}}</label>
														</div>
													</li>
												@endforeach
											</ul>
										</div>
									</div>
									<!--end group_sidebar-->
								</div>
							</div>
						</div>
						<script type="text/javascript">
							$(document).ready(function() {
							      
							  });
							
						</script>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection