@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent" ng-controller='ShoppingCartController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<a href="/product/cart" class="homepage-link" title="Back to the cart">Cart</a>
							<span>/</span>
							<span class="page-title">Checkout
							</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content">
				<div class="container">
					<div class="row">
						<div id="page-header" class="col-md-24">
							<h1 id="page-title">Checkout</h1>
						</div>
						<div class="row cart-body">
							{{--<form method="post" name='checkout_gateway' action="{{config('services.payu.action_url')}}">
								<input type="hidden" name="key"/>
								<input type="hidden" name="hash" />
								<input type="hidden" name="txnid"/>
								<input type="hidden" name="amount"/>
								<input type="hidden" name="productinfo"/>
								<input type="hidden" name="firstname"/>
								<input type="hidden" name="email"/>
								<input type="hidden" name="phone"/>
								<input type="hidden" name="surl"/>
								<input type="hidden" name="furl"/>
								<input type="hidden" name="curl"/>
								<input type="hidden" name="service_provider"/>
							</form>--}}
							<form class="form-horizontal ng-pristine ng-valid" method="post" name='checkout_order' ng-enter="checkoutOrder()">
								
								<div class="col-lg-18 ">
									<!--SHIPPING METHOD-->
									<div class="panel">
										<div class="abharan-panel-head panel-heading">Shipping Address</div>
										<div class="panel-body list-unstyled">
											<div class="form-group">
												<div class="col-md-12">
													<label>Name: <span class="req">*</span></label>
													<input type="text" required="required" name="name" class="form-control" value="@if (isset($order->recpnt_name)){{$order->recpnt_name}}@else{{$user_details->name}} @endif">
													<div class="clear-fix"></div>
												</div>
												<div class="col-md-12">
													<label>Phone: <span class="req">*</span></label>
													<input type="text" name="mobile" class="form-control" required="required" value="@if (isset($order->mobile)){{$order->mobile}}@else{{$user_details->mobile}}@endif"   data-parsley-pattern="^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$" data-parsley-pattern-message='Invalid Mobile'>
													<div class="clear-fix"></div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label>Pincode: <span class="req">*</span></label>
													<input type="text" required="required" name="pincode" class="form-control" value="@if (isset($order->pincode)){{$order->pincode}}@else{{$user_details->details->pincode}}@endif"  data-parsley-pattern="^\d{6}$" data-parsley-pattern-message='Invalid Pincode'>
													<div class="clear-fix"></div>
												</div>
												<div class="col-md-12">
													<label>Locality:</label>
													<input type="text" name="locality" class="form-control" value="@if (isset($order->locality)){{$order->locality}}@else{{$user_details->details->locality}} @endif">
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-24">
													<label>Address: <span class="req">*</span></label>
													<textarea name="address" required="required" class="form-control">@if (isset($order->address)){{$order->address}}@else{{$user_details->details->address}} @endif</textarea>
													<div class="clear-fix"></div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label>City: <span class="req">*</span></label>
													<input type="text" required="required" name="city" class="form-control" value="@if (isset($order->city)){{$order->city}}@else{{$user_details->details->city}} @endif">
													<div class="clear-fix"></div>
												</div>
												<div class="col-md-12">
													<label>State: <span class="req">*</span></label>
													<input type="text" required="required" name="state" class="form-control" value="@if (isset($order->state)){{$order->state}}@else{{$user_details->details->state}} @endif">
													<div class="clear-fix"></div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label>Landmark:</label>
													<input type="text" name="landmark" class="form-control" value="@if (isset($order->landmark)){{$order->landmark}}@else{{$user_details->details->landmark}} @endif">
												</div>
												<div class="col-md-12">
													<label>Alternate phone:</label>
													<input type="text" name="alternate_mobile" class="form-control" value="@if (isset($order->alternate_mobile)){{$order->alternate_mobile}}@else{{$user_details->details->alternate_mobile}}@endif" data-parsley-pattern="^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$" data-parsley-pattern-message='Invalid Mobile'>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-24">
													<label>Order mode:</label>
												</div>
												<div class="col-md-12">
													<div class="radio radio-abharan">
													  	<input name='order_mode' type="radio" value="1" id="via-mail" required="required" data-parsley-required-message="Please select order mode">
													  	<label for="via-mail">Via Mail</label>
													</div>
												</div>
												<div class="col-md-12">
													<div class="radio radio-abharan">
													  	<input name='order_mode' type="radio" value="2" id="online-pay" required="required" data-parsley-required-message="Please select order mode">
													  	<label for="online-pay">Online Payment</label>
													</div>
												</div>
											</div>
											<div class="unpadding-top action-last text-right">
					        					<input type="hidden"  name="_token" value="{{ csrf_token() }}" >
					        					<input type="hidden" name="order_id" value="@if (isset($order->p_order_id)){{$order->p_order_id}}@endif">
												<button class="btn" type="button" ng-click="checkoutOrder()">Proceed</button>
											</div>
										</div>
									</div>
									<!--SHIPPING METHOD END-->
								</div>
								<div class="col-lg-6 ">
									<!--REVIEW ORDER-->
									<div class="panel">
										<div class="abharan-panel-head panel-heading">Review Order</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-xs-24">
													<strong>Order Total</strong>
													<div class="pull-right"><span>Rs. {{$cart_total}}</span></div>
												</div>
											</div>
										</div>
									</div>
									<!--REVIEW ORDER END-->
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection