@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent" ng-controller='CartListController'>
	<div id="content-wrapper">
		<!-- Content -->

		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Your Shopping Cart</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content">
				<div class="container">
					<div class="row">
						<div id="page-header" class="col-md-24">
							<h1 id="page-title">Shopping Cart</h1>
						</div>
						<div id="col-main" class="col-md-24 cart-page content">
							<div >
								<div name="cartform" class="clearfix">
									<div class="row table-cart">
										<div class="wrap-table" id="append">
											
										</div>
								
									
								</div>
								<script src="https://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js" type="text/javascript"></script>
								<script src="https://cdn.shopify.com/s/assets/themes_support/shopify_common-560aead139dc90d812ab2864e525084f7410876b146af25d903c665092c98dc0.js" type="text/javascript"></script>
							</div>
			            	<p id='cartCount' class="def-height" style="display: none">You have no items in the shopping cart</p>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection