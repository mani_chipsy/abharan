@extends('Frontend.layouts.home_layout')
@section('content')

<div id="content-wrapper-parent" ng-controller='AuthController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Create Account</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content register-section">
				<div class="container">
					<form method="post" ng-enter="submitRegister()" name="create_customer" >
					<div class="row">
						<div id="page-header" ><div class="clear-fix signup-error"></div></div>
						<div id="col-main" class="col-md-7 register-page ">
							<h1 id="page-header" >
								Register
							</h1>
								<ul id="register-form" class="row list-unstyled">
									<li id="first_name">
										<label class="control-label" for="full_name">Full Name <span class="req">*</span></label>
										<input type="text" value="" name="full_name" id="full_name" class="form-control" required="required" data-parsley-error-message="Full name is required"/>
									</li>
									<li class="clear-fix"></li>
									<li id="email" class="">
										<label class="control-label" for="email">Your Email <span class="req">*</span></label>
										<input type="email" value="" name="email" id="email" class="form-control" required="required" data-parsley-error-message="Email is required"/>
									</li>
									<li class="clear-fix"></li>
									<li id="last_name">
										<label class="control-label" for="mobile">Your Mobile <span class="req">*</span></label>
										<input type="text" value="" name="mobile" id="mobile" class="form-control " required="required" data-parsley-error-message="Mobile no. is required"/>
									</li>
									<li class="clear-fix"></li>
									<li id="password" class="">
										<label class="control-label" for="ur-password">Your Password <span class="req">*</span></label>
										<input type="password" value="" name="password" id="ur-password" class="form-control password" data-parsley-required-message="	Password is required" required="required"/>
									</li>
									<li class="clear-fix"></li>
									<li id="password" class="">
										<label class="control-label" for="cp-password">Confirm Password <span class="req">*</span></label>
										<input type="password" value="" name="cp_password" id="cp-password" class="form-control password" data-parsley-equalto="#ur-password" data-parsley-required-message="Confirm Password is required"  data-parsley-equalto-message="Password Confirmation should match the Password" required="required"/>
									</li>
									<hr class='hr-dotted' />
									<li class="unpadding-top action-last text-center">
			        					<input type="hidden"  name="_token" value="{{ csrf_token() }}" >
										<button class="btn" type="button" ng-click="submitRegister()">Create an Account</button>
									</li>
								</ul>
						</div>
					</div>
					</form>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection