@extends('Frontend.layouts.home_layout')
@section('content')
<div id="content-wrapper-parent" ng-controller='AuthController'>
	<div id="content-wrapper">
		<!-- Content -->
		<div id="content" class="clearfix">
			<div id="breadcrumb" class="breadcrumb">
				<div itemprop="breadcrumb" class="container">
					<div class="row">
						<div class="col-md-24">
							<a href="/" class="homepage-link" title="Back to the frontpage">Home</a>
							<span>/</span>
							<span class="page-title">Reset Password</span>
						</div>
					</div>
				</div>
			</div>
			<section class="content register-section">
				<div class="container">
					<form method="post" ng-enter="resetPassword()" name="reset_password" >
					<div class="row">
						<div id="page-header" ><div class="clear-fix signup-error"></div></div>
						<div id="col-main" class="col-md-7 register-page ">
							<h1 id="page-header" >
								Reset Password
							</h1>
								<ul id="register-form" class="row list-unstyled">
									<li id="first_name">
										<label for="customer_mobile_box" class="control-label">One Time Password (OTP)<span class="req">*</span></label>
                             			<input autofocus tabindex='10' type="number" maxlength="4" value="" name="otp" class="form-control" required="required" data-parsley-error-message="OTP is required"/>
									</li>
									<li class="clear-fix"></li>
									<li id="first_name">
										<label for="customer_mobile_box" class="control-label">New Password <span class="req">*</span></label>
                             			<input tabindex='20' type="password" name="password" class="form-control" required="required"/>
									</li>
									<li class="clear-fix"></li>
									<li id="first_name">
										<label for="customer_mobile_box" class="control-label">Confirm Password<span class="req">*</span></label>
                             			<input tabindex='30' type="password" name="confirm_password" class="form-control" required="required" />
									</li>
									<li class="clear-fix"></li>
		                            <li class="clearfix  action-last block-btns">
		                              <input type="hidden" name="_token" value="{{csrf_token()}}">
		                              <input type="hidden" name="userid" value="{{$userid}}">
		                              <button class="btn" tabindex='40' type="button" ng-click="resetPassword()">Change Password</button>
		                            </li>
								</ul>
						</div>
					</div>
					</form>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection