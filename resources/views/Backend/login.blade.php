<!DOCTYPE html>
<html lang="en" ng-app='abharan-ad'>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>{{config('app.name')}}</title>
        <meta name="csrf_token" content="{{ csrf_token() }}" />
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="{{asset('Assets/bootstrap/css/bootstrap.css')}}" >

        <!-- Optional theme -->
        <link rel="stylesheet" href="{{asset('Assets/bootstrap/css/bootstrap-theme.css')}}">
        <link rel="stylesheet" href="{{asset('Backend/assets/css/signin.css')}}">
        <link rel="stylesheet" href="{{asset('Assets/parsley/parsley.css')}}">

        <script src="{{asset('Assets/jquery/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('Assets/angular/angular.js')}}"></script>
        <script src="{{asset('Assets/angular/route.js')}}"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="{{asset('Assets/bootstrap/js/bootstrap.js')}}"></script>
        <script src="{{asset('Assets/parsley/parsley.js')}}"></script>
    </head>
    <body>
        <div class="container" ng-controller='LoginController'>
            <form class='form-signin' name="user-signin" ng-enter="loginSubmit()">
                <h2 class="form-signin-heading">Please sign in</h2>
                <div class="form-group">
                    <label for="inputEmail" class="sr-only">Email address</label>
                    <input type="text" id="inputEmail" name="email" class="form-control" placeholder="Email address" required="" autofocus="">
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="sr-only">Password</label>
                    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <div class="clearfix signin_error" style="color:red"></div>
                <button class="btn btn-lg btn-primary btn-block" ng-click="loginSubmit()" type="button">Sign in</button>
            </form>
        </div>
        <script src="{{asset('Backend/jscontrols/login_controls.js')}}"></script>
    </body>
</html>