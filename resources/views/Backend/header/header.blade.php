<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{config('app.name')}}</title>

	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="csrf_token" content="{{ csrf_token() }}" />

  	<link rel="icon" href="favicon.ico" sizes="16x16" type="image/ico">
	<link rel="stylesheet" href="{{asset('Assets/bootstrap/css/bootstrap.css')}}" >
	<link rel="stylesheet" href="{{asset('Assets/bootstrap/css/bootstrap-theme.css')}}">
	<link rel="stylesheet" href="{{asset('Assets/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('Assets/parsley/parsley.css')}}">
	<link href="{{asset('Assets/preloader/preloader.css')}}" rel="stylesheet">
	<link href="{{asset('Assets/jquery-confirm/jquery-confirm.css')}}" rel="stylesheet">
	<link href="{{asset('Backend/assets/css/generic.css')}}" rel="stylesheet">

	<script src="{{asset('Assets/jquery/jquery-3.2.1.min.js')}}"></script>
	<script src="{{asset('Assets/angular/angular.js')}}"></script>
	<script src="{{asset('Assets/angular/route.js')}}"></script>
	<script src="{{asset('Assets/angular/ui-bootstrap-tpls-2.5.0.min.js')}}"></script>
	<script src="{{asset('Assets/bootstrap/js/bootstrap.js')}}"></script>
	<script src="{{asset('Assets/parsley/parsley.js')}}"></script>
	<script src="{{asset('Assets/preloader/preloader.js')}}"></script>
	<script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script>
</head>