@extends('Backend.layouts.main_layout')
@section('content')
<style type="text/css">
  .dyn-height table td {
    padding: 3px;
  }
  [name="feedback_date"] {
    margin-top: inherit;
  }
  [name="filter_export"] i {
    vertical-align: -50%;
  }
  .marg-pad {
    margin-left: -25px;
  }
</style>
<script type="text/javascript" src="/Assets/bootstrap/daterange/moment.min.js"></script> 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="/Assets/bootstrap/daterange/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/Assets/bootstrap/daterange/daterangepicker.css" />
<div class="container" ng-controller='AppFeedbackController'>	
    <div class="row">
        <div class="col-md-3">
            <h4>Customer Ratings (<%totalFeedbacks%>)</h4>
        </div>
        <div class="col-md-3 form-inline">
            <label>Search: </label>
            <input type="text" name="name" value="" accesskey="4" autocomplete="off" class="form-control" placeholder="Name">
        </div>
        <div class="col-md-3 form-inline"  >
            <input type="text" name="mobile" value="" accesskey="4" autocomplete="off" class="form-control" placeholder="Mobile No.">
        </div>
        <div class="col-md-2 form-inline marg-pad">
            <input type="text" name="feedback_date" value="" accesskey="4" autocomplete="off" class="form-control pull-right" placeholder="Date">
        </div>
        <div class="col-md-1 form-inline">
            <a name='filter_export' href="/superadmin/app/feedbacks/export" title="Download" style="margin-left: 22px"><i class="fa fa-download fa-lg"></i></a>
        </div>
    </div>
    <table class="table table-bordered">
        <thead >
          <tr>
            <th width="7%">Sl. No.</th>
            <th width="10%" >Staff Code</th>
            <th width="5%" > Bill Number</th>
            <th width="10%" >DOC No. </th>
            <th width="15%" >Rev No.</th>
            <th width="20%" >Name</th>
            <th width="15%" >Landline</th>
            <th width="12%" >Mobile</th>
            <th width="15%" >Date</th>
            <th width="15%" >View</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat='feedback in feedbacks'>
            <td><%(feedbacksPerPage * (currentPage-1)) + $index+1  %></td>
            <td><%feedback.staff_code%></td>
             <td><%feedback.bill_number%></td>
            <td><%feedback.doc_no %></td>
            <td><%feedback.rev_no %></td>
            <td><%feedback.name %></td>
            <td><%feedback.landline %></td>
            <td><%feedback.mobile %></td>
            <td><%feedback.date %></td>
            <td><a href="javascript:void(0);" title="View whole feedback" ng-click="showFeedback($index)">View </a></td>
          </tr>
          <tr class='text-right' ng-show='feedbacks.length'>
            <td colspan="29">
            	<ul uib-pagination total-items="totalFeedbacks" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="true" max-size="maxSize" items-per-page="feedbacksPerPage" ></ul>
            </td>
          </tr>
          <tr class='text-center' ng-hide='feedbacks.length'>
            <td colspan="29">No feedbacks to display</td>
          </tr>
        </tbody>
    </table>
</div>
<div id="message-modal" class="modal" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true" tabindex="-1" data-width="800">
  <div class="modal-dialog rotateInDownLeft">
    <div class="modal-content">
      <div class="modal-header">
        Feedback
        <i class="close fa fa-times btooltip" data-toggle="tooltip" data-placement="top" title="Close" data-dismiss="modal" aria-hidden="true"></i>
      </div>
      <div class="modal-body dyn-height">
      </div>
    </div>
  </div>
</div>
<script src="{{asset('Backend/jscontrols/product_controls.js')}}"></script>
<script type="text/javascript">
  $('[name="feedback_date"]').daterangepicker({    
    showCalendars: function() {
            this.container.addClass('show-calendar');
            this.container.find(".calendar").fadeIn();
            this.move();
        },

        hideCalendars: function() {
            this.container.removeClass('show-calendar');
            this.container.find(".calendar").fadeOut();
        },
     locale: {
      format: 'DD-MM-YYYY'
    },
     ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
  });
</script>
@endsection
