@extends('Backend.layouts.main_layout')
@section('content')
<style type="text/css">
    .table.table-striped .form-control{
    width: 250px;
    }
</style>
<div class="container">
<input type="hidden" name="product_id" value="{{$product['product_id']}}" />
    <div class="row" name="product_view">
        <div class="col-md-8">
            <h4>
                <span>Product ID ({{$product['product_id']}})</span>
                <div class="pull-right">
                    <a class="btn btn-success btn-sm " name="edit" href="javascript:void(0);">
                    <i class="fa fa-edit"></i> Edit
                    </a>
                    <a class="btn btn-info btn-sm " href="/superadmin/product">
                    <i class="fa fa-arrow-left"></i> Back
                    </a>
                </div>
            </h4>
            <table class="table table-bordered">
                <tr>
                    <th >SKU</th>
                    <td>{{$product['SKU']}}</td>
                </tr>
                <tr>
                    <th >MAIN CATEGORY </th>
                    <td>{{$product['main_category']['m_category_name']}}</td>
                </tr>
                <tr>
                    <th >CATEGORY</th>
                    <td>{{$product['sub_category']['s_category_name']}}</td>
                </tr>
                <tr>
                    <th >PRODUCT NAME</th>
                    <td>{{$product['name']}}</td>
                </tr>
                <tr>
                    <th >CATEGORY GRADES</th>
                    <td>{{$product['details']['category']}}</td>
                </tr>
                <tr>
                    <th >WEIGHT</th>
                    <td>{{$product['details']['weight']}}</td>
                </tr>
                <tr>
                    <th >QUOTE WEIGHT</th>
                    <td>{{$product['details']['quote']}}</td>
                </tr>
                <tr>
                    <th >PURITY</th>
                    <td>{{$product['details']['purity']}}</td>
                </tr>
                <tr>
                    <th >METAL</th>
                    <td>{{$product['details']['metal']}}</td>
                </tr>
                <tr>
                    <th >STONE WEIGHT</th>
                    <td>{{$product['details']['beeds']}}</td>
                </tr>
                <tr>
                    <th >STONE VALUE</th>
                    <td>{{$product['details']['beeds_value']}}</td>
                </tr>
                  <tr>
                    <th>STONE DETAILS</th>
                    <td>{{$product['details']['beeds_description']}}</td>
                </tr>
                <tr>
                    <th >WASTAGE</th>
                    <td>{{$product['details']['wastage']}}</td>
                </tr>
                <tr>
                    <th >MAKING CHARGES</th>
                    <td>{{$product['details']['making_amount']}}</td>
                </tr>
                <tr>
                    <th >MC PERCENTAGE</th>
                    <td>{{$product['details']['mak_amt_percentage']}}</td>
                </tr>
                <tr>
                    <th >MC PER GRAM</th>
                    <td>{{$product['details']['mak_amt_per_gram']}}</td>
                </tr>
                <tr>
                    <th >CENTS</th>
                    <td>{{$product['details']['cents']}}</td>
                </tr>
                <tr>
                    <th >DIAMOND VALUE</th>
                    <td>{{$product['details']['daimond']}}</td>
                </tr>
                <tr>
                    <th >STONES</th>
                    <td>{{$product['details']['stone']}}</td>
                </tr>
                <tr>
                    <th >COST</th>
                    <td>{{$product['details']['cost']}}</td>
                </tr>
                <tr>
                    <th >DETAILED DESCRIPTION</th>
                    <td>{{$product['details']['description']}}</td>
                </tr>
                <tr>
                    <th >PRODUCT TAXABLE(YES/NO)</th>
                    <td>{{$product['details']['production']}}</td>
                </tr>
                <tr>
                    <th >MAX SHIPPING DAYS</th>
                    <td>{{$product['details']['max']}}</td>
                </tr>
                <tr>
                    <th >CERTIFICATIONS</th>
                    <td>{{$product['details']['certification']}}</td>
                </tr>
                <tr>
                    <th >CUT</th>
                    <td>{{$product['details']['cut']}}</td>
                </tr>
                <tr>
                    <th >COLOR</th>
                    <td>{{$product['details']['colour']}}</td>
                </tr>
                <tr>
                    <th >CLARITY</th>
                    <td>{{$product['details']['clarity']}}</td>
                </tr>
                <tr>
                    <th >WOMEN</th>
                    <td>{{$product['details']['women']}}</td>
                </tr>
                <tr>
                    <th >MEN</th>
                    <td>{{$product['details']['men']}}</td>
                </tr>
                <tr>
                    <th >KIDS</th>
                    <td>{{$product['details']['kids']}}</td>
                </tr>
                <tr>
                    <th >COLLECTION</th>
                    <td>{{$product['details']['collection']}}</td>
                </tr>
                <tr>
                    <th >OCCASION</th>
                    <td>{{$product['details']['occasion']}}</td>
                </tr>
            </table>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="thumbnail">
                @if ($product['image'])
                <img src="/superadmin/product/storage/PH/{{$product['image']['global_name']}}_PH.{{$product['image']['image_ext']}}" class="img-responsive" alt="">
                @else
                <img src="/superadmin/product/storage/PH/NO_IMAGE_PH.png" class="img-responsive" alt="">
                @endif
            </div>
        </div>
    </div>
    <div name="product_edit" style="display: none">
        <h4>
            <span>Edit Product </span>
            <div class="pull-right">
                <a class="btn btn-info btn-sm " href="javascript:void(0);" name="edit_cancel">
                <i class="fa fa-arrow-left"></i> Cancel
                </a>
            </div>
        </h4>
        <form class="form-horizontal" name="update_product">
        <table class="table table-striped">
            <tbody>
                <tr>
                    <td colspan="1">
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">SKU <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input  name="sku" placeholder="SKU" class="form-control" required="true" value="{{$product['SKU']}}" type="text"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">MAIN CATEGORY <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group">
                                            <select name="main_category" required='true' class="selectpicker form-control">
                                                <option>-Select Main Category- </option>
                                                @foreach(MainCat::get() as $main_cat)
                                                <option @if ($product['main_category']['m_category_id'] == $main_cat->m_category_id) selected @endif  value="{{$main_cat->m_category_id}}">{{$main_cat->m_category_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                @php 
                                    $sub_cat = SubCat::query()->orderBy('s_category_name', 'ASC');
                                    $not_siver = [
                                        config('abharan.sub_category.waist_chain'),
                                        config('abharan.sub_category.haram'),
                                        config('abharan.sub_category.kanti')
                                    ];

                                    $not_diamond = [
                                        config('abharan.sub_category.waist_chain'),
                                        config('abharan.sub_category.haram'),
                                        config('abharan.sub_category.kanti'),
                                        config('abharan.sub_category.chain'),
                                        config('abharan.sub_category.bracelet'),
                                        config('abharan.sub_category.article')
                                    ];
                                    switch ($product['main_category']['m_category_id'])
                                    {
                                        case config('abharan.main_category.diamond') : 
                                            $sub_cat->whereNotIn('s_category_id', $not_diamond);
                                            break;
                                        case config('abharan.main_category.silver') : 
                                            $sub_cat->whereNotIn('s_category_id', $not_siver);
                                            break;
                                        case config('abharan.main_category.gold') : 
                                            $sub_cat->where('s_category_id', '!=', config('abharan.sub_category.article'));
                                            break;

                                    }
                                @endphp

                                <div class="form-group">
                                    <label class="col-md-4 control-label">CATEGORY</label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group">
                                            <select name="sub_category" class="selectpicker form-control">
                                                <option>-Select Category- </option>
                                                @foreach($sub_cat->get() as $sub_cat)
                                                <option @if ($product['sub_category']['s_category_id'] == $sub_cat->s_category_id) selected @endif  value="{{$sub_cat->s_category_id}}">{{$sub_cat->s_category_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">PRODUCT NAME <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="prod_name" placeholder="Product Name" class="form-control" required="true" value="{{$product['name']}}" type="text"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">CATEGORY GRADES <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="cat_grade" placeholder="Category Grades" class="form-control" required="true" value="{{$product['details']['category']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">WEIGHT <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="weight" placeholder="Weight" class="form-control" required="true" value="{{$product['details']['weight']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">QUOTE WEIGHT <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group">
                                            <div class="input-group"><input name="quote" placeholder="Quote Weight" class="form-control" required="true" value="{{$product['details']['quote']}}"  type="text" data-parsley-type="number" ></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">PURITY <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="purity" placeholder="Purity" class="form-control" required="true" value="{{$product['details']['purity']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">METAL <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="metal" placeholder="Metal" class="form-control" required="true" value="{{$product['details']['metal']}}" type="text"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">STONE WEIGHT <span class="text-danger">*</span> </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="beeds" placeholder="Beeds Weight" class="form-control" required="true" value="{{$product['details']['beeds']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">STONE VALUE <span class="text-danger">*</span>  </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="beeds_value" placeholder="Beeds Value" class="form-control" required="true" value="{{$product['details']['beeds_value']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-md-4 control-label">STONE DETAILS  </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="beeds_description" placeholder="Stone Details" class="form-control"  value="{{$product['details']['beeds_description']}}" type="text"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">WASTAGE <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="wastage" placeholder="Wastage" class="form-control" required="true" value="{{$product['details']['wastage']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">MAKING CHARGES  <span class="text-danger">*</span>  </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input  name="mak_charges" placeholder="Making Charges" class="form-control" required="true" value="{{$product['details']['making_amount']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">MC PERCENTAGE <span class="text-danger">*</span>   </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input  name="mc_percentage" placeholder="MC Percentage" class="form-control" required="true" value="{{$product['details']['mak_amt_percentage']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">MC PER GRAM  <span class="text-danger">*</span>  </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input  name="mc_per_gram" placeholder="MC Per Gram" class="form-control" required="true" value="{{$product['details']['mak_amt_per_gram']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">CENTS <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="cents" placeholder="Cents" class="form-control" required="true" value="{{$product['details']['cents']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                            </fieldset>
                    </td>
                    <td colspan="1">
                            <fieldset>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">DIAMOND VALUE <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="diamond_value" placeholder="Diamond Value" class="form-control" required="true" value="{{$product['details']['daimond']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">STONES <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group">
                                            <input name="stones" placeholder="Stones" class="form-control" required="true" value="{{$product['details']['stone']}}"  type="text" data-parsley-type="number" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">COST <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="cost" placeholder="Cost" class="form-control" required="true" value="{{$product['details']['cost']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">DETAILED DESCRIPTION  </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="description" placeholder="Detailed Description" class="form-control" value="{{$product['details']['description']}}" type="text"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">PRODUCT TAXABLE <span class="text-danger">*</span> </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="production" value="Yes" @if (strtolower($product['details']['production']) == 'yes') checked @endif type="radio"> Yes <input name="production"  value="No" @if (strtolower($product['details']['production']) == 'no') checked @endif type="radio"> No</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">MAX SHIPPING DAYS <span class="text-danger">*</span> </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="max" placeholder="Max Shipping Days" class="form-control" required="true" value="{{$product['details']['max']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">CERTIFICATIONS <span class="text-danger">*</span> </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="certification" placeholder="Certifications" class="form-control" required="true" value="{{$product['details']['certification']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">CUT  <span class="text-danger">*</span> </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="cut" placeholder="Cut" class="form-control" required="true" value="{{$product['details']['cut']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">COLOR   <span class="text-danger">*</span></label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="color" placeholder="Color" class="form-control" required="true" value="{{$product['details']['colour']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">CLARITY <span class="text-danger">*</span>  </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="clarity" placeholder="Clarity" class="form-control" required="true" value="{{$product['details']['clarity']}}"  type="text" data-parsley-type="number" ></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">WOMEN   </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="women" value="0" type="hidden"><input name="women" placeholder="Women"  @if ($product['details']['women']) checked @endif value="1" type="checkbox"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">MEN   </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="men" value="0" type="hidden"><input id="men" name="men" value="1" @if ($product['details']['men']) checked @endif type="checkbox"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">KIDS   </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="kids" value="0" type="hidden"><input name="kids"  @if ($product['details']['kids']) checked @endif value="1" type="checkbox"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">COLLECTION  <span class="text-danger">*</span> </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="collection" placeholder="Collection" class="form-control" required="true" value="{{$product['details']['collection']}}" type="text"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">OCCASION <span class="text-danger">*</span>  </label>
                                    <div class="col-md-8 inputGroupContainer">
                                        <div class="input-group"><input name="occasion" placeholder="Occasion" class="form-control" required="true" value="{{$product['details']['occasion']}}" type="text"></div>
                                    </div>
                                </div>
                            </fieldset>
                            <input type="submit" class="btn btn-info pull-right" name="btn_update" value="Update"/>
                    </td>
                </tr>
            </tbody>
        </table>
        </form>
    </div>
</div>
<script src="{{asset('Backend/jscontrols/product_controls.js')}}"></script>
<script src="{{asset('Backend/js/product.js')}}"></script>
@endsection