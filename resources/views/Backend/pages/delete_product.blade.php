@extends('Backend.layouts.main_layout')
@section('content')
<script src="{{asset('Assets/progressive-upload/fileuploade.js')}}"></script>
<div class="container" >
	<div class="row">
		<div class="col-md-12 ">
        <h1>Delete Products</h1>
        <hr>
			<form class="form-horizontal " method="post" action="/superadmin/product/delete-products" enctype='multipart/form-data'>
				<div class="form-group">
			      <label for="file-name" class="col-xs-2 control-label">Upload Excel</label>
			      <div class="col-xs-4">
			        <input type="file" id="file-name" name="prod_file" class="form-control" >
			      </div>
			      <div class="col-xs-1">
			        <input type="hidden"  name="_token" value="{{ csrf_token() }}" >
			        <input type="submit" class="btn-info btn" value="Upload">
			      </div>
                  <div class="col-xs-5 btn-group">
                    <a class="btn-warning btn" href="/superadmin/product/download/SAMPLE_TEMPLATE.xls">
                        <i class='fa fa-download'></i>Sample Template
                    </a>
                   
                  </div>
			    </div>	
			</form>
		</div>
	</div>
    </div>
		
    
<script src="{{asset('Backend/jscontrols/product_controls.js')}}"></script>
<script type="text/javascript">
	$('form').submit(function(e) {
	    var $this = $(this);
	    e.preventDefault();
        if ($('[name="prod_file"]').get(0).files.length !== 0) {
    	    $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure you want to Delete the products?',
                type: 'orange',
                buttons: {
                    yes: {
                        text: 'Yes',
                        btnClass: 'btn-orange',
                        action: function() {
                            uploadObject = $this.ajaxSubmit({
    					        beforeSubmit: function() {
    					        	      
    					        },
    					        uploadProgress: function(event, position, total, percentComplete) {
    					        	$.loader.start();
    					        },
    					        success: function(res) {
                                    $.loader.stop();

                                    $.alert({
                                        title: ((res.success) ? 'Success!' : 'Error!'),
                                        content: res.message,
                                        icon: 'fa '+((res.success) ? ' fa-rocket' : 'fa-times'),
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-blue',
                                                action: function () {
                                                     // location.href='/superadmin/up-delete-products';
                                                }
                                            }
                                        }
                                    });
    					        },
    					        error: function(jqXHR, status, error) {
                                    $.alert({
                                        title: 'Error!',
                                        content: error,
                                        icon: 'fa fa-times',
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-blue',
                                                action: function () {
                                                    // location.href='/superadmin/product';
                                                }
                                            }
                                        }
                                    });
                                },

    					    });
                        }
                    },
                    cancel: function() {
                    }
                }
            });
        } else {
            $.alert({
                title: 'Error!',
                content: 'Please select the file',
                icon: 'fa fa-times',
                animation: 'zoom',
                closeAnimation: 'zoom',
                buttons: {
                    okay: {
                        text: 'Okay',
                        btnClass: 'btn-blue'
                    }
                }
            });
        }
	});
</script>
@endsection
