@extends('Backend.layouts.main_layout')
@section('content')
<div class="container" ng-controller='ProductOffersController'>
	<div class="row">
		<div class="col-md-8 ">
			<input type="hidden"  name="_token" value="{{ csrf_token() }}" >
			<input type="hidden"  name="offer_id" value="@if(isset($offer->p_offer_id)){{$offer->p_offer_id}}@endif" >
			<h4>
                <span>Product Offer</span>
                <button class="btn btn-info btn-sm pull-right" name="edit-offer" ><i class="fa fa-pencil"></i> Edit </button>
                <div class="pull-right">
	                <button class="btn btn-success btn-sm hidden" name='edit-save'><i class="fa fa-save"></i> Save </button>
	                <button class="btn btn-danger btn-sm hidden" name='edit-cancel'><i class="fa fa-times"></i> Cancel </button>
                </div>
            </h4>
            <table class="table table-bordered" width="100%">
            	<tbody name="offer-price">
					<tr >
						<td width="35%">Offer Price (per gram)</td>
						<td width="65%" align="left" class="form-inline">
							<span>Rs.</span> <span>@if(isset($offer->discount)){{$offer->discount}} @else 0 @endif</span>
						</td>
					</tr>
					<tr >
						<td width="35%">Status</td>
						<td width="65%" align="left" class="form-inline">
							<span >@if(isset($offer->status_str)){{$offer->status_str}}@else -NA- @endif</span>
							<span name='offer_status' class="hidden">@if(isset($offer->status)){{$offer->status}}@else 0 @endif</span>
						</td>
					</tr>
				</tbody>
            </table>
		</div>
	</div>
</div>
<script src="{{asset('Backend/jscontrols/product_controls.js')}}"></script>
<script type="text/javascript">
	$(document).on('click', '[name="edit-offer"]', function() {
		$('[name="edit-offer"]').addClass('hidden');
		$('[name="edit-save"], [name="edit-cancel"]').removeClass('hidden');
		$('[name="offer-price"] tr td:nth-child(2)').each(function() {
		    $(this).data('old-html', $(this).html());
			var name = $(this).find('span:eq(1)').attr('name');
			var value = $(this).find('span:eq(1)').text();

			if (name == 'offer_status') {
				var input = $('<select class="form-control" name="offer_status" ><option value="0">Inactive</option><option value="1">Active</option></select>');
			} else {
			    var input = $('<span>Rs.</span> <input class="form-control" name="offer_amount" type="number" />');
			}
			input.val($.trim(value));
			$(this).html(input);				
		})
	});

	$(document).on('click', '[name="edit-cancel"]', function() {
		$('[name="edit-offer"]').removeClass('hidden');
		$('[name="edit-save"], [name="edit-cancel"]').addClass('hidden');

		$('[name="offer-price"] tr td:nth-child(2)').each(function() {
			$(this).html($(this).data('old-html'));
		})
	});

	$(document).on('click', '[name="edit-save"]', function() {
		var offer = {
			'offer_id': $('[name="offer_id"]').val(),
			'offer_price': $('[name="offer_amount"]').val(),
			'status': $('[name="offer_status"]').val()
		};


		offer['_token'] = $('[name="_token"]').val();

		$.post( "/superadmin/product/up-offers", offer)
		  .done(function( data ) {
		  	if (data.offer_id) {
		  		$('[name="offer_id"]').val(data.offer_id);
		  	}
			$('[name="edit-offer"]').removeClass('hidden');
			$('[name="edit-save"], [name="edit-cancel"]').addClass('hidden');
			
			$('[name="offer-price"] tr td:nth-child(2)').each(function(index) {
				var old =  $('<div />', {html: $(this).data('old-html')});

				if (old.find('[name="offer_status"]').length) {
					old.find('span:eq(0)').text((offer['status'] == '1') ? 'Active' : 'Inactive');
					old.find('[name="offer_status"]').text(offer['status']);
				} else {
					old.find('span:eq(1)').text(offer['offer_price']);					
				}

				$(this).html(old.html());
			})
		});

	});
</script>
@endsection
