@extends('Backend.layouts.main_layout')
@section('content')
<script src="{{asset('Assets/progressive-upload/fileuploade.js')}}"></script>
<div class="container" ng-controller='ProductController'>
	<div class="row">
		<div class="col-md-12 ">
			<form class="form-horizontal " method="post" action="/superadmin/product/up-products" enctype='multipart/form-data'>
				<div class="form-group">
			      <label for="file-name" class="col-xs-2 control-label">Upload Excel</label>
			      <div class="col-xs-4">
			        <input type="file" id="file-name" name="prod_file" class="form-control" >
			      </div>
			      <div class="col-xs-1">
			        <input type="hidden"  name="_token" value="{{ csrf_token() }}" >
			        <input type="submit" class="btn-info btn" value="Upload">
			      </div>
                  <div class="col-xs-5 btn-group">
                    <a class="btn-warning btn" href="/superadmin/product/download/GOLD_TEMPLATE.xls">
                        <i class='fa fa-download'></i> Gold Template
                    </a>
                    <a class="btn-default btn" href="/superadmin/product/download/SILVER_TEMPLATE.xls">
                       <i class='fa fa-download'></i> Silver Template
                    </a>    
                    <a class="btn-info btn" href="/superadmin/product/download/DIAMOND_TEMPLATE.xls">
                       <i class='fa fa-download'></i> Diamond Template
                    </a>
                  </div>
			    </div>	
			</form>
		</div>
	</div>
	<hr>	
    <div class="row">
        <div class="col-md-9">
            <h4>Total Products (<%totalProducts%>)</h4>
        </div>
        <div class="col-md-3 form-inline">
            <label>Search: </label>
            <input type="text" name="search_sku" value="" accesskey="4" autocomplete="off" class="form-control" placeholder="SKU">
        </div>
    </div>
    <table class="table table-bordered">
        <thead >
          <tr>
            <th >Sl. No.</th>
            <th >SKU</th>
            <th >MAIN CATEGORY </th>
            <th >CATEGORY</th>
            <th >PRODUCT NAME</th>
            <th >CATEGORY GRADES</th>
            <th >WEIGHT</th>
            <th >QUOTE WEIGHT</th>
            <th >#</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat='product in products'>
            <td><%(productsPerPage * (currentPage-1)) + $index+1  %></td>
            <td><%product.SKU%></td>
            <td><%product.main_category.m_category_name %></td>
            <td><%product.sub_category.s_category_name%></td>
            <td><%product.name%></td>
            <td><%product.details.category%></td>
            <td><%product.details.weight%></td>
            <td><%product.details.quote%></td>
            <td>
            	<a href='/superadmin/product/get/<%product.product_id%>'>View</a>
            </td>
          </tr>
          <tr class='text-right' ng-show='products.length'>
            <td colspan="29">
            	<ul uib-pagination total-items="totalProducts" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="true" max-size="maxSize" items-per-page="productsPerPage" ></ul>
            </td>
          </tr>
          <tr class='text-center' ng-hide='products.length'>
            <td colspan="29">No products to display</td>
          </tr>
        </tbody>
    </table>
</div>
<script src="{{asset('Backend/jscontrols/product_controls.js')}}"></script>
<script type="text/javascript">
	$('form').submit(function(e) {
	    var $this = $(this);
	    e.preventDefault();
        if ($('[name="prod_file"]').get(0).files.length !== 0) {
    	    $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure you want to upload the products?',
                type: 'orange',
                buttons: {
                    yes: {
                        text: 'Yes',
                        btnClass: 'btn-orange',
                        action: function() {
                            uploadObject = $this.ajaxSubmit({
    					        beforeSubmit: function() {
    					        	      
    					        },
    					        uploadProgress: function(event, position, total, percentComplete) {
    					        	$.loader.start();
    					        },
    					        success: function(res) {
                                    $.loader.stop();

                                    $.alert({
                                        title: ((res.success) ? 'Success!' : 'Error!'),
                                        content: res.message,
                                        icon: 'fa '+((res.success) ? ' fa-rocket' : 'fa-times'),
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-blue',
                                                action: function () {
                                                    location.href='/superadmin/product';
                                                }
                                            }
                                        }
                                    });
    					        },
    					        error: function(jqXHR, status, error) {
                                    $.alert({
                                        title: 'Error!',
                                        content: error,
                                        icon: 'fa fa-times',
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-blue',
                                                action: function () {
                                                    location.href='/superadmin/product';
                                                }
                                            }
                                        }
                                    });
                                },

    					    });
                        }
                    },
                    cancel: function() {
                    }
                }
            });
        } else {
            $.alert({
                title: 'Error!',
                content: 'Please select the file',
                icon: 'fa fa-times',
                animation: 'zoom',
                closeAnimation: 'zoom',
                buttons: {
                    okay: {
                        text: 'Okay',
                        btnClass: 'btn-blue'
                    }
                }
            });
        }
	});
</script>
@endsection
