@extends('Backend.layouts.main_layout')
@section('content')
<div class="container" ng-controller='ProductRatesController'>
	<div class="row">
		<div class="col-md-8 ">
			<input type="hidden"  name="_token" value="{{ csrf_token() }}" >
			<h4>
                <span>Daily Rates</span>
                <button class="btn btn-info btn-sm pull-right" name="edit-rates" ><i class="fa fa-pencil"></i> Edit </button>
                <div class="pull-right">
	                <button class="btn btn-success btn-sm hidden" name='edit-save'><i class="fa fa-save"></i> Save </button>
	                <button class="btn btn-danger btn-sm hidden" name='edit-cancel'><i class="fa fa-times"></i> Cancel </button>
                </div>
            </h4>
            <table class="table table-bordered" width="100%">
	            <thead>	            	
					<tr >
						<th width="65%">Metal Type</td>
						<th width="35%">Per Gram</td>
					</tr>
	            </thead>
            	<tbody name="daily-rates">
            	@foreach ($metals as $k => $metal)
					<tr >
						<td>
							@if ($metal->purity && $metal->purity == 916)
								Gold 22 kt ({{$metal->purity}})
							@elseif ($metal->purity && $metal->purity == 18)
								Gold 18 kt ({{$metal->purity}})
							@else
								Silver
							@endif 
							<input type="hidden" name="{{$k}}" value="{{$metal->purity}}">
						</td>
						<td class="form-inline"><span>Rs.</span> <span>{{$metal->amount}}</span>  </td>
					</tr>
				@endforeach
            </table>
		</div>
	</div>
</div>
<script src="{{asset('Backend/jscontrols/product_controls.js')}}"></script>
<script type="text/javascript">
	$(document).on('click', '[name="edit-rates"]', function() {
		$('[name="edit-rates"]').addClass('hidden');
		$('[name="edit-save"], [name="edit-cancel"]').removeClass('hidden');
		$('[name="daily-rates"] tr td:nth-child(2)').each(function() {
			var value = $(this).find('span:eq(1)').text();
		    $(this).data('old-html', $(this).html());
		    var input = $('<span>Rs.</span> <input class="form-control" type="number" />');
		    input.val($.trim(value));
		    $(this).html(input);
		})
	});

	$(document).on('click', '[name="edit-cancel"]', function() {
		$('[name="edit-rates"]').removeClass('hidden');
		$('[name="edit-save"], [name="edit-cancel"]').addClass('hidden');

		$('[name="daily-rates"] tr td:nth-child(2)').each(function() {
			$(this).html($(this).data('old-html'));
		})
	});

	$(document).on('click', '[name="edit-save"]', function() {
		var metal_rates = {'data':{}};
		$('[name="daily-rates"] tr').each(function(index) {
			metal_rates['data'][index] = {
				'purity': $(this).find('td:eq(0) [type="hidden"]').val(),
				'amount': $(this).find('td:eq(1) input').val(),
			}
		})

		metal_rates['_token'] = $('[name="_token"]').val();

		$.post( "/superadmin/product/up-rates", metal_rates)
		  .done(function( data ) {
			$('[name="edit-rates"]').removeClass('hidden');
			$('[name="edit-save"], [name="edit-cancel"]').addClass('hidden');
			
			$('[name="daily-rates"] tr td:nth-child(2)').each(function(index) {
				var old =  $('<div />', {html: $(this).data('old-html')});
				old.find('span:eq(1)').text(metal_rates['data'][index]['amount']);
				$(this).html(old.html());
			})
		});

	});
</script>
@endsection
