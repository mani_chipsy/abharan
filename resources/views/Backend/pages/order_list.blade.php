@extends('Backend.layouts.main_layout')
@section('content')
<style type="text/css">
  .dyn-height table td {
    padding: 3px;
  }
  [name="created_at"] {
    margin-top: inherit;
  }
  [name="filter_export"] i {
    vertical-align: -50%;
  }
  .marg-pad {
    margin-left: -25px;
  }
</style>
<script type="text/javascript" src="/Assets/bootstrap/daterange/moment.min.js"></script> 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="/Assets/bootstrap/daterange/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/Assets/bootstrap/daterange/daterangepicker.css" />
<div class="container" ng-controller='OrderController'>
   <br>
   <br>
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#online">Online Payment</a></li>
    <li><a data-toggle="tab" href="#cod">COD</a></li>
    <li><a data-toggle="tab" href="#missed">Missed Order</a></li>
  </ul>

  <div class="tab-content">
    <div id="online" class="tab-pane fade in active">
    <br>
     <div class="row">
        <div class="col-md-3">
            <h4> Online Order (<%totalOnline%>)</h4>
        </div>
    </div>
      <table class="table table-bordered">
        <thead >
          <tr>
            <th width="7%">Sl. No.</th>
            <th width="10%" >Order-ID</th>
            <th width="5%" > Name</th>
            <th width="10%" >Mobile </th>
            <th width="15%" >Adress</th>
            <th width="10%" >City</th>
            <th width="5%" >Qty</th>
            <th width="12%" >Price</th>
            <th width="15%" >Date</th>
            <th width="15%" >Order Mode</th>
            <th width="15%" >Print</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat='order in online'>
            <td><%(onlinePerPage * (onlinePage-1)) + $index+1  %></td>
            <td><%order.order_no%></td>
            <td><%order.recpnt_name%></td>
            <td><%order.mobile%></td>
            <td><%order.address%></td>
            <td><%order.city%></td>
             <td><%order.details[0].qty%></td>
              <td><%order.details[0].price%></td>
              <td><%order.date%></td>
              <td >Online Payment</td>
           <!--   <td><%feedback.bill_number%></td>
            <td><%feedback.doc_no %></td>
            <td><%feedback.rev_no %></td>
            <td><%feedback.name %></td>
            <td><%feedback.landline %></td>
            <td><%feedback.mobile %></td>
            <td><%feedback.date %></td> -->
            <td style="text-align: center;"><a href="/superadmin/product/order_print/<%order.p_order_id%>" target="_blank" title="View whole feedback" ><i class="fa fa-print fa-lg"></i></a></td>
          </tr>
          <tr class='text-right' ng-show='online.length'>
            <td colspan="29">
                <ul uib-pagination total-items="totalOnline" ng-model="onlinePage" class="pagination-sm" boundary-links="true" rotate="true" max-size="maxSize" ng-change="onlineChanged()"  items-per-page="onlinePerPage" ></ul>
            </td>
          </tr>
          <tr class='text-center' ng-hide='online.length'>
            <td colspan="29">No orders to display</td>
          </tr>
        </tbody>
    </table>
    </div>
    <div id="cod" class="tab-pane fade">
    <br>
    <div class="row">
        <div class="col-md-3">
            <h4> COD Order (<%totalCod%>)</h4>
        </div>
    </div>
      <table class="table table-bordered">
        <thead >
          <tr>
            <th width="7%">Sl. No.</th>
            <th width="10%" >Order-ID</th>
            <th width="5%" > Name</th>
            <th width="10%" >Mobile </th>
            <th width="15%" >Adress</th>
            <th width="10%" >City</th>
            <th width="5%" >Qty</th>
            <th width="12%" >Price</th>
            <th width="15%" >Date</th>
            <th width="15%" >Order Mode</th>
            <th width="15%" >Print</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat='order in cod'>
            <td><%(codPerPage * (codPage-1)) + $index+1  %></td>
            <td><%order.order_no%></td>
            <td><%order.recpnt_name%></td>
            <td><%order.mobile%></td>
            <td><%order.address%></td>
            <td><%order.city%></td>
             <td><%order.details[0].qty%></td>
              <td><%order.details[0].price%></td>
              <td><%order.date%></td>
              <td >COD</td>
           <!--   <td><%feedback.bill_number%></td>
            <td><%feedback.doc_no %></td>
            <td><%feedback.rev_no %></td>
            <td><%feedback.name %></td>
            <td><%feedback.landline %></td>
            <td><%feedback.mobile %></td>
            <td><%feedback.date %></td> -->
            <td style="text-align: center;"><a href="/superadmin/product/order_print/<%order.p_order_id%>" target="_blank" title="View whole feedback" ><i class="fa fa-print fa-lg"></i></a></td>
          </tr>
          <tr class='text-right' ng-show='cod.length'>
            <td colspan="29">
                <ul uib-pagination total-items="totalCod" ng-model="codPage" class="pagination-sm" boundary-links="true" rotate="true" max-size="maxSize" ng-change="codChanged()" items-per-page="codPerPage" ></ul>
            </td>
          </tr>
          <tr class='text-center' ng-hide='cod.length'>
            <td colspan="29">No orders to display</td>
          </tr>
        </tbody>
    </table>
    </div>
    <div id="missed" class="tab-pane fade">
    <br>
    <div class="row">
        <div class="col-md-3">
            <h4> Missed Order (<%totalMissed%>)</h4>
        </div>
    </div>
     <table class="table table-bordered">
        <thead >
          <tr>
            <th width="7%">Sl. No.</th>
            <th width="10%" >Order-ID</th>
            <th width="5%" > Name</th>
            <th width="10%" >Mobile </th>
            <th width="15%" >Adress</th>
            <th width="10%" >City</th>
            <th width="5%" >Qty</th>
            <th width="12%" >Price</th>
            <th width="15%" >Date</th>
            <th width="15%" >Order Mode</th>
            <th width="15%" >Reason</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat='order in missed'>
            <td><%(missedPerPage * (missedPage-1)) + $index+1  %></td>
            <td><%order.order_no%></td>
            <td><%order.recpnt_name%></td>
            <td><%order.mobile%></td>
            <td><%order.address%></td>
            <td><%order.city%></td>
             <td><%order.details[0].qty%></td>
              <td><%order.details[0].price%></td>
              <td><%order.date%></td>
              <td ng-if='order.mode==1'>COD</td>
              <td ng-if='order.mode==2'>Online Payment</td>
           <!--   <td><%feedback.bill_number%></td>
            <td><%feedback.doc_no %></td>
            <td><%feedback.rev_no %></td>
            <td><%feedback.name %></td>
            <td><%feedback.landline %></td>
            <td><%feedback.mobile %></td>
            <td><%feedback.date %></td> -->
            <td style="text-align: center;"><a href="javascript:void(0);" ng-click='viewMissed($index)' title="View Reason" ><i class="fa fa-eye fa-sm"></i></a></td>
          </tr>
          <tr class='text-right' ng-show='missed.length'>
            <td colspan="29">
                <ul uib-pagination total-items="totalMissed" ng-model="missedPage" class="pagination-sm" boundary-links="true" rotate="true" max-size="maxSize" ng-change="missedChanged()" items-per-page="missedPerPage" ></ul>
            </td>
          </tr>
          <tr class='text-center' ng-hide='missed.length'>
            <td colspan="29">No orders to display</td>
          </tr>
        </tbody>
    </table>
    </div>
  </div>
</div>
<div id="message-modal" class="modal" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true" tabindex="-1" data-width="800">
  <div class="modal-dialog rotateInDownLeft">
    <div class="modal-content">
      <div class="modal-header">
        Reason
        <i class="close fa fa-times btooltip" data-toggle="tooltip" data-placement="top" title="Close" data-dismiss="modal" aria-hidden="true"></i>
      </div>
      <div class="modal-body dyn-height">
      </div>
    </div>
  </div>
</div>
<script src="{{asset('Backend/jscontrols/product_controls.js')}}"></script>
<script type="text/javascript">
  $('[name="feedback_date"]').daterangepicker({    
    showCalendars: function() {
            this.container.addClass('show-calendar');
            this.container.find(".calendar").fadeIn();
            this.move();
        },

        hideCalendars: function() {
            this.container.removeClass('show-calendar');
            this.container.find(".calendar").fadeOut();
        },
     locale: {
      format: 'DD-MM-YYYY'
    },
     ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
  });
</script>
@endsection
