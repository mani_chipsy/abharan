@extends('Backend.layouts.main_layout')
@section('content')
<link href="{{asset('Assets/lightbox/ekko-lightbox.min.css')}}" rel="stylesheet">
<script src="{{asset('Assets/lightbox/ekko-lightbox.js')}}"></script>
<script src="{{asset('Assets/progressive-upload/fileuploade.js')}}"></script>
<div class="container" ng-controller='ProductImageController'>
	<div class="row">
		<div class="col-md-12 ">
			<form class="form-horizontal " method="post" action="/superadmin/product/up-image" enctype='multipart/form-data'>
				<div class="form-group">
			      <label for="glob-name" class="col-xs-3 control-label">Upload images</label>
			      <div class="col-xs-6">
			        <input type="file" accept='image/*' multiple id="glob-name" name="image_file[]" class="form-control" >
			      </div>
			      <div class="col-xs-3">
			        <input type="hidden"  name="_token" value="{{ csrf_token() }}" >
			        <input type="submit" class="btn-info btn" value="Upload">
			      </div>
			    </div>	
			</form>
		</div>
	</div>
	<hr>
	<div class="row">
        <div class="col-md-9">
            <h4>Total Images (<%totalImages%>)</h4>
        </div>
        <div class="col-md-3 form-inline">
            <label>Search: </label>
            <input type="text" name="global_name" value="" accesskey="4" autocomplete="off" class="form-control" placeholder="Image Name( SKU )">
        </div>
    </div>
    <table class="table table-bordered">
    <thead >
      <tr>
        <th >Sl. No.</th>
        <th >Image Name</th>
        <th >PH (Large) Image </th>
        <th >IN (Medium) Image</th>
        <th >TH (Thumb) Image</th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat='image in images'>
        <td><%(imagesPerPage * (currentPage-1)) + $index+1  %></td>
        <td><%image.global_name%></td>
        <td><a data-toggle="lightbox" href='/superadmin/product/storage/PH/<%image.global_name%>_PH.<%image.image_ext%>' data-footer="PH Image"  data-gallery="<%image.global_name%>-<%$index%>">View</a></td>
        <td><a data-toggle="lightbox" href='/superadmin/product/storage/IN/<%image.global_name%>_IN.<%image.image_ext%>' data-footer="IN Image"  data-gallery="<%image.global_name%>-<%$index%>">View</a></td>
        <td><a data-toggle="lightbox" href='/superadmin/product/storage/TH/<%image.global_name%>_TH.<%image.image_ext%>' data-footer="TH Image"  data-gallery="<%image.global_name%>-<%$index%>">View</a></td>
      </tr>
      <tr class='text-right' ng-show='images.length'>
        <td colspan="5">
        	<ul uib-pagination total-items="totalImages" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="true" max-size="maxSize" items-per-page="imagesPerPage"></ul>
        </td>
      </tr>
      <tr class='text-center' ng-hide='images.length'>
        <td colspan="5">No images to display</td>
      </tr>
    </tbody>
  </table>
</div>
<div id="progressbar-modal" class="modal" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true" tabindex="-1" data-width="800">
  <div class="modal-dialog rotateInDownLeft">
    <div class="modal-content">
      <div class="modal-header">
      	Upload Progress
        <i class="close fa fa-times btooltip" data-toggle="tooltip" data-placement="top" title="Close" data-dismiss="modal" aria-hidden="true"></i>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>

<div class="progress hidden" name="up-file-progress">
	<div class="progress-bar progress-bar-info" role="progressbar" style="width: 0%"  name="progressbar">
		<span class="sr-only"></span>
	</div>
	<span class="progress-type" name="progress_type"></span>
	<span class="progress-completed" name="progress_completed"></span>
</div>
<script src="{{asset('Backend/jscontrols/product_controls.js')}}"></script>
<script type="text/javascript">
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
	    event.preventDefault();
	    $(this).ekkoLightbox({
	        // If you can't/don't want to set the href property of an element
	        // data-remote="http://www...."
	        remote: '',
	        // For grouping elements
	        // data-gallery="galleryname"
	        gallery: '',
	        // If you have multiple galleries per page, this will restrict the gallery items to the parent that matches this selector.
	        gallery_parent_selector: 'document.body',
	        // CSS classes for navigation arrows
	        left_arrow_class: '.glyphicon .glyphicon-chevron-left',
	        right_arrow_class: '.glyphicon .glyphicon-chevron-right',
	        // Enable the navigation arrows
	        directional_arrows: true,
	        // Force the lightbox into image/YouTube mode.
	        // image|youtube|vimeo
	        // data-type="(image|youtube|vimeo)"
	        type: null,
	        // Always show the close button, even if no title is present
	        always_show_close: true,
	        // Don't show related videos when finished playing
	        no_related: false,
	        // Scale the height as well as width
	        scale_height: true,
	        // Message injected for loading
	        // loadingMessage: 'Loading...',           
	        // Callback functions
	        onShow: function() {},
	        onShown: function() {},
	        onHide: function() {},
	        onHidden: function() {},
	        onNavigate: function() {},
	        onContentLoaded: function() {}
	    });
	});

	$('form').submit(function(e) {
	    var $this = $(this);
	    e.preventDefault();

	    if ($('[name="image_file[]"]').get(0).files.length !== 0) {
	    	$.confirm({
	            title: 'Confirm!',
	            icon: 'fa fa-question-circle',
	            content: 'Are you sure you want to upload the images?',
	            type: 'orange',
	            buttons: {
	                yes: {
	                    text: 'Yes',
	                    btnClass: 'btn-orange',
	                    action: function() {
	                    	var files = $('[name="image_file[]"]').get(0).files;
	                    	var tot_files = files.length;

                    		var progress_bar = $('[name="up-file-progress"]').clone();
                    		progress_bar.removeClass('hidden').attr('name', 'file_prg');
                    		progress_bar.find('[name="progressbar"]').css('width', '0%');
                    		progress_bar.find('[name="progress_type"]').text(files.length+' image(s)');
                    		progress_bar.find('[name="progress_completed"]').text('0%');

                    		$('#progressbar-modal .modal-body').html(progress_bar[0].outerHTML);
	    					$('#progressbar-modal').modal('show');

	                    	var token = $this.find('[name="_token"]').val();
	                    	var up_file_cnt = 1;
	                    	for (var count=0; count < tot_files; count++)
	                    	{
		                    	var postdata = new FormData();

		                    	postdata.append('_token', token);
	                    		if (typeof files[count] === 'undefined') {
								  	continue;
							  	}

							  	postdata.append('image_file', files[count]);

							  	$.ajax({
							  		url: $this.attr('action'),
								    type:'POST',
								    data: postdata,
								    contentType: false,
								    processData: false,
							     	success: function(res) {
							     		if (res.success) {
								     		var percentComplete = up_file_cnt / tot_files;
								     		var per_val = Math.round(percentComplete * 100);
							                percentComplete =  per_val + "%";

							                file_prgress = $('#progressbar-modal .modal-body').find('[name="file_prg"]');

							                if (per_val == 100) {
							                	file_prgress.find('[name="progressbar"]').removeClass('progress-bar-info').addClass('progress-bar-success');
							            		
							            		file_prgress.find('[name="progressbar"]').css('width', percentComplete);

							            		file_prgress.find('[name="progress_completed"]').text('Successfully Uploaded');

							            			$.alert({
					                                    title: 'Success!',
					                                    content: 'Successfully Uploaded',
					                                    icon: 'fa fa-rocket',
					                                    animation: 'zoom',
					                                    closeAnimation: 'zoom',
					                                    buttons: {
					                                        okay: {
					                                            text: 'Okay',
					                                            btnClass: 'btn-blue',
					                                            action: function () {
													                location.href='/superadmin/product/images';
													            }
					                                        }
					                                    }
					                                });
							                } else {
							            		file_prgress.find('[name="progressbar"]').css('width', percentComplete);
							            		file_prgress.find('[name="progress_completed"]').text(percentComplete);
							                }
							     			up_file_cnt+=1;
							     		}
							     	}
							  	});
							  	
							  	// $this.ajaxSubmit({
							   //      beforeSubmit: function() {
							   //          file_prgress = $('#progressbar-modal .modal-body').find('[name="file_'+count+'"]');
							   //          file_prgress.find('[name="progressbar"]').css('width', '1%');
							   //          file_prgress.find('[name="progress_completed"]').text('1%');
							   //      },
							   //      uploadProgress: function(event, position, total, percentComplete) {
							   //          file_prgress = $('#progressbar-modal .modal-body').find('[name="file_'+count+'"]');
							   //          file_prgress.find('[name="progressbar"]').css('width', percentComplete+'%');

							   //          file_prgress.find('[name="progress_completed"]').text(percentComplete+'%');

							   //          if (percentComplete == 100) {
							   //          	file_prgress.find('[name="progressbar"]').removeClass('progress-bar-info').addClass('progress-bar-success');

							   //          	file_prgress.find('[name="progress_completed"]').text('Successfully Uploaded');

							   //          }
							   //      },
							   //      success: function() {
							   //          file_prgress.find('[name="progressbar"]').removeClass('progress-bar-info').addClass('progress-bar-success');

						    //         	file_prgress.find('[name="progress_completed"]').text('Successfully Uploaded');
						    //         	nextsubmit();
							   //      },
							   //      error: function() {},
							   //  });
	                    	}

						    // uploadObject = $(this).ajaxSubmit({
						    //     beforeSubmit: function() {
						    //         id = $this.find('[type=file]').attr('id');
						    //         $('div#progressbar').find('div#' + id).find('.loader').width('1%');
						    //     },
						    //     uploadProgress: function(event, position, total, percentComplete) {
						    //         $('div#progressbar').find('div#' + id).find('.loader').width(percentComplete + '%');
						    //         if (percentComplete == 100) {
						    //             $('div#progressbar').find('div#' + id).find('.loader').css('background-color', 'yellow').find('p').html('Pushing To S3 Bucket').css('color', 'black');
						    //         }
						    //     },
						    //     success: function() {
						    //         $('div#progressbar').find('div#' + id).find('.loader').css('background-color', 'green').find('p').html('Successfully uploaded').css('color', 'white');;
						    //         nextsubmit();
						    //     },
						    //     error: function() {},
						    // });
							// uploadObject = $this.ajaxSubmit({
						 //        beforeSubmit: function() {
						        	      
						 //        },
						 //        uploadProgress: function(event, position, total, percentComplete) {
						 //        	$.loader.start();
						 //        },
						 //        success: function(res) {
						 //        	$.loader.stop();

						 //        	$.alert({
	      //                               title: ((res.success) ? 'Success!' : 'Error!'),
	      //                               content: res.message,
	      //                               icon: 'fa '+((res.success) ? ' fa-rocket' : 'fa-times'),
	      //                               animation: 'zoom',
	      //                               closeAnimation: 'zoom',
	      //                               buttons: {
	      //                                   okay: {
	      //                                       text: 'Okay',
	      //                                       btnClass: 'btn-blue',
	      //                                       action: function () {
							// 		                location.href='/superadmin/product/images';
							// 		            }
	      //                                   }
	      //                               }
	      //                           });
						 //        },
						 //        error: function(jqXHR, status, error) {
						 //        	$.alert({
	      //                               title: 'Error!',
	      //                               content: error,
	      //                               icon: 'fa fa-times',
	      //                               animation: 'zoom',
	      //                               closeAnimation: 'zoom',
	      //                               buttons: {
	      //                                   okay: {
	      //                                       text: 'Okay',
	      //                                       btnClass: 'btn-blue',
	      //                                       action: function () {
							// 		                location.href='/superadmin/product/images';
							// 		            }
	      //                                   }
	      //                               }
	      //                           });
						 //        },
							// });
	                    }
	                },
	                cancel: function() {
	                }
	            }
	        });
	    } else {
	    	$.alert({
	            title: 'Error!',
	            content: 'Please select images',
	            icon: 'fa fa-times',
	            animation: 'zoom',
	            closeAnimation: 'zoom',
	            buttons: {
	                okay: {
	                    text: 'Okay',
	                    btnClass: 'btn-blue'
	                }
	            }
	        });
	    }	    
	});
</script>
@endsection
