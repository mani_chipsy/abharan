@extends('Backend.layouts.main_layout')
@section('content')
<script src="{{asset('Assets/progressive-upload/fileuploade.js')}}"></script>
<div class="container" ng-controller='FeedbackController'>	
    <div class="row">
        <div class="col-md-9">
            <h4>Customer Feedbacks (<%totalFeedbacks%>)</h4>
        </div>
        <div class="col-md-3 form-inline">
            <label>Search: </label>
            <input type="text" name="email" value="" accesskey="4" autocomplete="off" class="form-control" placeholder="Email">
        </div>
    </div>
    <table class="table table-bordered">
        <thead >
          <tr>
            <th width="5%">Sl. No.</th>
            <th width="20%" >NAME</th>
            <th width="20%" >EMAIL </th>
            <th width="40%" >MESSAGE</th>
            <th width="15%" >CREATED ON.</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat='feedback in feedbacks'>
            <td><%(feedbacksPerPage * (currentPage-1)) + $index+1  %></td>
            <td><%feedback.name%></td>
            <td><%feedback.email %></td>
            <td><a href="javascript:void(0);" title="Show full message" ng-click="showMessage($index)"><%feedback.message|limitTo:150%><span ng-if="feedback.message.length > 150">...</span></a></td>
            <td><%feedback.created_str%></td>
          </tr>
          <tr class='text-right' ng-show='feedbacks.length'>
            <td colspan="29">
            	<ul uib-pagination total-items="totalFeedbacks" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="true" max-size="maxSize" items-per-page="feedbacksPerPage" ></ul>
            </td>
          </tr>
          <tr class='text-center' ng-hide='feedbacks.length'>
            <td colspan="29">No feedbacks to display</td>
          </tr>
        </tbody>
    </table>
</div>
<div id="message-modal" class="modal" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true" tabindex="-1" data-width="800">
  <div class="modal-dialog rotateInDownLeft">
    <div class="modal-content">
      <div class="modal-header">
        Message
        <i class="close fa fa-times btooltip" data-toggle="tooltip" data-placement="top" title="Close" data-dismiss="modal" aria-hidden="true"></i>
      </div>
      <div class="modal-body dyn-height">
      </div>
    </div>
  </div>
</div>
<script src="{{asset('Backend/jscontrols/product_controls.js')}}"></script>
@endsection
