@extends('Backend.layouts.main_layout')
@section('content')
<script src="{{asset('Assets/progressive-upload/fileuploade.js')}}"></script>
<div class="container" ng-controller='UserController'>	
    <div class="row">
        <div class="col-md-9">
            <h4>Customer  (<%totalusers%>)</h4>
        </div>
        <div class="col-md-3 form-inline">
            <label>Search: </label>
            <input type="text" name="email" value="" accesskey="4" autocomplete="off" class="form-control" placeholder="Email">
        </div>
    </div>
    <table class="table table-bordered">
        <thead >
          <tr>
            <th width="5%">Sl. No.</th>
            <th width="20%" >NAME</th>
            <th width="20%" >EMAIL </th>
            <th width="20%" >Mobile Number</th>
            <th width="15%" >CREATED ON.</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat='user in users'>
            <td><%(usersPerPage * (currentPage-1)) + $index+1  %></td>
            <td><%user.name%></td>
            <td><%user.email %></td>
            <td><%user.mobile%></td>
            <td><%user.created_at%></td>
          </tr>
          <tr class='text-right' ng-show='users.length'>
            <td colspan="29">
            	<ul uib-pagination total-items="totalusers" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="true" max-size="maxSize" items-per-page="usersPerPage" ></ul>
            </td>
          </tr>
          <tr class='text-center' ng-hide='users.length'>
            <td colspan="29">No users to display</td>
          </tr>
        </tbody>
    </table>
</div>
<div id="message-modal" class="modal" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true" tabindex="-1" data-width="800">
  <div class="modal-dialog rotateInDownLeft">
    <div class="modal-content">
      <div class="modal-header">
        Message
        <i class="close fa fa-times btooltip" data-toggle="tooltip" data-placement="top" title="Close" data-dismiss="modal" aria-hidden="true"></i>
      </div>
      <div class="modal-body dyn-height">
      </div>
    </div>
  </div>
</div>
<script src="{{asset('Backend/jscontrols/product_controls.js')}}"></script>
@endsection
