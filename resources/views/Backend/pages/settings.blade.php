@extends('Backend.layouts.main_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="/Assets/parsley/parsley.css">
<link rel="stylesheet" type="text/css" href="/Assets/jquery-confirm/jquery-confirm.css">
<section class="content" ng-controller="SettingsController">
    <div class="container-fluid">
    <form ng-enter="updateProfile()" name='update_profile'>
    	{{csrf_field()}}
        <div class="block-header">
            <div class="resp-msg"></div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h4>
                            Change Username/ Password
                        </h4>
                        <hr />
                    </div>
                    <div class="body">
                        <label for="username">Username</label>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="username" id="username" required="required" name="username" class="form-control" placeholder="Username" value="{{Auth::user()->email}}" data-parsley-errors-container="#username_error">
                            </div>
                            <div id="username_error"></div>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="change_pass" value="1"> Change Password
                            </label>
                        </div>
                        <label for="current_password">Current Password</label>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="password" id="current_password" required="required" name="current_password" class="form-control" placeholder="Current Password" data-parsley-errors-container="#old_pass_error">
                            </div>
                            <div id="old_pass_error"></div>
                        </div>
                        <div name="checkd-chg-pass" >
                        </div>
                        <button type="button" ng-click="updateProfile()" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</section>
<script src="{{asset('Backend/jscontrols/settings_controls.js')}}"></script>
<script src="{{asset('Assets/parsley/parsley.js')}}"></script>
<script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script>

@endsection
