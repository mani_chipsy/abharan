<!DOCTYPE html>
<html ng-app="abharan-ad" ng-cloak>
  @include('Backend.header.header')
	<body class="hold-transition skin-blue sidebar-mini" style="height: auto;">
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>                        
		      </button>
		      <a class="navbar-brand" href="/superadmin/product/">{{config('app.name')}}</a>
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
		      <ul class="nav navbar-nav">
		        <li {{ (Request::is('superadmin/product') ? 'class=active' : '') }} ><a href="/superadmin/product/">Products</a></li>		        
		        <li {{ (Request::is('superadmin/product/images') ? 'class=active' : '') }} ><a href="/superadmin/product/images">Images</a></li>

		        <li {{ (Request::is('superadmin/product/up-delete-products') ? 'class=active' : '') }} ><a href="/superadmin/product/up-delete-products">Delete Products</a></li>	

		        <li {{ (Request::is('superadmin/product/rates') ? 'class=active' : '') }} ><a href="/superadmin/product/rates">Daily Rates</a></li>
		        <li {{ (Request::is('superadmin/product/offers') ? 'class=active' : '') }} ><a href="/superadmin/product/offers">Product Offer</a></li>
		        <li {{ (Request::is('superadmin/feedbacks') ? 'class=active' : '') }} ><a href="/superadmin/feedbacks">Customer Feedbacks</a></li>
		        <li {{ (Request::is('superadmin/app/feedbacks') ? 'class=active' : '') }} ><a href="/superadmin/app/feedbacks">Customer Ratings</a></li>
		        <li {{ (Request::is('superadmin/blog') ? 'class=active' : '') }} ><a href="/superadmin/blog">Blog</a></li>
		         <li {{ (Request::is('superadmin/product/orderlist') ? 'class=active' : '') }} ><a href="/superadmin/product/orderlist">Order-List</a></li>
				<li {{ (Request::is('superadmin/userlist') ? 'class=active' : '') }} ><a href="/superadmin/userlist">User-List</a></li>
		      </ul>
		      <ul class="nav navbar-nav navbar-right">
		        <li><a href="/superadmin/signout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
		      </ul>
		    </div>
		  </div>
		</nav>
		<div class="container">
			<div id="preloader">
				<div id="spinner">
					<img style="width:100%" src="{{asset('Backend/assets/images/loading.gif')}}" alt="">
				</div>
				<div id="disable-preloader" class=""></div>
			</div>
        	@yield('content')
        </div>
	<script src="{{asset('Backend/assets/js/generic.js')}}"></script>

	</body>
</html>
