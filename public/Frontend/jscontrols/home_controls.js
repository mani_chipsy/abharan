var myApp = angular.module('abharan', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("FiltersController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
	$scope.getFilters = function(page) {
		$http.get('/filters')
            .then(function(response) {
            	$scope.g_sub_cat = response.data.g_sub_cat;
            	$scope.g_filter_cat = response.data.g_filter_cat;
            	$scope.s_sub_cat = response.data.s_sub_cat;
            	$scope.s_filter_cat = response.data.s_filter_cat;
            	$scope.d_sub_cat = response.data.d_sub_cat;
            	$scope.d_filter_cat = response.data.d_filter_cat;
        }, 'json');
	}
    $scope.getFilters();
}]);

myApp.controller("AuthController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
	$(document).on('keydown', '.form-control', function() {
        $(this).parents('.container').find('.clear-fix').html('');
        $('#resp-msg').removeClass('text-success text-danger').empty();
    });
    
    $scope.loginUser = function() {
    	var form = $('[name=customer_login]');
    	var proceed = $('[name=customer_login]').parsley().validate();
        if (proceed) {
            var postdata = {};
            var data = $('[name=customer_login]').serializeArray();
            $.each(data, function() {
                postdata[this.name] = this.value || '';
            });
            $.post('/auth-login', postdata, function(response) {
                if (response.success) {
                	location.href =  response.redirect;                    
                } else {
                    var display = "";
                    if ((typeof response.message) == 'object') {
                        $.each(response.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.message;
                    }
                    form.find('.signup-error').html('* '+display).css('color', 'red');
                    // $('[name=user-register] input').val('');
                }
            }, 'json');
        } else {
            $('.clear-fix').html('');
        }
    }

    $scope.submitRegister = function() {
    	var form = $('[name=create_customer]');
        var proceed = $('[name=create_customer]').parsley().validate();
        if (proceed) {
            var postdata = {};
            var data = $('[name=create_customer]').serializeArray();
            $.each(data, function() {
                postdata[this.name] = this.value || '';
            });
            $.post('/create-account', postdata, function(response) {
                if (response.success) {
                    var modal_content = '<section id="otp">'+
						'<div class="row">'+
						'<div class="col-xs-24">'+
						'<div class="form-wrap">'+
						'<h1 class="text-center">Enter One Time Password (OTP)</h1><br>'+
						'<p class="text-center" id="resp-msg"></p>'+
						'<p>OTP has been sent to your mobile('+response.mobile+')</p>'+
						'<form role="form" ng-enter="otpVerify()" name="otp_verify" method="post" autocomplete="off">'+
						'<div class="form-group">'+
						'<input type="text" name="otp" class="form-control" placeholder="OTP" data-parsley-required-message="Enter OTP">'+
						'</div>'+
						'<div class="clear-fix"></div>'+
						'<p class="text-right"><span style="cursor:pointer;color:blue" ng-click="resendOtp()">Resend OTP'+
						'</span></p>'+
						'<input type="button" id="btn-login" class="btn btn-custom btn-lg btn-block" ng-click="otpVerify()" value="Verify">'+
						'</form>'+
						'<hr>'+
						'</div>'+
						'</div> <!-- /.col-xs-24 -->'+
						'</div> <!-- /.row -->'+
						'</section>';
						// console.log(modal_content);
					
					angular.element('#quick-shop-modal').find('.modal-dialog').addClass('otp-modal');
					angular.element('#quick-shop-modal .modal-body').html($compile(modal_content)($scope));
					angular.element('#quick-shop-modal').modal('show');
                } else {
                    var display = "";
                    if ((typeof response.message) == 'object') {
                        $.each(response.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.message;
                    }
                    // console.log(display);
                    form.find('.signup-error').html('* '+display).css('color', 'red');
                    // $('[name=user-register] input').val('');
                }
            }, 'json');
        } else {
            $('.clear-fix').html('');
        }
    }

    $scope.resendOtp = function() {
    	$http.get('/resend-otp').then(function(response) {
            if (response.data.success) {
				angular.element('#resp-msg').removeClass('text-danger').addClass('text-success').text(response.data.message);
        	}
        })
    }

    $scope.otpVerify = function() {
    	var proceed = $('[name=otp_verify]').parsley().validate();
        if (proceed) {
        	var postdata = {};
            var data = $('[name=otp_verify]').serializeArray();
            $.each(data, function() {
                postdata[this.name] = this.value || '';
            });
            postdata['_token'] = angular.element('[name=_token]').val();
            $.post('/verify-otp', postdata, function(response) {
                if (response.success) {
                	location.href =  response.redirect;
					angular.element('#quick-shop-modal').find('.modal-dialog').removeClass('otp-modal');
					angular.element('#quick-shop-modal .modal-body').empty();
					angular.element('#quick-shop-modal').modal('hide');
            	} else {
					angular.element('#resp-msg').removeClass('text-success').addClass('text-danger').text(response.message);
            	}
            })
        } else {
            $('.clear-fix').html('');
        }
    }

    $scope.changePass = function() {
    	var proceed = $('[name=change_pass]').parsley().validate();
        if (proceed) {
            var postdata = {};
            var data = $('[name=change_pass]').serializeArray();
            $.each(data, function() {
                postdata[this.name] = this.value || '';
            });
            $.post('/change-pass', postdata, function(response) {
                if (response.success) {
                	location.href =  response.redirect;                    
                } else {
                    var display = "";
                    if ((typeof response.message) == 'object') {
                        $.each(response.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.message;
                    }
                    $('.signup-error').html('* '+display).css('color', 'red');
                    // $('[name=user-register] input').val('');
                }
            }, 'json');
        } else {
            $('.clear-fix').html('');
        }
    }

    $scope.sendResetOtp = function() {
    	var proceed = $('[name=forgot_password]').parsley().validate();
        if (proceed) {
        	var postdata = {};
            var data = $('[name=forgot_password]').serializeArray();
            $.each(data, function() {
                postdata[this.name] = this.value || '';
            });
            postdata['_token'] = angular.element('[name=_token]').val();
            $.post('/recovery-mobile', postdata, function(response) {
                if (response.success) {
                	$('[name=forgot_password]')[0].reset();

	            	var modal_content = '<section id="otp">'+
						'<div class="row">'+
						'<div class="col-xs-24">'+
						'<div class="form-wrap">'+
						'<h1 class="text-center">One Time Password (OTP) sent successfully</h1><br>'+
						'<hr>'+
						'<p>OTP has been sent to your mobile('+response.mobile+'). Enter the same to reset your password</p>'+
						'<a href="'+response.redirect+'">Go to password reset form</a>'
						'</div>'+
						'</div> <!-- /.col-xs-24 -->'+
						'</div> <!-- /.row -->'+
						'</section>';
						// console.log(modal_content);
					
					angular.element('#quick-shop-modal').find('.modal-dialog').addClass('otp-modal');
					angular.element('#quick-shop-modal .modal-body').html($compile(modal_content)($scope));
					angular.element('#quick-shop-modal').modal('show');
            	} else {
            		var display = "";
                    if ((typeof response.message) == 'object') {
                        $.each(response.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.message;
                    }
                    $('.signup-error').html('* '+display).css('color', 'red');
            	}
            })
        } else {
            $('.clear-fix').html('');
        }
    }

    $scope.resetPassword = function() {
    	var proceed = $('[name=reset_password]').parsley().validate();
        if (proceed) {
        	var postdata = {};
            var data = $('[name=reset_password]').serializeArray();
            $.each(data, function() {
                postdata[this.name] = this.value || '';
            });
            postdata['_token'] = angular.element('[name=_token]').val();
            $.post('/reset-request', postdata, function(response) {
                if (response.success) {
                	$('[name=reset_password]')[0].reset();

	            	var modal_content = '<section id="otp">'+
						'<div class="row">'+
						'<div class="col-xs-24">'+
						'<div class="form-wrap">'+
						'<h1 class="text-center">Congratulations! You\'ve successfully changed your password.</h1><br>'+
						'<hr>'+
						'<p>You can now login using your new password </p>'+
						'<a href="/login">Go to login page</a>'
						'</div>'+
						'</div> <!-- /.col-xs-24 -->'+
						'</div> <!-- /.row -->'+
						'</section>';
						// console.log(modal_content);
					
					angular.element('#quick-shop-modal').find('.modal-dialog').addClass('otp-modal');
					angular.element('#quick-shop-modal .modal-body').html($compile(modal_content)($scope));
					angular.element('#quick-shop-modal').modal('show');
            	} else {
            		var display = "";
                    if ((typeof response.message) == 'object') {
                        $.each(response.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.message;
                    }
                    $('.signup-error').html('* '+display).css('color', 'red');
            	}
            })
        } else {
            $('.clear-fix').html('');
        }
    }

}]);

myApp.controller("HomeCollectionsController", ['$scope', '$http', function($scope, $http) {
    $scope.maxSize = 10;

    if ($(window).width() < 767) {
        $scope.maxSize = 3;
    }

    $scope.$watch("currentPage", function() {
        $scope.getCollections($scope.currentPage);
    });

	$scope.getCollections = function(page) {
		$http.get('/product?page='+page)
            .then(function(response) {
            	$scope.collections = response.data.data;

                $scope.totalCollections = response.data.total;
                $scope.currentPage = response.data.current_page;
                $scope.collectionsPerPage = response.data.per_page;
                $scope.collectionsFrom = response.data.from;
                $scope.collectionsTo = response.data.to;

        }, 'json');
	}

	$scope.quickView = function ($index) {
		var collection = $scope.collections[$index];

		var modal_content = '<div class="quick-shop-modal-bg"></div>'+
			'<div class="row">'+
			'<div class="col-md-12 product-image">'+
			'<div id="quick-shop-image" class="product-image-wrapper" style="border: 1px solid gray">';
			
			if (collection.image) {
				modal_content += '<img src="/api/product/storage/IN/'+collection.image.global_name+'_IN.'+collection.image.image_ext+'">';				
			} else {
				modal_content += '<img src="/api/product/storage/IN/NO_IMAGE_IN.png">';
			}

			modal_content += '</div><br>'+
			'</div>'+

			'<div class="col-md-12 product-information">'+
			'<div id="quick-shop-image" class="product-image-wrapper">'+
			'<div style="font-size:18px; font-weight:bold;">'+
			'<span class="productname1"><strong>'+collection.name+' - '+collection.sub_category.s_category_name+'</strong></span>'+
			'</div>'+
			'<hr class="stylehr">'+
			'<div class="section group">'+
			'<div class="col span_3_of_3">'+
			'<span class="style3">Item Code &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;'+ collection.SKU+'</span>'+
			'</div>'+
			'<div class="col span_3_of_3">'+
			'<span class="style3">Gross Weight&nbsp;&nbsp;:&nbsp;&nbsp;'+collection.details.weight+''+
			'</span>'+
			'</div>'+
			'<div class="col span_3_of_3">'+
			'<span class="style3">Beeds Weight&nbsp;&nbsp;:&nbsp;&nbsp;'+collection.details.beeds+'</span>'+
			'</div>'+
			'<div class="col span_3_of_3">'+
			'<span class="style3">Cents&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;'+collection.details.cents+
			'</span>'+
			'</div>'+
			'<div class="col span_3_of_3">'+
			'<span class="style3">Metal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;'+collection.details.metal+
			'</span>'+
			'</div>'+
			'<div class="col span_3_of_3">'+
			'<span class="style3">Purity&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+collection.details.purity+
			'</span>'+
			'</div>'+
			// '<div class="col span_3_of_3">'+
			// '<span class="style3">Availability&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;Unknown'+
			// '</span>'+
			// '</div>'+
			'</div>'+
			'<hr class="stylehr">'+
			'<div class="section group">'+
			'<div class="col span_1_of_1">'+
			'<span class="style2"> <strong>';
			
			if (!collection.offer_value) {
				modal_content += '<div class="product-price abharan-compare"> '+
					'<span class="price_sale"><span class="money">Rs.'+collection.gross_value+'</span></span>';
			} else {
				modal_content += '<div class="product-price abharan-compare">'+
					'<span class="price_sale"><span class="money">Rs.'+collection.offer_value+'</span></span>'+
					'<del class="price_compare"> <span class="money">Rs.'+collection.gross_value+'</span></del>';
			}			
			
			modal_content += ((collection.gst)? (' (Inclusive of '+collection.gst+'% GST: '+collection.gst_amount+')') : '')+' (Starting price)&nbsp;&nbsp;  Web Orders Only '+
			'</div></strong></span>'+
			'<span class="WebOrderTextMobile" style="display:none">Web Orders Only</span>'+
			'</div>'+
			'</div>'+
			'<h1 id="quick-shop-title"></h1>'+
			'<div id="quick-shop-infomation" class="description">'+
			'<div id="quick-shop-description" class="text-left"></div>'+
			'</div>'+
			'<div id="quick-shop-container">'+
			'<div id="quick-shop-relative" class="relative text-left">'+
			'<ul class="list-unstyled">'+
			'<li class="control-group vendor">'+
			'<span class="control-label">Vendor :</span>'+
			'</li>'+
			'<li class="control-group type">'+
			'<span class="control-label">Type :</span>'+
			'</li>'+
			'</ul>'+
			'</div>'+
			'<div class="others-bottom">'+
			// '<input id="quick-shop-add" class="btn small add-to-cart" type="submit" name="add" value="View More" />'+
			'<a id="quick-shop-add" class="btn small add-to-cart" href="/product-details?product_id='+collection.product_id+'" >View More</a>'+
			'</div>'+
			'</div>'+
			'</div>'+
			'</div>'+
			'</div>';
			// console.log(modal_content);
		
		angular.element('#quick-shop-modal .modal-body').html(modal_content);
		angular.element('#quick-shop-modal').modal('show');
	};

	$scope.getPopularCollections = function() {
		$http.get('/product/get-main-cat')
            .then(function(response) {
            	$scope.pop_collections = response.data;
        }, 'json');
	}

    $scope.getPopularCollections();
	$scope.getWeeklyCollections = function() {
		$http.get('/product/get-sub-cat')
            .then(function(response) {
            	var collections = response.data;
            	angular.forEach(collections, function(collection, index) {
            		// console.log(collection);
                    collection.imgsrc_out = '/Frontend/assets/images/Sub_cata/'+collection.image_name;
                    collection.imgsrc_hover = '/Frontend/assets/images/Sub_cata/'+collection.hover_img;
                });
            	// console.log(collections);
            	$scope.weekly_collections = collections;
        }, 'json');
	}
	$scope.getWeeklyCollections();
}]);

myApp.controller("CartListController", ['$scope', '$http', '$interval', function($scope, $http, $interval) {
	$scope.getCartItems = function() {
		$http.get('/product/cart/data').then(function(response) {
			$scope.cartItems = response.data.cartItems;
			$scope.cartCount = response.data.cartCount;
			$scope.cartTax = response.data.cartTax;
			$scope.cartSubtotal = response.data.cartSubtotal;
			$scope.cartTotal = response.data.cartTotal;
        }, 'json');
	}

	$scope.getCartItems();
	$interval($scope.getCartItems, 3000);

    $scope.addToCart = function() {
        var qty = $('[name="cartform"] [name="qty[]"]');
        var product_id = $('[name="cartform"] [name="product_id[]"]');
        // console.log(qty);
        var qty_updates = {};

        angular.forEach (product_id, function(product, index) {
            qty_updates[product.value] = qty[index].value;
        });

        var post_data = {
            qty_updates: qty_updates,
            task : 'up_qty'
        };

        $http.post('/product/cart/add', post_data).then(function(response) {
            if (response.data.success) {
                $scope.getCartItems();
                $scope.cartTotal = response.data.cartTotal;
            }
        }, 'json');
    }

	$scope.deleteItem = function($product_id) {
		$http.get('/product/cart/delete?product_id='+$product_id).then(function(response) {
            // console.log(response);
            if (response.data.success) {
				$scope.getCartItems();
				$scope.cartTotal = response.data.cartTotal;
            }
        }, 'json');
	}
}]);

myApp.controller("CollectionsController", ['$scope', '$http', function($scope, $http) {
	var tag_tmplate = '<span class="ab-theme-color" id="{filter_id}">'
		+'{title}'
		+'<i data-role="remove"></i>'
		+'</span>';

	$scope.maxSize = 10;

    $(document).on('change', '.main-cat, .sub-cat, .filter-cat, [name=price-filter]', function() {
    	$.loader.start();
		var m_category_id = {};
		var s_category_id = {};
		var filter_cat = {};

		$("[name='main-cat'] :checkbox").each(function(index) {
			if ($(this).is(':checked')) {
				m_category_id[index] = $(this).val();				

				var title = $.trim($(this).parent().find('label').text());

			    var mapObj = {
			       '{title}': title,
			       '{filter_id}': 'main-'+$(this).val(),
			    };

			    tag = tag_tmplate.replace(/{title}|{filter_id}/gi, function(matched){
			      return mapObj[matched];
			    });

			    if (!$(this).data('filter-tag')) {
			    	$(this).data('filter-tag', 1);
			    	$("[name='filter-tags']").append(tag);
			    }
			} else {
			    $(this).data('filter-tag', 0);
			    $("[name='filter-tags']").find('#main-'+$(this).val()).remove();
			}
		});

		$("[name='sub-cat'] :checkbox").each(function(index) {
			if ($(this).is(':checked')) {
				s_category_id[index] = $(this).val();

				var title = $.trim($(this).parent().find('label').text());

			    var mapObj = {
			       '{title}': title,
			       '{filter_id}': 'sub-'+$(this).val(),
			    };

			    tag = tag_tmplate.replace(/{title}|{filter_id}/gi, function(matched){
			      return mapObj[matched];
			    });

			    if (!$(this).data('filter-tag')) {
			    	$(this).data('filter-tag', 1);
			    	$("[name='filter-tags']").append(tag);
			    }
			} else {
			    $(this).data('filter-tag', 0);
			    $("[name='filter-tags']").find('#sub-'+$(this).val()).remove();
			}
		});

		$("[name='filter-cat'] :checkbox").each(function(index) {
			if ($(this).is(':checked')) {
				filter_cat[index] = $(this).val();
				// filter_cat[$(this).val()] = 1;

				var title = $.trim($(this).parent().find('label').text());

			    var mapObj = {
			       '{title}': title,
			       '{filter_id}': 'filter-'+$(this).val(),
			    };

			    tag = tag_tmplate.replace(/{title}|{filter_id}/gi, function(matched){
			      return mapObj[matched];
			    });

			    if (!$(this).data('filter-tag')) {
			    	$(this).data('filter-tag', 1);
			    	$("[name='filter-tags']").append(tag);
			    }
			} else {
			    $(this).data('filter-tag', 0);
			    $("[name='filter-tags']").find('#filter-'+$(this).val()).remove();
			}
		});

	    var myObject = {
          m_category_id: m_category_id,
          s_category_id: s_category_id,
          filter_cat: filter_cat,
          price_filter: $('[name="price-filter"]:checked').val(),
          smart_query: $('[name=search-collection]').val()
        };

        var cond = $.param( myObject );
        $scope.getCollections(1, cond);
	});

    $(document).on('click', '[data-role="remove"]', function() {
    	$.loader.start();

    	var filterTag = $(this).parent().attr('id').split('-');
    	$('#'+filterTag[0]+'-box-'+filterTag[1]).prop('checked', false);
    	$(this).parent().remove();

		var m_category_id = {};
		var s_category_id = {};
		var filter_cat = {};

		$("[name='main-cat'] :checkbox:checked").each(function(index) {
			m_category_id[index] = $(this).val();		
		});

		$("[name='sub-cat'] :checkbox:checked").each(function(index) {
			s_category_id[index] = $(this).val();
		});

	    $("[name='filter-cat'] :checkbox").each(function(index) {
			if ($(this).is(':checked')) {
				// filter_cat[$(this).val()] = 1;
				filter_cat[index] = $(this).val();

				var title = $.trim($(this).parent().find('label').text());

			    var mapObj = {
			       '{title}': title,
			       '{filter_id}': 'filter-'+$(this).val(),
			    };

			    tag = tag_tmplate.replace(/{title}|{filter_id}/gi, function(matched){
			      return mapObj[matched];
			    });

			    if (!$(this).data('filter-tag')) {
			    	$(this).data('filter-tag', 1);
			    	$("[name='filter-tags']").append(tag);
			    }
			} else {
			    $(this).data('filter-tag', 0);
			    $("[name='filter-tags']").find('#filter-'+$(this).val()).remove();
			}
		});

	    var myObject = {
          m_category_id: m_category_id,
          s_category_id: s_category_id,
          filter_cat: filter_cat,
          price_filter: $('[name="price-filter"]:checked').val(),
          smart_query: $('[name=search-collection]').val()
        };
        var cond = $.param( myObject );
        $scope.getCollections(1, cond);
    });

    $(document).on('keyup', '[name=search-collection]', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13 || $(this).val()) {
            $.loader.start();

	    	var m_category_id = {};
			var s_category_id = {};
			var filter_cat = {};

			$("[name='main-cat'] :checkbox").each(function(index) {
				if ($(this).is(':checked')) {
					m_category_id[index] = $(this).val();				

					var title = $.trim($(this).parent().find('label').text());

				    var mapObj = {
				       '{title}': title,
				       '{filter_id}': 'main-'+$(this).val(),
				    };

				    tag = tag_tmplate.replace(/{title}|{filter_id}/gi, function(matched){
				      return mapObj[matched];
				    });

				    if (!$(this).data('filter-tag')) {
				    	$(this).data('filter-tag', 1);
				    	$("[name='filter-tags']").append(tag);
				    }
				} else {
				    $(this).data('filter-tag', 0);
				    $("[name='filter-tags']").find('#main-'+$(this).val()).remove();
				}
			});

			$("[name='sub-cat'] :checkbox").each(function(index) {
				if ($(this).is(':checked')) {
					s_category_id[index] = $(this).val();

					var title = $.trim($(this).parent().find('label').text());

				    var mapObj = {
				       '{title}': title,
				       '{filter_id}': 'sub-'+$(this).val(),
				    };

				    tag = tag_tmplate.replace(/{title}|{filter_id}/gi, function(matched){
				      return mapObj[matched];
				    });

				    if (!$(this).data('filter-tag')) {
				    	$(this).data('filter-tag', 1);
				    	$("[name='filter-tags']").append(tag);
				    }
				} else {
				    $(this).data('filter-tag', 0);
				    $("[name='filter-tags']").find('#sub-'+$(this).val()).remove();
				}
			});

		    $("[name='filter-cat'] :checkbox").each(function(index) {
				if ($(this).is(':checked')) {
					// filter_cat[$(this).val()] = 1;
					filter_cat[index] = $(this).val();

					var title = $.trim($(this).parent().find('label').text());

				    var mapObj = {
				       '{title}': title,
				       '{filter_id}': 'filter-'+$(this).val(),
				    };

				    tag = tag_tmplate.replace(/{title}|{filter_id}/gi, function(matched){
				      return mapObj[matched];
				    });

				    if (!$(this).data('filter-tag')) {
				    	$(this).data('filter-tag', 1);
				    	$("[name='filter-tags']").append(tag);
				    }
				} else {
				    $(this).data('filter-tag', 0);
				    $("[name='filter-tags']").find('#filter-'+$(this).val()).remove();
				}
			});

		    var myObject = {
	          m_category_id: m_category_id,
	          s_category_id: s_category_id,
	          filter_cat: filter_cat,
      		  price_filter: $('[name="price-filter"]:checked').val(),
	          smart_query: $('[name=search-collection]').val()
	        };

	        var cond = $.param( myObject );

	        $scope.getCollections(1, cond);
        }
    });

    $scope.$watch("currentPage", function() {
    	$.loader.start();

    	var m_category_id = {};
		var s_category_id = {};
		var filter_cat = {};

		$("[name='main-cat'] :checkbox").each(function(index) {
			if ($(this).is(':checked')) {
				m_category_id[index] = $(this).val();				

				var title = $.trim($(this).parent().find('label').text());

			    var mapObj = {
			       '{title}': title,
			       '{filter_id}': 'main-'+$(this).val(),
			    };

			    tag = tag_tmplate.replace(/{title}|{filter_id}/gi, function(matched){
			      return mapObj[matched];
			    });

			    if (!$(this).data('filter-tag')) {
			    	$(this).data('filter-tag', 1);
			    	$("[name='filter-tags']").append(tag);
			    }
			} else {
			    $(this).data('filter-tag', 0);
			    $("[name='filter-tags']").find('#main-'+$(this).val()).remove();
			}
		});

		$("[name='sub-cat'] :checkbox").each(function(index) {
			if ($(this).is(':checked')) {
				s_category_id[index] = $(this).val();

				var title = $.trim($(this).parent().find('label').text());

			    var mapObj = {
			       '{title}': title,
			       '{filter_id}': 'sub-'+$(this).val(),
			    };

			    tag = tag_tmplate.replace(/{title}|{filter_id}/gi, function(matched){
			      return mapObj[matched];
			    });

			    if (!$(this).data('filter-tag')) {
			    	$(this).data('filter-tag', 1);
			    	$("[name='filter-tags']").append(tag);
			    }
			} else {
			    $(this).data('filter-tag', 0);
			    $("[name='filter-tags']").find('#sub-'+$(this).val()).remove();
			}
		});

	    $("[name='filter-cat'] :checkbox").each(function(index) {
			if ($(this).is(':checked')) {
				// filter_cat[$(this).val()] = 1;
				filter_cat[index] = $(this).val();

				var title = $.trim($(this).parent().find('label').text());

			    var mapObj = {
			       '{title}': title,
			       '{filter_id}': 'filter-'+$(this).val(),
			    };

			    tag = tag_tmplate.replace(/{title}|{filter_id}/gi, function(matched){
			      return mapObj[matched];
			    });

			    if (!$(this).data('filter-tag')) {
			    	$(this).data('filter-tag', 1);
			    	$("[name='filter-tags']").append(tag);
			    }
			} else {
			    $(this).data('filter-tag', 0);
			    $("[name='filter-tags']").find('#filter-'+$(this).val()).remove();
			}
		});

	    var myObject = {
          m_category_id: m_category_id,
          s_category_id: s_category_id,
          filter_cat: filter_cat,
          price_filter: $('[name="price-filter"]:checked').val(),
          smart_query: $('[name=search-collection]').val()
        };

        var cond = $.param( myObject );

        $scope.getCollections($scope.currentPage, cond);
    });

	$scope.getCollections = function(page, $cond='') {
		$http.get('/product/get-collection?page='+page+(($cond) ? ('&'+$cond) : ''))
            .then(function(response) {
            	$scope.collections = response.data.data;

                $scope.totalCollections = response.data.total;
                $scope.currentPage = response.data.current_page;
                $scope.collectionsPerPage = response.data.per_page;
                $scope.collectionsFrom = response.data.from;
                $scope.collectionsTo = response.data.to;
    			$.loader.stop();
        }, 'json');
	}

	$scope.quickView = function ($index) {
		console.log($scope.collections);
		var collection = $scope.collections[$index];

		var modal_content = '<div class="quick-shop-modal-bg"></div>'+
            '<div class="row">'+
            '<div class="col-md-24 product-image">'+
            '<h1 style="    font-weight: 500;margin-bottom: 12px;">'+collection.sub_category.s_category_name+'</h1><div id="quick-shop-image" class="product-image-wrapper" style="border: 1px solid gray">';
            
            if (collection.image) {
                modal_content += '<img src="/product/storage/IN/'+collection.image.global_name+'_IN.'+collection.image.image_ext+'">';              
            } else {
                modal_content += '<img src="/product/storage/IN/NO_IMAGE_IN.png?width=800&height=500" style="width:800px;height:500px">';
            }

            modal_content += '</div><br>'+
            
            
            '<div class="others-bottom">'+
            // '<input id="quick-shop-add" class="btn small add-to-cart" type="submit" name="add" value="View More" />'+
            '<a id="quick-shop-add" class="btn add-to-cart" style="border: none;background:transparent;" href="/product-details?product_id='+collection.product_id+'" ><button name="add" type="submit" id="add-to-cart" data-loading-text="Loading..." class="btn btn-primary btn-lg button-cart" style="    min-width: 200px;">View More</button></a>'+
            '</div>'+
        

            '</div>';
            // console.log(modal_content);
		
		angular.element('#quick-shop-modal .modal-body').html(modal_content);
		angular.element('#quick-shop-modal').modal('show');
	};
}]);

myApp.controller("DailyRatesController", ['$scope', '$http', function($scope, $http) {
	$scope.getDailyRates= function(page) {
		 $http.get('/product/rates')
            .then(function(response) {
            	$scope.metal_rates = response.data;
        }, 'json');
	}

	$scope.getDailyRates();
}]);

myApp.controller("ProductDetailsController", ['$scope', '$http', function($scope, $http) {
	$scope.returnPolicy = function () {
		var modal_content = '<div class="quick-shop-modal-bg"></div>'+
			'<div class="row">'+
			'<div class="col-md-24">'+
			'<h3>Return Policies</h3>'+
			'<p>Abharan is committed to making sure that you are completely satisfied with the quality of our products and the service that you receive.</p>'+
			'<h3>Returns and Exchanges</h3>'+
			'<p>Your complete shopping satisfaction is our No.1 Priority.'+
			' If an item you ordered from abharan.com does not meet your expectations,'+
			'please contact us within fifteen (15) days of purchase to obtain a return authorization and instructions.'+
			' The product must be returned in original condition in the original packaging.'+
			' Upon arrival and inspection, product refunds will be promptly credited to the original purchase account,'+
			' shipping excluded. Certain items may have additional specific return conditions'+
			' published on the respective product web page.<p>'+
			'<h3>Warranty and Defect Claims</h3>'+
			'<p>Abharan takes complete responsibility for return and replacement shipping costs'+
			' if any product is determined to be defective. Please contact us within the warranty period to obtain instructions.<p>'+			
			'</div>'+
			'</div><br>';
			// console.log(modal_content);
		
		angular.element('#quick-shop-modal .modal-body').html(modal_content);
		angular.element('#quick-shop-modal').modal('show');
	};	
}]);

myApp.controller("OrdHistoryController", ['$scope', '$http', function($scope, $http) {
	$scope.getOrderHistory = function(page) {
		$http.get('/product/order-history/get_data?page='+page).then(function(response) {
			// console.log(response);
			$scope.histories = response.data.data;
			$scope.totalHistory = response.data.total;
            $scope.currentPage = response.data.current_page;
            $scope.historyPerPage = response.data.per_page;
			// $scope.cartCount = response.data.cartCount;
			// $scope.cartTax = response.data.cartTax;
			// $scope.cartSubtotal = response.data.cartSubtotal;
			// $scope.cartTotal = response.data.cartTotal;
        }, 'json');
	}

	$scope.maxSize = 10;
    $scope.$watch("currentPage", function() {
        $scope.getOrderHistory($scope.currentPage);
    });
}]);
myApp.controller("OopsController", ['$scope', '$http', function($scope, $http) {
}]);

myApp.controller("ViewOrderController", ['$scope', '$http', function($scope, $http) {
}]);


myApp.controller("ShoppingCartController", ['$scope', '$http', function($scope, $http) {
	$(document).on('keydown', '.form-control', function() {
        $('.clear-fix').html('');
        $('#resp-msg').removeClass('text-success text-danger').empty();
    });
	$scope.getCartItems = function() {
		$http.get('/product/cart/data').then(function(response) {
			$scope.cartItems = response.data.cartItems;
			$scope.cartCount = response.data.cartCount;

			$scope.cartTax = response.data.cartTax;
			$scope.cartSubtotal = response.data.cartSubtotal;

			$scope.cartTotal = response.data.cartTotal;
        }, 'json');
	}

	$scope.getCartItems();

	$scope.updateQty = function() {
		var qty = $('[name="cartform"] [name="qty[]"]');
		var product_id = $('[name="cartform"] [name="product_id[]"]');
		// console.log(qty);
		var qty_updates = {};

		angular.forEach (product_id, function(product, index) {
	        qty_updates[product.value] = qty[index].value;
	    });

		var post_data = {
            qty_updates: qty_updates,
            task : 'up_qty'
        };

        $http.post('/product/cart/add', post_data).then(function(response) {
            if (response.data.success) {
				$scope.getCartItems();
				$scope.cartTotal = response.data.cartTotal;
            }
        }, 'json');
	}

	$scope.deleteItem = function($product_id) {
		$http.get('/product/cart/delete?product_id='+$product_id).then(function(response) {
            // console.log(response);
            if (response.data.success) {
				$scope.getCartItems();
				$scope.cartTotal = response.data.cartTotal;
            }
        }, 'json');
	}

//     $("[name='checkout_order']").submit(function(e) { 
//         // this code prevents form from actually being submitted
//         e.preventDefault();
//         e.returnValue = false;

//         var $form = $(this);

//         var postdata = {};
//         var data = $(this).serializeArray();
//         $.each(data, function() {
//             postdata[this.name] = this.value || '';
//         });
//         // this is the important part. you want to submit
//         // the form but only after the ajax call is completed
//         $.ajax({ 
//             type: 'post',
//             data: postdata,
//             url: '/product/checkout-order', 
//             context: $form, // context will be "this" in your handlers
//             success: function() { // your success handler
//             },
//             error: function() { // your error handler
//             },
//             complete: function(res) { 
//                 res = $.parseJSON(res.responseText);
//                 $.each(res.params, function(key, value) {
//                     $("[name="+key+"]").val(value);
//                 });
//                 // make sure that you are no longer handling the submit event; clear handler
//                 this.off('submit');
//                 // actually submit the form
//                 this.submit();
//             }
//         });
// });

	$scope.checkoutOrder = function() {
		var proceed = $('[name=checkout_order]').parsley().validate();
        if (proceed) {
            var postdata = {};
            var data = $('[name=checkout_order]').serializeArray();
            $.each(data, function() {
                postdata[this.name] = this.value || '';
            });
            $.post('/product/checkout-order', postdata, function(response) {
            	// console.log(response);
                if (response.success) {
                    /*var postdata = response.params;
                    $.each(postdata, function(key, value) {
                        // console.log(key+' => '+value);
                        $("[name="+key+"]").val(value);
                    });*/

                    // $("[name='checkout_gateway']").attr('action', response.redirect_url);

                    location.href =  response.redirect_url+((response.order_no) ? ('?order_no='+response.order_no) : '');                    
                    // location.href =  response.redirect_url;
                } else {
                    var display = "";
                    if ((typeof response.message) == 'object') {
                        $.each(response.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.message;
                    }
                    $('.signup-error').html('* '+display).css('color', 'red');
                    // $('[name=user-register] input').val('');
                }
            }, 'json');
        } else {
            $('.clear-fix').html('');
        }
	}
}]);

myApp.controller("NewThisWeekController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
	$scope.getNewThisWeek = function() {
		$http.get('/product/new-collection')
            .then(function(response) {
            	$scope.new_colctns = response.data;
        }, 'json');
	}

    $scope.getNewThisWeek();

	$scope.quickView = function ($index) {
		var collection = $scope.new_colctns[$index];

		var modal_content = '<div class="quick-shop-modal-bg"></div>'+
			'<div class="row">'+
			'<div class="col-md-24 product-image">'+
            '<h1 style="    font-weight: 500;margin-bottom: 12px;">'+collection.sub_category.s_category_name+'</h1><div id="quick-shop-image" class="product-image-wrapper" style="border: 1px solid gray">';
			
			if (collection.image) {
				modal_content += '<img src="/product/storage/IN/'+collection.image.global_name+'_IN.'+collection.image.image_ext+'">';				
			} else {
				modal_content += '<img src="/product/storage/IN/NO_IMAGE_IN.png?width=800&height=500" style="width:800px;height:500px">';
			}

			modal_content += '</div><br>'+
			
			
			'<div class="others-bottom">'+
			// '<input id="quick-shop-add" class="btn small add-to-cart" type="submit" name="add" value="View More" />'+
			'<a id="quick-shop-add" class="btn add-to-cart" style="border: none;background:transparent;" href="/product-details?product_id='+collection.product_id+'" ><button name="add" type="submit" id="add-to-cart" data-loading-text="Loading..." class="btn btn-primary btn-lg button-cart" style="    min-width: 200px;">View More</button></a>'+
			'</div>'+
		

			'</div>';
			// console.log(modal_content);
		
		angular.element('#quick-shop-modal .modal-body').html(modal_content);
		angular.element('#quick-shop-modal').modal('show');
	};
}]);

myApp.controller("FeedbackController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
	$(document).on('keydown', '.form-control', function() {
        $(this).parents('.container').find('.clear-fix').html('');
    });

	$scope.submitFeedback = function() {
    	var form = $('#contact_form');
        var proceed = form.parsley().validate();
        if (proceed) {
            var postdata = {};
            var data = form.serializeArray();
            $.each(data, function() {
                postdata[this.name] = this.value || '';
            });
            $.post('/submit-feedback', postdata, function(response) {
                if (response.success) {
					form[0].reset();

	            	var modal_content = '<section id="otp">'+
						'<div class="row">'+
						'<div class="col-xs-24">'+
						'<div class="form-wrap">'+
						'<h1 class="text-center">Thank you for your feedback</h1><br>'+
						'<hr>'+
						'<p>Thank you for your feedback. Our goal is to make our clients happy, so we\'ll use your feedback to create a greater experience for everyone. We appreciate your time and value your opinions; please don\'t hesitate to contact us with questions and comments at 1800 5999 777 (Toll free) </p>'+
						'</div>'+
						'</div> <!-- /.col-xs-24 -->'+
						'</div> <!-- /.row -->'+
						'</section>';
						// console.log(modal_content);
					
					angular.element('#quick-shop-modal').find('.modal-dialog').addClass('otp-modal');
					angular.element('#quick-shop-modal .modal-body').html($compile(modal_content)($scope));
					angular.element('#quick-shop-modal').modal('show');

                } else {
                    var display = "";
                    if ((typeof response.message) == 'object') {
                        $.each(response.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.message;
                    }
                    form.find('.signup-error').html('* '+display).css('color', 'red');
                    // $('[name=user-register] input').val('');
                }
            }, 'json');
        } else {
            $('.clear-fix').html('');
        }
    }
}]);

myApp.filter('INR', function () {
    return function (input) {
    	// console.log(input);
    	if (input && isNaN(input)) {
    		input = Number(input.replace(/[^0-9\.]+/g,""))
    	}
    	// console.log(input);

        if (! isNaN(input)) {
        	var currencySymbol = 'Rs.';
            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            
            if (result.length > 1) {
                output += "." + result[1];
            }            

            return currencySymbol + output;
        }
    }
});

myApp.filter('keylength', function(){
  return function(input){
  	if (input) {
	    if(!angular.isObject(input)){
	      throw Error("Usage of non-objects with keylength filter!!")
	    }
	    return Object.keys(input).length;	  		
  	}
  }
});

myApp.filter('cartlimit', function(){
  return function(input){
  	if (input) {
  		var i=1;
  		var newArr = {};
  		for (var value in input) {
  			if (i > 4) {
  				break;
  			} else {
  				newArr[value] = input[value];  				
  			}
  			i++;
  		}
	    return newArr;	  		
  	}
  }
});

myApp.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keypress", function(e) {
            if (e.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter, {
                        'e': e
                    });
                });
                e.preventDefault();
            }
        });
    };
});

myApp.directive("owlCarousel", function() {
    return {
        restrict: 'E',
        transclude: false,
        link: function (scope) {
            scope.initCarousel = function(element) {
              // provide any default options you want
                var defaultOptions = {
                };
                // var customOptions = scope.$eval($(element).attr('data-options'));
                var customOptions = {
			        navigation : true,
			        pagination: false,
			        autoPlay: true,
			        stopOnHover: true,
			        items: 4,
			        itemsDesktop : [1199,4],
			        itemsDesktopSmall : [979,3],
			        itemsTablet: [768,3],
			        itemsTabletSmall: [540,2],
			        itemsMobile : [360,1],
			        scrollPerPage: true,
			        navigationText: ['<span class="btooltip" title="Previous"></span>', '<span class="btooltip" title="Next"></span>'],
			        afterInit: function(elem){
			          if(touch == false){
			          elem.find('.btooltip').tooltip();
			          }
			        }
			    };

                // combine the two options objects
                for(var key in customOptions) {
                    defaultOptions[key] = customOptions[key];
                }
                // init carousel
                $(element).owlCarousel(defaultOptions);
            };
        }
    };
});

myApp.directive('owlCarouselItem', [function() {
    return {
        restrict: 'A',
        transclude: false,
        link: function(scope, element) {
          // wait for the last item in the ng-repeat then call init
            if(scope.$last) {
                scope.initCarousel(element.parent());
            }
        }
    };
}]);