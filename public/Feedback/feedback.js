var myApp = angular.module('Feedback', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

// var app = angular.module('Feedback', []);

myApp.controller("FeedbackController", ['$scope', '$http', function($scope, $http) {
	// Pagination of Users
   

	$scope.save = function() {
        // var form = $('[name=feedback]');
  

 
         var proceed = $('[name=feedback]').parsley().validate();
         
          if (proceed) {

                            var postdata ={}
                            
                   postdata={'Staff':$('#Staff').val(),
                              'name':$('#name').val(),
                              'land_line':$('#land_line').val(),
                              'mobile_no':$('#mobile_no').val(),
                              'quality_service':$('[name=qualitys]:checked').val(),
                              'exclusive': $('[name=exclusive]:checked').val(),
                              'service': $('[name=service]:checked').val(),
                              'convenience': $('[name=convenience]:checked').val(),
                              'response': $('[name=response]:checked').val(),
                              'quality': $('[name=quality]:checked').val(),
                              'delivery': $('[name=delivery]:checked').val(),
                              'showroom': $('[name=showroom]:checked').val(),
                              'information': $('[name=information]:checked').val(),
                              'attending': $('[name=attending]:checked').val(),

                              'general1': $('[name=general1]:checked').val(),
                              'general2': $('[name=general1]:checked').val(),
                              'mobile_bill': $('[name=general1]:checked').val(),
                              'comment':$('#comment').val(),
                              'place':$('#mySelect').val(),
                              'mood':$('ul.mood').find('li.active').val()
                               };
                              // alert(JSON.stringify(postdata));
                           

                  $.post('post-feedback', postdata, function(response) {
                                if (response.success) {
                                 location.href = '/';
                                
                                
                                } else {
                                  // alert("hi");
                                    var display = "";
                                    if ((typeof response.message) == 'object') {
                                        $.each(response.message, function(key, value) {
                                            display = value[0];
                                        });
                                    } else {
                                        display = response.message;
                                    }
                                    
                                    $.alert({
                                        title: 'Error!',
                                        content: display,
                                        icon: 'fa fa-rocket',
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-red',
                                                action: function() {
                                                    
                                                    // window.location='/nightout/pgp-office/request';
                                                }
                                            }
                                        }
                                    });
                                             
                                }
                            }, 'json');
                        
        }
      }
    

}]);

myApp.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keypress", function(e) {
            if (e.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter, {
                        'e': e
                    });
                });
                e.preventDefault();
            }
        });
    };
});