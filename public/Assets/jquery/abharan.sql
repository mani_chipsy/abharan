-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2017 at 07:46 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abharan`
--

-- --------------------------------------------------------

--
-- Table structure for table `image_master`
--

CREATE TABLE `image_master` (
  `image_master_id` int(10) NOT NULL,
  `global_name` varchar(20) NOT NULL,
  `dirYear` varchar(4) NOT NULL,
  `dirMonth` varchar(2) NOT NULL,
  `image_name` varchar(20) NOT NULL,
  `image_time` varchar(20) NOT NULL,
  `image_ext` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `product_id` int(10) NOT NULL,
  `category` varchar(10) NOT NULL,
  `weight` varchar(10) NOT NULL,
  `quote` int(10) NOT NULL,
  `purity` int(10) NOT NULL,
  `metal` varchar(20) NOT NULL,
  `beeds` float NOT NULL,
  `beeds_value` float NOT NULL,
  `wastage` float NOT NULL,
  `making_amount` float NOT NULL,
  `cents` float NOT NULL,
  `daimond` float NOT NULL,
  `stone` float NOT NULL,
  `cost` int(10) NOT NULL,
  `description` varchar(100) NOT NULL,
  `production` varchar(20) NOT NULL,
  `max` int(10) NOT NULL,
  `certification` int(10) NOT NULL,
  `cut` varchar(30) NOT NULL,
  `colour` varchar(20) NOT NULL,
  `clarity` varchar(20) NOT NULL,
  `women` int(1) NOT NULL,
  `men` int(1) NOT NULL,
  `kids` int(1) NOT NULL,
  `collection` varchar(100) NOT NULL,
  `occasion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE `product_image` (
  `product_id` int(10) NOT NULL,
  `global_name` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_master`
--

CREATE TABLE `product_master` (
  `product_id` int(10) NOT NULL,
  `SKU` bigint(255) NOT NULL,
  `m_category_id` int(10) NOT NULL,
  `s_category_id` int(10) NOT NULL,
  `created_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_orders`
--

CREATE TABLE `user_orders` (
  `id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `order_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_current_rates`
--

CREATE TABLE `_current_rates` (
  `m_category_id` int(10) NOT NULL,
  `weight` float NOT NULL,
  `amount` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_current_tax`
--

CREATE TABLE `_current_tax` (
  `gst` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_current_tax`
--

INSERT INTO `_current_tax` (`gst`) VALUES
('3%');

-- --------------------------------------------------------

--
-- Table structure for table `_main_category`
--

CREATE TABLE `_main_category` (
  `m_category_id` int(10) NOT NULL,
  `m_category_name` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_sub_category`
--

CREATE TABLE `_sub_category` (
  `s_category_id` int(10) NOT NULL,
  `s_category_name` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `image_master`
--
ALTER TABLE `image_master`
  ADD PRIMARY KEY (`image_master_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_master`
--
ALTER TABLE `product_master`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `SKU` (`SKU`),
  ADD KEY `m_category_id` (`m_category_id`),
  ADD KEY `s_category_id` (`s_category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mobile` (`mobile`);

--
-- Indexes for table `user_orders`
--
ALTER TABLE `user_orders`
  ADD KEY `id` (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `_current_rates`
--
ALTER TABLE `_current_rates`
  ADD KEY `m_category_id` (`m_category_id`);

--
-- Indexes for table `_main_category`
--
ALTER TABLE `_main_category`
  ADD PRIMARY KEY (`m_category_id`);

--
-- Indexes for table `_sub_category`
--
ALTER TABLE `_sub_category`
  ADD PRIMARY KEY (`s_category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `image_master`
--
ALTER TABLE `image_master`
  MODIFY `image_master_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product_master`
--
ALTER TABLE `product_master`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_main_category`
--
ALTER TABLE `_main_category`
  MODIFY `m_category_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_sub_category`
--
ALTER TABLE `_sub_category`
  MODIFY `s_category_id` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
