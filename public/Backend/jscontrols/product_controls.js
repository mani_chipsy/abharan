var myApp = angular.module('abharan-ad', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("ProductController", ['$scope', '$http', function($scope, $http) {
	// Pagination of Users
    $scope.maxSize = 10;
    $scope.$watch("currentPage", function() {
        $.loader.start();

        var myObject = {
          sku: $('[name=search_sku]').val()
        };

        var cond = $.param( myObject );

        $scope.getProducts($scope.currentPage, cond);
    });

    $(document).on('keyup', '[name=search_sku]', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13 || $(this).val()) {
            $.loader.start();

            var myObject = {
              sku: $('[name=search_sku]').val()
            };

            var cond = $.param( myObject );

            $scope.getProducts(1, cond);
        }
    });

	$scope.getProducts = function(page, $cond='') {
		 $http.get('/superadmin/product/get/get_data?page='+page+(($cond) ? ('&'+$cond) : ''))
            .then(function(response) {
            	$scope.products = response.data.data;

                $scope.totalProducts = response.data.total;
                $scope.currentPage = response.data.current_page;
                $scope.productsPerPage = response.data.per_page;
                $scope.productsFrom = response.data.from;
                $scope.productsTo = response.data.to;
                $.loader.stop();
        }, 'json');
	}
}]);

myApp.controller("ProductImageController", ['$scope', '$http', function($scope, $http) {
	// Pagination of Users
    $scope.maxSize = 10;
    $scope.$watch("currentPage", function() {
        $.loader.start();
        $scope.getImages($scope.currentPage);
    });


	$scope.getImages = function(page) {
		 $http.get('/superadmin/product/images/get/get_data?page='+page)
            .then(function(response) {
            	$scope.images = response.data.data;

                $scope.totalImages = response.data.total;
                $scope.currentPage = response.data.current_page;
                $scope.imagesPerPage = response.data.per_page;
                $scope.imagesFrom = response.data.from;
                $scope.imagesTo = response.data.to;

                $.loader.stop();
        }, 'json');
	}
}]);

myApp.controller("ProductRatesController", ['$scope', '$http', function($scope, $http) {
    // // Pagination of Users
    // $scope.maxSize = 3;
    // $scope.$watch("currentPage", function() {
    //     $scope.getImages($scope.currentPage);
    // });


    // $scope.getImages = function(page) {
    //      $http.get('/superadmin/product/daily/rates/get/get_data?page='+page)
    //         .then(function(response) {
    //             $scope.images = response.data.data;

    //             $scope.totalImages = response.data.total;
    //             $scope.currentPage = response.data.current_page;
    //             $scope.imagesPerPage = response.data.per_page;
    //             $scope.imagesFrom = response.data.from;
    //             $scope.imagesTo = response.data.to;

    //     }, 'json');
    // }
}]);

myApp.controller("ProductOffersController", ['$scope', '$http', function($scope, $http) {
}]);
myApp.controller("FeedbackController", ['$scope', '$http', function($scope, $http) {
    $scope.maxSize = 10;
    $scope.$watch("currentPage", function() {
        $.loader.start();

        var myObject = {
          email: $('[name=email]').val()
        };

        var cond = $.param( myObject );

        $scope.getFeedbacks($scope.currentPage, cond);
    });

    $(document).on('keyup', '[name=email]', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13 || $(this).val()) {
            $.loader.start();

            var myObject = {
              email: $('[name=email]').val()
            };

            var cond = $.param( myObject );

            $scope.getFeedbacks(1, cond);
        }
    });

    $scope.getFeedbacks = function(page, $cond='') {
         $http.get('/superadmin/feedbacks/get/get_data?page='+page+(($cond) ? ('&'+$cond) : ''))
            .then(function(response) {
                $scope.feedbacks = response.data.data;

                $scope.totalFeedbacks = response.data.total;
                $scope.currentPage = response.data.current_page;
                $scope.feedbacksPerPage = response.data.per_page;
                $scope.feedbacksFrom = response.data.from;
                $scope.feedbacksTo = response.data.to;
                $.loader.stop();
        }, 'json');
    }

    $scope.showMessage = function($index) {
        var message = $scope.feedbacks[$index].message;

        $('#message-modal .modal-body').html(message);
        $('#message-modal').modal('show');
    }
}]);

myApp.controller("AppFeedbackController", ['$scope', '$http', function($scope, $http) {
    $scope.maxSize = 10;
    $scope.$watch("currentPage", function() {
        $.loader.start();

        var myObject = {
          name: $('[name=name]').val(),
          mobile: $('[name=mobile]').val()
        };

        var cond = $.param( myObject );

        $scope.getFeedbacks($scope.currentPage, cond);
    });

    $(document).on('keyup', '[name=name]', function(e) {
        $.loader.start();

        var myObject = {
          name: $('[name=name]').val(),
          mobile: $('[name=mobile]').val()
        };

        var cond = $.param( myObject );

        $scope.getFeedbacks(1, cond);
    });

    $(document).on('keyup', '[name=mobile]', function(e) {
        $.loader.start();

        var myObject = {
          name: $('[name=name]').val(),
          mobile: $('[name=mobile]').val()
        };

        var cond = $.param( myObject );

        $scope.getFeedbacks(1, cond);
    });

    $scope.getFeedbacks = function(page, $cond='') {
         $http.get('/superadmin/app/feedbacks/get/get_data?page='+page+(($cond) ? ('&'+$cond) : ''))
            .then(function(response) {
                var old_url = $("[name='filter_export']").attr('href');

                if (old_url) {
                    var index = 0;
                    var newURL = old_url;
                    index = old_url.indexOf('?');
                    if(index == -1){
                        index = old_url.indexOf('#');
                    }
                    if(index != -1){
                        newURL = old_url.substring(0, index);
                    }

                    $("[name='filter_export']").attr('href', newURL+(($cond) ? ('?'+$cond) : ''));
                }
                $scope.feedbacks = response.data.data;

                $scope.totalFeedbacks = response.data.total;
                $scope.currentPage = response.data.current_page;
                $scope.feedbacksPerPage = response.data.per_page;
                $scope.feedbacksFrom = response.data.from;
                $scope.feedbacksTo = response.data.to;
                $.loader.stop();
        }, 'json');
    }

    $scope.showFeedback = function($index) {
        var feedback = $scope.feedbacks[$index];
        var message = '';
        message += '<table border="0" width="100%" >'+
                  '<tr>'+
                    '<td align="center" rowspan="4" width="60%"><img src="/Frontend/assets/images/logo1.png" /></td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td ><b>DOC No.<span style="float:right">:</span><b> </td>'+
                    '<td width="20%">'+feedback.doc_no+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td ><b>Rev No.<span style="float:right">:</span></b></td>'+
                    '<td width="20%">'+feedback.rev_no+' </td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td ><b>Date<span style="float:right">:</span></b></td>'+
                    '<td width="20%">'+feedback.date+' </td>'+
                  '</tr>'+        
            '</table>';

        message += '<table border="0" style="margin-top: 2%" width="100%">'+
              '<tr>'+
                '<td width="50%" align="center" ><span style="border: 1px solid black;padding:5px"><b>Staff Code:</b>'+feedback.staff_code+'</span></td>'+
                '<td colspan="2" >&nbsp;</td>'+
              '</tr>'+
              '<tr>'+
                '<td align="center" colspan="3" style="padding-top: 20px"><b><u>Customer Feedback Form</u></b></td>'+
              '</tr>'+
            '</table>';

        message += '<table border="0" style="margin-top: 2%" width="100%">'+
              '<tr>'+
                '<td align="center" colspan="3"><b>About You</b></td>'+
              '</tr>'+
              '<tr>'+
                '<td width="20%"><b>Name</b> <span style="float:right">:</span></td>'+
                '<td colspan="2" style="border-bottom: 1px solid black">'+feedback.name+'</td>'+
              '</tr>'+
              '<tr>'+
                '<td ><b>Landline</b><span style="float:right">:</span></td>'+
                '<td colspan="2" style="border-bottom: 1px solid black">'+feedback.landline+'</td>'+
              '</tr>'+
              '<tr>'+
                '<td ><b>Mobile</b><span style="float:right">:</span></td>'+
                '<td colspan="2" style="border-bottom: 1px solid black">'+feedback.mobile+'</td>'+
              '</tr>'+
            '</table>';

        message += '<table border="0" style="margin-top: 2%" width="100%">'+
             ' <tr>'+
                '<td colspan="3"><b>(Please tick in the boxes provided below)</b></td>'+
              '</tr>'+
            '</table>';

        message += '<table border="1" style="border-collapse: collapse;margin-top: 2%" width="100%">'+
              '<tr>'+
                '<td width="60%"><b>Criteria</b></td>'+
                '<td ><b>Excellent</b></td>'+
                '<td ><b>Good</b></td>'+
                '<td ><b>Average</b></td>'+
                '<td ><b>Poor</b></td>'+
              '</tr>'+
              '<tr>'+
                '<td >Quality of Service</td>'+
                '<td >'+((feedback.quality_of_service == '1') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.quality_of_service == '2') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.quality_of_service == '3') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.quality_of_service == '4') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
              '</tr>'+
              '<tr>'+
                '<td >Exclusive Collection</td>'+
                '<td >'+((feedback.exclusive_collection == '1') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.exclusive_collection == '2') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.exclusive_collection == '3') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.exclusive_collection == '4') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
              '</tr>'+
               '<tr>'+
                '<td > Service of sales staff</td>'+
                '<td >'+((feedback.service_sales_statff == '1') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.service_sales_statff == '2') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.service_sales_statff == '3') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.service_sales_statff == '4') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
              '</tr>'+
               '<tr>'+
                '<td >Convenience of shopping the Showroom</td>'+
                '<td >'+((feedback.convenience_of_shopping == '1') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.convenience_of_shopping == '2') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.convenience_of_shopping == '3') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.convenience_of_shopping == '4') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
              '</tr>'+
               '<tr>'+
                '<td >Response to enquiry</td>'+
                '<td >'+((feedback.response_to_enquiry == '1') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.response_to_enquiry == '2') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.response_to_enquiry == '3') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.response_to_enquiry == '4') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
              '</tr>'+
               '<tr>'+
                '<td >Quality of ornaments</td>'+
                '<td >'+((feedback.quality_of_ornaments == '1') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.quality_of_ornaments == '2') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.quality_of_ornaments == '3') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.quality_of_ornaments == '4') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
              '</tr>'+
               '<tr>'+
                '<td >Delivery Time</td>'+
                '<td >'+((feedback.delivery_time == '1') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.delivery_time == '2') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.delivery_time == '3') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.delivery_time == '4') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
              '</tr>'+
               '<tr>'+
                '<td >Showroom ambience</td>'+
                '<td >'+((feedback.showroom_ambience == '1') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.showroom_ambience == '2') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.showroom_ambience == '3') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.showroom_ambience == '4') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
              '</tr>'+
               '<tr>'+
                '<td >Information on product features</td>'+
                '<td >'+((feedback.info_on_prod_features == '1') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.info_on_prod_features == '2') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.info_on_prod_features == '3') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.info_on_prod_features == '4') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
              '</tr>'+
               '<tr>'+
                '<td >Attending to queries</td>'+
                '<td >'+((feedback.attending_to_queries == '1') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.attending_to_queries == '2') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.attending_to_queries == '3') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
                '<td >'+((feedback.attending_to_queries == '4') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
              '</tr>'+
            '</table>';

        message += '<table border="0" style="border-collapse: collapse;margin-top: 2%" width="100%">'+
              '<tr>'+
                '<td colspan="3"><b>If the rating is Average or Below, please provide additional information for improvement of our Services </b></td>'+
              '</tr>'+
            '</table>';

        message += '<table border="1" style="border-collapse: collapse;margin-top: 2%" width="100%">'+
          '<tr>'+
            '<td width="60%" ><b>General </b></td>'+
            '<td  ><b>Yes </b></td>'+
            '<td  ><b>No </b></td>'+
            '<td  ><b>Can\'t Say</b></td>'+
          '</tr>'+
          '<tr>'+
            '<td >Is the selection of our showroom your first choice? </td>'+
            '<td  >'+((feedback.suggestion.is_abharan_first_choice == '1') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
            '<td  >'+((feedback.suggestion.is_abharan_first_choice == '3') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
            '<td  >'+((feedback.suggestion.is_abharan_first_choice == '2') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
          '</tr>'+
          '<tr>'+
            '<td >Will you recommend our services/ products to others? </td>'+
            '<td  >'+((feedback.suggestion.recommend_us_to_othrs == '1') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
            '<td  >'+((feedback.suggestion.recommend_us_to_othrs == '3') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
            '<td  >'+((feedback.suggestion.recommend_us_to_othrs == '2') ? '<i class="fa fa-check"></i>': '&nbsp;')+'</td>'+
          '</tr>'+
        '</table>';

        message += '<table  style="border-collapse: collapse;margin-top: 2%;border:1px solid black" width="100%">'+
          '<tr>'+
            '<td colspan="4"><b>Whether the phone number given by you while billing belongs to you? </b></td>'+
          '</tr>'+
          '<tr>'+
            '<td colspan="2" ><span style="border:1px solid black">'+((feedback.suggestion.your_contact == '1') ? '<i class="fa fa-check"></i>': '&nbsp;&nbsp;&nbsp;&nbsp;')+' </span>Yes </td>'+
            '<td colspan="2" ><span style="border:1px solid black">'+((feedback.suggestion.your_contact == '0') ? '<i class="fa fa-check"></i>': '&nbsp;&nbsp;&nbsp;&nbsp;')+'  </span>No </td>'+
          '</tr>'+
        '</table>';

        message += '<table border="0" style="border-collapse: collapse;margin-top: 2%;border:1px solid black" width="100%">'+
          '<tr>'+
            '<td colspan="4" ><b>Your valuable suggestions for improvement our customer service </b></td>'+
            '</tr>'+
          '<tr>'+
            '<td colspan="4" >'+feedback.suggestion.more_suggestions+' </td>'+
          '</tr>'+
        '</table>';

        $('#message-modal .modal-body').html(message);
        $('#message-modal').modal('show');
    }
}]);

myApp.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keypress", function(e) {
            if (e.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter, {
                        'e': e
                    });
                });
                e.preventDefault();
            }
        });
    };
});