var myApp = angular.module('abharan-ad', ['ngRoute'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("LoginController", ['$scope', '$http', function($scope, $http) {
    $(document).on('keydown', '.form-control', function() {
        $('.clearfix').html('');
    });

    $scope.loginSubmit = function() {
        var proceed = $('[name=user-signin]').parsley().validate();

        if (proceed) {
            var postdata = new FormData();

            postdata.append('user_email', $('[name="user-signin"] [name="email"]').val());
            postdata.append('user_password', $('[name="user-signin"] [name="password"]').val());

            postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));
            
            $http.post('/superadmin/auth-login', postdata, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            }).then(function(response) {
                if (response.data.success) {
                    location.href = '/superadmin/product';
                } else {
                    var display = "";
                    if ((typeof response.data.message) == 'object') {
                        $.each(response.data.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.data.message;
                    }
                    $('.signin_error').html(display).css('color', 'red');
                }
            }, 'json');
        }
    }


}]);
myApp.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keypress", function(e) {
            if (e.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter, {
                        'e': e
                    });
                });
                e.preventDefault();
            }
        });
    };
});