<?php

return [
    'main_category' => [
        'diamond'  => 1,
        'gems'     => 2,
        'gold'     => 3,
        'pearl'    => 4,
        'platinum' => 5,
        'silver'   => 6,
    ],

    'sub_category' => [
        'waist_chain'  => 1,
        'haram'     => 2,
        'studs'     => 3,
        'bangles'    => 5,
        'necklace' => 6,
        'kanti'   => 7,
        'ear_rings'   => 8,
        'finger_ring'   => 9,
        'pendent'   => 10,
        'chain'   => 11,
        'bracelet'   => 12,
        'article'   => 13,
    ],
];
