<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'mastercard' => [
        'action_url' => 'https://migs.mastercard.com.au/vpcpay',
        'secure_secret' => 'C7775CA1F14B8757174A7D360636C986',
        'params' => [
            "vpc_AccessCode" => 'D13B3AB5',//$accessCode,
            "vpc_Amount" => 0,
            "vpc_Command" => 'pay',
            "vpc_Locale" => 'en',
            "vpc_Currency" => 'INR',
            "vpc_MerchTxnRef" => '',//random_unique_value(),
            "vpc_Merchant" => 'ABHRJWPMIGS',//$merchantId,
            "vpc_OrderInfo" => "Some Comment",
            "vpc_ReturnURL" => "http://localhost:8000/payment/callback",//here code for db updation, return variable here
            "vpc_Version" => '1'
        ]
    ],

    'payu' => [
        // 'action_url' => 'https://secure.payu.in/_payment',
        // 'merchant_key' => 'yvRdJBMM',
        // 'salt' => 'ZBDB5sFwlf',
        'action_url' => 'https://test.payu.in/_payment',
        'merchant_key' => 'zS22aqxK',
        'salt' => 'JnRIXtQCPs',
        'hash_seq' => [
            "key",//$accessCode,
            "txnid", // Unique Txn id
            "amount",// Total Amount to be paid
            "productinfo",// give details about jewellery,
            "firstname", // Customer Name
            "email", // Customer Email
            "udf1",
            "udf2",
            "udf3",
            "udf4",
            "udf5",
            "udf6",
            "udf7",
            "udf8",
            "udf9",
            "udf10"
        ],
        'params' => [
            // Mandatory Api parameters
            "key" => '',//$accessCode,
            "txnid" => '', // Unique Txn id
            "amount" => '',// Total Amount to be paid
            "firstname" => '', // Customer Name
            "email" => '', // Customer Email
            "phone" => '',// Customer phone no,
            "productinfo" => '',// give details about jewellery,
            "surl" => "http://localhost:8000/payment/callback",
            "furl" => "http://localhost:8000/payment/callback",//here code for db updation, return variable here
            "curl" => "http://localhost:8000/product/callback",//here code for db updation, return variable here
            "service_provider" => 'payu_paisa'
        ]
    ]
];
